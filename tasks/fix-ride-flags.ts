import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { CREDENTIALS, ORGANIZATION_ID } from './_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  const snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/rideRequests`)
    .where('flag', '>', '')
    .where('hasFlag', '==', false)
    .get();

  console.log('count', snaps.docs.length);
  // const now = new Date().valueOf();
  // console.log(
  //   'past',
  //   snaps.docs.filter(
  //     doc =>
  //       (doc.data().datetime as admin.firestore.Timestamp).toDate().valueOf() <
  //       now,
  //   ).length,
  // );
  // console.log('count', snaps.docs[0].data());

  // await Promise.all(
  //   snaps.docs.map(async doc => {
  //     if (!doc.data().flag) return;

  //     return doc.ref.update({
  //       hasFlag: true,
  //     });
  //   }),
  // );
}

main();
