import { execSync } from 'child_process';
import { writeFileSync } from 'fs';

async function main() {
  console.log('starting...');

  const json = execSync(`firebase firestore:indexes`);

  writeFileSync('./firestore.indexes.json', json);

  console.log('complete');
}

main();
