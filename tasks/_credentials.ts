import * as admin from 'firebase-admin';

export const ORGANIZATION_ID = '0f0d957c-fed9-4823-b853-b1657b1fbcd5' as const;

export const CREDENTIALS = {
  safety() {
    throw new Error(`Ooops! This wasn't suppose to be called.`);
  },
  dev() {
    return {
      credential: admin.credential.cert(
        'apps/functions/tasks/trans-hub-dev-emulators-credentials.private.json',
      ),
      databaseURL: 'https://trans-hub-dev.firebaseio.com',
    };
  },
  prod() {
    return {
      credential: admin.credential.cert(
        'apps/functions/tasks/prod-helping-hands-credentials.private.json',
      ),
      databaseURL: 'https://helping-hands-d70d6.firebaseio.com',
    };
  },
} as const;
