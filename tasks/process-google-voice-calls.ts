import * as fs from 'fs';
import { utils } from 'xlsx';

// relative to script cwd
const CALL_LOG_DIR = 'tasks/calls';
const NEW_CALL_LOG_CSV_FILEPATH = `google-voice-call-history.csv`;

async function main() {
  deleteExtraniousFiles();
  deleteMalformedTimestamps();
  convertCallLogsToCSV();
}

main()
  .then(() => console.log('Complete'))
  .catch(console.error);

function deleteExtraniousFiles() {
  const fileNames = fs
    .readdirSync(CALL_LOG_DIR, {
      encoding: 'utf8',
    })
    .filter((name) => !name.endsWith('.html'));

  for (const name of fileNames) {
    fs.rmSync(`${CALL_LOG_DIR}/${name}`);
  }

  console.log('extranious files deleted', fileNames.length);
}

function deleteMalformedTimestamps() {
  const fileNames = fs
    .readdirSync(CALL_LOG_DIR, {
      encoding: 'utf8',
    })
    .filter((name) => {
      const parts = name.replace('.html', '').split(' - ');

      return parts[3]?.length !== 20;
    });

  for (const name of fileNames) {
    fs.rmSync(`${CALL_LOG_DIR}/${name}`);
  }

  console.log('malformed timestamp files deleted', fileNames.length);
}

function convertCallLogsToCSV() {
  const fileNames = fs.readdirSync(CALL_LOG_DIR, {
    encoding: 'utf8',
  });

  const result: Array<any> = [];

  for (const name of fileNames) {
    const parts = name.replace('.html', '').split(' - ');

    const caller = parts.shift() || 'n/a';
    const type = parts.shift()!;
    const timestamp = parseTime(parts.shift()!);

    result.push({
      Caller: caller,
      Type: type,
      Timestamp: timestamp.toISOString().replace('T', ' ').replace('.000Z', ''),
    });
  }

  const csv = utils.sheet_to_csv(utils.json_to_sheet(result, { dateNF: 22 }));

  fs.writeFileSync(NEW_CALL_LOG_CSV_FILEPATH, csv, {
    encoding: 'utf8',
  });
}

function parseTime(input: string) {
  const [date, time] = input.split('T');
  const [y, m, d] = date.split('-');
  const [h, mm, s] = time.replace('Z', '').split('_');

  const { parseInt, isNaN } = Number;

  const value = new Date(
    Date.UTC(
      parseInt(y),
      parseInt(m) - 1,
      parseInt(d),
      isNaN(parseInt(h)) ? 0 : parseInt(h),
      isNaN(parseInt(mm)) ? 0 : parseInt(mm),
      isNaN(parseInt(s)) ? 0 : parseInt(s),
    ),
  );

  if (Number.isNaN(value)) {
    throw new Error(`is NaN: ${input}`);
  }

  return value;
}
