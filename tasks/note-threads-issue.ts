import * as admin from 'firebase-admin';
import type { INoteThread } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from './_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  const noteThreadSnaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/noteThreads`)
    .limit(500)
    .offset(500 * 0)
    .get();

  if (noteThreadSnaps.docs.length === 0) {
    console.log('Completely done!');
    return;
  }

  let updated = 0;

  for (const snap of noteThreadSnaps.docs) {
    const noteThread = snap.data() as INoteThread;

    updated++;

    if (Array.isArray(noteThread.notes)) continue;

    console.log(`Doesn't have notes: ${noteThread.uid}`);
  }

  console.log(`${updated} records updated`);
}

main();
