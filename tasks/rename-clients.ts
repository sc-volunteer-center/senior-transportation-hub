import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as faker from 'faker';
import { CREDENTIALS, ORGANIZATION_ID } from './_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  let snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/clients`)
    .get();

  await Promise.all(
    snaps.docs.map(snap => {
      const doc = snap.data();

      doc.firstName = faker.name.firstName();
      doc.middleName = null;
      doc.lastName = faker.name.lastName();
      doc.phoneNumber = faker.phone.phoneNumber();

      return snap.ref.update(doc);
    }),
  );

  snaps = await db.collection(`organizations/${ORGANIZATION_ID}/drivers`).get();

  await Promise.all(
    snaps.docs.map(snap => {
      const doc = snap.data();

      doc.firstName = faker.name.firstName();
      doc.middleName = null;
      doc.lastName = faker.name.lastName();
      doc.phoneNumber = faker.phone.phoneNumber();
      doc.emailAddress = faker.internet.email();

      return snap.ref.update(doc);
    }),
  );

  snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/dispatchers`)
    .get();

  await Promise.all(
    snaps.docs.map(snap => {
      const doc = snap.data();

      doc.firstName = faker.name.firstName();
      doc.middleName = null;
      doc.lastName = faker.name.lastName();
      doc.phoneNumber = faker.phone.phoneNumber();
      doc.emailAddress = faker.internet.email();

      return snap.ref.update(doc);
    }),
  );
}

main();
