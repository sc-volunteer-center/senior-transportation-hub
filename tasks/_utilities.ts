import * as admin from 'firebase-admin';
import { CREDENTIALS } from './_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

export const db = app.firestore();

export function createId() {
  return db.collection('/fake').doc().id;
}

export const config = {
  uri: {
    root: 'http://localhost:3000',
  },
};
