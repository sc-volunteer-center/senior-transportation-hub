import * as admin from 'firebase-admin';
import { IClient, IPersonContact } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  const snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/clients`)
    .get();

  let updated = 0;

  await Promise.all(
    snaps.docs.map(async (snap) => {
      const doc = snap.data() as IClient;

      if (doc.contacts) return;

      const contacts: IPersonContact[] = [
        {
          createdAt: doc.emergencyContact.updatedAt.toDate().valueOf(),
          updatedAt: doc.emergencyContact.updatedAt.toDate().valueOf(),
          createdById: 'kpJBbvmf3WwQa3OzvBBo',
          label: 'Emergency Contact',
          firstName: doc.emergencyContact.firstName,
          lastName: doc.emergencyContact.lastName,
          relationshipToPerson: doc.emergencyContact.relationshipToClient,
          phoneNumber: doc.emergencyContact.phoneNumber,
        },
      ];

      await snap.ref.update({ contacts });

      updated++;
    }),
  );

  console.log(`${updated} clients updated`);
}

main();
