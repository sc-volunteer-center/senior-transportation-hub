import * as admin from 'firebase-admin';
import { IRideRequestBase } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

/**
 * Date: 2022-01-18
 *
 * In order to be able to filter requests properly on
 * cancelled/un-cancelled status, I've realized I need to store
 * isRequestCancelled status as a boolean on each request.
 *
 * This is because our inequality filter is reserved for the `datetime`
 * prop. So, in order to filter on requestIsCancelled, we (previously)
 * were stripping it from the query params sent to Firestore and
 * later filtering for `requestIsCancelled` on the client side. But
 * this often won't work. For example:
 *   - Say we have requestIsCancelled = false and limit = 1. We ignore
 *     requestIsCancelled when we send the query to Firestore and Firestore
 *     returns 1 cancelled request. We then filter out that request on the
 *     client so no requests are returned. Except, as it turns out, the
 *     second request (i.e. limit = 2) in the query is not cancelled. But
 *     since we limit 1 we never see the second request in the client.
 */

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  // migrating recurring requests should happen first since, theoretically
  // recurring requests can generate regular requests.
  await migrateRecurringRideRequests();
  await migrateRideRequests();
}

main();

async function migrateRecurringRideRequests() {
  console.log('Start migrateRecurringRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/recurringRideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let documentsFound = 0;
  let numberOfChanges = 0;

  do {
    console.log('iteration', iteration);

    [documentsFound, numberOfChanges] = await updateRideRequestBases(
      collection,
      iteration,
    );

    console.log('changes', numberOfChanges);
    iteration++;
    await wait(500);
  } while (documentsFound > 0);

  console.log('Done migrateRecurringRideRequests!');
}

async function migrateRideRequests() {
  console.log('Start migrateRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/rideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let documentsFound = 0;
  let numberOfChanges = 0;

  do {
    console.log('iteration', iteration);

    [documentsFound, numberOfChanges] = await updateRideRequestBases(
      collection,
      iteration,
    );

    console.log('numberOfChanges', numberOfChanges);
    iteration++;
    await wait(500);
  } while (documentsFound > 0);

  console.log('Done migrateRideRequests!');
}

async function updateRideRequestBases(
  query: FirebaseFirestore.CollectionReference<IRideRequestBase>,
  iteration: number,
): Promise<[documentsFound: number, numberOfChanges: number]> {
  const snaps = await query
    .limit(500)
    .offset(iteration * 500)
    .get();

  const batch = db.batch();

  const changedDocs: unknown[] = [];

  snaps.docs.forEach((snap) => {
    const data = snap.data();

    if ('requestCancelled' in data) return;

    changedDocs.push(data);

    const update: Partial<IRideRequestBase> = {
      requestCancelled: !!data.requestCancelledAt,
    };

    batch.update(snap.ref, update);
  });

  await batch.commit();

  return [snaps.size, changedDocs.length];
}

function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
