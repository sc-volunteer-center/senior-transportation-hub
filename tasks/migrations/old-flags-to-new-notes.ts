import * as admin from 'firebase-admin';
import { Timestamp } from '@firebase/firestore-types';
import { IClient, IDriver, IRideRequest } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  // await clearNotes();
  // await migrateClients();
  // await migrateDrivers();
  await migrateRideRequests();
}

main();

async function migrateClients() {
  const snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/clients`)
    .get();

  let updated = 0;

  await Promise.all(
    snaps.docs.map(async (snap) => {
      const doc = snap.data() as IClient & {
        flag: string | null;
        flagLastUpdatedAt: Timestamp | null;
        flagLastUpdatedById: string | null;
        hasFlag: boolean;
      };

      if (doc.flag) {
        const threadId = createId();

        await db
          .doc(`organizations/${ORGANIZATION_ID}/noteThreads/${threadId}`)
          .set({
            uid: threadId,
            createdAt: admin.firestore.FieldValue.serverTimestamp(),
            updatedAt: admin.firestore.FieldValue.serverTimestamp(),
            noteAddedAt: admin.firestore.FieldValue.serverTimestamp(),
            clientId: doc.uid,
            flag: true,
          });

        const noteId = createId();

        await db.doc(`organizations/${ORGANIZATION_ID}/notes/${noteId}`).set({
          uid: noteId,
          createdAt: admin.firestore.FieldValue.serverTimestamp(),
          createdById: doc.flagLastUpdatedById,
          updatedAt: admin.firestore.FieldValue.serverTimestamp(),
          threadId,
          text: doc.flag,
          firstNoteInThread: true,
        });

        updated++;
      }

      await db
        .doc(`organizations/${ORGANIZATION_ID}/clients/${doc.uid}`)
        .update({
          flag: admin.firestore.FieldValue.delete(),
          flagLastUpdatedAt: admin.firestore.FieldValue.delete(),
          flagLastUpdatedById: admin.firestore.FieldValue.delete(),
          hasFlag: admin.firestore.FieldValue.delete(),
          flags: doc.flag ? 1 : 0,
        });
    }),
  );

  console.log(`${updated} clients updated`);
}

async function migrateDrivers() {
  const snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/drivers`)
    .get();

  let updated = 0;

  await Promise.all(
    snaps.docs.map(async (snap) => {
      const doc = snap.data() as IDriver & {
        flag: string | null;
        flagLastUpdatedAt: Timestamp | null;
        flagLastUpdatedById: string | null;
        hasFlag: boolean;
      };

      if (doc.flag) {
        const threadId = createId();

        await db
          .doc(`organizations/${ORGANIZATION_ID}/noteThreads/${threadId}`)
          .set({
            uid: threadId,
            createdAt: admin.firestore.FieldValue.serverTimestamp(),
            updatedAt: admin.firestore.FieldValue.serverTimestamp(),
            noteAddedAt: admin.firestore.FieldValue.serverTimestamp(),
            driverId: doc.uid,
            flag: true,
          });

        const noteId = createId();

        await db.doc(`organizations/${ORGANIZATION_ID}/notes/${noteId}`).set({
          uid: noteId,
          createdAt: admin.firestore.FieldValue.serverTimestamp(),
          createdById: doc.flagLastUpdatedById,
          updatedAt: admin.firestore.FieldValue.serverTimestamp(),
          threadId,
          text: doc.flag,
          firstNoteInThread: true,
        });

        updated++;
      }

      await db
        .doc(`organizations/${ORGANIZATION_ID}/drivers/${doc.uid}`)
        .update({
          flag: admin.firestore.FieldValue.delete(),
          flagLastUpdatedAt: admin.firestore.FieldValue.delete(),
          flagLastUpdatedById: admin.firestore.FieldValue.delete(),
          hasFlag: admin.firestore.FieldValue.delete(),
          flags: doc.flag ? 1 : 0,
        });
    }),
  );

  console.log(`${updated} drivers updated`);
}

async function migrateRideRequests() {
  const snaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/rideRequests`)
    .limit(500)
    .offset(12 * 500)
    .get();

  let updated = 0;

  await Promise.all(
    snaps.docs.map(async (snap) => {
      const doc = snap.data() as IRideRequest & {
        flag: string | null;
        hasFlag: boolean;
      };

      if (doc.flag) {
        const threadId = createId();

        await db
          .doc(`organizations/${ORGANIZATION_ID}/noteThreads/${threadId}`)
          .set({
            uid: threadId,
            createdAt: admin.firestore.FieldValue.serverTimestamp(),
            updatedAt: admin.firestore.FieldValue.serverTimestamp(),
            noteAddedAt: admin.firestore.FieldValue.serverTimestamp(),
            rideRequestId: doc.uid,
            flag: true,
          });

        const noteId = createId();

        await db.doc(`organizations/${ORGANIZATION_ID}/notes/${noteId}`).set({
          uid: noteId,
          createdAt: admin.firestore.FieldValue.serverTimestamp(),
          createdById: doc.requestTakenById,
          updatedAt: admin.firestore.FieldValue.serverTimestamp(),
          threadId,
          text: doc.flag,
          firstNoteInThread: true,
        });
      }

      await db
        .doc(`organizations/${ORGANIZATION_ID}/rideRequests/${doc.uid}`)
        .update({
          flag: admin.firestore.FieldValue.delete(),
          hasFlag: admin.firestore.FieldValue.delete(),
          flags: doc.flag ? 1 : 0,
        });

      updated++;
    }),
  );

  console.log(`${updated} ride requests updated`);
}

async function clearNotes() {
  const threadSnaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/noteThreads`)
    .get();

  const batch = db.batch();

  threadSnaps.forEach((snap) => {
    batch.delete(snap.ref);
  });

  const noteSnaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/notes`)
    .get();

  noteSnaps.forEach((snap) => {
    batch.delete(snap.ref);
  });

  return batch.commit();
}

function createId() {
  return db.collection('test').doc().id;
}
