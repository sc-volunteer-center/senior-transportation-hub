import * as admin from 'firebase-admin';
import type { INoteThread, INote } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  const noteThreadSnaps = await db
    .collection(`organizations/${ORGANIZATION_ID}/noteThreads`)
    .limit(500)
    .offset(500 * 1)
    .get();

  if (noteThreadSnaps.docs.length === 0) {
    console.log('Completely done!');
    return;
  }

  let updated = 0;

  for (const snap of noteThreadSnaps.docs) {
    const noteThread = snap.data() as INoteThread;

    updated++;

    await db.runTransaction(async transaction => {
      const noteSnaps = await transaction.get(
        db
          .collection(`organizations/${ORGANIZATION_ID}/notes`)
          .where('threadId', '==', noteThread.uid)
          .orderBy('createdAt', 'asc'),
      );

      transaction.update(snap.ref, {
        notes: noteSnaps.docs.map(doc => {
          const oldNote = doc.data();

          const note: INote = {
            uid: oldNote.uid,
            createdAt: oldNote.createdAt.toDate().valueOf(),
            createdById: oldNote.createdById,
            updatedAt: oldNote.updatedAt.toDate().valueOf(),
            text: oldNote.text,
          };

          return note;
        }),
      });

      noteSnaps.docs.forEach(noteSnap => {
        transaction.delete(noteSnap.ref);
      });
    });
  }

  console.log(`${updated} records updated`);
}

main();
