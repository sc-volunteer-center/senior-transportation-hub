import * as admin from 'firebase-admin';
import { IRideRequestBase } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

/**
 * Date: 2021-04-06
 *
 * Second estimated migration of "estimatedRideMeters". See the first migration
 * for more information. While I was at it, I also provided default
 * values for `peopleImpacted` and `peopleImpactedDescription`.
 */

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  // migrating recurring requests should happen first since, theoretically
  // recurring requests can generate regular requests.
  await migrateRecurringRideRequests();
  await migrateRideRequests();
}

main();

async function migrateRecurringRideRequests() {
  console.log('Start migrateRecurringRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/recurringRideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let changes = [];

  do {
    console.log('iteration', iteration);
    changes = await updateRideRequestBases(collection, iteration);
    console.log('changes', changes.length);
    iteration++;
    await wait(500);
  } while (changes.length > 0);

  console.log('Done migrateRecurringRideRequests!');
}

async function migrateRideRequests() {
  console.log('Start migrateRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/rideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let changes = [];

  do {
    console.log('iteration', iteration);
    changes = await updateRideRequestBases(collection, iteration);
    console.log('changes', changes.length);
    iteration++;
    await wait(500);
  } while (changes.length > 0);

  console.log('Done migrateRideRequests!');
}

async function updateRideRequestBases(
  query: FirebaseFirestore.CollectionReference<IRideRequestBase>,
  iteration: number,
) {
  const snaps = await query
    .limit(500)
    .offset(iteration * 500)
    .get();

  const batch = db.batch();

  snaps.docs.map((snap) => {
    const data = snap.data();

    const update: Partial<IRideRequestBase> = {
      peopleImpacted: data.peopleImpacted ?? 1,
      peopleImpactedDescription: data.peopleImpactedDescription || null,
      estimatedRideMeters:
        data.estimatedRideFeet === null
          ? null
          : Math.round(data.estimatedRideFeet * 0.3048),
      estimatedRideFeet: admin.firestore.FieldValue.delete() as any,
    };

    batch.update(snap.ref, update);
  });

  await batch.commit();

  return snaps.docs;
}

function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
