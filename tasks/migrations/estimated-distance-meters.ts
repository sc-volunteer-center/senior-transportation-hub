import * as admin from 'firebase-admin';
import { IRideRequestBase } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

/**
 * Date: 2021-04-06
 *
 * Yesterday I realized that the Hub was requesting Google to estimate
 * ride request distance in "IMPERIAL" units, which returns an estimated
 * distance in feet. This number was being saved in
 * `RideRequest#estimatedRideMeters` and was being treated as "meters"
 * instead of "feet", resulting in mileage calculations in reports being very
 * wrong.
 *
 * This migration copies the current
 * value of the ride request's "estimatedRideMeters" property to a new
 * "estimatedRideFeet" property. As a future migration, I think I want
 * to standardize on "meters" being the cononical unit used in the Hub.
 * I'll need to iterate through all of the ride requests to update the
 * current "estimatedRideMeters" property with the correct value and then
 * delete the "estimatedRideFeet" value. In the interum, the Hub will
 * also need to save the estimated ride distance (in feet) to both
 * "estimatedRideFeet" and "estimatedRideMeters".
 */

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  // migrating recurring requests should happen first since, theoretically
  // recurring requests can generate regular requests.
  await migrateRecurringRideRequests();
  await migrateRideRequests();
}

main();

async function migrateRecurringRideRequests() {
  console.log('Start migrateRecurringRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/recurringRideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let changes = [];

  do {
    console.log('iteration', iteration);
    changes = await updateRideRequestBases(collection, iteration);
    console.log('changes', changes.length);
    iteration++;
    await wait(500);
  } while (changes.length > 0);

  console.log('Done migrateRecurringRideRequests!');
}

async function migrateRideRequests() {
  console.log('Start migrateRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/rideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let changes = [];

  do {
    console.log('iteration', iteration);
    changes = await updateRideRequestBases(collection, iteration);
    console.log('changes', changes.length);
    iteration++;
    await wait(500);
  } while (changes.length > 0);

  console.log('Done migrateRideRequests!');
}

async function updateRideRequestBases(
  query: FirebaseFirestore.CollectionReference<IRideRequestBase>,
  iteration: number,
) {
  const snaps = await query
    .limit(500)
    .offset(iteration * 500)
    .get();

  const batch = db.batch();

  snaps.docs.map((snap) => {
    const data = snap.data();

    const update: Partial<IRideRequestBase> = {
      estimatedRideFeet: data.estimatedRideMeters || null,
    };

    batch.update(snap.ref, update);
  });

  await batch.commit();

  return snaps.docs;
}

function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
