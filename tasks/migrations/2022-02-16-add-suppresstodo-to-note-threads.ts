import * as admin from 'firebase-admin';
import { INoteThread, IRideRequestBase } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

/**
 * Date: 2022-02-16
 *
 * This migrates the old note threads in the database so they have
 * a default value of "false" for "suppressTodos".
 */

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  await migrateNoteThreads();
}

main();

async function migrateNoteThreads() {
  console.log('Start migrateNoteThreads!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/noteThreads`,
  ) as FirebaseFirestore.CollectionReference<INoteThread>;

  let iteration = 0;
  let documentsFound = 0;
  let numberOfChanges = 0;

  do {
    console.log('iteration', iteration);

    [documentsFound, numberOfChanges] = await updateNoteThread(
      collection,
      iteration,
    );

    console.log('changes', numberOfChanges);
    iteration++;
    await wait(500);
  } while (documentsFound > 0);

  console.log('Done migrateNoteThreads!');
}

async function updateNoteThread(
  query: FirebaseFirestore.CollectionReference<INoteThread>,
  iteration: number,
): Promise<[documentsFound: number, numberOfChanges: number]> {
  const snaps = await query
    .limit(500)
    .offset(iteration * 500)
    .get();

  const batch = db.batch();

  const changedDocs: unknown[] = [];

  snaps.docs.map((snap) => {
    const data = snap.data();

    changedDocs.push(data);

    const update: Partial<INoteThread> = {
      suppressTodo: data.suppressTodo ?? false,
    };

    batch.update(snap.ref, update);
  });

  await batch.commit();

  return [snaps.size, changedDocs.length];
}

function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
