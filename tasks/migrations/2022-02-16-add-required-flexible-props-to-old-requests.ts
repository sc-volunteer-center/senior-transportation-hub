import * as admin from 'firebase-admin';
import { IRideRequestBase } from '@local/models';
import { CREDENTIALS, ORGANIZATION_ID } from '../_credentials';

/**
 * Date: 2022-02-16
 *
 * When I originally added the "flexible requests" feature
 * as well as the ability to add multiple, flagged notes to
 * requests, I didn't migrate all the old requests in the
 * database to the new format (instead, I handled old requests
 * on the client side). I'm not performing an overdue migration
 * to update the database.
 *
 * Specifically, I'm updating these IRideRequestBase props with the
 * following default values
 *
 * - flags: 0,
 * - flexible: false,
 * - flexibleSchedule: null,
 */

admin.initializeApp(CREDENTIALS.safety());

const app = admin.app();

const db = app.firestore();

async function main() {
  // migrating recurring requests should happen first since, theoretically
  // recurring requests can generate regular requests.
  await migrateRecurringRideRequests();
  await migrateRideRequests();
}

main();

async function migrateRecurringRideRequests() {
  console.log('Start migrateRecurringRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/recurringRideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let documentsFound = 0;
  let numberOfChanges = 0;

  do {
    console.log('iteration', iteration);

    [documentsFound, numberOfChanges] = await updateRideRequestBases(
      collection,
      iteration,
    );

    console.log('numberOfChanges', numberOfChanges);
    iteration++;
    await wait(500);
  } while (documentsFound > 0);

  console.log('Done migrateRecurringRideRequests!');
}

async function migrateRideRequests() {
  console.log('Start migrateRideRequests!');

  const collection = db.collection(
    `organizations/${ORGANIZATION_ID}/rideRequests`,
  ) as FirebaseFirestore.CollectionReference<IRideRequestBase>;

  let iteration = 0;
  let documentsFound = 0;
  let numberOfChanges = 0;

  do {
    console.log('iteration', iteration);

    [documentsFound, numberOfChanges] = await updateRideRequestBases(
      collection,
      iteration,
    );

    console.log('numberOfChanges', numberOfChanges);
    iteration++;
    await wait(500);
  } while (documentsFound > 0);

  console.log('Done migrateRideRequests!');
}

async function updateRideRequestBases(
  query: FirebaseFirestore.CollectionReference<IRideRequestBase>,
  iteration: number,
): Promise<[documentsFound: number, numberOfChanges: number]> {
  const snaps = await query
    .limit(500)
    .offset(iteration * 500)
    .get();

  const batch = db.batch();

  const changedDocs: unknown[] = [];

  snaps.docs.map((snap) => {
    const data = snap.data();

    changedDocs.push(data);

    const update: Partial<IRideRequestBase> = {
      flags: data.flags ?? 0,
      flexible: data.flexible ?? false,
      flexibleSchedule: data.flexibleSchedule ?? null,
    };

    batch.update(snap.ref, update);
  });

  await batch.commit();

  return [snaps.size, changedDocs.length];
}

function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}
