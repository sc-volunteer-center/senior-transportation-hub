# Senior Transportation Hub

This is a web application designed to facilitate coordinating free rides for low income seniors. It's creation was sponsored by the [Volunteer Center of Santa Cruz County](https://scvolunteercenter.org/) for their Senior Transportation Program.

In the Volunteer Center's program, volunteers acting as dispatchers use this application to facilitate the process of coordinating with clients (low income seniors) and volunteer
drivers.

While the application has been created for coordinating free rides for low income seniors, it can probably serve as a base for another application coordinating free rides for another
group.

## Where to start

For a general overview of how to use this application as a program administrator, go to the [General overview of this application](#general-overview-of-this-application) section.

For a technical description of how this application is set up (and the related accounts are configured), go to the [Technical description of this application](#technical-description-of-this-application) section.

## General overview of this application

This web application is intended to make lives easier for dispatchers by removing a lot of their repetitive tasks (I'm told the feature to auto-generate ride request emails is perhaps it's most important--from their perspective). It's intended to make lives easier for program administrators by automatically generating most of the quarterly reporting required by various grants.

To get started with the application, navigate to its webpage (only applicable to SC Volunteer Center staff): https://helping-hands-d70d6.firebaseapp.com/.

If you aren't already signed in, you'll be presented with a login screen. At minimum, there are two Google Accounts associated with the application: one which is designated as the admin account, another which is designated as the dispatcher email address. There can be multiple Google Accounts marked as an admin, but there is only ever one dispatcher email address (though you can change the email address associated).

This setup was chosen because all of the dispatchers share the same transportation at scvolunteercenter.org email address. So, all dispatchers first log in using the transportation Google Account. Then, as a second step, they "signin" again by selecting their dispatcher profile. When a dispatcher signs into their profile, the app automatically records the start of their shift. Later, when they sign out of their profile, the app automatically records the end of their shift. The app then calculates how long the shift was. If someone forgets to sign out of their profile, the default shift length of 4 hours is used.

When a dispatcher signs out of their profile, they are not actually signed out of the transportation Google Account. This is because dispatchers all use the same computer, and it's easier to just leave that computer logged into the transportation Google Account (this way, when a dispatcher starts their shift they only need to select their profile and they are good to go). To actually log out of the transportation Google Account, a dispatcher must first sign out of their profile and then click the "exit website" button in the left-hand menu bar.

- To learn more about using this application as an admin, [login to the app](https://helping-hands-d70d6.firebaseapp.com/) and navigate to the "Help" page using the left-hand menu bar.

- To learn more about using this application as a dispatcher, [login to the app](https://helping-hands-d70d6.firebaseapp.com/) and navigate to the "Help" page using the left-hand menu bar.

- Bugs can be reported by [creating an issue in the Gitlab repository](https://gitlab.com/sc-volunteer-center/senior-transportation-hub/issues) or by sending an email to "incoming+sc-volunteer-center-senior-transportation-hub-8690342-issue-@incoming.gitlab.com". All bug reports / issues are public.

## Technical description of this application

This is a Javascript web application written in the [Typescript programming language](https://www.typescriptlang.org/) (the Typescript is turned into javascript before publishing) and built using the [Angular web framework](https://angular.io/) and Google's [Firebase](https://firebase.google.com/) platform. Firebase hosts this app's webpage as well as provides the database that this app's information is stored in. Additionally, this app takes advantage of the [Google Maps](https://cloud.google.com/maps-platform/) and [Gmail](https://developers.google.com/gmail/api/) APIs to provide mapping and email integration for the volunteers using it.

To give you an understanding of how all of this application's pieces fit together, here is a list of the steps you would need to complete if you were to take this source code and launch a new instance of this application from scratch.

1. Create a Firebase account.
2. Create two new Firebase projects
   1. One for development/testing
      - Enable Google as a sign in option for Firebase authentication
      - Create firestore database
   2. One for production (the one volunteers will actually use)
      - Enable Google as a sign in option for Firebase authentication
      - Create firestore database
3. Update this application's source code with both the development and production Firebase project configuration details.
4. Enable the following, additional Google APIs for that Firebase account.
   - Google Maps Embed API
   - Gmail API
5. Update this application's source code with the google API configuration details.
6. Configure the Google OAUTH consent screen for the Firebase Account.
7. Configure credential restrictions for the Firebase Account (make sure Google API keys can only be used by appropriate hosts).

8. Deploy the application to your development/testing project by:

   1. Deploy the Firebase Functions associated with this application to your development/testing project.
   2. Deploy the Firebase Firestore rules/indexes associated with this application to your development/testing project.
   3. Deploy the application source code to your development/testing project using Firebase Hosting.
   4. Visit the development/testing webpage you just launched (Firebase will automatically generate a URL for you) and make sure everything is working.

9. Finally, assuming everything is working, deploy the application to your production project by:
   1. Deploy the Firebase Functions associated with this application to your production project.
   2. Deploy the Firebase Firestore rules/indexes associated with this application to your production project.
   3. Deploy the application source code to your production project using Firebase Hosting.
   4. Visit the production webpage you just launched (Firebase will automatically generate a URL for you) and make sure everything is working.

### Source code repository

This application's source code is saved in a [git repository on Gitlab](https://gitlab.com/sc-volunteer-center/senior-transportation-hub). The source code is owned by the Santa Cruz Volunteer Center and associated with the volunteer center's Gitlab organization account: [sc-volunteer-center](https://gitlab.com/sc-volunteer-center). It is shared under the MIT License. You can see the members of the [sc-volunteer-center account here](https://gitlab.com/groups/sc-volunteer-center/-/group_members).

## Sample job application description

If, in the future, someone at the Volunteer Center wishes to update this application and they cannot get ahold of John Carroll (the original developer), here is a sample job description you could use to help look for a qualified developer.

> Looking for a Typescript developer with experience using the Angular 2+ Web Framework and Google's Firebase Platform.

If you are unable to find someone using the above description, you can broaden the qualifications using the following job description.

> Looking for a front-end Javascript developer. Familiarity with Typescript, build tools, the Angular 2+ Web Framework, and Google's Firebase Platform is preferred.

## Contact information

- Original developer: [John Carroll](https://twitter.com/johncarroll)
