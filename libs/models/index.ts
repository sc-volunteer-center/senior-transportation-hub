import {
  differenceInYears,
  format,
  formatDistanceToNow,
  startOfDay,
  subDays,
} from 'date-fns';
import {
  objectD,
  undefinableD,
  nullableD,
  numberD,
  stringD,
  optionalD,
  anyD,
  arrayD,
  booleanD,
  predicateD,
  integerD,
  lazyD,
  tupleD,
  instanceOfD,
  anyOfD,
  exactlyD,
  emailD,
  chainOfD,
  constantD,
} from 'ts-decoders/decoders';
import {
  areDecoderErrors,
  DecoderSuccess,
  AsyncDecoder,
  DecoderError,
} from 'ts-decoders';
import { Schedule, IRuleOptions, DateInput } from '@local/rschedule';
import type { FieldValue, Timestamp } from '@firebase/firestore-types';
import * as moment from 'moment-timezone';

moment.updateLocale('en', {
  week: {
    dow: 1, // First day of week is Monday
  },
});

type TimestampInput<T> = T extends Timestamp
  ? FieldValue | Date | Timestamp
  : T;

export type FirestoreInput<T> = {
  [P in keyof T]: TimestampInput<T[P]>;
};

export interface IEmail {
  to: string[];
  message: {
    subject: string;
    text?: string;
    html: string;
  };
  delivery?: {
    attempts: number | null;
    endTime: Timestamp | null;
    error: string | null;
    leaseExpireTime: Timestamp | null;
    startTime: Timestamp;
    state: 'SUCCESS' | 'ERROR';
  };
}

/** Simple decoders */

export const STRING_DECODER = stringD()
  .map((s) => s.trim())
  .chain(
    predicateD<string>((input) => input.length > 0, {
      errorMsg: 'Cannot be blank',
    }),
  );

export const NULLABLE_STRING_DECODER = nullableD(
  stringD()
    .map((s) => s.trim())
    .map((i) => (i === '' ? null : i)),
);

export const NULLABLE_EMAIL_DECODER = nullableD(
  stringD()
    .map((s) => s.trim())
    .chain(
      anyOfD([exactlyD('').map(() => null), emailD()], {
        errorMsg: 'Invalid email address',
      }),
    ),
);

export const TIMESTAMP_INPUT_DECODER = predicateD<
  FieldValue | Date | Timestamp
>((input) => typeof input === 'object' && input !== null);

/** Address */

interface AddressLocation {
  street: string;
  city: string;
  zip: string;
}

export function getAddressURLString(a: {
  street: string;
  city: string;
  zip: string;
  state: string;
}) {
  return `${a.street}, ${a.city} ${a.zip} ${a.state}`.replace(/\s/g, '+');
}

export function areAddressLocationsEqual(
  a: AddressLocation,
  b: AddressLocation,
) {
  return a.street === b.street && a.city === b.city && a.zip === b.zip;
}

interface IMapService {
  getLatLong(a: {
    street: string;
    city: string;
    zip: string;
    state: string;
  }): Promise<{ latitude: number; longitude: number }>;
}

export function addressD(
  mapsService: IMapService,
  options: { update?: IAddress } = {},
) {
  return objectD(
    {
      label: undefinableD(NULLABLE_STRING_DECODER),
      street: STRING_DECODER,
      apartmentNumber: undefinableD(NULLABLE_STRING_DECODER),
      city: STRING_DECODER,
      zip: STRING_DECODER,
      comments: NULLABLE_STRING_DECODER,
      tripPurpose: undefinableD(STRING_DECODER),
    },
    {
      removeUndefinedProperties: true,
    },
  )
    .toAsyncDecoder()
    .map<FirestoreInput<IAddress>>(async (input) => {
      if (options.update && areAddressLocationsEqual(input, options.update)) {
        return {
          ...options.update,
          ...input,
        };
      }

      const latLong = await mapsService.getLatLong({ state: 'CA', ...input });

      return {
        ...input,
        ...latLong,
        state: 'CA',
      };
    });
}

export interface IAddress {
  label?: string | null;
  street: string;
  apartmentNumber?: string | null;
  city: string;
  zip: string;
  state: string;
  latitude: number | null;
  longitude: number | null;
  comments: string | null;
  tripPurpose?: string | null; // only applicable for ride request destinations
}

export class Address {
  label?: string | null;
  street: string;
  apartmentNumber?: string | null;
  city: string;
  zip: string;
  state: string;
  latitude: number | null;
  longitude: number | null;
  comments: string | null;
  tripPurpose?: string | null;

  constructor(address: IAddress) {
    Object.assign(this, address);
  }

  toString() {
    let strings: string[] = [];

    if (this.label) {
      strings.push(this.label);
    }
    strings.push(this.street);
    if (this.apartmentNumber) {
      strings.push(this.apartmentNumber);
    }

    strings = [strings.join(', ')];

    strings.push(`${this.city} ${this.zip} ${this.state}`);

    return strings.join(', ');
  }
}

/** Destination */

export function destinationD(
  mapsService: IMapService,
  options: { update: IAddress },
): AsyncDecoder<
  FirestoreInput<
    IAddress &
      Partial<{
        uid: string;
        createdAt: Timestamp;
        updatedAt: Timestamp;
        lastUsedAt: Timestamp | null;
      }>
  >
>;

export function destinationD(
  mapsService: IMapService,
  options?: { update?: IAddress },
): AsyncDecoder<FirestoreInput<IDestination>>;

export function destinationD(
  mapsService: IMapService,
  options: { update?: IAddress } = {},
) {
  const address = addressD(mapsService, options);

  if (options.update) {
    const extrasD = objectD(
      {
        updatedAt: undefinableD(TIMESTAMP_INPUT_DECODER),
        lastUsedAt: optionalD(TIMESTAMP_INPUT_DECODER),
      },
      {
        removeUndefinedProperties: true,
      },
    );

    return new AsyncDecoder(async (input) => {
      const resultA = extrasD.decode(input);

      if (areDecoderErrors(resultA)) return resultA;

      const resultB = await address.decode(input);

      if (areDecoderErrors(resultB)) return resultB;

      return new DecoderSuccess({
        ...resultA.value,
        ...resultB.value,
      });
    });
  } else {
    const extrasD = objectD(
      {
        uid: STRING_DECODER,
        createdAt: TIMESTAMP_INPUT_DECODER,
        updatedAt: TIMESTAMP_INPUT_DECODER,
        lastUsedAt: nullableD(TIMESTAMP_INPUT_DECODER),
        label: NULLABLE_STRING_DECODER,
        apartmentNumber: NULLABLE_STRING_DECODER,
      },
      {
        removeUndefinedProperties: true,
      },
    );

    return new AsyncDecoder(async (input) => {
      const resultA = extrasD.decode(input);

      if (areDecoderErrors(resultA)) return resultA;

      const resultB = await address.decode(input);

      if (areDecoderErrors(resultB)) return resultB;

      return new DecoderSuccess({
        ...resultA.value,
        ...resultB.value,
      });
    });
  }
}

export interface IDestination extends IAddress {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  lastUsedAt: Timestamp | null;

  label: string | null;
  apartmentNumber: string | null;
}

export class Destination extends Address {
  uid!: string;
  createdAt: Date;
  updatedAt: Date;
  lastUsedAt: Date | null = null;

  label: string | null;
  apartmentNumber: string | null;

  constructor(params: IDestination) {
    super(params);

    this.createdAt = params.createdAt.toDate();
    this.updatedAt = params.updatedAt.toDate();
    if (params.lastUsedAt) {
      this.lastUsedAt = params.lastUsedAt.toDate();
    }
  }
}

export type TagColors =
  | 'Pink'
  | 'Purple'
  | 'Indigo'
  | 'Blue'
  | 'Cyan'
  | 'Teal'
  | 'Green'
  | 'Lime'
  | 'Orange'
  | 'Deep Orange'
  | 'Brown'
  | 'Gray'
  | 'Light Gray';

export interface ITag {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  type: string;
  label: string;
  description: string;
  color: TagColors;
  favorite: boolean;
}

export function tagCreateD() {
  return objectD(
    {
      uid: STRING_DECODER,
      createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      type: STRING_DECODER,
      label: STRING_DECODER,
      description: STRING_DECODER,
      color: STRING_DECODER,
      favorite: booleanD(),
    },
    {
      removeUndefinedProperties: true,
    },
  );
}

export function tagUpdateD() {
  return objectD(
    {
      // uid: STRING_DECODER,
      // createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      type: undefinableD(STRING_DECODER),
      label: undefinableD(STRING_DECODER),
      description: undefinableD(STRING_DECODER),
      color: undefinableD(STRING_DECODER),
      favorite: undefinableD(booleanD()),
    },
    {
      removeUndefinedProperties: true,
    },
  );
}

export class Tag {
  uid: string;
  createdAt: Date;
  updatedAt: Date;
  type: string;
  label: string;
  description: string;
  color: TagColors;
  favorite: boolean;

  constructor(params: ITag) {
    this.createdAt = params.createdAt.toDate();
    this.updatedAt = params.updatedAt.toDate();

    this.uid = params.uid;
    this.type = params.type;
    this.label = params.label;
    this.description = params.description;
    this.color = params.color;
    this.favorite = params.favorite;
  }
}

export interface IOrganization {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  name: string;
  adminEmailAddresses: string[];
  dispatcherEmailAddress: string;
  dispatchers: IDispatcher[];
  clientQuestions: IQuestion[];
  // accessToken: boolean;
  // accessTokenError: string | null;
  // accessTokenErrorTime: Timestamp | null;
}

export interface IOrganizationGoogleAccessToken {
  // token has same id as organization
  // organizationId: string;
  emailAddress: string;
  refreshToken: string;
  refreshTokenFetchedAt: Timestamp;
  error: string | null;
  errorTime: Timestamp | null;
}

export interface IWeeklyReminderEmailError {
  from: string;
  to: string;
  body: string;
  createdAt: Timestamp;
  error: string | null;
}

export interface IQuestion {
  updatedAt: Timestamp;
  retiredAt: Timestamp | null;
  uid: string;
  text: string;
  type: 'boolean' | 'select' | 'text' | 'section-break';
  required: boolean;
  order: number;
}

export abstract class Question {
  updatedAt: Date;
  retiredAt: Date | null;
  uid: string;
  text: string;
  type: 'boolean' | 'select' | 'text' | 'section-break';
  required: boolean;
  order: number;

  constructor(args: IQuestion) {
    Object.assign(this, args);

    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
    if (args.retiredAt) {
      this.retiredAt = args.retiredAt.toDate();
    }
  }

  get active() {
    return !this.retiredAt;
  }
}

type QuestionTypeName<T> = T extends 'select'
  ? ISelectQuestion
  : T extends 'text'
  ? ITextQuestion
  : T extends 'boolean'
  ? IBooleanQuestion
  : T extends 'section-break'
  ? IQuestionSectionBreak
  : any;

export function clientQuestionIs<
  T extends 'select' | 'text' | 'boolean' | 'section-break',
>(question: any, type: T): question is QuestionTypeName<T> {
  return (question && question.type) === type;
}

export function instantiateClientQuestion(args?: IQuestion | null) {
  if (clientQuestionIs(args, 'select')) {
    return new SelectQuestion(args);
  }
  if (clientQuestionIs(args, 'text')) {
    return new TextQuestion(args);
  }
  if (clientQuestionIs(args, 'boolean')) {
    return new BooleanQuestion(args);
  }
  if (clientQuestionIs(args, 'section-break')) {
    return new QuestionSectionBreak(args);
  } else {
    return null;
  }
}

export interface ISelectQuestion extends IQuestion {
  type: 'select';
  options: string[];
  allowOther: boolean;
}

export class SelectQuestion extends Question {
  type: 'select';
  options: string[];
  allowOther: boolean;

  constructor(args: ISelectQuestion) {
    super(args);
  }
}

export interface ITextQuestion extends IQuestion {
  type: 'text';
  valueType: 'text' | 'integer' | 'date';
}

export class TextQuestion extends Question {
  type: 'text';
  valueType: 'text' | 'integer' | 'date';

  constructor(args: ITextQuestion) {
    super(args);
  }
}

export interface IBooleanQuestion extends IQuestion {
  type: 'boolean';
}

export class BooleanQuestion extends Question {
  type: 'boolean';

  constructor(args: IBooleanQuestion) {
    super(args);
  }
}

export interface IQuestionSectionBreak extends IQuestion {
  type: 'section-break';
  body: string | null;
}

export class QuestionSectionBreak extends Question {
  type: 'section-break';
  body: string | null;

  constructor(args: IQuestionSectionBreak) {
    super(args);
  }
}

export interface IUser {
  emailAddress: string; // email address is uid
  updatedAt: Timestamp;
  organizationId?: string;
  name: string | null;
  photoUrl: string | null;
}

export interface IDispatcher {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  retiredAt: Timestamp | null;
  organizationId: string;
  firstName: string;
  middleName: string | null;
  lastName: string;
  phoneNumber: string | null;
  emailAddress: string | null;
  notes: string | null;
}

export function dispatcherCreateD() {
  return objectD<FirestoreInput<IDispatcher>>(
    {
      uid: STRING_DECODER,
      createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      retiredAt: nullableD(TIMESTAMP_INPUT_DECODER),
      organizationId: STRING_DECODER,
      firstName: STRING_DECODER,
      middleName: NULLABLE_STRING_DECODER,
      lastName: STRING_DECODER,
      phoneNumber: NULLABLE_STRING_DECODER,
      emailAddress: NULLABLE_STRING_DECODER,
      notes: NULLABLE_STRING_DECODER,
    },
    {
      removeUndefinedProperties: true,
    },
  );
}

export function dispatcherUpdateD() {
  return objectD<Partial<FirestoreInput<IDispatcher>>>(
    {
      // uid: STRING_DECODER,
      // createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      retiredAt: undefinableD(nullableD(TIMESTAMP_INPUT_DECODER)),
      // organizationId: undefinableD(STRING_DECODER),
      firstName: undefinableD(STRING_DECODER),
      middleName: undefinableD(NULLABLE_STRING_DECODER),
      lastName: undefinableD(STRING_DECODER),
      phoneNumber: undefinableD(NULLABLE_STRING_DECODER),
      emailAddress: undefinableD(NULLABLE_STRING_DECODER),
      notes: undefinableD(NULLABLE_STRING_DECODER),
    },
    {
      removeUndefinedProperties: true,
    },
  );
}

export class Dispatcher {
  uid: string;
  createdAt: Date;
  updatedAt: Date;
  retiredAt: Date | null;
  organizationId: string;
  firstName: string;
  middleName: string | null;
  lastName: string;
  phoneNumber: string | null;
  emailAddress: string | null;
  notes: string | null;

  constructor(args: IDispatcher) {
    Object.assign(this, args);

    if (args.createdAt) {
      this.createdAt = args.createdAt.toDate();
    }
    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
    if (args.retiredAt) {
      this.retiredAt = args.retiredAt.toDate();
    }
  }

  get name() {
    return [this.firstName, this.middleName, this.lastName]
      .filter((s) => !!s)
      .join(' ');
  }

  get nameWithRetiredStatus() {
    return this.retiredAt ? `${this.name} (retired)` : this.name;
  }

  get active() {
    return !this.retiredAt;
  }
}

export interface IDispatcherShift {
  uid: string;
  startAt: Timestamp;
  endAt: Timestamp | null;
  dispatcherId: string;
  updatedAt: Timestamp;
}

export class DispatcherShift {
  uid: string;
  startAt: Date;
  endAt: Date | null;
  dispatcherId: string;
  updatedAt: Date;

  constructor(args: IDispatcherShift) {
    Object.assign(this, args);

    if (args.startAt) {
      this.startAt = args.startAt.toDate();
    }
    if (args.endAt) {
      this.endAt = args.endAt.toDate();
    }
    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
  }
}

export interface IClient {
  createdAt: Timestamp;
  updatedAt: Timestamp;
  updatedById: string;
  retiredAt: Timestamp | null;
  retiredById: string | null;
  retiredReason: string | null;
  uid: string;
  createdById: string;
  nickname?: string | null;
  firstName: string;
  middleName: string | null;
  lastName: string;
  phoneNumber: string;
  emailAddress?: string | null;
  authorizeSharingInfoWithDrivers: boolean;
  mediCalOrSsi: boolean;
  ethnicity: string;
  birthdate: Timestamp;
  address: IAddress;
  answers: IAnswer[];
  comments: string | null;
  restrictions: string | null;
  tagIds?: string[];
  contacts: IPersonContact[];
  defaultPeopleImpacted?: number;
  defaultPeopleImpactedDescription?: string | null;
  flags: number;
}

export function clientCreateD(args: { mapService: IMapService }) {
  return objectD(
    {
      uid: STRING_DECODER,
      createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      updatedById: STRING_DECODER,
      retiredAt: nullableD(TIMESTAMP_INPUT_DECODER),
      retiredById: NULLABLE_STRING_DECODER,
      retiredReason: NULLABLE_STRING_DECODER,
      createdById: STRING_DECODER,
      nickname: NULLABLE_STRING_DECODER,
      firstName: STRING_DECODER,
      middleName: NULLABLE_STRING_DECODER,
      lastName: STRING_DECODER,
      phoneNumber: STRING_DECODER,
      emailAddress: NULLABLE_EMAIL_DECODER,
      authorizeSharingInfoWithDrivers: booleanD(),
      mediCalOrSsi: booleanD(),
      ethnicity: STRING_DECODER,
      birthdate: TIMESTAMP_INPUT_DECODER,
      answers: arrayD(ANSWER_INPUT_DECODER),
      comments: NULLABLE_STRING_DECODER,
      restrictions: NULLABLE_STRING_DECODER,
      contacts:
        arrayD<
          Omit<IPersonContact, 'updatedAt' | 'createdAt' | 'createdById'>
        >(),
      address: addressD(args.mapService),
      tagIds: arrayD(STRING_DECODER),
      defaultPeopleImpacted: integerD().chain(
        predicateD<number>((input) => input > 0),
      ),
      defaultPeopleImpactedDescription: undefinableD(NULLABLE_STRING_DECODER),
      flags: integerD(),
    },
    {
      allErrors: true,
      removeUndefinedProperties: true,
    },
  )
    .map((input) => {
      if (
        input.defaultPeopleImpacted > 1 &&
        !input.defaultPeopleImpactedDescription
      ) {
        throw new Error(
          `"defaultPeopleImpactedDescription" must be provided if "defaultPeopleImpacted" is greater than 1.`,
        );
      }

      input.contacts = input.contacts.map((c) => {
        return {
          ...c,
          updatedAt: Date.now(),
          createdAt: Date.now(),
          createdById: input.createdById,
        };
      });

      return input;
    })
    .chain<
      Omit<FirestoreInput<IClient>, 'answers'> & {
        answers: FirestoreInput<IAnswer>[];
      }
    >((input) => {
      const result = objectD(
        {
          contacts: arrayD(PERSON_CONTACT_INPUT_DECODER),
        },
        {
          removeUndefinedProperties: true,
        },
      ).decode(input);

      if (areDecoderErrors(result)) return result;

      return new DecoderSuccess({
        ...input,
        ...result.value,
        peopleImpactedDescription:
          input.defaultPeopleImpacted > 1
            ? input.defaultPeopleImpactedDescription
            : null,
      });
    });
}

export function clientUpdateD(args: {
  mapService: IMapService;
  clientAddress: IAddress;
}) {
  return objectD(
    {
      // uid: STRING_DECODER,
      // createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      updatedById: STRING_DECODER,
      retiredAt: optionalD(TIMESTAMP_INPUT_DECODER),
      retiredById: undefinableD(NULLABLE_STRING_DECODER),
      retiredReason: undefinableD(NULLABLE_STRING_DECODER),
      createdById: undefinableD(STRING_DECODER),
      nickname: undefinableD(NULLABLE_STRING_DECODER),
      firstName: undefinableD(STRING_DECODER),
      middleName: undefinableD(NULLABLE_STRING_DECODER),
      lastName: undefinableD(STRING_DECODER),
      phoneNumber: undefinableD(STRING_DECODER),
      emailAddress: undefinableD(NULLABLE_EMAIL_DECODER),
      // authorizeSharingInfoWithDrivers: undefinableD(booleanD()),
      mediCalOrSsi: undefinableD(booleanD()),
      ethnicity: undefinableD(STRING_DECODER),
      birthdate: undefinableD(TIMESTAMP_INPUT_DECODER),
      answers: undefinableD(arrayD(ANSWER_INPUT_DECODER)),
      comments: undefinableD(NULLABLE_STRING_DECODER),
      restrictions: undefinableD(NULLABLE_STRING_DECODER),
      contacts: undefinableD(
        arrayD<
          Omit<IPersonContact, 'updatedAt' | 'createdAt' | 'createdById'>
        >(),
      ),
      address: undefinableD(
        addressD(args.mapService, {
          update: args.clientAddress,
        }),
      ),
      tagIds: undefinableD(arrayD(STRING_DECODER)),
      defaultPeopleImpacted: undefinableD(numberD()),
      defaultPeopleImpactedDescription: undefinableD(NULLABLE_STRING_DECODER),
      flags: undefinableD(integerD()),
    },
    {
      allErrors: true,
      removeUndefinedProperties: true,
    },
  )
    .map((input) => {
      if ('contacts' in input) {
        input.contacts = input.contacts!.map((c) => {
          if (!(c as any).createdById) {
            return {
              // use Date.now() because firestore doesn't support timestamps
              // inside arrays
              ...c,
              createdAt: Date.now(),
              updatedAt: Date.now(),
              createdById: input.updatedById,
            };
          }

          return {
            ...(c as IPersonContact),
            updatedAt: Date.now(),
          };
        });
      }

      if (input.defaultPeopleImpacted === 1) {
        input.defaultPeopleImpactedDescription = null;
      } else if (
        input.defaultPeopleImpacted &&
        input.defaultPeopleImpacted > 1 &&
        !input.defaultPeopleImpactedDescription
      ) {
        throw new Error(
          `"defaultPeopleImpactedDescription" must be provided if "defaultPeopleImpacted" is greater than 1.`,
        );
      }

      return input;
    })
    .chain<
      Partial<
        Omit<FirestoreInput<IClient>, 'answers'> & {
          answers: FirestoreInput<IAnswer>[];
        }
      >
    >((input) => {
      const result = objectD(
        {
          contacts: undefinableD(arrayD(PERSON_CONTACT_INPUT_DECODER)),
        },
        {
          removeUndefinedProperties: true,
        },
      ).decode(input);

      if (areDecoderErrors(result)) return result;

      return new DecoderSuccess({
        ...input,
        ...result.value,
      });
    });
}

export class Client {
  createdAt: Date;
  updatedAt: Date;
  /** This property should be made NonNullable with a migration */
  updatedById?: string;
  retiredAt: Date | null;
  retiredById: string | null;
  retiredBy?: Dispatcher | null;
  retiredReason: string | null;
  uid: string;
  organizationId: string;
  createdById: string;
  createdBy?: Dispatcher;
  nickname: string | null = null;
  firstName: string;
  middleName: string | null;
  lastName: string;
  phoneNumber: string;
  emailAddress?: string | null;
  authorizeSharingInfoWithDrivers: boolean;
  mediCalOrSsi: boolean;
  ethnicity: string;
  birthdate: Date;
  address: Address;
  answers: Answer[];
  comments: string | null;
  restrictions: string | null;
  tagIds: string[] = [];
  tags: Tag[] = [];
  contacts: PersonContact[] = [];
  defaultPeopleImpacted = 1;
  defaultPeopleImpactedDescription = null;
  flags = 0;

  constructor(args: IClient) {
    Object.assign(this, args);

    if (args.createdAt) {
      this.createdAt = args.createdAt.toDate();
    }
    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
    if (args.retiredAt) {
      this.retiredAt = args.retiredAt.toDate();
    }
    // if (args.flagLastUpdatedAt) {
    //   this.flagLastUpdatedAt = args.flagLastUpdatedAt.toDate();
    // }
    if (args.address) {
      this.address = new Address(args.address);
    }
    if (args.answers) {
      this.answers = args.answers.map((answer) => new Answer(answer));
    }
    if (args.birthdate) {
      this.birthdate = args.birthdate.toDate();
    }
    if (args.contacts) {
      this.contacts = args.contacts.map((c) => new PersonContact(c));
    }
  }

  get nicknameOrFirstName() {
    return this.nickname || this.firstName;
  }

  get name() {
    return [this.nickname || this.firstName, this.middleName, this.lastName]
      .filter((s) => !!s)
      .join(' ');
  }

  get nameSecondaryIndex() {
    return [this.firstName, this.middleName, this.lastName]
      .filter((s) => !!s)
      .join(' ');
  }

  get nameWithRetiredStatus() {
    return this.retiredAt ? `${this.name} (retired)` : this.name;
  }

  get fullnameWithRetiredStatus() {
    const name = [
      this.nickname ? `"${this.nickname}"` : this.firstName,
      this.middleName,
      this.lastName,
      this.nickname && `(real first name: ${this.firstName})`,
    ]
      .filter((s) => !!s)
      .join(' ');

    return this.retiredAt ? `${name} (retired)` : name;
  }

  matchName(text = '') {
    if (this.nickname && this.nameSecondaryIndex.toLowerCase().includes(text)) {
      return true;
    }

    return this.name.toLowerCase().includes(text);
  }

  get active() {
    return !this.retiredAt;
  }

  get age() {
    return differenceInYears(new Date(), this.birthdate);
  }
}

export interface IAnswer {
  updatedAt: Timestamp;
  questionId: string;
  answer: string | boolean | Date | null;
}

const ANSWER_INPUT_DECODER = objectD(
  {
    updatedAt: TIMESTAMP_INPUT_DECODER,
    questionId: STRING_DECODER,
    answer: nullableD(
      anyOfD([NULLABLE_STRING_DECODER, booleanD(), instanceOfD(Date)], {
        errorMsg: 'invalid answer',
      }),
    ),
  },
  {
    removeUndefinedProperties: true,
  },
);

export class Answer {
  updatedAt: Date;
  questionId: string;
  question?: Question;
  answer: string | boolean | Date | null;
  renderedAnswer?: string;

  constructor(args: IAnswer) {
    Object.assign(this, args);

    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
  }
}

export interface IDriver {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  retiredAt: Timestamp | null;
  retiredById: string | null;
  retiredReason: string | null;
  vacationStartsAt: Timestamp;
  vacationEndsAt: Timestamp;
  vacationingById: string | null;
  organizationId: string;
  createdById: string;
  nickname?: string | null;
  firstName: string;
  middleName: string | null;
  lastName: string;
  ethnicity: string;
  driversLicenseExpirationDate: Timestamp;
  carInsuranceExpirationDate: Timestamp;
  phoneNumber: string;
  emailAddress: string | null;
  address: IAddress;
  birthdate: Timestamp | null;
  genderIdentity: string | null;
  comments: string | null;
  availability: {
    sun: boolean;
    mon: boolean;
    tue: boolean;
    wed: boolean;
    thu: boolean;
    fri: boolean;
    sat: boolean;
  };
  blacklistedClientIds: string[];
  carInfo: string | null;
  tagIds?: string[];
  flags: number;
}

export function driverCreateD(args: { mapService: IMapService }) {
  return objectD(
    {
      uid: STRING_DECODER,
      createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      retiredAt: nullableD(TIMESTAMP_INPUT_DECODER),
      retiredById: NULLABLE_STRING_DECODER,
      retiredReason: NULLABLE_STRING_DECODER,
      vacationStartsAt: TIMESTAMP_INPUT_DECODER,
      vacationEndsAt: TIMESTAMP_INPUT_DECODER,
      vacationingById: NULLABLE_STRING_DECODER,
      organizationId: STRING_DECODER,
      createdById: STRING_DECODER,
      nickname: NULLABLE_STRING_DECODER,
      firstName: STRING_DECODER,
      middleName: NULLABLE_STRING_DECODER,
      lastName: STRING_DECODER,
      ethnicity: STRING_DECODER,
      driversLicenseExpirationDate: TIMESTAMP_INPUT_DECODER,
      carInsuranceExpirationDate: TIMESTAMP_INPUT_DECODER,
      phoneNumber: STRING_DECODER,
      emailAddress: NULLABLE_STRING_DECODER,
      address: addressD(args.mapService),
      birthdate: nullableD(TIMESTAMP_INPUT_DECODER),
      genderIdentity: NULLABLE_STRING_DECODER,
      comments: NULLABLE_STRING_DECODER,
      availability: objectD({
        sun: booleanD(),
        mon: booleanD(),
        tue: booleanD(),
        wed: booleanD(),
        thu: booleanD(),
        fri: booleanD(),
        sat: booleanD(),
      }),
      blacklistedClientIds: arrayD(STRING_DECODER),
      carInfo: NULLABLE_STRING_DECODER,
      tagIds: arrayD(STRING_DECODER),
      flags: integerD(),
    },
    {
      allErrors: true,
      removeUndefinedProperties: true,
    },
  );
}

export function driverUpdateD(args: {
  mapService: IMapService;
  driverAddress: IAddress;
}) {
  return objectD(
    {
      // uid: STRING_DECODER,
      // createdAt: TIMESTAMP_INPUT_DECODER,
      updatedAt: TIMESTAMP_INPUT_DECODER,
      retiredAt: optionalD(TIMESTAMP_INPUT_DECODER),
      retiredById: undefinableD(NULLABLE_STRING_DECODER),
      retiredReason: undefinableD(NULLABLE_STRING_DECODER),
      vacationStartsAt: undefinableD(TIMESTAMP_INPUT_DECODER),
      vacationEndsAt: undefinableD(TIMESTAMP_INPUT_DECODER),
      vacationingById: undefinableD(NULLABLE_STRING_DECODER),
      organizationId: undefinableD(STRING_DECODER),
      createdById: undefinableD(STRING_DECODER),
      nickname: undefinableD(NULLABLE_STRING_DECODER),
      firstName: undefinableD(STRING_DECODER),
      middleName: undefinableD(NULLABLE_STRING_DECODER),
      lastName: undefinableD(STRING_DECODER),
      ethnicity: undefinableD(STRING_DECODER),
      driversLicenseExpirationDate: undefinableD(TIMESTAMP_INPUT_DECODER),
      carInsuranceExpirationDate: undefinableD(TIMESTAMP_INPUT_DECODER),
      phoneNumber: undefinableD(STRING_DECODER),
      emailAddress: undefinableD(NULLABLE_STRING_DECODER),
      address: undefinableD(
        addressD(args.mapService, {
          update: args.driverAddress,
        }),
      ),
      birthdate: optionalD(TIMESTAMP_INPUT_DECODER),
      genderIdentity: undefinableD(NULLABLE_STRING_DECODER),
      comments: undefinableD(NULLABLE_STRING_DECODER),
      availability: undefinableD(
        objectD({
          sun: booleanD(),
          mon: booleanD(),
          tue: booleanD(),
          wed: booleanD(),
          thu: booleanD(),
          fri: booleanD(),
          sat: booleanD(),
        }),
      ),
      blacklistedClientIds: undefinableD(arrayD(STRING_DECODER)),
      carInfo: undefinableD(NULLABLE_STRING_DECODER),
      tagIds: undefinableD(arrayD(STRING_DECODER)),
      flags: undefinableD(integerD()),
    },
    {
      allErrors: true,
      removeUndefinedProperties: true,
    },
  );
}

export class Driver {
  createdAt: Date;
  updatedAt: Date;
  retiredAt: Date | null;
  retiredById: string | null;
  retiredBy?: Dispatcher;
  retiredReason: string | null;
  vacationStartsAt: Date;
  vacationEndsAt: Date;
  vacationingById: string | null;
  vacationingBy?: Dispatcher | null;
  uid: string;
  organizationId: string;
  createdById: string;
  createdBy?: Dispatcher;
  nickname: string | null = null;
  firstName: string;
  middleName: string | null;
  lastName: string;
  ethnicity: string;
  driversLicenseExpirationDate: Date;
  carInsuranceExpirationDate: Date;
  phoneNumber: string;
  emailAddress: string | null;
  address: Address;
  birthdate: Date | null = null;
  genderIdentity: string | null = null;
  comments: string | null;
  availability: {
    sun: boolean;
    mon: boolean;
    tue: boolean;
    wed: boolean;
    thu: boolean;
    fri: boolean;
    sat: boolean;
  };
  blacklistedClientIds: string[] = [];
  blacklistedClients: Client[] = [];
  carInfo: string | null;
  tagIds: string[] = [];
  tags: Tag[] = [];
  flags = 0;

  constructor(protected args: IDriver) {
    Object.assign(this, args);

    if (args.createdAt) {
      this.createdAt = args.createdAt.toDate();
    }
    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }
    if (args.retiredAt) {
      this.retiredAt = args.retiredAt.toDate();
    }
    // if (args.flagLastUpdatedAt) {
    //   this.flagLastUpdatedAt = args.flagLastUpdatedAt.toDate();
    // }
    if (args.vacationStartsAt) {
      this.vacationStartsAt = args.vacationStartsAt.toDate();
    }
    if (args.vacationEndsAt) {
      this.vacationEndsAt = args.vacationEndsAt.toDate();
    }
    if (args.driversLicenseExpirationDate) {
      this.driversLicenseExpirationDate =
        args.driversLicenseExpirationDate.toDate();
    }
    if (args.carInsuranceExpirationDate) {
      this.carInsuranceExpirationDate =
        args.carInsuranceExpirationDate.toDate();
    }
    if (args.address) {
      this.address = new Address(args.address);
    }
    if (args.birthdate) {
      this.birthdate = args.birthdate.toDate();
    }
  }

  get nicknameOrFirstName() {
    return this.nickname || this.firstName;
  }

  get name() {
    return [this.nickname || this.firstName, this.middleName, this.lastName]
      .filter((s) => !!s)
      .join(' ');
  }

  get nameSecondaryIndex() {
    return [this.firstName, this.middleName, this.lastName]
      .filter((s) => !!s)
      .join(' ');
  }

  get nameWithRetiredStatus() {
    return this.retiredAt ? `${this.name} (retired)` : this.name;
  }

  get fullnameWithRetiredStatus() {
    const name = [
      this.nickname ? `"${this.nickname}"` : this.firstName,
      this.middleName,
      this.lastName,
      this.nickname && `(real first name: ${this.firstName})`,
    ]
      .filter((s) => !!s)
      .join(' ');

    return this.retiredAt ? `${name} (retired)` : name;
  }

  get active() {
    return !this.retiredAt;
  }

  get age() {
    return this.birthdate
      ? differenceInYears(new Date(), this.birthdate).toString()
      : 'n/a';
  }

  matchName(text = '') {
    if (this.nickname && this.nameSecondaryIndex.toLowerCase().includes(text)) {
      return true;
    }

    return this.name.toLowerCase().includes(text);
  }

  isOnVacation(date?: Date | null) {
    if (!date) {
      date = new Date();
    }

    const time = date.valueOf();

    return (
      this.vacationEndsAt.valueOf() > time &&
      this.vacationStartsAt.valueOf() <= time
    );
  }

  renderAvailability() {
    const availability: string[] = [];

    if (this.availability.mon) {
      availability.push('Monday');
    }
    if (this.availability.tue) {
      availability.push('Tuesday');
    }
    if (this.availability.wed) {
      availability.push('Wednesday');
    }
    if (this.availability.thu) {
      availability.push('Thursday');
    }
    if (this.availability.fri) {
      availability.push('Friday');
    }
    if (this.availability.sat) {
      availability.push('Saturday');
    }
    if (this.availability.sun) {
      availability.push('Sunday');
    }

    if (availability.length === 0) {
      return null;
    }

    return availability.join(', ');
  }

  availableOn(date: Date) {
    switch (date.getDay()) {
      case 1:
        return this.availability.mon;
      case 2:
        return this.availability.tue;
      case 3:
        return this.availability.wed;
      case 4:
        return this.availability.thu;
      case 5:
        return this.availability.fri;
      case 6:
        return this.availability.sat;
      default:
        return this.availability.sun;
    }
  }

  clone() {
    return new Driver(this.args);
  }
}

export interface IRideRequestBase {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  clientId: string;

  pickupAddress: IAddress;
  destinationAddresses: IAddress[];
  priority: string;
  requestMadeBy: string;
  requestTakenById: string;
  comments: string | null;
  peopleImpacted: number;
  peopleImpactedDescription: string | null;
  // The flags property could be refactored out I believe.
  // It was originally added to facilitate the "Todos" section and the
  // ability to filter requests by whether or not they had any flags,
  // however this was needed because, originally, notes were stored in
  // `rideRequest/{rideRequestId}/noteThreads` in Firestore and searching
  // collection groups wasn't yet a feature. I've since migrated noteThreads
  // to all be in one collection so it's possible to search for flagged notes
  // which have a particular `rideRequestId` property to see if a specific
  // ride request has any flags.
  flags: number;

  flexible: boolean;
  flexibleSchedule: string | null;

  driverId: string | null;
  driverAssignedById: string | null;
  driverNotifiedAt: Timestamp | null;
  driverNotifiedById: string | null;
  clientNotifiedOfDriverAt: Timestamp | null;
  clientNotifiedOfDriverById: string | null;
  /** Only calculated when a driver is assigned, null otherwise. */
  estimatedRideSeconds: number | null;
  /** Only calculated when a driver is assigned, null otherwise. */
  estimatedRideMeters: number | null;
  hasWillingDriver: boolean;

  idOfDispatcherWhoCancelledRequest: string | null;
  requestCancelled: boolean;
  requestCancelledAt: Timestamp | null;
  requestCancelledBy: string | null;
  cancellationReason: string | null;
  driverNotifiedOfCancellationAt: Timestamp | null;
  driverNotifiedOfCancellationById: string | null;
}

export interface IRideRequest extends IRideRequestBase {
  datetime: Timestamp;
  recurringRideRequestId: string | null;
}

export interface IRecurringRideRequest extends IRideRequestBase {
  startDate: Timestamp;
  endDate: Timestamp | null;
  time: string;
  scheduleName:
    | 'Once a month'
    | 'Once every 3 weeks'
    | 'Once every 2 weeks'
    | 'Once a week';
  lastGeneratedEndDate: Timestamp | null;
}

export function rideRequestCreateD(
  args:
    | {
        recurring: true;
        trustAddresses: true;
      }
    | {
        recurring: true;
        mapService: IMapService;
        clientAddress: IAddress;
        trustAddresses?: boolean;
        trustDate?: boolean;
      },
): AsyncDecoder<FirestoreInput<IRecurringRideRequest>, any>;
export function rideRequestCreateD(
  args:
    | {
        recurring: false;
        trustAddresses: true;
      }
    | {
        recurring: false;
        mapService: IMapService;
        clientAddress: IAddress;
        trustAddresses?: boolean;
        trustDate?: boolean;
      },
): AsyncDecoder<FirestoreInput<IRideRequest>, any>;
export function rideRequestCreateD(args: {
  recurring: boolean;
  mapService: IMapService;
  clientAddress: IAddress;
  trustAddresses?: boolean;
  trustDate?: boolean;
}): AsyncDecoder<FirestoreInput<IRideRequest | IRecurringRideRequest>, any>;
export function rideRequestCreateD(args: {
  recurring: boolean;
  mapService?: IMapService;
  clientAddress?: IAddress;
  // this option is used when duplicating ride requests to avoid excess maps API usage
  // without this option, a QUOTE_LIMIT_ERROR is raised when duplicating as few as 8
  // ride requests
  trustAddresses?: boolean;
  // this option is used when the provided date is in another timezone vs the host computer
  trustDate?: boolean;
}): AsyncDecoder<FirestoreInput<IRideRequest | IRecurringRideRequest>, any> {
  if (!args.trustAddresses && !(args.mapService && args.clientAddress)) {
    throw new Error(
      `mapService and clientService must be provided unless trustAddresses === true`,
    );
  }

  const pickupAddress = args.trustAddresses
    ? anyD<FirestoreInput<IAddress>>()
    : undefinableD(addressD(args.mapService!)).map((input) => {
        if (input) return input;

        return {
          ...args.clientAddress!,
          label: "Client's standard address",
        };
      });

  const destinationAddresses = args.trustAddresses
    ? anyD<Array<FirestoreInput<IAddress>>>()
    : arrayD(addressD(args.mapService!), {
        allErrors: true,
      }).chain((input) => {
        if (input.length < 1) {
          return new DecoderError(
            input,
            'destinations-required',
            'must have at least one destination',
          );
        } else if (input.some((d) => !d.tripPurpose)) {
          return new DecoderError(
            input,
            'trip-purpose-required',
            'destinations must include a trip purpose',
          );
        }

        return new DecoderSuccess(input);
      });

  const baseDecoderArgs = {
    uid: STRING_DECODER,
    createdAt: TIMESTAMP_INPUT_DECODER,
    updatedAt: TIMESTAMP_INPUT_DECODER,
    clientId: STRING_DECODER,
    pickupAddress,
    destinationAddresses,
    priority: STRING_DECODER,
    requestMadeBy: STRING_DECODER,
    requestTakenById: STRING_DECODER,
    comments: NULLABLE_STRING_DECODER,
    peopleImpacted: integerD().chain(predicateD<number>((input) => input > 0)),
    peopleImpactedDescription: NULLABLE_STRING_DECODER,
    flags: integerD(),

    flexible: undefinableD(booleanD()).map((v) => v ?? false),
    flexibleSchedule: undefinableD(NULLABLE_STRING_DECODER).map(
      (v) => v || null,
    ),

    driverId: NULLABLE_STRING_DECODER,
    driverAssignedById: NULLABLE_STRING_DECODER,
    driverNotifiedAt: nullableD(TIMESTAMP_INPUT_DECODER),
    driverNotifiedById: NULLABLE_STRING_DECODER,
    clientNotifiedOfDriverAt: nullableD(TIMESTAMP_INPUT_DECODER),
    clientNotifiedOfDriverById: NULLABLE_STRING_DECODER,
    estimatedRideSeconds: nullableD(numberD()),
    estimatedRideMeters: nullableD(numberD()),
    hasWillingDriver: booleanD(),

    idOfDispatcherWhoCancelledRequest: NULLABLE_STRING_DECODER,
    requestCancelled: booleanD(),
    requestCancelledAt: nullableD(TIMESTAMP_INPUT_DECODER),
    requestCancelledBy: NULLABLE_STRING_DECODER,
    cancellationReason: NULLABLE_STRING_DECODER,
    driverNotifiedOfCancellationAt: nullableD(TIMESTAMP_INPUT_DECODER),
    driverNotifiedOfCancellationById: NULLABLE_STRING_DECODER,
  };

  let decoder: AsyncDecoder<
    FirestoreInput<IRideRequest> | FirestoreInput<IRecurringRideRequest>
  >;

  if (args.recurring) {
    decoder = objectD(
      {
        ...baseDecoderArgs,
        lastGeneratedEndDate: nullableD(TIMESTAMP_INPUT_DECODER),
        startDate: instanceOfD(Date),
        time: STRING_DECODER, // format hh:mm as 24 hour clock
        scheduleName: anyOfD([
          exactlyD('Once a month'),
          exactlyD('Once every 3 weeks'),
          exactlyD('Once every 2 weeks'),
          exactlyD('Once a week'),
        ]),
        endDate: optionalD(instanceOfD(Date)),
      },
      {
        decoderName: 'CreateRecurringRideRequestDecoder',
        allErrors: true,
        // noExcessProperties: true,
        removeUndefinedProperties: true,
      },
    ).map<FirestoreInput<IRecurringRideRequest>>((input) => {
      const startDate = convertDateToSameWallclockTimeInCaliZone(
        setToStartOf(input.startDate, 'day'),
      );

      const endDate = !input.endDate
        ? null
        : convertDateToSameWallclockTimeInCaliZone(
            setToStartOf(input.endDate, 'day'),
          );

      return {
        ...input,
        startDate,
        endDate,
        peopleImpactedDescription:
          input.peopleImpacted > 1 ? input.peopleImpactedDescription : null,
      };
    });
  } else {
    decoder = objectD(
      {
        ...baseDecoderArgs,
        recurringRideRequestId: NULLABLE_STRING_DECODER,
        date: instanceOfD(Date),
        time: STRING_DECODER,
      },
      {
        decoderName: 'CreateRideRequestDecoder',
        allErrors: true,
        // noExcessProperties: true,
        removeUndefinedProperties: true,
      },
    ).map<FirestoreInput<IRideRequest>>((input) => {
      const datetime = args.trustDate
        ? new Date(input.date)
        : convertDateToSameWallclockTimeInCaliZone(
            setTime(input.date, input.time),
          );

      delete (input as { date?: Date }).date;
      delete (input as { time?: string }).time;

      return {
        ...input,
        datetime,
        peopleImpactedDescription:
          input.peopleImpacted > 1 ? input.peopleImpactedDescription : null,
      };
    });
  }

  return decoder.chain((input) => {
    if (input.peopleImpacted! > 1 && !input.peopleImpactedDescription) {
      return new DecoderError(
        input,
        'invalid-value',
        `"peopleImpactedDescription" must be provided if "peopleImpacted" is greater than 1.`,
      );
    } else if (input.flexible && !input.flexibleSchedule) {
      return new DecoderError(
        input,
        'invalid-value',
        `"flexibleSchedule" must be provided if "flexible" is true.`,
      );
    } else if (!input.flexible) {
      input.flexibleSchedule = null;
    }

    return new DecoderSuccess(input);
  });
}

export function rideRequestUpdateD(args: {
  recurring: true;
  mapService: IMapService;
  rideRequest: IRecurringRideRequest | RecurringRideRequest;
}): AsyncDecoder<
  Partial<FirestoreInput<IRecurringRideRequest>> &
    Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>,
  any
>;
export function rideRequestUpdateD(args: {
  recurring: false;
  mapService: IMapService;
  rideRequest: IRideRequest | RideRequest;
}): AsyncDecoder<
  Partial<FirestoreInput<IRideRequest>> &
    Pick<FirestoreInput<IRideRequest>, 'updatedAt'>,
  any
>;
export function rideRequestUpdateD(args: {
  recurring: boolean;
  mapService: IMapService;
  rideRequest:
    | IRideRequest
    | RideRequest
    | IRecurringRideRequest
    | RecurringRideRequest;
}): AsyncDecoder<
  | (Partial<FirestoreInput<IRideRequest>> &
      Pick<FirestoreInput<IRideRequest>, 'updatedAt'>)
  | (Partial<FirestoreInput<IRecurringRideRequest>> &
      Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>)
>;
export function rideRequestUpdateD(args: {
  recurring: boolean;
  mapService: IMapService;
  rideRequest:
    | IRideRequest
    | RideRequest
    | IRecurringRideRequest
    | RecurringRideRequest;
}) {
  const baseDecoderArgs = {
    // uid: STRING_DECODER,
    // createdAt: TIMESTAMP_INPUT_DECODER,
    updatedAt: TIMESTAMP_INPUT_DECODER,
    // clientId: STRING_DECODER,
    pickupAddress: undefinableD(addressD(args.mapService)),
    destinationAddresses: undefinableD(
      arrayD()
        .toAsyncDecoder()
        .chain(
          lazyD<IAddress[], unknown[]>(
            (input) => {
              if (
                input.length !== args.rideRequest.destinationAddresses.length
              ) {
                return arrayD(addressD(args.mapService));
              }

              return tupleD<[IAddress, ...IAddress[]]>(
                input.map((_, index) => {
                  return addressD(args.mapService, {
                    update: args.rideRequest.destinationAddresses[index],
                  });
                }) as [any, ...any[]],
              );
            },
            {
              promise: true,
            },
          ),
        ),
    ),
    priority: undefinableD(STRING_DECODER),
    requestMadeBy: undefinableD(STRING_DECODER),
    requestTakenById: undefinableD(STRING_DECODER),
    comments: undefinableD(NULLABLE_STRING_DECODER),
    peopleImpacted: undefinableD(numberD()),
    peopleImpactedDescription: undefinableD(NULLABLE_STRING_DECODER),
    flags: undefinableD(integerD()),

    flexible: undefinableD(booleanD()),
    flexibleSchedule: undefinableD(NULLABLE_STRING_DECODER),

    driverId: undefinableD(NULLABLE_STRING_DECODER),
    driverAssignedById: undefinableD(NULLABLE_STRING_DECODER),
    driverNotifiedAt: undefinableD(nullableD(TIMESTAMP_INPUT_DECODER)),
    driverNotifiedById: undefinableD(NULLABLE_STRING_DECODER),
    clientNotifiedOfDriverAt: optionalD(TIMESTAMP_INPUT_DECODER),
    clientNotifiedOfDriverById: undefinableD(NULLABLE_STRING_DECODER),
    estimatedRideSeconds: optionalD(numberD()),
    estimatedRideMeters: optionalD(numberD()),
    hasWillingDriver: undefinableD(booleanD()),

    idOfDispatcherWhoCancelledRequest: undefinableD(NULLABLE_STRING_DECODER),
    requestCancelled: undefinableD(booleanD()),
    requestCancelledAt: optionalD(TIMESTAMP_INPUT_DECODER),
    requestCancelledBy: undefinableD(NULLABLE_STRING_DECODER),
    cancellationReason: undefinableD(NULLABLE_STRING_DECODER),
    driverNotifiedOfCancellationAt: optionalD(TIMESTAMP_INPUT_DECODER),
    driverNotifiedOfCancellationById: undefinableD(NULLABLE_STRING_DECODER),
  };

  let decoder: AsyncDecoder<
    | (Partial<FirestoreInput<IRideRequest>> &
        Pick<FirestoreInput<IRideRequest>, 'updatedAt'>)
    | (Partial<FirestoreInput<IRecurringRideRequest>> &
        Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>)
  >;

  if (args.recurring) {
    decoder = objectD(
      {
        ...baseDecoderArgs,
        startDate: undefinableD(instanceOfD(Date)),
        time: undefinableD(STRING_DECODER),
        scheduleName: undefinableD(
          anyOfD([
            exactlyD('Once a month'),
            exactlyD('Once every 3 weeks'),
            exactlyD('Once every 2 weeks'),
            exactlyD('Once a week'),
          ]),
        ),
        endDate: optionalD(instanceOfD(Date)),
      },
      {
        allErrors: true,
        // noExcessProperties: true,
        removeUndefinedProperties: true,
      },
    ).map<
      Partial<FirestoreInput<IRecurringRideRequest>> &
        Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>
    >((input) => {
      if (input.startDate) {
        input.startDate = convertDateToSameWallclockTimeInCaliZone(
          setToStartOf(input.startDate, 'day'),
        );
      }

      if (input.endDate) {
        input.endDate = convertDateToSameWallclockTimeInCaliZone(
          setToStartOf(input.endDate, 'day'),
        );
      }

      return input;
    });
  } else {
    decoder = objectD(
      {
        ...baseDecoderArgs,
        date: undefinableD(instanceOfD(Date)),
        time: undefinableD(STRING_DECODER),
      },
      {
        allErrors: true,
        // noExcessProperties: true,
        removeUndefinedProperties: true,
      },
    )
      .chain((input) => {
        if (
          ('date' in input || 'time' in input) &&
          !(input.date && input.time)
        ) {
          return new DecoderError(
            input,
            'invalid-value',
            `Both "date" and "time" arguments must be provided when updating a ride request.`,
          );
        }

        return new DecoderSuccess(input);
      })
      .map<
        Partial<FirestoreInput<IRideRequest>> &
          Pick<FirestoreInput<IRideRequest>, 'updatedAt'>
      >((input) => {
        const output: typeof input & { datetime?: Date } = { ...input };

        if (output.date && output.time) {
          const datetime = convertDateToSameWallclockTimeInCaliZone(
            setTime(output.date, output.time),
          );

          delete output.date;
          delete output.time;
          output.datetime = datetime;
        }

        return output;
      });
  }

  return decoder.chain((input) => {
    if (input.peopleImpacted === 1) {
      input.peopleImpactedDescription = null;
    } else if (
      input.peopleImpacted &&
      input.peopleImpacted > 1 &&
      !input.peopleImpactedDescription
    ) {
      return new DecoderError(
        input,
        'invalid-value',
        `"peopleImpactedDescription" must be provided if "peopleImpacted" is greater than 1.`,
      );
    } else if (typeof input.flexible === 'boolean') {
      if (input.flexible && !input.flexibleSchedule) {
        return new DecoderError(
          input,
          'invalid-value',
          `"flexibleSchedule" must be provided if "flexible" is true.`,
        );
      } else if (!input.flexible && input.flexibleSchedule !== null) {
        return new DecoderError(
          input,
          'invalid-value',
          `"flexibleSchedule" must be null if "flexible" is false.`,
        );
      }
    }

    return new DecoderSuccess(input);
  });
}

export const SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL =
  '**FIRESTORE_SERVER_TIMESTAMP**';

export const SERIALIZABLE_SERVER_DATE_SENTINAL = '**FIRESTORE_SERVER_DATE**';

export class RideRequestBase {
  createdAt: Date;
  updatedAt: Date;
  uid: string;
  clientId: string;

  private _client?: Client;

  set client(value: Client) {
    this._client = value;
  }
  get client() {
    if (!this._client) {
      throw new Error('RideRequestBase#client called when client is undefined');
    }

    return this._client;
  }

  pickupAddress: Address;
  destinationAddresses: Address[];
  priority: string;
  requestMadeBy: string;
  requestTakenById: string;
  requestTakenBy?: Dispatcher;
  comments: string | null;
  peopleImpacted: number;
  peopleImpactedDescription: string | null;
  flags: number;

  flexible: boolean;
  flexibleSchedule: string | null;

  driverId: string | null;
  driver?: Driver | null;
  driverAssignedById: string | null;
  driverAssignedBy?: Dispatcher;
  driverNotifiedAt: Date | null;
  driverNotifiedById: string | null;
  driverNotifiedBy?: Dispatcher;
  clientNotifiedOfDriverAt: Date | null;
  clientNotifiedOfDriverById: string | null;
  clientNotifiedOfDriverBy?: Dispatcher;
  estimatedRideSeconds: number | null;
  estimatedRideMeters: number | null;
  hasWillingDriver: boolean;

  idOfDispatcherWhoCancelledRequest: string | null;
  dispatcherWhoCancelledRequest?: Dispatcher;
  requestCancelledAt: Date | null;
  requestCancelledBy: string | null;
  cancellationReason: string | null;
  driverNotifiedOfCancellationAt: Date | null;
  driverNotifiedOfCancellationById: string | null;
  driverNotifiedOfCancellationBy?: Dispatcher;

  constructor(rideRequest: IRideRequestBase, client?: Client) {
    Object.assign(this, rideRequest);

    this._client = client;

    if (rideRequest.createdAt) {
      this.createdAt = rideRequest.createdAt.toDate();
    }
    if (rideRequest.updatedAt) {
      this.updatedAt = rideRequest.updatedAt.toDate();
    }

    if (rideRequest.pickupAddress) {
      this.pickupAddress = new Address(rideRequest.pickupAddress);
    }
    if (rideRequest.destinationAddresses) {
      this.destinationAddresses = rideRequest.destinationAddresses.map(
        (address) => new Address(address),
      );
    }
    if (rideRequest.driverNotifiedAt) {
      this.driverNotifiedAt = rideRequest.driverNotifiedAt.toDate();
    }
    if (rideRequest.clientNotifiedOfDriverAt) {
      this.clientNotifiedOfDriverAt =
        rideRequest.clientNotifiedOfDriverAt.toDate();
    }
    if (rideRequest.requestCancelledAt) {
      this.requestCancelledAt = rideRequest.requestCancelledAt.toDate();
    }
    if (rideRequest.driverNotifiedOfCancellationAt) {
      this.driverNotifiedOfCancellationAt =
        rideRequest.driverNotifiedOfCancellationAt.toDate();
    }
  }

  get category() {
    return this.destinationAddresses[0].tripPurpose ===
      'Grocery Shopper Program'
      ? 'GSP'
      : 'STP';
  }
}

export class RideRequest extends RideRequestBase {
  datetime: Date;
  recurringRideRequestId: string | null = null;
  recurringRideRequest: RecurringRideRequest | null = null;

  constructor(protected args: IRideRequest, client?: Client) {
    super(args, client);

    // very confused as to why we need to set recurringRideRequestId here
    // It should be covered by the `Object.assign(this, rideRequest)` call
    // in the ancestor class, but for some reason it isn't.
    this.recurringRideRequestId = args.recurringRideRequestId;
    this.datetime = args.datetime?.toDate();
  }

  clone() {
    const r = new RideRequest(this.args, this.client);
    r.driver = this.driver?.clone();
    r.recurringRideRequest = this.recurringRideRequest?.clone() ?? null;
    return r;
  }
}

export class RecurringRideRequest extends RideRequestBase {
  startDate: Date;
  time: string;
  endDate: Date | null;
  lastGeneratedEndDate: Date | null;
  scheduleName:
    | 'Once a month'
    | 'Once every 3 weeks'
    | 'Once every 2 weeks'
    | 'Once a week';

  schedule: Schedule<{
    name:
      | 'Once a month'
      | 'Once every 3 weeks'
      | 'Once every 2 weeks'
      | 'Once a week';
  }>;

  constructor(protected args: IRecurringRideRequest, client?: Client) {
    super(args, client);

    this.startDate = args.startDate.toDate();
    this.endDate = args.endDate?.toDate() ?? null;
    this.lastGeneratedEndDate = args.lastGeneratedEndDate?.toDate() ?? null;
    this.scheduleName = args.scheduleName;

    const [hours, minutes] = this.time.split(':').map((s) => parseInt(s, 10));

    const start = moment
      .tz(this.startDate, 'America/Los_Angeles')
      .set('hours', hours)
      .set('minutes', minutes);

    this.schedule = createRecurringRideRequestSchedule({
      start,
      end: this.endDate && moment.tz(this.endDate, 'America/Los_Angeles'),
      scheduleName: this.scheduleName,
    });
  }

  clone() {
    const r = new RecurringRideRequest(this.args, this.client);
    r.driver = this.driver?.clone();
    return r;
  }
}

export function createRecurringRideRequestSchedule(args: {
  start: moment.Moment;
  end?: moment.Moment | null;
  scheduleName:
    | 'Once a month'
    | 'Once every 3 weeks'
    | 'Once every 2 weeks'
    | 'Once a week';
}) {
  let rrule: IRuleOptions;

  if (args.scheduleName === 'Once a month') {
    rrule = {
      start: args.start,
      end: args.end?.add(1, 'day') ?? undefined,
      frequency: 'MONTHLY',
      interval: 1,
    };
  } else {
    const interval =
      args.scheduleName === 'Once a week'
        ? 1
        : args.scheduleName === 'Once every 2 weeks'
        ? 2
        : args.scheduleName === 'Once every 3 weeks'
        ? 3
        : (() => {
            throw new Error(`Unexpected recurring ride request schedule name`);
          })();

    rrule = {
      start: args.start,
      end: args.end?.add(1, 'day') ?? undefined,
      frequency: 'WEEKLY',
      interval,
    };
  }

  return new Schedule({
    data: {
      name: args.scheduleName,
    },
    rrules: [rrule],
    timezone: 'America/Los_Angeles',
  });
}

export interface IPersonContact {
  createdAt: number; // utc milliseconds used because Firestore Timestamp not supported inside arrays
  updatedAt: number; // utc milliseconds used because Firestore Timestamp not supported inside arrays
  createdById: string;
  label: string;
  firstName: string;
  lastName: string;
  relationshipToPerson: string;
  phoneNumber: string;
  // address?: IAddress;
}

const PERSON_CONTACT_INPUT_DECODER = objectD(
  {
    createdAt: integerD(),
    updatedAt: integerD(),
    createdById: STRING_DECODER,
    label: STRING_DECODER,
    firstName: STRING_DECODER,
    lastName: STRING_DECODER,
    relationshipToPerson: STRING_DECODER,
    phoneNumber: STRING_DECODER,
    // address?: IAddress;
  },
  {
    removeUndefinedProperties: true,
  },
);

export class PersonContact {
  createdAt: Date;
  updatedAt: Date;
  createdById: string;
  createdBy?: Dispatcher;
  label: string;
  firstName: string;
  lastName: string;
  relationshipToPerson: string;
  phoneNumber: string;
  // address?: IAddress;

  constructor(params: IPersonContact) {
    Object.assign(this, params);

    if (params.createdAt) {
      this.createdAt = new Date(params.createdAt);
    }

    if (params.updatedAt) {
      this.updatedAt = new Date(params.updatedAt);
    }
  }

  get name() {
    return [this.firstName, this.lastName].join(' ');
  }
}

export interface INote {
  uid: string;
  createdAt: number;
  createdById: string;
  updatedAt: number;
  text: string;
}

export const NOTE_CREATE_DECODER = objectD(
  {
    uid: STRING_DECODER,
    createdAt: integerD(),
    createdById: STRING_DECODER,
    updatedAt: integerD(),
    text: STRING_DECODER,
  },
  {
    removeUndefinedProperties: true,
  },
);

export class Note {
  uid: string;
  createdAt: Date;
  createdAtText: string;
  createdById: string;
  updatedAt: Date;
  updatedAtText: string;
  text: string;

  // Assigned in NoteThread constructor
  noteThread: NoteThread;
  dispatcher: Dispatcher;

  constructor(params: INote) {
    Object.assign(this, params);

    if (params.createdAt) {
      this.createdAt = new Date(params.createdAt);
      this.createdAtText =
        subDays(new Date(), 7).valueOf() > this.createdAt.valueOf()
          ? format(this.createdAt, 'L/d/yy @ h:mmaaa')
          : formatDistanceToNow(this.createdAt, {
              addSuffix: true,
            });
    }

    if (params.updatedAt) {
      this.updatedAt = new Date(params.updatedAt);
      this.updatedAtText = formatDistanceToNow(this.updatedAt, {
        addSuffix: true,
      });
    }
  }

  get firstNoteInThread() {
    return this.noteThread!.notes[0].uid === this.uid;
  }
}

export interface INoteThread {
  uid: string;
  createdAt: Timestamp;
  updatedAt: Timestamp;
  noteAddedAt: Timestamp;
  clientId?: string;
  driverId?: string;
  rideRequestId?: string;
  flag: boolean;
  suppressTodo: boolean;
  notes: INote[];
}

export const NOTE_THREAD_CREATE_DECODER = objectD<FirestoreInput<INoteThread>>(
  {
    uid: STRING_DECODER,
    createdAt: TIMESTAMP_INPUT_DECODER,
    updatedAt: TIMESTAMP_INPUT_DECODER,
    noteAddedAt: TIMESTAMP_INPUT_DECODER,
    clientId: undefinableD(STRING_DECODER),
    driverId: undefinableD(STRING_DECODER),
    rideRequestId: undefinableD(STRING_DECODER),
    flag: booleanD(),
    suppressTodo: constantD(false),
    notes: arrayD(NOTE_CREATE_DECODER),
  },
  {
    removeUndefinedProperties: true,
  },
).chain((input) => {
  if (!(input.clientId || input.driverId || input.rideRequestId)) {
    return new DecoderError(
      input,
      'missing-prop',
      'Must have a clientId, driverId, and/or rideRequestId',
    );
  }

  return new DecoderSuccess(input);
});

export const NOTE_THREAD_UPDATE_DECODER = objectD(
  {
    updatedAt: TIMESTAMP_INPUT_DECODER,
    noteAddedAt: undefinableD(TIMESTAMP_INPUT_DECODER),
    flag: undefinableD(booleanD()),
    suppressTodo: undefinableD(booleanD()),
    notes: anyD(),
  },
  {
    removeUndefinedProperties: true,
  },
).chain((input) => {
  if (input.flag !== undefined && input.suppressTodo === undefined) {
    return new DecoderError(
      input,
      'missing-prop',
      '"suppressTodo" must be provided if "flag" is provided',
    );
  }

  if (input.suppressTodo !== undefined && input.flag === undefined) {
    return new DecoderError(
      input,
      'invalid-prop',
      '"suppressTodo" cannot be provided if "flag" is not provided',
    );
  }

  return new DecoderSuccess(input);
});

export class NoteThread {
  uid: string;
  createdAt: Date;
  updatedAt: Date;
  noteAddedAt: Date;
  clientId?: string;
  driverId?: string;
  rideRequestId?: string;
  flag: boolean;
  suppressTodo: boolean;
  notes: Note[] = [];

  client?: Client;
  driver?: Driver;
  rideRequest?: RideRequest;

  constructor(args: INoteThread) {
    Object.assign(this, args);

    if (args.createdAt) {
      this.createdAt = args.createdAt.toDate();
    }

    if (args.updatedAt) {
      this.updatedAt = args.updatedAt.toDate();
    }

    if (args.noteAddedAt) {
      this.noteAddedAt = args.noteAddedAt.toDate();
    }

    this.notes = args.notes.map((n) => {
      const note = new Note(n);
      note.noteThread = this;
      return note;
    });
  }
}

export function setTime(_date: Date, time: string) {
  const date = moment(new Date(_date));
  const [h, m] = time.split(':').map((nm) => parseInt(nm, 10));
  date.set('hours', h);
  date.set('minutes', m);
  return date.toDate();
}

export function setToStartOf(
  _date: Date,
  time: 'minute' | 'hour' | 'day' | 'week' | 'month',
) {
  return moment(new Date(_date)).startOf(time).toDate();
}

export function setToEndOf(
  _date: Date,
  time: 'minute' | 'hour' | 'day' | 'week' | 'month',
) {
  return moment(new Date(_date)).endOf(time).toDate();
}

export function convertDateToSameWallclockTimeInCaliZone(date: Date) {
  return moment(new Date(date)).tz('America/Los_Angeles', true).toDate();
}
