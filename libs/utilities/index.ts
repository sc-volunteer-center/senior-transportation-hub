import { RideRequest, Address, getAddressURLString } from '@local/models';
import { isAfter, isBefore } from 'date-fns';

export function indexToString(index: number) {
  switch (index) {
    case 0:
      return 'First';
    case 1:
      return 'Second';
    case 2:
      return 'Third';
    case 3:
      return 'Fouth';
    case 4:
      return 'Fifth';
    case 5:
      return 'Sixth';
    default:
      throw new Error('Unexpected indexToString() param');
  }
}

export function directionsMapLink(addresses: Address[]) {
  if (addresses.length < 2) {
    throw new Error('Must provide a origin and destination address');
  }

  return (
    'https://www.google.com/maps/dir/' +
    addresses.map((address) => getAddressURLString(address)).join('/')
  );
}

export function wait(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}

export async function generateRequestDetailText(
  requests: RideRequest[],
  buildTitleFn: (r: RideRequest, order: number) => Promise<string>,
) {
  requests = requests.slice().sort((a, b) => {
    if (isBefore(a.datetime, b.datetime)) {
      return -1;
    } else if (isAfter(a.datetime, b.datetime)) {
      return 1;
    } else {
      return 0;
    }
  });

  let index = 1;
  let email = ``;

  for (const request of requests) {
    const isGSP = request.category === 'GSP';

    email = email.concat(await buildTitleFn(request, index));

    if (request.destinationAddresses.length > 1) {
      email = email.concat(
        `* * ${request.destinationAddresses.length} destinations * *\n\n`,
      );
    }

    email = email.concat(`What is being asked:\n`);

    if (isGSP) {
      email = email.concat(
        `- For you to go grocery shopping on behalf of this client.\n`,
      );
    } else {
      email = email.concat(
        `- For you to give a ride to this client.\n`,
        `- Trip purpose: ${request.destinationAddresses
          .map((dest) => dest.tripPurpose)
          .join(' and ')}.\n`,
      );
    }

    if (request.flexible) {
      email = email.concat(`- Flexible timing: ${request.flexibleSchedule}.\n`);
    }

    email += `\n`;

    if (request.comments) {
      email = email.concat(`${request.comments}\n\n`);
    }

    email = email.concat(
      `Client:\n`,
      `- name: ${request.client.name}.\n`,
      `- phone number: ${request.client.phoneNumber}.\n`,
    );

    if (request.client.comments) {
      email = email.concat(`- notes: ${request.client.comments}.\n`);
    }

    email = email.concat('\n');

    const pickup = request.pickupAddress;

    email = email.concat(`Pickup:\n`, `- ${pickup.toString()}.\n`);

    // if (!request.flexible) {
    //   email = email.concat(
    //     `- appointment time (please confirm pick up time with client the day before): ${momentDatetime.format(
    //       'h:mma',
    //     )}\n`,
    //   );
    // }

    if (pickup.comments) {
      email = email.concat(`- notes: ${pickup.comments}.\n`);
    }

    email = email.concat('\n');

    request.destinationAddresses.forEach((address, i) => {
      if (request.destinationAddresses.length === 1) {
        email = email.concat(`Destination:\n`);
      } else {
        email = email.concat(`${indexToString(i)} destination:\n`);
      }

      email = email.concat(`- ${address.toString()}.\n`);

      if (request.destinationAddresses.length !== 1) {
        email = email.concat(`- purpose: ${address.tripPurpose}.\n`);
      }

      if (address.comments) {
        email = email.concat(`- notes: ${address.comments}.\n`);
      }

      email = email.concat('\n');
    });

    email = email.concat(
      `Google maps link:\n` +
        `${directionsMapLink([pickup, ...request.destinationAddresses])}\n\n` +
        `- - -\n\n\n`,
    );

    index++;
  }

  return email;
}
