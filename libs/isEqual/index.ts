import { isEqual as _isEqual } from 'lodash-es';

export const isEqual = _isEqual as <T>(a: T, b: T) => boolean;
