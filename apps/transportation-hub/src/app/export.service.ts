import { Injectable } from '@angular/core';
import { FirestoreService } from './firestore.service';
import {
  endOfMonth,
  startOfMonth,
  differenceInMinutes,
  format,
  getQuarter,
  isAfter,
  endOfQuarter,
  addMonths,
  isBefore,
  isEqual,
} from 'date-fns';
import { toPromise, isNonNullable, stringComparer } from './utilities';
import { Client, Driver, Address, RideRequest } from '@local/models';
import uniqBy from 'lodash-es/uniqBy';

import { utils, write, WorkBook } from 'xlsx';

// types are messed up, this is easiest.
const JSZip: any = require('jszip');

// interface QuarterlyStatistics {
//   uniqClients: Client[];
//   uniqDrivers: Driver[];
//   fulfilledDestinations: string[];
//   unFulfilledRequests: string[];
//   dispatcherVolunteerHours: number;
//   operationalVolunteers: number;
// }

// interface QuarterObject {
//   dates: Date[];
//   int: number[];
//   str: string[];
//   id: number;
// }

@Injectable({
  providedIn: 'root',
})
export class ExportService {
  constructor(private fs: FirestoreService) {}

  getFiscalYear(date: Date) {
    const fq = getFiscalQuarter(date);
    const currentYear = date.getFullYear();

    if (fq < 3) {
      return `${currentYear}-${currentYear + 1}`;
    } else {
      return `${currentYear - 1}-${currentYear}`;
    }
  }

  /**
   * `year` arg specifies the fiscal
   * year you wish to download info for. The value
   * has the form "YYYY-YYYY". e.g. "2019-2020"
   */
  async export(year: string) {
    const peopleWB = utils.book_new();
    const quarterlyReportWB = utils.book_new();
    const quarters = getFiscalQuartersForYear(year);
    const start = quarters[1];
    const end = endOfQuarter(quarters[4]);

    const [rideRequestsWB, dispatcherShiftsWB] = await Promise.all([
      this.rideRequests(start, end),
      this.dispatcherShifts(start, end),
      this.ytdReport(quarterlyReportWB, start, end),
      this.quarterlyReport(quarterlyReportWB, quarters[1]),
      this.quarterlyReport(quarterlyReportWB, quarters[2]),
      this.quarterlyReport(quarterlyReportWB, quarters[3]),
      this.quarterlyReport(quarterlyReportWB, quarters[4]),
      this.dispatchers(peopleWB, start, end),
      this.clients(peopleWB, start, end),
      this.drivers(peopleWB, start, end),
      this.tags(peopleWB),
    ]);

    const timestamp = format(new Date(), 'yyyyMdHHmmss');

    // Need to zip because firefox blocks multi-file downloads
    const zip = await new JSZip()
      .file(
        `${timestamp}-${year}-transportation-hub-quarterly-report.xlsx`,
        write(quarterlyReportWB, { type: 'array' }),
      )
      .file(
        `${timestamp}-${year}-transportation-hub-people.xlsx`,
        write(peopleWB, { type: 'array' }),
      )
      .file(
        `${timestamp}-${year}-transportation-hub-ride-requests.xlsx`,
        write(rideRequestsWB, { type: 'array' }),
      )
      .file(
        `${timestamp}-${year}-transportation-hub-dispatcher-shifts.xlsx`,
        write(dispatcherShiftsWB, { type: 'array' }),
      )
      .generateAsync({ type: 'blob' });

    downloadFile(zip, `${timestamp}-${year}-transportation-hub-export.zip`);
  }

  async rideRequests(start: Date, end: Date) {
    const allRideRequests = await this.allRideRequestDocs(start, end);

    const data = allRideRequests.reverse().map((rideRequests) =>
      rideRequests.map((rideRequest) => {
        const firstDest = rideRequest.destinationAddresses[0];
        const secondDest: Address | undefined =
          rideRequest.destinationAddresses[1];
        const thirdDest: Address | undefined =
          rideRequest.destinationAddresses[2];
        const fourthDest: Address | undefined =
          rideRequest.destinationAddresses[3];
        const fifthDest: Address | undefined =
          rideRequest.destinationAddresses[4];

        return {
          id: rideRequest.uid,
          clientId: rideRequest.clientId,
          createdAt: rideRequest.createdAt,
          createdByDispatcherId: rideRequest.requestTakenById,
          updatedAt: rideRequest.updatedAt,
          datetime: rideRequest.datetime,
          priority: rideRequest.priority,
          requestMadeBy: rideRequest.requestMadeBy,
          comments: rideRequest.comments,
          numberOfPeopleImpacted: rideRequest.peopleImpacted ?? 1,
          descriptionOfPeopleImpacted: rideRequest.peopleImpactedDescription,

          driverId: rideRequest.driverId,
          driverAssignedByDispatcherId: rideRequest.driverAssignedById,
          driverNotifiedAt: rideRequest.driverNotifiedAt,
          driverNotifiedByDispatcherId: rideRequest.driverNotifiedById,
          clientNotifiedOfDriverAt: rideRequest.clientNotifiedOfDriverAt,
          clientNotifiedOfDriverByDispatcherId:
            rideRequest.clientNotifiedOfDriverById,
          estimatedRideSeconds: rideRequest.estimatedRideSeconds,
          estimatedRideMeters: rideRequest.estimatedRideMeters,

          idOfDispatcherWhoCancelledRequest:
            rideRequest.idOfDispatcherWhoCancelledRequest,
          requestCancelledAt: rideRequest.requestCancelledAt,
          requestCancelledBy: rideRequest.requestCancelledBy,
          cancellationReason: rideRequest.cancellationReason,
          driverNotifiedOfCancellationAt:
            rideRequest.driverNotifiedOfCancellationAt,
          driverNotifiedOfCancellationByDispatcherId:
            rideRequest.driverNotifiedOfCancellationById,

          ['pickup address label']: rideRequest.pickupAddress.label,
          ['pickup address street']: rideRequest.pickupAddress.street,
          ['pickup address apartment / space number']:
            rideRequest.pickupAddress.apartmentNumber,
          ['pickup address city']: rideRequest.pickupAddress.city,
          ['pickup address zip']: rideRequest.pickupAddress.zip,
          ['pickup address comments']: rideRequest.pickupAddress.comments,
          ['pickup address latitude']: rideRequest.pickupAddress.latitude,
          ['pickup address longitude']: rideRequest.pickupAddress.longitude,
          totalDestinations: rideRequest.destinationAddresses.length,
          ['first destination address trip purpose']:
            firstDest && firstDest.tripPurpose,
          ['first destination address label']: firstDest && firstDest.label,
          ['first destination address street']: firstDest && firstDest.street,
          ['first destination address apartment / space number']:
            firstDest && firstDest.apartmentNumber,
          ['first destination address city']: firstDest && firstDest.city,
          ['first destination address zip']: firstDest && firstDest.zip,
          ['first destination address comments']:
            firstDest && firstDest.comments,
          ['first destination address latitude']:
            firstDest && firstDest.latitude,
          ['first destination address longitude']:
            firstDest && firstDest.longitude,
          ['second destination address trip purpose']:
            secondDest && secondDest.tripPurpose,
          ['second destination address label']: secondDest && secondDest.label,
          ['second destination address street']:
            secondDest && secondDest.street,
          ['second destination address apartment / space number']:
            secondDest && secondDest.apartmentNumber,
          ['second destination address city']: secondDest && secondDest.city,
          ['second destination address zip']: secondDest && secondDest.zip,
          ['second destination address comments']:
            secondDest && secondDest.comments,
          ['second destination address latitude']:
            secondDest && secondDest.latitude,
          ['second destination address longitude']:
            secondDest && secondDest.longitude,
          ['third destination address trip purpose']:
            thirdDest && thirdDest.tripPurpose,
          ['third destination address label']: thirdDest && thirdDest.label,
          ['third destination address street']: thirdDest && thirdDest.street,
          ['third destination address apartment / space number']:
            thirdDest && thirdDest.apartmentNumber,
          ['third destination address city']: thirdDest && thirdDest.city,
          ['third destination address zip']: thirdDest && thirdDest.zip,
          ['third destination address comments']:
            thirdDest && thirdDest.comments,
          ['third destination address latitude']:
            thirdDest && thirdDest.latitude,
          ['third destination address longitude']:
            thirdDest && thirdDest.longitude,
          ['fourth destination address trip purpose']:
            fourthDest && fourthDest.tripPurpose,
          ['fourth destination address label']: fourthDest && fourthDest.label,
          ['fourth destination address street']:
            fourthDest && fourthDest.street,
          ['fourth destination address apartment / space number']:
            fourthDest && fourthDest.apartmentNumber,
          ['fourth destination address city']: fourthDest && fourthDest.city,
          ['fourth destination address zip']: fourthDest && fourthDest.zip,
          ['fourth destination address comments']:
            fourthDest && fourthDest.comments,
          ['fourth destination address latitude']:
            fourthDest && fourthDest.latitude,
          ['fourth destination address longitude']:
            fourthDest && fourthDest.longitude,
          ['fifth destination address trip purpose']:
            fifthDest && fifthDest.tripPurpose,
          ['fifth destination address label']: fifthDest && fifthDest.label,
          ['fifth destination address street']: fifthDest && fifthDest.street,
          ['fifth destination address apartment / space number']:
            fifthDest && fifthDest.apartmentNumber,
          ['fifth destination address city']: fifthDest && fifthDest.city,
          ['fifth destination address zip']: fifthDest && fifthDest.zip,
          ['fifth destination address comments']:
            fifthDest && fifthDest.comments,
          ['fifth destination address latitude']:
            fifthDest && fifthDest.latitude,
          ['fifth destination address longitude']:
            fifthDest && fifthDest.longitude,
        };
      }),
    );

    const wb = utils.book_new();

    const months = this.getMonths(start, end, 'DESC');

    data.forEach((datum, index) => {
      const ws = utils.json_to_sheet(datum);

      utils.book_append_sheet(wb, ws, format(months[index], 'MMM yyyy'));
    });

    return wb;
  }

  async drivers(wb: WorkBook, start: Date, end: Date) {
    const allRideRequests = await this.allRideRequestDocs(start, end);

    const driverIds = Array.from(
      new Set(
        allRideRequests
          .reverse()
          .flatMap((requests) => requests.map((r) => r.driverId)),
      ),
    ).filter(isNonNullable);

    const drivers = (
      await Promise.all(driverIds.map((id) => toPromise(this.fs.getDriver(id))))
    )
      .filter(isNonNullable)
      .sort((a, b) => stringComparer(a.name, b.name));

    const data = await Promise.all(
      drivers.map(async (driver) => ({
        id: driver.uid,
        createdAt: driver.createdAt,
        createdByDispatcherId: driver.createdById,
        updatedAt: driver.updatedAt,
        retiredAt: driver.retiredAt,
        retiredByDispatcherId: driver.retiredById,
        firstName: driver.firstName,
        middleName: driver.middleName,
        lastName: driver.lastName,
        driversLicenseExpirationDate: driver.driversLicenseExpirationDate,
        carInsuranceExpirationDate: driver.carInsuranceExpirationDate,
        birthdate: driver.birthdate,
        genderIdentity: driver.genderIdentity,
        racialIdentity: driver.ethnicity,
        phoneNumber: driver.phoneNumber,
        emailAddress: driver.emailAddress,
        ['address label']: driver.address.label,
        ['address street']: driver.address.street,
        ['address apartment / space number']: driver.address.apartmentNumber,
        ['address city']: driver.address.city,
        ['address zip']: driver.address.zip,
        ['address comments']: driver.address.comments,
        ['address latitude']: driver.address.latitude,
        ['address longitude']: driver.address.longitude,
        comments: driver.comments,
        tagIds: driver.tagIds && driver.tagIds.join(', '),
        tags:
          driver.tagIds &&
          (
            await Promise.all(
              driver.tagIds.map((id) => toPromise(this.fs.getTag(id))),
            )
          )
            .filter(isNonNullable)
            .map((tag) => tag.label)
            .join(', '),
      })),
    );

    const ws = utils.json_to_sheet(data);

    utils.book_append_sheet(wb, ws, 'Drivers');

    return wb;
  }

  async clients(wb: WorkBook, start: Date, end: Date) {
    const allRideRequests = await this.allRideRequestDocs(start, end);

    const clientIds = Array.from(
      new Set(
        allRideRequests
          .reverse()
          .flatMap((requests) => requests.map((r) => r.clientId)),
      ),
    ).filter(isNonNullable);

    const clients = (
      await Promise.all(clientIds.map((id) => toPromise(this.fs.getClient(id))))
    )
      .filter(isNonNullable)
      .sort((a, b) => stringComparer(a.name, b.name));

    const clientData = await Promise.all(
      clients.map(async (client) => {
        const emergencyContact = client.contacts.filter(
          (c) => c.label.toLowerCase().trim() === 'emergency contact',
        )[0];

        return {
          id: client.uid,
          createdAt: client.createdAt,
          createdByDispatcherId: client.createdById,
          updatedAt: client.updatedAt,
          retiredAt: client.retiredAt,
          retiredByDispatcherId: client.retiredById,
          firstName: client.firstName,
          middleName: client.middleName,
          lastName: client.lastName,
          phoneNumber: client.phoneNumber,
          birthdate: client.birthdate,
          racialIdentity: client.ethnicity,
          authorizeSharingInfoWithDrivers:
            client.authorizeSharingInfoWithDrivers,
          mediCalOrSsi: client.mediCalOrSsi,
          ['address label']: client.address.label,
          ['address street']: client.address.street,
          ['address apartment / space number']: client.address.apartmentNumber,
          ['address city']: client.address.city,
          ['address zip']: client.address.zip,
          ['address comments']: client.address.comments,
          ['address latitude']: client.address.latitude,
          ['address longitude']: client.address.longitude,
          ['emergency contact first name']:
            emergencyContact && emergencyContact.firstName,
          ['emergency contact last name']:
            emergencyContact && emergencyContact.lastName,
          ['emergency contact relationship to client']:
            emergencyContact && emergencyContact.relationshipToPerson,
          ['emergency contact phone number']:
            emergencyContact && emergencyContact.phoneNumber,
          comments: client.comments,
          tagIds: client.tagIds && client.tagIds.join(', '),
          tags:
            client.tagIds &&
            (
              await Promise.all(
                client.tagIds.map((id) => toPromise(this.fs.getTag(id))),
              )
            )
              .filter(isNonNullable)
              .map((tag) => tag.label)
              .join(', '),
          clientContacts: client.contacts
            .map((c, i) => {
              return `[${i} - "${c.label}" - "${c.firstName}" - "${c.lastName}" - "${c.relationshipToPerson}" - "${c.phoneNumber}"]`;
            })
            .join(', '),
        };
      }),
    );

    const clientAnswersData: {
      clientId: string;
      questionId: string;
      answer: string | boolean | Date | null;
      updatedAt: Date;
    }[] = [];

    clients.forEach((client) => {
      client.answers.forEach((answer) => {
        clientAnswersData.push({
          clientId: client.uid,
          questionId: answer.questionId,
          answer: answer.answer,
          updatedAt: answer.updatedAt,
        });
      });
    });

    const [activeQuestions, retiredQuestions] = await Promise.all([
      toPromise(this.fs.getClientQuestions()),
      toPromise(this.fs.getClientQuestions({ retired: true })),
    ]);

    const clientQuestionsData = [...activeQuestions, ...retiredQuestions].map(
      (question) => ({
        id: question.uid,
        type: question.type,
        updatedAt: question.updatedAt,
        retiredAt: question.retiredAt,
        required: question.required,
        order: question.order,
        text: question.text,

        selectQuestionOptions: (question as any).options,
        selectQuestionAllowOther: (question as any).allowOther,

        textQuestionValueType: (question as any).valueType,

        sectionBreakBody: (question as any).body,
      }),
    );

    const clientWS = utils.json_to_sheet(clientData);
    const clientQuestionsWS = utils.json_to_sheet(clientQuestionsData);
    const clientAnswersWS = utils.json_to_sheet(clientAnswersData);

    utils.book_append_sheet(wb, clientWS, 'Clients');
    utils.book_append_sheet(wb, clientQuestionsWS, 'Client Questions');
    utils.book_append_sheet(wb, clientAnswersWS, 'Client Answers');

    return wb;
  }

  async tags(wb: WorkBook) {
    const tags = await toPromise(this.fs.getTags());

    const tagData = tags.map((tag) => ({
      id: tag.uid,
      createdAt: tag.createdAt,
      updatedAt: tag.updatedAt,
      type: tag.type,
      label: tag.label,
      description: tag.description,
      color: tag.color,
      favorite: tag.favorite,
    }));

    const tagsWS = utils.json_to_sheet(tagData);

    utils.book_append_sheet(wb, tagsWS, 'Tags');

    return wb;
  }

  async dispatcherShifts(start: Date, end: Date) {
    const months = this.getMonths(start, end, 'DESC');

    const allDispatcherShifts = await Promise.all(
      months.map((month) =>
        toPromise(
          this.fs.getDispatcherShifts({
            startDate: month,
            endDate: endOfMonth(month),
          }),
        ),
      ),
    );

    const data = allDispatcherShifts.map((dispatcherShifts) =>
      dispatcherShifts.map((shift) => ({
        id: shift.uid,
        dispatcherId: shift.dispatcherId,
        startAt: shift.startAt,
        endAt: shift.endAt,
      })),
    );

    const wb = utils.book_new();

    data.forEach((datum, index) => {
      const ws = utils.json_to_sheet(datum);

      utils.book_append_sheet(wb, ws, format(months[index], 'MMM yyyy'));
    });

    return wb;
  }

  async dispatchers(wb: WorkBook, start: Date, end: Date) {
    const months = this.getMonths(start, end, 'DESC');

    const dispatcherIds = Array.from(
      new Set(
        (
          await Promise.all(
            months.map((month) =>
              toPromise(
                this.fs.getDispatcherShifts({
                  startDate: month,
                  endDate: endOfMonth(month),
                }),
              ).then((ds) => ds.map((s) => s.dispatcherId)),
            ),
          )
        ).flat(),
      ),
    );

    const dispatchers = (
      await Promise.all(
        dispatcherIds.map((id) => toPromise(this.fs.getDispatcher(id))),
      )
    )
      .filter(isNonNullable)
      .sort((a, b) => stringComparer(a.name, b.name));

    const data = dispatchers.map((disp) => ({
      id: disp.uid,
      createdAt: disp.createdAt,
      updatedAt: disp.updatedAt,
      retiredAt: disp.retiredAt,
      organizationId: disp.organizationId,
      firstName: disp.firstName,
      middleName: disp.middleName,
      lastName: disp.lastName,
      phoneNumber: disp.phoneNumber,
      emailAddress: disp.emailAddress,
      notes: disp.notes,
    }));

    const ws = utils.json_to_sheet(data);

    utils.book_append_sheet(wb, ws, 'Dispatchers');

    return wb;
  }

  async quarterlyReport(workbook: WorkBook, targetQuarterStart: Date) {
    const now = new Date();
    const targetQuarterNumber = getFiscalQuarter(targetQuarterStart);
    const targetQuarterEnd = endOfQuarter(targetQuarterStart);

    if (isAfter(targetQuarterStart, now)) return workbook;

    const month1Start = targetQuarterStart;
    const month2Start = addMonths(targetQuarterStart, 1);
    const month3Start = addMonths(targetQuarterStart, 2);

    const data = await Promise.all([
      this.extractStatistics([targetQuarterStart, targetQuarterEnd]),
      this.extractStatistics([month1Start, endOfMonth(month1Start)]),
      this.extractStatistics([month2Start, endOfMonth(month2Start)]),
      this.extractStatistics([month3Start, endOfMonth(month3Start)]),
    ]);

    const [total, month1, month2, month3] = data.map((period) => {
      const totalClients = period.uniqClients.map((client) =>
        client.address.city.toLowerCase(),
      );
      const totalClientsLength = totalClients.length;

      const totalDrivers = period.uniqDrivers.map((driver) =>
        driver.address.city.toLowerCase(),
      );
      const totalDriversLength = totalDrivers.length;

      const santaCruzClients = totalClients.filter(
        (loc) => loc === 'santa cruz',
      ).length;

      const santaCruzDrivers = totalDrivers.filter(
        (loc) => loc === 'santa cruz',
      ).length;

      const scottsValleyClients = totalClients.filter(
        (loc) => loc === 'scotts valley',
      ).length;

      const scottsValleyDrivers = totalDrivers.filter(
        (loc) => loc === 'scotts valley',
      ).length;

      const capitolaClients = totalClients.filter((loc) => loc === 'capitola')
        .length;

      const capitolaDrivers = totalDrivers.filter((loc) => loc === 'capitola')
        .length;

      const watsonvilleClients = totalClients.filter(
        (loc) => loc === 'watsonville',
      ).length;

      const watsonvilleDrivers = totalDrivers.filter(
        (loc) => loc === 'watsonville',
      ).length;

      const sanLorenzoValleyCities = [
        'ben lomond',
        'felton',
        'brookdale',
        'boulder creek',
      ];

      const sanLorenzoValleyClients = totalClients.filter((loc) =>
        sanLorenzoValleyCities.includes(loc),
      ).length;

      const sanLorenzoValleyDrivers = totalDrivers.filter((loc) =>
        sanLorenzoValleyCities.includes(loc),
      ).length;

      const otherClients = totalClients.filter(
        (loc) =>
          ![
            'santa cruz',
            'scotts valley',
            'capitola',
            'watsonville',
            'ben lomond',
            'felton',
            'brookdale',
            'boulder creek',
          ].includes(loc),
      ).length;

      const otherDrivers = totalDrivers.filter(
        (loc) =>
          ![
            'santa cruz',
            'scotts valley',
            'capitola',
            'watsonville',
            'ben lomond',
            'felton',
            'brookdale',
            'boulder creek',
          ].includes(loc),
      ).length;

      const asianClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Asian',
      ).length;
      const africanAmericanClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'African American',
      ).length;
      const caucasianClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Caucasian',
      ).length;
      const latinoClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Latino',
      ).length;
      const nativeAmericanClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Native American',
      ).length;
      const pacificIslanderClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Pacific Islander',
      ).length;
      const declineToStateClients = period.uniqClients.filter(
        (client) => client.ethnicity === 'Decline to state',
      ).length;
      const otherEthnicityClients =
        totalClientsLength -
        asianClients -
        africanAmericanClients -
        caucasianClients -
        latinoClients -
        nativeAmericanClients -
        pacificIslanderClients -
        declineToStateClients;

      const asianDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Asian',
      ).length;
      const africanAmericanDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'African American',
      ).length;
      const caucasianDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Caucasian',
      ).length;
      const latinoDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Latino',
      ).length;
      const nativeAmericanDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Native American',
      ).length;
      const pacificIslanderDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Pacific Islander',
      ).length;
      const declineToStateDrivers = period.uniqDrivers.filter(
        (driver) => driver.ethnicity === 'Decline to state',
      ).length;
      const otherEthnicityDrivers =
        totalDriversLength -
        asianDrivers -
        africanAmericanDrivers -
        caucasianDrivers -
        latinoDrivers -
        nativeAmericanDrivers -
        pacificIslanderDrivers -
        declineToStateDrivers;

      const destDoctor = period.fulfilledRideRequests.filter(
        (purpose) => purpose === 'Doctor',
      ).length;
      const destShopping = period.fulfilledRideRequests.filter(
        (purpose) => purpose === 'Shopping',
      ).length;
      const destGoingHome = period.fulfilledRideRequests.filter(
        (purpose) => purpose === 'Going home',
      ).length;
      const destOther = period.fulfilledRideRequests.filter(
        (purpose) => !['Doctor', 'Shopping', 'Going home'].includes(purpose),
      ).length;
      const destTotal = period.fulfilledRideRequests.length;
      const unFulfilledRequestNoDriver = period.unfulfilledRideRequests.filter(
        (reason) => reason === 'No driver',
      ).length;
      const unFulfilledRequestNonVital = period.unfulfilledRideRequests.filter(
        (reason) => reason === 'Non-vital request',
      ).length;
      const unFulfilledRequestTotal = period.unfulfilledRideRequests.filter(
        (reason) => reason !== 'Cancelled',
      ).length;
      const requestsCancelled = period.unfulfilledRideRequests.filter(
        (reason) => reason === 'Cancelled',
      ).length;

      return {
        totalClientsLength,
        totalDriversLength,
        santaCruzClients,
        santaCruzDrivers,
        scottsValleyClients,
        scottsValleyDrivers,
        capitolaClients,
        capitolaDrivers,
        watsonvilleClients,
        watsonvilleDrivers,
        sanLorenzoValleyClients,
        sanLorenzoValleyDrivers,
        otherClients,
        otherDrivers,
        asianClients,
        africanAmericanClients,
        caucasianClients,
        latinoClients,
        nativeAmericanClients,
        pacificIslanderClients,
        declineToStateClients,
        otherEthnicityClients,
        asianDrivers,
        africanAmericanDrivers,
        caucasianDrivers,
        latinoDrivers,
        nativeAmericanDrivers,
        pacificIslanderDrivers,
        declineToStateDrivers,
        otherEthnicityDrivers,
        destDoctor,
        destShopping,
        destGoingHome,
        destOther,
        destTotal,
        unFulfilledRequestNoDriver,
        unFulfilledRequestNonVital,
        unFulfilledRequestTotal,
        requestsCancelled,
        operationalVolunteers: period.operationalVolunteers,
        totalVolunteers: period.operationalVolunteers + totalDriversLength,
        operationalVolunteerHours: period.dispatcherVolunteerHours,
        driverMilesDriven: period.driverMilesDriven,
        totalRideRequests:
          period.fulfilledRideRequests.length +
          period.unfulfilledRideRequests.length,
        totalFulfilledRideRequests: period.fulfilledRideRequests.length,
        fulfilledRideRequestsPeopleImpacted:
          period.fulfilledRideRequestsPeopleImpacted,
        unfulfilledRideRequestsPeopleImpacted:
          period.unfulfilledRideRequestsPeopleImpacted,
      };
    });

    const worksheetData = [
      [`Quarter ${targetQuarterNumber} ride request report`],
      [`FY ${this.getFiscalYear(targetQuarterStart)}`],
      ['TransportationSC'],
      [`Report generated on ${format(new Date(), 'M/d/yyyy')}`],
      [],
      [
        `Quarter ${targetQuarterNumber}`,
        format(month1Start, 'MMMM'),
        format(month2Start, 'MMMM'),
        format(month3Start, 'MMMM'),
        'Total',
      ],
      [],
      [],
      ['Fulfilled rides'],
      [
        'Doctors',
        month1.destDoctor,
        month2.destDoctor,
        month3.destDoctor,
        total.destDoctor,
      ],
      [
        'Shopping',
        month1.destShopping,
        month2.destShopping,
        month3.destShopping,
        total.destShopping,
      ],
      [
        'Going home',
        month1.destGoingHome,
        month2.destGoingHome,
        month3.destGoingHome,
        total.destGoingHome,
      ],
      [
        'Other',
        month1.destOther,
        month2.destOther,
        month3.destOther,
        total.destOther,
      ],
      [
        'Total',
        month1.destTotal,
        month2.destTotal,
        month3.destTotal,
        total.destTotal,
      ],
      [],
      ['Unfulfilled rides'],
      [
        'No driver',
        month1.unFulfilledRequestNoDriver,
        month2.unFulfilledRequestNoDriver,
        month3.unFulfilledRequestNoDriver,
        total.unFulfilledRequestNoDriver,
      ],
      [
        'Non-vital request',
        month1.unFulfilledRequestNonVital,
        month2.unFulfilledRequestNonVital,
        month3.unFulfilledRequestNonVital,
        total.unFulfilledRequestNonVital,
      ],
      [
        'Total',
        month1.unFulfilledRequestTotal,
        month2.unFulfilledRequestTotal,
        month3.unFulfilledRequestTotal,
        total.unFulfilledRequestTotal,
      ],
      [],
      [
        'Requests cancelled by client',
        month1.requestsCancelled,
        month2.requestsCancelled,
        month3.requestsCancelled,
        total.requestsCancelled,
      ],
      [],
      [
        'Percentage of rides fulfilled',
        getPercentage(
          month1.totalRideRequests,
          month1.totalFulfilledRideRequests,
        ),
        getPercentage(
          month2.totalRideRequests,
          month2.totalFulfilledRideRequests,
        ),
        getPercentage(
          month3.totalRideRequests,
          month3.totalFulfilledRideRequests,
        ),
        getPercentage(
          total.totalRideRequests,
          total.totalFulfilledRideRequests,
        ),
      ],
      [
        'Total requested rides',
        month1.totalRideRequests,
        month2.totalRideRequests,
        month3.totalRideRequests,
        total.totalRideRequests,
      ],
      [],
      ['Clients by Jurisdiction'],
      [
        'Santa Cruz City',
        month1.santaCruzClients,
        month2.santaCruzClients,
        month3.santaCruzClients,
        total.santaCruzClients,
      ],
      [
        'Scotts Valley City',
        month1.scottsValleyClients,
        month2.scottsValleyClients,
        month3.scottsValleyClients,
        total.scottsValleyClients,
      ],
      [
        'San Lorenzo Valley',
        month1.sanLorenzoValleyClients,
        month2.sanLorenzoValleyClients,
        month3.sanLorenzoValleyClients,
        total.sanLorenzoValleyClients,
      ],
      [
        'Capitola City',
        month1.capitolaClients,
        month2.capitolaClients,
        month3.capitolaClients,
        total.capitolaClients,
      ],
      [
        'Watsonville City',
        month1.watsonvilleClients,
        month2.watsonvilleClients,
        month3.watsonvilleClients,
        total.watsonvilleClients,
      ],
      ['MidCounty Uninc.'],
      ['South County'],
      ['North County'],
      ['Out of County'],
      [
        'Other',
        month1.otherClients,
        month2.otherClients,
        month3.otherClients,
        total.otherClients,
      ],
      [
        'Total',
        month1.totalClientsLength,
        month2.totalClientsLength,
        month3.totalClientsLength,
        total.totalClientsLength,
      ],
      [],
      ['Clients by Ethnicity'],
      [
        'Asian',
        month1.asianClients,
        month2.asianClients,
        month3.asianClients,
        total.asianClients,
      ],
      [
        'African American',
        month1.africanAmericanClients,
        month2.africanAmericanClients,
        month3.africanAmericanClients,
        total.africanAmericanClients,
      ],
      [
        'Caucasian',
        month1.caucasianClients,
        month2.caucasianClients,
        month3.caucasianClients,
        total.caucasianClients,
      ],
      [
        'Latino',
        month1.latinoClients,
        month2.latinoClients,
        month3.latinoClients,
        total.latinoClients,
      ],
      [
        'Native American',
        month1.nativeAmericanClients,
        month2.nativeAmericanClients,
        month3.nativeAmericanClients,
        total.nativeAmericanClients,
      ],
      [
        'Pacific Islander',
        month1.pacificIslanderClients,
        month2.pacificIslanderClients,
        month3.pacificIslanderClients,
        total.pacificIslanderClients,
      ],
      [
        'Other',
        month1.otherEthnicityClients,
        month2.otherEthnicityClients,
        month3.otherEthnicityClients,
        total.otherEthnicityClients,
      ],
      [
        'Declined to State',
        month1.declineToStateClients,
        month2.declineToStateClients,
        month3.declineToStateClients,
        total.declineToStateClients,
      ],
      [
        'Total',
        month1.totalClientsLength,
        month2.totalClientsLength,
        month3.totalClientsLength,
        total.totalClientsLength,
      ],
      [],
      ['Volunteers (drivers) by Jurisdiction'],
      [
        'Santa Cruz City',
        month1.santaCruzDrivers,
        month2.santaCruzDrivers,
        month3.santaCruzDrivers,
        total.santaCruzDrivers,
      ],
      [
        'Scotts Valley City',
        month1.scottsValleyDrivers,
        month2.scottsValleyDrivers,
        month3.scottsValleyDrivers,
        total.scottsValleyDrivers,
      ],
      [
        'San Lorenzo Valley',
        month1.sanLorenzoValleyDrivers,
        month2.sanLorenzoValleyDrivers,
        month3.sanLorenzoValleyDrivers,
        total.sanLorenzoValleyDrivers,
      ],
      [
        'Capitola City',
        month1.capitolaDrivers,
        month2.capitolaDrivers,
        month3.capitolaDrivers,
        total.capitolaDrivers,
      ],
      [
        'Watsonville City',
        month1.watsonvilleDrivers,
        month2.watsonvilleDrivers,
        month3.watsonvilleDrivers,
        total.watsonvilleDrivers,
      ],
      ['MidCounty Uninc.'],
      ['South County'],
      ['North County'],
      ['Out of County'],
      [
        'Other',
        month1.otherDrivers,
        month2.otherDrivers,
        month3.otherDrivers,
        total.otherDrivers,
      ],
      [
        'Total',
        month1.totalDriversLength,
        month2.totalDriversLength,
        month3.totalDriversLength,
        total.totalDriversLength,
      ],
      [],
      ['Volunteers (drivers) by Ethnicity'],
      [
        'Asian',
        month1.asianDrivers,
        month2.asianDrivers,
        month3.asianDrivers,
        total.asianDrivers,
      ],
      [
        'African American',
        month1.africanAmericanDrivers,
        month2.africanAmericanDrivers,
        month3.africanAmericanDrivers,
        total.africanAmericanDrivers,
      ],
      [
        'Caucasian',
        month1.caucasianDrivers,
        month2.caucasianDrivers,
        month3.caucasianDrivers,
        total.caucasianDrivers,
      ],
      [
        'Latino',
        month1.latinoDrivers,
        month2.latinoDrivers,
        month3.latinoDrivers,
        total.latinoDrivers,
      ],
      [
        'Native American',
        month1.nativeAmericanDrivers,
        month2.nativeAmericanDrivers,
        month3.nativeAmericanDrivers,
        total.nativeAmericanDrivers,
      ],
      [
        'Pacific Islander',
        month1.pacificIslanderDrivers,
        month2.pacificIslanderDrivers,
        month3.pacificIslanderDrivers,
        total.pacificIslanderDrivers,
      ],
      [
        'Other',
        month1.otherEthnicityDrivers,
        month2.otherEthnicityDrivers,
        month3.otherEthnicityDrivers,
        total.otherEthnicityDrivers,
      ],
      [
        'Declined to State',
        month1.declineToStateDrivers,
        month2.declineToStateDrivers,
        month3.declineToStateDrivers,
        total.declineToStateDrivers,
      ],
      [
        'Total',
        month1.totalDriversLength,
        month2.totalDriversLength,
        month3.totalDriversLength,
        total.totalDriversLength,
      ],
      [],
      ['Volunteers by role'],
      [
        'Operational/support volunteers',
        month1.operationalVolunteers,
        month2.operationalVolunteers,
        month3.operationalVolunteers,
        total.operationalVolunteers,
      ],
      [
        'Volunteer drivers',
        month1.totalDriversLength,
        month2.totalDriversLength,
        month3.totalDriversLength,
        total.totalDriversLength,
      ],
      [
        'Total',
        month1.totalVolunteers,
        month2.totalVolunteers,
        month3.totalVolunteers,
        total.totalVolunteers,
      ],
      [],
      ['Volunteer hours'],
      [
        'Operational/support volunteers',
        month1.operationalVolunteerHours,
        month2.operationalVolunteerHours,
        month3.operationalVolunteerHours,
        total.operationalVolunteerHours,
      ],
      ['Volunteer drivers', '', '', '', ''],
      ['Total'],
      [],
      [
        'Volunteer driver miles driven',
        month1.driverMilesDriven,
        month2.driverMilesDriven,
        month3.driverMilesDriven,
        total.driverMilesDriven,
      ],
      [
        'Non-unique people impacted by fulfilled requests',
        month1.fulfilledRideRequestsPeopleImpacted,
        month2.fulfilledRideRequestsPeopleImpacted,
        month3.fulfilledRideRequestsPeopleImpacted,
        total.fulfilledRideRequestsPeopleImpacted,
      ],
      [
        'Non-unique people impacted by unfulfilled requests',
        month1.unfulfilledRideRequestsPeopleImpacted,
        month2.unfulfilledRideRequestsPeopleImpacted,
        month3.unfulfilledRideRequestsPeopleImpacted,
        total.unfulfilledRideRequestsPeopleImpacted,
      ],
      [],
    ];

    const worksheet = utils.aoa_to_sheet(worksheetData);

    const sheetName = `Q${targetQuarterNumber} ${format(
      targetQuarterStart,
      'MMM',
    )}-${format(targetQuarterEnd, 'MMM')} ${format(targetQuarterEnd, 'yyyy')}`;

    utils.book_append_sheet(workbook, worksheet, sheetName);

    return workbook;
  }

  async ytdReport(workbook: WorkBook, startDate: Date, endDate: Date) {
    const period = await this.extractStatistics([startDate, endDate]);

    const totalClients = period.uniqClients.map((client) =>
      client.address.city.toLowerCase(),
    );
    const totalClientsLength = totalClients.length;

    const totalDrivers = period.uniqDrivers.map((driver) =>
      driver.address.city.toLowerCase(),
    );
    const totalDriversLength = totalDrivers.length;

    const santaCruzClients = totalClients.filter((loc) => loc === 'santa cruz')
      .length;

    const santaCruzDrivers = totalDrivers.filter((loc) => loc === 'santa cruz')
      .length;

    const scottsValleyClients = totalClients.filter(
      (loc) => loc === 'scotts valley',
    ).length;

    const scottsValleyDrivers = totalDrivers.filter(
      (loc) => loc === 'scotts valley',
    ).length;

    const capitolaClients = totalClients.filter((loc) => loc === 'capitola')
      .length;

    const capitolaDrivers = totalDrivers.filter((loc) => loc === 'capitola')
      .length;

    const watsonvilleClients = totalClients.filter(
      (loc) => loc === 'watsonville',
    ).length;

    const watsonvilleDrivers = totalDrivers.filter(
      (loc) => loc === 'watsonville',
    ).length;

    const sanLorenzoValleyCities = [
      'ben lomond',
      'felton',
      'brookdale',
      'boulder creek',
    ];

    const sanLorenzoValleyClients = totalClients.filter((loc) =>
      sanLorenzoValleyCities.includes(loc),
    ).length;

    const sanLorenzoValleyDrivers = totalDrivers.filter((loc) =>
      sanLorenzoValleyCities.includes(loc),
    ).length;

    const otherClients = totalClients.filter(
      (loc) =>
        ![
          'santa cruz',
          'scotts valley',
          'capitola',
          'watsonville',
          'ben lomond',
          'felton',
          'brookdale',
          'boulder creek',
        ].includes(loc),
    ).length;

    const otherDrivers = totalDrivers.filter(
      (loc) =>
        ![
          'santa cruz',
          'scotts valley',
          'capitola',
          'watsonville',
          'ben lomond',
          'felton',
          'brookdale',
          'boulder creek',
        ].includes(loc),
    ).length;

    const asianClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Asian',
    ).length;
    const africanAmericanClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'African American',
    ).length;
    const caucasianClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Caucasian',
    ).length;
    const latinoClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Latino',
    ).length;
    const nativeAmericanClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Native American',
    ).length;
    const pacificIslanderClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Pacific Islander',
    ).length;
    const declineToStateClients = period.uniqClients.filter(
      (client) => client.ethnicity === 'Decline to state',
    ).length;
    const otherEthnicityClients =
      totalClientsLength -
      asianClients -
      africanAmericanClients -
      caucasianClients -
      nativeAmericanClients -
      pacificIslanderClients -
      declineToStateClients;

    const destDoctor = period.fulfilledRideRequests.filter(
      (purpose) => purpose === 'Doctor',
    ).length;
    const destShopping = period.fulfilledRideRequests.filter(
      (purpose) => purpose === 'Shopping',
    ).length;
    const destGoingHome = period.fulfilledRideRequests.filter(
      (purpose) => purpose === 'Going home',
    ).length;
    const destOther = period.fulfilledRideRequests.filter(
      (purpose) => !['Doctor', 'Shopping', 'Going home'].includes(purpose),
    ).length;
    const destTotal = period.fulfilledRideRequests.length;
    const unFulfilledRequestNoDriver = period.unfulfilledRideRequests.filter(
      (reason) => reason === 'No driver',
    ).length;
    const unFulfilledRequestNonVital = period.unfulfilledRideRequests.filter(
      (reason) => reason === 'Non-vital request',
    ).length;
    const unFulfilledRequestTotal = period.unfulfilledRideRequests.filter(
      (reason) => reason !== 'Cancelled',
    ).length;
    const requestsCancelled = period.unfulfilledRideRequests.filter(
      (reason) => reason === 'Cancelled',
    ).length;

    const data = {
      totalClientsLength,
      totalDriversLength,
      santaCruzClients,
      santaCruzDrivers,
      scottsValleyClients,
      scottsValleyDrivers,
      capitolaClients,
      capitolaDrivers,
      watsonvilleClients,
      watsonvilleDrivers,
      sanLorenzoValleyClients,
      sanLorenzoValleyDrivers,
      otherClients,
      otherDrivers,
      asianClients,
      africanAmericanClients,
      caucasianClients,
      latinoClients,
      nativeAmericanClients,
      pacificIslanderClients,
      declineToStateClients,
      otherEthnicityClients,
      destDoctor,
      destShopping,
      destGoingHome,
      destOther,
      destTotal,
      unFulfilledRequestNoDriver,
      unFulfilledRequestNonVital,
      unFulfilledRequestTotal,
      requestsCancelled,
      operationalVolunteers: period.operationalVolunteers,
      totalVolunteers: period.operationalVolunteers + totalDriversLength,
      operationalVolunteerHours: period.dispatcherVolunteerHours,
      driverMilesDriven: period.driverMilesDriven,
      totalRideRequests:
        period.fulfilledRideRequests.length +
        period.unfulfilledRideRequests.length,
      totalFulfilledRideRequests: period.fulfilledRideRequests.length,
      fulfilledRideRequestsPeopleImpacted:
        period.fulfilledRideRequestsPeopleImpacted,
      unfulfilledRideRequestsPeopleImpacted:
        period.unfulfilledRideRequestsPeopleImpacted,
    };

    const worksheetData = [
      ['Year to date ride request report'],
      [`FY ${startDate.getFullYear()}-${startDate.getFullYear() + 1}`],
      ['TransportationSC'],
      [`Report generated on ${format(new Date(), 'M/d/yyyy')}`],
      [],
      [`YTD`, 'Total', 'Percent of Total'],
      [],
      [],
      ['Fulfilled rides'],
      [
        'Doctors',
        data.destDoctor,
        getPercentage(data.destTotal, data.destDoctor),
      ],
      [
        'Shopping',
        data.destShopping,
        getPercentage(data.destTotal, data.destShopping),
      ],
      [
        'Going home',
        data.destGoingHome,
        getPercentage(data.destTotal, data.destGoingHome),
      ],
      ['Other', data.destOther, getPercentage(data.destTotal, data.destOther)],
      ['Total', data.destTotal, getPercentage(data.destTotal, data.destTotal)],
      [],
      ['Unfulfilled rides'],
      [
        'No driver',
        data.unFulfilledRequestNoDriver,
        getPercentage(
          data.unFulfilledRequestTotal,
          data.unFulfilledRequestNoDriver,
        ),
      ],
      [
        'Non-vital request',
        data.unFulfilledRequestNonVital,
        getPercentage(
          data.unFulfilledRequestTotal,
          data.unFulfilledRequestNonVital,
        ),
      ],
      [
        'Total',
        data.unFulfilledRequestTotal,
        getPercentage(
          data.unFulfilledRequestTotal,
          data.unFulfilledRequestTotal,
        ),
      ],
      [],
      [
        'Requests cancelled by client',
        data.requestsCancelled,
        getPercentage(data.totalRideRequests, data.requestsCancelled),
      ],
      [],
      [
        'Fulfilled rides',
        data.totalFulfilledRideRequests,
        getPercentage(data.totalRideRequests, data.totalFulfilledRideRequests),
      ],
      [
        'Total requested rides',
        data.totalRideRequests,
        getPercentage(data.totalRideRequests, data.totalRideRequests),
      ],
      [],
      ['Clients by Jurisdiction'],
      [
        'Santa Cruz City',
        data.santaCruzClients,
        getPercentage(data.totalClientsLength, data.santaCruzClients),
      ],
      [
        'Scotts Valley City',
        data.scottsValleyClients,
        getPercentage(data.totalClientsLength, data.scottsValleyClients),
      ],
      [
        'San Lorenzo Valley',
        data.sanLorenzoValleyClients,
        getPercentage(data.totalClientsLength, data.sanLorenzoValleyClients),
      ],
      [
        'Capitola City',
        data.capitolaClients,
        getPercentage(data.totalClientsLength, data.capitolaClients),
      ],
      [
        'Watsonville City',
        data.watsonvilleClients,
        getPercentage(data.totalClientsLength, data.watsonvilleClients),
      ],
      ['MidCounty Uninc.'],
      ['South County'],
      ['North County'],
      ['Out of County'],
      [
        'Other',
        data.otherClients,
        getPercentage(data.totalClientsLength, data.otherClients),
      ],
      [
        'Total',
        data.totalClientsLength,
        getPercentage(data.totalClientsLength, data.totalClientsLength),
      ],
      [],
      ['Clients by Ethnicity'],
      [
        'Asian',
        data.asianClients,
        getPercentage(data.totalClientsLength, data.asianClients),
      ],
      [
        'African American',
        data.africanAmericanClients,
        getPercentage(data.totalClientsLength, data.africanAmericanClients),
      ],
      [
        'Caucasian',
        data.caucasianClients,
        getPercentage(data.totalClientsLength, data.caucasianClients),
      ],
      [
        'Latino',
        data.latinoClients,
        getPercentage(data.totalClientsLength, data.latinoClients),
      ],
      [
        'Native American',
        data.nativeAmericanClients,
        getPercentage(data.totalClientsLength, data.nativeAmericanClients),
      ],
      [
        'Pacific Islander',
        data.pacificIslanderClients,
        getPercentage(data.totalClientsLength, data.pacificIslanderClients),
      ],
      [
        'Other',
        data.otherEthnicityClients,
        getPercentage(data.totalClientsLength, data.otherEthnicityClients),
      ],
      [
        'Declined to State',
        data.declineToStateClients,
        getPercentage(data.totalClientsLength, data.declineToStateClients),
      ],
      [
        'Total',
        data.totalClientsLength,
        getPercentage(data.totalClientsLength, data.totalClientsLength),
      ],
      [],
      ['Volunteers (drivers) by Jurisdiction'],
      [
        'Santa Cruz City',
        data.santaCruzDrivers,
        getPercentage(data.totalDriversLength, data.santaCruzDrivers),
      ],
      [
        'Scotts Valley City',
        data.scottsValleyDrivers,
        getPercentage(data.totalDriversLength, data.scottsValleyDrivers),
      ],
      [
        'San Lorenzo Valley',
        data.sanLorenzoValleyDrivers,
        getPercentage(data.totalDriversLength, data.sanLorenzoValleyDrivers),
      ],
      [
        'Capitola City',
        data.capitolaDrivers,
        getPercentage(data.totalDriversLength, data.capitolaDrivers),
      ],
      [
        'Watsonville City',
        data.watsonvilleDrivers,
        getPercentage(data.totalDriversLength, data.watsonvilleDrivers),
      ],
      ['MidCounty Uninc.'],
      ['South County'],
      ['North County'],
      ['Out of County'],
      [
        'Other',
        data.otherDrivers,
        getPercentage(data.totalDriversLength, data.otherDrivers),
      ],
      [
        'Total',
        data.totalDriversLength,
        getPercentage(data.totalDriversLength, data.totalDriversLength),
      ],
      [],
      ['Volunteers (drivers) by Ethnicity'],
      ['Asian', '', '', '', ''],
      ['African American', '', '', '', ''],
      ['Caucasian', '', '', '', ''],
      ['Latino', '', '', '', ''],
      ['Native American', '', '', '', ''],
      ['Pacific Islander', '', '', '', ''],
      ['Other', '', '', '', ''],
      ['Declined to State', '', '', '', ''],
      ['Unknown', '', '', '', ''],
      ['Total', '', '', '', ''],
      [],
      ['Volunteers by role'],
      [
        'Operational/support volunteers',
        data.operationalVolunteers,
        getPercentage(data.totalVolunteers, data.operationalVolunteers),
      ],
      [
        'Volunteer drivers',
        data.totalDriversLength,
        getPercentage(data.totalVolunteers, data.totalDriversLength),
      ],
      [
        'Total',
        data.totalVolunteers,
        getPercentage(data.totalVolunteers, data.totalVolunteers),
      ],
      [],
      ['Volunteer hours'],
      ['Operational/support volunteers', data.operationalVolunteerHours],
      ['Volunteer drivers', '', '', '', ''],
      ['Total'],
      [],
      ['Volunteer driver miles driven', data.driverMilesDriven],
      [
        'Non-unique people impacted by fulfilled requests',
        data.fulfilledRideRequestsPeopleImpacted,
      ],
      [
        'Non-unique people impacted by unfulfilled requests',
        data.unfulfilledRideRequestsPeopleImpacted,
      ],
      [],
    ];

    const worksheet = utils.aoa_to_sheet(worksheetData);

    const sheetName = `YTD FY ${startDate.getFullYear()}-${
      startDate.getFullYear() + 1
    }`;

    utils.book_append_sheet(workbook, worksheet, sheetName);

    return workbook;
  }

  private async extractStatistics([startDate, endDate]: [Date, Date]) {
    const [rideRequests, dispatcherShifts] = await Promise.all([
      this.allRideRequestDocs(startDate, endDate).then((rr) =>
        rr.reverse().flat(),
      ),
      toPromise(
        this.fs.getDispatcherShifts({
          startDate,
          endDate,
        }),
      ),
    ]);

    if (rideRequests.length > 0) {
      await Promise.all(
        rideRequests.map(async (request) => {
          if (!request.driverId) return null;

          request.driver = await toPromise(this.fs.getDriver(request.driverId));

          return request;
        }),
      );
    }

    // For reporting purposes, a "ride request" is pickup to
    // destination. Destination to pickup is another "ride request."
    // Destination to another destination is another "ride request."

    const fulfilledRideRequests: string[] = [];
    let fulfilledRideRequestsPeopleImpacted = 0;
    /** Total rides = total fulfilled RR destinations + 1 */
    // let numberOfFulfilledRides = 0;
    const unfulfilledRideRequests: string[] = [];
    let unfulfilledRideRequestsPeopleImpacted = 0;
    let driverMetersDriven = 0;
    /** Total rides = total RR destinations + 1 */
    // let numberOfRideRequests = 0;

    rideRequests.forEach((request) => {
      if (request.requestCancelledAt) {
        unfulfilledRideRequests.push(
          'Cancelled',
          ...request.destinationAddresses.map(() => `Cancelled`),
        );
        unfulfilledRideRequestsPeopleImpacted += request.peopleImpacted || 1;
      } else if (request.driverId) {
        request.destinationAddresses.forEach((address) => {
          fulfilledRideRequests.push(address.tripPurpose!);
        });

        fulfilledRideRequests.push('Going home');
        driverMetersDriven += request.estimatedRideMeters!;
        fulfilledRideRequestsPeopleImpacted += request.peopleImpacted || 1;
      } else if (request.priority !== 'high') {
        unfulfilledRideRequests.push(
          'Non-vital request',
          ...request.destinationAddresses.map(() => `Non-vital request`),
        );
        unfulfilledRideRequestsPeopleImpacted += request.peopleImpacted || 1;
      } else {
        unfulfilledRideRequests.push(
          'No driver',
          ...request.destinationAddresses.map(() => `No driver`),
        );
        unfulfilledRideRequestsPeopleImpacted += request.peopleImpacted || 1;
      }
    });

    const uniqClients: Client[] = uniqBy(
      rideRequests.map((req) => req.client),
      'uid',
    );
    const uniqDrivers: Driver[] = uniqBy(
      rideRequests
        .map((req) => req.driver)
        .filter((driv) => !!driv) as Driver[],
      'uid',
    );
    const uniqDispatcherIds = new Set(
      dispatcherShifts.map((shift) => shift.dispatcherId),
    );

    const operationalVolunteers = uniqDispatcherIds.size;

    let dispatcherVolunteerMinutes = 0;

    dispatcherShifts.forEach((shift) => {
      if (shift.endAt) {
        dispatcherVolunteerMinutes += differenceInMinutes(
          shift.endAt,
          shift.startAt,
        );
      } else {
        dispatcherVolunteerMinutes += 4 * 60;
      }
    });

    const dispatcherVolunteerHours = dispatcherVolunteerMinutes / 60;

    return {
      uniqClients,
      uniqDrivers,
      fulfilledRideRequests,
      unfulfilledRideRequests,
      dispatcherVolunteerHours,
      operationalVolunteers,
      driverMilesDriven: driverMetersDriven * 0.0006213712,
      fulfilledRideRequestsPeopleImpacted,
      unfulfilledRideRequestsPeopleImpacted,
      // totalRideRequests: numberOfRideRequests,
      // totalFulfilledRideRequests: numberOfFulfilledRides,
    };
  }

  /**
   * Generates an array of dates where one da
   */
  getMonths(
    inclusiveStart: Date,
    inclusiveEnd: Date,
    direction: 'ASC' | 'DESC' = 'ASC',
  ) {
    const end = startOfMonth(inclusiveEnd);
    const months: Date[] = [];

    for (
      let currentMonth = startOfMonth(inclusiveStart);
      isBefore(currentMonth, end) || isEqual(currentMonth, end);
      currentMonth = addMonths(currentMonth, 1)
    ) {
      months.push(currentMonth);
    }

    return direction === 'ASC' ? months : months.reverse();
  }

  numberOfRides(request: RideRequest) {
    return request.destinationAddresses.length + 1;
  }

  private lastRideRequestInvocation: [Date, Date, RideRequest[][]] | undefined;

  async allRideRequestDocs(startMonth: Date, endMonth: Date) {
    if (
      this.lastRideRequestInvocation?.[0].valueOf() === startMonth.valueOf() &&
      this.lastRideRequestInvocation?.[1].valueOf() === endMonth.valueOf()
    ) {
      return this.lastRideRequestInvocation[2];
    }

    const months = this.getMonths(startMonth, endMonth);

    this.lastRideRequestInvocation = [
      startMonth,
      endMonth,
      await Promise.all(
        months.map((month) =>
          toPromise(
            this.fs.getRideRequests({
              startAfterOrOnDate: month,
              endBeforeDate: endOfMonth(month),
            }),
          ),
        ),
      ),
    ];

    return this.lastRideRequestInvocation[2];
  }
}

function getPercentage(total: number, partial: number) {
  if (total === 0) {
    return 0;
  }

  return partial / total;
}

function downloadFile(file: any, filename: string) {
  const a = document.createElement('a');
  const url = URL.createObjectURL(file);

  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();

  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 0);
}

function getFiscalQuarter(date: Date) {
  const fiscQuarter = FISCAL_QUARTER_MAP[getQuarter(date)];

  if (!fiscQuarter) {
    throw new Error(`Expected a number between 1-4 but got ${fiscQuarter}`);
  }

  return fiscQuarter;
}

/** Year is in the form "YYYY-YYYY". e.g. "2019-2020" */
function getFiscalQuartersForYear(year: string) {
  const [start, end] = year.split('-').map((s) => Number.parseInt(s));

  return {
    1: new Date(start, 6, 1),
    2: new Date(start, 9, 1),
    3: new Date(end, 0, 1),
    4: new Date(end, 3, 1),
  };
}

/** This maps a calendar quarter to a fiscal quarter */
const FISCAL_QUARTER_MAP: { [key: number]: number } = {
  1: 3,
  2: 4,
  3: 1,
  4: 2,
};
