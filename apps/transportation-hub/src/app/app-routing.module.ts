import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { HomeGuard } from './home.guard';
import { IsAuthenticatedGuard } from './is-authenticated.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
    canActivate: [HomeGuard],
  },
  {
    path: 'welcome',
    loadChildren: () =>
      import('./modules/welcome/welcome.module').then(m => m.WelcomeModule),
    canActivate: [IsAuthenticatedGuard],
  },
  {
    path: 'dispatcher',
    loadChildren: () =>
      import('./modules/dispatcher/dispatcher.module').then(
        m => m.DispatcherModule,
      ),
    canActivate: [IsAuthenticatedGuard],
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./modules/admin/admin.module').then(m => m.AdminModule),
    canActivate: [IsAuthenticatedGuard],
  },
  {
    path: 'down-for-maintenance',
    loadChildren: () =>
      import('./modules/maintenance-warning/maintenance-warning.module').then(
        m => m.MaintenanceWarningModule,
      ),
    canActivate: [IsAuthenticatedGuard],
  },
  { path: '**',
    redirectTo: 'welcome' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
