import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-persistance-popup',
  template: `
    <h1 mat-dialog-title>
      Are you on a secure device?
    </h1>

    <mat-dialog-content>
      <p>
        i.e. are you using a device that you or the Volunteer Center owns?
        Examples of insecure devices would be a computer at a public library or
        a computer owned by a friend.
      </p>
    </mat-dialog-content>

    <mat-dialog-actions>
      <button mat-button type="button" (click)="secure(false)">
        This is *not* a secure device
      </button>

      <div class="flex-1"></div>

      <button mat-button type="button" (click)="secure(true)">
        This is a secure device
      </button>
    </mat-dialog-actions>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistancePopupComponent implements OnInit {
  constructor(private ref: MatDialogRef<PersistancePopupComponent>) {}

  ngOnInit(): void {}

  secure(value: boolean) {
    this.ref.close(value);
  }
}
