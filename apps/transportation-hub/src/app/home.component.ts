import { Component, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
// import { FirestoreService } from './firestore.service';
import { IsLoadingService } from '@service-work/is-loading';
import { Router } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-home',
  template: `
    <div class="wrapper">
      <div class="flex-center" style="margin-bottom: 2rem;">
        <img
          src="/assets/VC-heart-logo-compressed-clear.png"
          style="height: 100px;"
        />
      </div>

      <mat-card>
        <h1>Volunteer Senior Transportation Hub</h1>

        <div class="flex-center">
          <button mat-raised-button swIsLoading="login" (click)="login()">
            Login with Google
          </button>
        </div>
      </mat-card>

      <div class="hint">
        <p>
          This website requires a recent version of Google Chrome, Mozilla
          Firefox, Microsoft Edge, or Apple Safari.
        </p>
      </div>
    </div>
  `,
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(
    // private fs: FirestoreService,
    private loadingService: IsLoadingService,
    // private cdr: ChangeDetectorRef,
    private router: Router,
    private titleService: Title,
    private auth: AuthService,
  ) {}

  ngOnInit() {
    this.titleService.setTitle('Senior Transportation Hub');

    // this.subscriptions.push(
    //   this.fs
    //     .getCurrentUser()
    //     .pipe(
    //       switchMap(user =>
    //         this.fs.getOrganization().pipe(
    //           tap(org => {
    //             if (
    //               user &&
    //               org &&
    //               org.adminEmailAddresses.includes(user.emailAddress)
    //             ) {
    //               this.router.navigate(['admin/overview']);
    //             } else if (user && org) {
    //               this.router.navigate(['dispatcher/signin']);
    //             } else if (user) {
    //               this.router.navigate(['welcome']);
    //             }
    //           }),
    //         ),
    //       ),
    //     )
    //     .subscribe(),
    // );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async login() {
    const user = await this.loadingService
      .add(this.auth.startLogin(), {
        key: 'login',
      })
      .catch((e) => console.error(e));

    if (user) {
      setTimeout(() => this.router.navigate(['welcome']), 0);
    }
  }
}
