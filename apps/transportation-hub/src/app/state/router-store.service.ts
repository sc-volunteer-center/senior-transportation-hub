import { Injectable, Type } from '@angular/core';
import { Router, RouterStateSnapshot } from '@angular/router';
import { isEqual } from '@local/isEqual';
import { RouterStoreService as _RouterStoreService } from '@service-work/router-store';

export interface IRouterStoreState {
  params: { [key: string]: string | undefined };
  queryParams: { [key: string]: string | undefined };
  breadcrumbs: Type<any>[];
}

@Injectable({
  providedIn: 'root',
})
export class RouterStoreService extends _RouterStoreService<IRouterStoreState> {
  constructor(router: Router) {
    super(router, {
      serializer: (routerState: RouterStateSnapshot): IRouterStoreState => {
        let route = routerState.root;
        const breadcrumbs: Type<any>[] = [];
        let params: any = {};

        while (route.firstChild) {
          route = route.firstChild;

          if (route.outlet === 'primary') {
            if (route.data.breadcrumb) {
              breadcrumbs.push(route.data.breadcrumb);
            }

            params = {
              ...params,
              ...route.params,
            };
          }
        }

        return {
          params,
          queryParams: routerState.root.queryParams,
          breadcrumbs,
        };
      },
      isEqual,
    });
  }
}
