import { Injectable } from '@angular/core';
import { switchMap, filter, toArray, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { INoteThread, NoteThread } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction, combineLatest } from 'rxjs';
import { DispatcherService } from './dispatcher.service';
import type firebase from 'firebase/compat';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';

export interface IGetNoteThreadsParams {
  clientId?: string;
  driverId?: string;
  rideRequestId?: string;
  flag?: boolean;
  suppressTodo?: boolean;
  startBefore?: Date;
  /** Default limit is 20 */
  limit?: number;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class NoteThreadService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private dispatcherService: DispatcherService,
    private queryCache: QueryCacheService,
  ) {}

  getNoteThread(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<INoteThread>(
                  `organizations/${info.organization.uid}/noteThreads/${id}`,
                )
                .valueChanges(),
        ),
        switchMap((threadDoc) => {
          if (!threadDoc) return of(threadDoc);

          const thread = new NoteThread(threadDoc);

          return combineLatest(
            thread.notes.map((note) => {
              return this.dispatcherService
                .getDispatcher(note.createdById)
                .pipe(
                  map((dispatcher) => {
                    note.dispatcher = dispatcher!;
                    return note;
                  }),
                );
            }),
          ).pipe(map(() => thread));
        }),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'NoteThreadService',
      'getNoteThread',
      [id],
      query,
      cacheOptions,
    );
  }

  getNoteThreads(args: IGetNoteThreadsParams = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of([])
            : this.fstore
                .collection<INoteThread>(
                  `organizations/${info.organization.uid}/noteThreads`,
                  (ref) => {
                    let result: firebase.firestore.Query = ref
                      .orderBy('noteAddedAt', 'desc')
                      .limit(args.limit ?? 20);

                    if (args.clientId) {
                      result = result.where('clientId', '==', args.clientId);
                    }

                    if (args.driverId) {
                      result = result.where('driverId', '==', args.driverId);
                    }

                    if (args.rideRequestId) {
                      result = result.where(
                        'rideRequestId',
                        '==',
                        args.rideRequestId,
                      );
                    }

                    if (args.flag) {
                      result = result.where('flag', '==', args.flag);
                    }

                    if (args.suppressTodo !== undefined) {
                      result = result.where(
                        'suppressTodo',
                        '==',
                        args.suppressTodo,
                      );
                    }

                    if (args.startBefore) {
                      result = result.where(
                        'noteAddedAt',
                        '<',
                        args.startBefore,
                      );
                    }

                    return result;
                  },
                )
                .valueChanges(),
        ),
        map((noteThreads) => {
          // we use an observable to filter because it is more performant than
          // chaining `.filter()` calls.

          const filters: MonoTypeOperatorFunction<INoteThread>[] = [];

          if (args.clientId !== undefined) {
            filters.push(filter((note) => note.clientId === args.clientId));
          }

          if (args.driverId !== undefined) {
            filters.push(filter((note) => note.driverId === args.driverId));
          }

          if (args.rideRequestId !== undefined) {
            filters.push(
              filter((note) => note.rideRequestId === args.rideRequestId),
            );
          }

          if (args.flag !== undefined) {
            filters.push(filter((note) => note.flag === args.flag));
          }

          if (args.suppressTodo !== undefined) {
            filters.push(
              filter((note) => note.suppressTodo === args.suppressTodo),
            );
          }

          if (args.startBefore !== undefined) {
            const startBefore = args.startBefore.valueOf();
            filters.push(
              filter(
                (note) => note.noteAddedAt.toDate().valueOf() < startBefore,
              ),
            );
          }

          let obs = from(noteThreads);

          filters.forEach((filt) => (obs = obs.pipe(filt)));

          let filteredNoteThreads: INoteThread[];

          obs.pipe(toArray()).subscribe((vals) => (filteredNoteThreads = vals));

          // apply the limit at the end
          return filteredNoteThreads!
            .sort((aa, bb) => {
              const a = aa.noteAddedAt.toDate().valueOf();
              const b = bb.noteAddedAt.toDate().valueOf();

              if (a > b) return 1;
              if (b > a) return -1;
              return 0;
            })
            .slice(0, args.limit ?? 20);
        }),
        switchMap((threads) => {
          if (threads.length === 0) return of([]);

          return combineLatest(
            threads.map((noteThead) => {
              const thread = new NoteThread(noteThead);

              return combineLatest(
                thread.notes.map((note) => {
                  return this.dispatcherService
                    .getDispatcher(note.createdById)
                    .pipe(
                      map((dispatcher) => {
                        note.dispatcher = dispatcher!;
                        return note;
                      }),
                    );
                }),
              ).pipe(map(() => thread));
            }),
          );
        }),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'NoteThreadService',
      'getNoteThreads',
      [args],
      query,
      cacheOptions,
    );
  }
}
