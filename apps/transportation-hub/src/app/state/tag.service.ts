import { Injectable } from '@angular/core';
import { switchMap, filter, toArray, map, share } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ITag, Tag } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction } from 'rxjs';
import { stringComparer } from '../utilities';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';

export interface IGetTagsParams {
  type?: string;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class TagService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getTag(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<ITag>(`organizations/${info.organization.uid}/tags/` + id)
                .valueChanges(),
        ),
        // the server is returning updatedAt === null after an update
        // and then immediately sending down a new document with the actual updatedAt value
        filter((tag) => !!(tag && tag.updatedAt)),
        map((tag) => (tag && new Tag(tag)) || null),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'TagService',
      'getTag',
      [id],
      query,
      cacheOptions,
    );
  }

  getTags(args: IGetTagsParams = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of([])
            : this.fstore
                .collection<ITag>(`organizations/${info.organization.uid}/tags`)
                .valueChanges(),
        ),
        map((tags) => {
          // we use an observable to filter because it is more performant than
          // chaining `.filter()` calls.

          const filters: MonoTypeOperatorFunction<ITag>[] = [];

          if (typeof args.type !== 'undefined') {
            filters.push(filter((tag) => tag.type === args.type));
          }

          let obs = from(tags);

          filters.forEach((filt) => (obs = obs.pipe(filt)));

          let filteredTags: ITag[];

          obs.pipe(toArray()).subscribe((vals) => (filteredTags = vals));

          filteredTags!.sort((a, b) => {
            const res = stringComparer(a.label, b.label);

            if (res === 0) return stringComparer(a.uid, b.uid);

            return res;
          });

          return filteredTags!;
        }),
        // the server is returning updatedAt === null after an update
        // and then immediately sending down a new document with the actual updatedAt value
        map((tags) =>
          tags
            .filter((tag) => !!(tag && tag.updatedAt))
            .map((tag) => new Tag(tag)),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'TagService',
      'getTags',
      [args],
      query,
      cacheOptions,
    );
  }
}
