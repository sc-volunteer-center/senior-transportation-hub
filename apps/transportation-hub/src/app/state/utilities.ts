// export function stringifyParams(args: {}) {
//   return JSON.stringify(
//     Object.entries(args).sort((a, b) => {
//       if (a[0] > b[0]) return -1;
//       if (a[0] < b[0]) return 1;
//       else return 0;
//     }),
//   );
// }

import { defer, MonoTypeOperatorFunction, Observable } from 'rxjs';
import { startOfWeek as _startOfWeek, endOfWeek as _endOfWeek } from 'date-fns';

/** Triggers callback every time a new observer subscribes to this chain. */
export function tapOnSubscribe<T>(
  callback: () => void,
): MonoTypeOperatorFunction<T> {
  return (source: Observable<T>): Observable<T> =>
    defer(() => {
      callback();
      return source;
    });
}

export const startOfWeek = (val: Date) =>
  _startOfWeek(val, { weekStartsOn: 1 });

export const endOfWeek = (val: Date) => _endOfWeek(val, { weekStartsOn: 1 });

export function stringComparer(aa: string, bb: string) {
  const a = aa.toLowerCase();
  const b = bb.toLowerCase();

  if (a === b) return 0;
  else if (a === '') return -1;
  else if (b === '') return 1;
  else if (a > b) return 1;
  else return -1;
}

export function isDefined<T>(value: T): value is Exclude<T, undefined> {
  return value !== undefined;
}
