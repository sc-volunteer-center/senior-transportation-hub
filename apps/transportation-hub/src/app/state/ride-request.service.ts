import { Injectable } from '@angular/core';
import {
  switchMap,
  filter,
  toArray,
  map,
  debounceTime,
  take,
} from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import {
  IRideRequest,
  RideRequest,
  Client,
  IRecurringRideRequest,
  RecurringRideRequest,
} from '@local/models';
import { UserService } from './user.service';
import {
  of,
  from,
  MonoTypeOperatorFunction,
  Observable,
  OperatorFunction,
  combineLatest,
} from 'rxjs';
import { ClientService } from './client.service';
import type firebase from 'firebase/compat';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';
import { startOfWeek, endOfWeek, stringComparer, isDefined } from './utilities';

export interface IRideRequestsGetParams {
  clientId?: string;
  clientNotifiedOfDriverAt?: Date | null;
  clientNotifiedOfDriverAtIsNotNull?: boolean;
  driverId?: string | null;
  driverIdIsNotNull?: boolean;
  driverNotifiedAt?: Date | null;
  requestCancelledAt?: Date | null;
  requestIsCancelled?: boolean;
  driverNotifiedOfCancellationAt?: Date | null;
  hasWillingDriver?: boolean;
  startAfterOrOnDate?: Date;
  endBeforeDate?: Date;
  endBeforeOrOnDate?: Date;
  isFlexible?: boolean;
  limit?: number;
  startAfterDoc?: AngularFirestoreDocument<IRideRequest>;
  hasFlag?: boolean;
  recurringRideRequestId?: string | null;
  // getOnce?: boolean;
  order?: 'asc' | 'desc';
  cache?: IQueryCacheOptions;
  /**
   * USE IN DEV ONLY
   * Turns on the debugger for this request
   */
  _debugger?: true;
}

export interface IRecurringRideRequestsGetParams {
  clientId?: string;
  clientNotifiedOfDriverAt?: Date | null;
  clientNotifiedOfDriverAtIsNotNull?: boolean;
  driverId?: string | null;
  driverIdIsNotNull?: boolean;
  driverNotifiedAt?: Date | null;
  requestCancelledAt?: Date | null;
  requestIsCancelled?: boolean;
  driverNotifiedOfCancellationAt?: Date | null;
  hasWillingDriver?: boolean;
  startAfterOrOnDate?: Date;
  startsBeforeOrOnDate?: Date;
  endBeforeOrOnDate?: Date;
  endAfterDate?: Date;
  isFlexible?: boolean;
  limit?: number;
  startAfterDoc?: AngularFirestoreDocument<IRideRequest>;
  hasFlag?: boolean;
  // getOnce?: boolean;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class RideRequestService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private clientService: ClientService,
    private queryCache: QueryCacheService,
  ) {}

  getRideRequest(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IRideRequest>(
                  `organizations/${info.organization.uid}/rideRequests/` + id,
                )
                .valueChanges(),
        ),
        switchMap((rideRequest) =>
          !rideRequest
            ? of(null)
            : this.clientService
                .getClient(rideRequest.clientId)
                .pipe(
                  map(
                    (client) =>
                      (client && new RideRequest(rideRequest, client)) || null,
                  ),
                ),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'RideRequestService',
      'getRideRequest',
      [id],
      query,
      cacheOptions,
    );
  }

  getRecurringRideRequest(
    id: string,
    options: { cache?: IQueryCacheOptions } = {},
  ) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IRecurringRideRequest>(
                  `organizations/${info.organization.uid}/recurringRideRequests/` +
                    id,
                )
                .valueChanges(),
        ),
        switchMap((r) =>
          !r
            ? of(null)
            : this.clientService
                .getClient(r.clientId)
                .pipe(
                  map(
                    (client) =>
                      (client && new RecurringRideRequest(r, client)) || null,
                  ),
                ),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'RideRequestService',
      'getRecurringRideRequest',
      [id],
      query,
      cacheOptions,
    );
  }

  getRideRequests(args: IRideRequestsGetParams = {}) {
    if (args.endBeforeOrOnDate && args.endBeforeDate) {
      throw new Error(
        `Cannot include both "endBeforeOrOnDate" and "endBeforeDate"`,
      );
    }

    const query = () =>
      this.userService
        .getCurrentDispatcherInfo([], (info) => {
          if (isDefined(args.limit) && !isDefined(args.isFlexible)) {
            const base1 = this.fstore
              .collection<IRideRequest>(
                `organizations/${info.organization.uid}/rideRequests`,
                (ref) => {
                  let result = buildRideRequestBaseQuery(args, ref).where(
                    'flexible',
                    '==',
                    true,
                  );

                  if (isDefined(args.startAfterOrOnDate)) {
                    result = result.where(
                      'datetime',
                      '>=',
                      startOfWeek(args.startAfterOrOnDate),
                    );
                  }

                  return result;
                },
              )
              .valueChanges();

            const base2 = this.fstore
              .collection<IRideRequest>(
                `organizations/${info.organization.uid}/rideRequests`,
                (ref) => {
                  let result = buildRideRequestBaseQuery(args, ref).where(
                    'flexible',
                    '==',
                    false,
                  );

                  if (isDefined(args.startAfterOrOnDate)) {
                    result = result.where(
                      'datetime',
                      '>=',
                      args.startAfterOrOnDate,
                    );
                  }

                  return result;
                },
              )
              .valueChanges();

            return combineLatest([base1, base2]).pipe(
              map(([one, two]) => [...one, ...two]),
            );
          }

          return this.fstore
            .collection<IRideRequest>(
              `organizations/${info.organization.uid}/rideRequests`,
              (ref) => {
                let result = buildRideRequestBaseQuery(args, ref);

                if (isDefined(args.isFlexible)) {
                  result = result.where('flexible', '==', args.isFlexible);
                }

                if (isDefined(args.startAfterOrOnDate)) {
                  result = result.where(
                    'datetime',
                    '>=',
                    args.isFlexible === false
                      ? args.startAfterOrOnDate
                      : startOfWeek(args.startAfterOrOnDate),
                  );
                }

                return result;
              },
            )
            .valueChanges();
        })
        .pipe(
          debounceTime(50),
          map((rideRequests) => {
            // we use an observable to filter because it is more performant than
            // chaining `.filter()` calls.

            if (args._debugger) debugger;

            const operators: (
              | MonoTypeOperatorFunction<IRideRequest>
              | OperatorFunction<any, any>
            )[] = [];

            if (isDefined(args.clientId)) {
              operators.push(
                filter((rideRequest) => rideRequest.clientId === args.clientId),
              );
            }

            if (isDefined(args.driverId)) {
              operators.push(
                filter((rideRequest) => rideRequest.driverId === args.driverId),
              );
            } else if (args.driverIdIsNotNull) {
              operators.push(
                filter((rideRequest) => rideRequest.driverId !== null),
              );
            }

            if (isDefined(args.hasWillingDriver)) {
              operators.push(
                filter(
                  (rideRequest) =>
                    rideRequest.hasWillingDriver === args.hasWillingDriver,
                ),
              );
            }

            if (isDefined(args.startAfterOrOnDate)) {
              const fixStartValue = args.startAfterOrOnDate.valueOf();
              const flexStartValue = startOfWeek(
                args.startAfterOrOnDate,
              ).valueOf();

              operators.push(
                filter((rideRequest: IRideRequest) => {
                  const effectiveStart = rideRequest.flexible
                    ? flexStartValue
                    : fixStartValue;

                  return (
                    rideRequest.datetime.toDate().valueOf() >= effectiveStart
                  );
                }),
              );
            }

            if (isDefined(args.endBeforeDate)) {
              const endValue = args.endBeforeDate.valueOf();

              operators.push(
                filter(
                  (rideRequest: IRideRequest) =>
                    rideRequest.datetime.toDate().valueOf() < endValue,
                ),
              );
            }

            if (isDefined(args.endBeforeOrOnDate)) {
              const endValue = args.endBeforeOrOnDate.valueOf();

              operators.push(
                filter(
                  (rideRequest: IRideRequest) =>
                    rideRequest.datetime.toDate().valueOf() <= endValue,
                ),
              );
            }

            if (isDefined(args.isFlexible)) {
              operators.push(
                filter(
                  (rideRequest: IRideRequest) =>
                    rideRequest.flexible === args.isFlexible,
                ),
              );
            }

            if (isDefined(args.driverNotifiedOfCancellationAt)) {
              const value =
                args.driverNotifiedOfCancellationAt &&
                args.driverNotifiedOfCancellationAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.driverNotifiedOfCancellationAt &&
                      rideRequest.driverNotifiedOfCancellationAt
                        .toDate()
                        .valueOf()),
                ),
              );
            }

            if (isDefined(args.requestCancelledAt)) {
              const value =
                args.requestCancelledAt && args.requestCancelledAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.requestCancelledAt &&
                      rideRequest.requestCancelledAt.toDate().valueOf()),
                ),
              );
            }

            if (isDefined(args.requestIsCancelled)) {
              operators.push(
                filter(
                  (rideRequest) =>
                    rideRequest.requestCancelled === args.requestIsCancelled,
                ),
              );
            }

            if (isDefined(args.clientNotifiedOfDriverAt)) {
              const value =
                args.clientNotifiedOfDriverAt &&
                args.clientNotifiedOfDriverAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.clientNotifiedOfDriverAt &&
                      rideRequest.clientNotifiedOfDriverAt.toDate().valueOf()),
                ),
              );
            } else if (args.clientNotifiedOfDriverAtIsNotNull) {
              operators.push(
                filter(
                  (rideRequest) =>
                    rideRequest.clientNotifiedOfDriverAt !== null,
                ),
              );
            }

            if (isDefined(args.driverNotifiedAt)) {
              const value =
                args.driverNotifiedAt && args.driverNotifiedAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.driverNotifiedAt &&
                      rideRequest.driverNotifiedAt.toDate().valueOf()),
                ),
              );
            }

            if (isDefined(args.hasFlag)) {
              operators.push(
                filter((r: IRideRequest) => args.hasFlag === (r.flags !== 0)),
              );
            }

            operators.push(
              toArray<IRideRequest>(),
              map((rr: IRideRequest[]) => rr.sort(requestComparer)),
            );

            if (args.order === 'desc') {
              operators.push(map((rr: IRideRequest[]) => rr.reverse()));
            }

            // startAfterDoc must come after order
            if (args.startAfterDoc) {
              const id = args.startAfterDoc.ref.id;

              operators.push(
                map((rideRequests: IRideRequest[]) => {
                  const start = rideRequests.findIndex((rr) => rr.uid === id);

                  return rideRequests.slice(start);
                }),
              );
            }

            // limit must come after order
            if (args.limit) {
              operators.push(
                map((rideRequests: IRideRequest[]) =>
                  rideRequests.slice(0, args.limit),
                ),
              );
            }

            let filteredRideRequests: IRideRequest[];

            from(rideRequests)
              .pipe(...(operators as []))
              .subscribe((vals) => (filteredRideRequests = vals as any));

            return filteredRideRequests!;
          }),
          switchMap((rideRequests) =>
            rideRequests.length === 0
              ? (of([]) as Observable<RideRequest[]>)
              : combineLatest(
                  rideRequests.map((request) =>
                    this.clientService.getClient(request.clientId),
                  ),
                ).pipe(
                  map(
                    (clients) =>
                      clients.filter((client) => !!client) as Client[],
                  ),
                  map((clients) =>
                    clients.map(
                      (client, index) =>
                        new RideRequest(rideRequests[index], client),
                    ),
                  ),
                ),
          ),
        );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'RideRequestService',
      'getRideRequests',
      [args],
      query,
      cacheOptions,
    );
  }

  getRecurringRideRequests(args: IRecurringRideRequestsGetParams = {}) {
    const query = () =>
      this.userService
        .getCurrentDispatcherInfo([], (info) => {
          const base1 = this.fstore.collection<IRecurringRideRequest>(
            `organizations/${info.organization.uid}/recurringRideRequests`,
            (ref) => {
              let result = buildRecurringRideRequestBaseQuery(args, ref);

              if (args.endAfterDate) {
                result = result.where('endDate', '==', null);
              }

              return result;
            },
          );

          const base2 = this.fstore.collection<IRecurringRideRequest>(
            `organizations/${info.organization.uid}/recurringRideRequests`,
            (ref) => {
              let result = buildRecurringRideRequestBaseQuery(args, ref);

              if (args.endAfterDate) {
                result = result.where('endDate', '>', args.endAfterDate);
              }

              return result;
            },
          );

          let obs1: Observable<IRecurringRideRequest[]>;
          let obs2: Observable<IRecurringRideRequest[]> = of([]);

          // if (args.getOnce) {
          //   obs1 = base1.get({ source: 'server' }).pipe(
          //     take(1),
          //     map((snaps) => snaps.docs.map((snap) => snap.data())),
          //   );

          //   if (args.endAfterDate) {
          //     obs2 = base2.get({ source: 'server' }).pipe(
          //       take(1),
          //       map((snaps) => snaps.docs.map((snap) => snap.data())),
          //     );
          //   }
          // } else {
          obs1 = base1.valueChanges();

          if (args.endAfterDate) {
            obs2 = base2.valueChanges();
          }
          // }

          return combineLatest([obs1, obs2]);
        })
        .pipe(
          debounceTime(50),
          map(([r1, r2]) => {
            // we use an observable to filter because it is more performant than
            // chaining `.filter()` calls.

            const rideRequests = [...r1, ...r2];

            const operators: (
              | MonoTypeOperatorFunction<IRecurringRideRequest>
              | OperatorFunction<any, any>
            )[] = [];

            if (isDefined(args.startsBeforeOrOnDate)) {
              const fixStart = args.startsBeforeOrOnDate.valueOf();
              const flexStart = startOfWeek(
                args.startsBeforeOrOnDate,
              ).valueOf();

              operators.push(
                filter(
                  (r: IRecurringRideRequest) =>
                    r.startDate.toDate().valueOf() <=
                    (r.flexible ? flexStart : fixStart),
                ),
              );
            }

            if (isDefined(args.startAfterOrOnDate)) {
              const fixStart = args.startAfterOrOnDate.valueOf();
              const flexStart = startOfWeek(args.startAfterOrOnDate).valueOf();

              operators.push(
                filter(
                  (r: IRecurringRideRequest) =>
                    r.startDate.toDate().valueOf() >=
                    (r.flexible ? flexStart : fixStart),
                ),
              );
            }

            if (isDefined(args.endBeforeOrOnDate)) {
              const fixEnd = args.endBeforeOrOnDate.valueOf();
              const flexEnd = endOfWeek(args.endBeforeOrOnDate).valueOf();

              operators.push(
                filter(
                  (r: IRecurringRideRequest) =>
                    !!r.endDate &&
                    r.endDate.toDate().valueOf() <=
                      (r.flexible ? flexEnd : fixEnd),
                ),
              );
            }

            if (isDefined(args.endAfterDate)) {
              const end = args.endAfterDate.valueOf();

              operators.push(
                filter(
                  (r: IRecurringRideRequest) =>
                    !r.endDate || r.endDate.toDate().valueOf() > end,
                ),
              );
            }

            // if (args.clientId !== undefined) {
            //   operators.push(
            //     filter((rideRequest) => rideRequest.clientId === args.clientId),
            //   );
            // }

            if (isDefined(args.driverId)) {
              operators.push(
                filter((rideRequest) => rideRequest.driverId === args.driverId),
              );
            } else if (args.driverIdIsNotNull) {
              operators.push(
                filter((rideRequest) => rideRequest.driverId !== null),
              );
            }

            // if (args.hasWillingDriver !== undefined) {
            //   operators.push(
            //     filter(
            //       (rideRequest) =>
            //         rideRequest.hasWillingDriver === args.hasWillingDriver,
            //     ),
            //   );
            // }

            if (isDefined(args.driverNotifiedOfCancellationAt)) {
              const value =
                args.driverNotifiedOfCancellationAt &&
                args.driverNotifiedOfCancellationAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.driverNotifiedOfCancellationAt &&
                      rideRequest.driverNotifiedOfCancellationAt
                        .toDate()
                        .valueOf()),
                ),
              );
            }

            if (isDefined(args.requestCancelledAt)) {
              const value =
                args.requestCancelledAt && args.requestCancelledAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.requestCancelledAt &&
                      rideRequest.requestCancelledAt.toDate().valueOf()),
                ),
              );
            } else {
              if (args.requestIsCancelled) {
                operators.push(
                  filter(
                    (rideRequest) => rideRequest.requestCancelledBy !== null,
                  ),
                );
              } else if (args.requestIsCancelled === false) {
                operators.push(
                  filter(
                    (rideRequest) => rideRequest.requestCancelledBy === null,
                  ),
                );
              }
            }

            if (isDefined(args.clientNotifiedOfDriverAt)) {
              const value =
                args.clientNotifiedOfDriverAt &&
                args.clientNotifiedOfDriverAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.clientNotifiedOfDriverAt &&
                      rideRequest.clientNotifiedOfDriverAt.toDate().valueOf()),
                ),
              );
            } else if (args.clientNotifiedOfDriverAtIsNotNull) {
              operators.push(
                filter(
                  (rideRequest) =>
                    rideRequest.clientNotifiedOfDriverAt !== null,
                ),
              );
            }

            if (isDefined(args.driverNotifiedAt)) {
              const value =
                args.driverNotifiedAt && args.driverNotifiedAt.valueOf();

              operators.push(
                filter(
                  (rideRequest) =>
                    value ===
                    (rideRequest.driverNotifiedAt &&
                      rideRequest.driverNotifiedAt.toDate().valueOf()),
                ),
              );
            }

            if (isDefined(args.hasFlag)) {
              operators.push(
                filter(
                  (r: IRecurringRideRequest) =>
                    args.hasFlag === (r.flags !== 0),
                ),
              );
            }

            // if (args.endDate) {
            //   const end = args.endDate.valueOf();

            //   operators.push(
            //     filter((rr: IRecurringRideRequest) => !rr.endDate || rr.endDate.toDate().valueOf() <= end),
            //   );
            // }

            operators.push(
              toArray<IRecurringRideRequest>(),
              map(
                (rr: IRecurringRideRequest[]) =>
                  rr.sort((a, b) => {
                    const x = a.startDate;
                    const y = b.startDate;

                    if (x.seconds > y.seconds) return 1;
                    if (x.seconds < y.seconds) return -1;
                    if (x.nanoseconds > y.nanoseconds) return 1;
                    if (x.nanoseconds < y.nanoseconds) return -1;
                    return stringComparer(a.uid, b.uid);
                  }) as IRecurringRideRequest[],
              ),
            );

            if (isDefined(args.startAfterDoc)) {
              const id = args.startAfterDoc.ref.id;

              operators.push(
                map((rideRequests: IRecurringRideRequest[]) => {
                  const start = rideRequests.findIndex((rr) => rr.uid === id);

                  return rideRequests.slice(start);
                }),
              );
            }

            // if (args.startAfterOrOnDate) {
            //   const start = args.startAfterOrOnDate.valueOf();

            //   operators.push(
            //     map((rideRequests: IRecurringRideRequest[]) => {
            //       const index = rideRequests.findIndex(
            //         (rr) => rr.startDate.toDate().valueOf() >= start,
            //       );

            //       if (index < 0) return [];

            //       return rideRequests.slice(index);
            //     }),
            //   );
            // }

            if (isDefined(args.limit)) {
              operators.push(
                map((rideRequests: IRecurringRideRequest[]) =>
                  rideRequests.slice(0, args.limit),
                ),
              );
            }

            let filteredRideRequests: IRecurringRideRequest[];

            from(rideRequests)
              .pipe(...(operators as []))
              .subscribe((vals) => (filteredRideRequests = vals as any));

            return filteredRideRequests!;
          }),
          switchMap((rideRequests) =>
            rideRequests.length === 0
              ? (of([]) as Observable<RecurringRideRequest[]>)
              : combineLatest(
                  rideRequests.map((request) =>
                    this.clientService.getClient(request.clientId),
                  ),
                ).pipe(
                  map(
                    (clients) =>
                      clients.filter((client) => !!client) as Client[],
                  ),
                  map((clients) =>
                    clients.map(
                      (client, index) =>
                        new RecurringRideRequest(rideRequests[index], client),
                    ),
                  ),
                ),
          ),
        );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'RideRequestService',
      'getRecurringRideRequests',
      [args],
      query,
      cacheOptions,
    );
  }
}

function buildRecurringRideRequestBaseQuery(
  args: IRecurringRideRequestsGetParams,
  ref: firebase.firestore.Query,
) {
  // not bothering with "orderBy"
  let result = ref;

  if (isDefined(args.clientId)) {
    result = result.where('clientId', '==', args.clientId);
  }

  if (isDefined(args.driverId)) {
    result = result.where('driverId', '==', args.driverId);
  }

  if (isDefined(args.hasWillingDriver)) {
    result = result.where('hasWillingDriver', '==', args.hasWillingDriver);
  }

  if (isDefined(args.requestCancelledAt)) {
    result = result.where('requestCancelledAt', '==', args.requestCancelledAt);
  }

  if (isDefined(args.clientNotifiedOfDriverAt)) {
    result = result.where(
      'clientNotifiedOfDriverAt',
      '==',
      args.clientNotifiedOfDriverAt,
    );
  }
  if (isDefined(args.driverNotifiedAt)) {
    result = result.where('driverNotifiedAt', '==', args.driverNotifiedAt);
  }
  if (isDefined(args.driverNotifiedOfCancellationAt)) {
    result = result.where(
      'driverNotifiedOfCancellationAt',
      '==',
      args.driverNotifiedOfCancellationAt,
    );
  }
  if (isDefined(args.isFlexible)) {
    result = result.where('flexible', '==', args.isFlexible);
  }
  // Can't use a range filter on both startDate and endDate
  // if (args.startDate) {
  //   result = result.where('startDate', '>=', args.startDate);
  // }
  // if (args.endBeforeDate === null) {
  //   result = result.where('endDate', '==', args.endBeforeDate);
  // }
  if (isDefined(args.endBeforeOrOnDate)) {
    result = result.where('endDate', '<=', endOfWeek(args.endBeforeOrOnDate));
  }

  if (isDefined(args.startAfterDoc)) {
    result = result.startAfter(args.startAfterDoc);
  }
  if (isDefined(args.limit)) {
    result = result.limit(args.limit);
  }

  return result;
}

function buildRideRequestBaseQuery(
  args: IRideRequestsGetParams,
  ref: firebase.firestore.Query,
) {
  // not bothering with "orderBy"
  let result = ref.orderBy('datetime', args.order || 'asc').orderBy('uid');

  if (args.clientId) {
    result = result.where('clientId', '==', args.clientId);
  }

  if (isDefined(args.driverId)) {
    result = result.where('driverId', '==', args.driverId);
  }

  if (args.recurringRideRequestId) {
    result = result.where(
      'recurringRideRequestId',
      '==',
      args.recurringRideRequestId,
    );
  }

  if (isDefined(args.hasWillingDriver)) {
    result = result.where('hasWillingDriver', '==', args.hasWillingDriver);
  }

  if (isDefined(args.requestCancelledAt)) {
    result = result.where('requestCancelledAt', '==', args.requestCancelledAt);
  }

  if (isDefined(args.requestIsCancelled)) {
    result = result.where('requestCancelled', '==', args.requestIsCancelled);
  }

  if (isDefined(args.clientNotifiedOfDriverAt)) {
    result = result.where(
      'clientNotifiedOfDriverAt',
      '==',
      args.clientNotifiedOfDriverAt,
    );
  }
  if (isDefined(args.driverNotifiedAt)) {
    result = result.where('driverNotifiedAt', '==', args.driverNotifiedAt);
  }
  if (isDefined(args.driverNotifiedOfCancellationAt)) {
    result = result.where(
      'driverNotifiedOfCancellationAt',
      '==',
      args.driverNotifiedOfCancellationAt,
    );
  }
  if (isDefined(args.isFlexible)) {
    result = result.where('flexible', '==', args.isFlexible);
  }
  if (args.endBeforeDate) {
    result = result.where('datetime', '<', args.endBeforeDate);
  }
  if (args.endBeforeOrOnDate) {
    result = result.where('datetime', '<=', args.endBeforeOrOnDate);
  }
  if (args.startAfterDoc) {
    result = result.startAfter(args.startAfterDoc);
  }
  if (args.limit) {
    result = result.limit(args.limit);
  }

  return result;
}

function requestComparer(a: IRideRequest, b: IRideRequest) {
  const x = a.datetime;
  const y = b.datetime;

  if (x.seconds > y.seconds) return 1;
  if (x.seconds < y.seconds) return -1;
  if (x.nanoseconds > y.nanoseconds) return 1;
  if (x.nanoseconds < y.nanoseconds) return -1;
  return stringComparer(a.uid, b.uid);
}
