import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import stringify from 'fast-json-stable-stringify';
import { delay, finalize, shareReplay, takeUntil } from 'rxjs/operators';
import { tapOnSubscribe } from './utilities';

/** Amount of milliseconds to hold onto cached queries */
const HOLD_CACHED_QUERIES_DURATION = 1000 * 60 * 3; // 3 minutes

export interface IQueryCacheOptions {
  /** Number of milliseconds to cache query for */
  expirationTime?: number;
}

export function extractQueryCacheOptions(
  args: { cache?: IQueryCacheOptions } = {},
) {
  const { cache } = args;
  delete args.cache;
  return cache;
}

@Injectable({
  providedIn: 'root',
})
export class QueryCacheService {
  private readonly cache = new Map<string, Observable<unknown>>();

  resolve<T>(
    service: string,
    method: string,
    args: unknown[],
    queryFactory: () => Observable<T>,
    options: IQueryCacheOptions = {},
  ): Observable<T> {
    const key = stringify({ service, method, args });

    let query = this.cache.get(key) as Observable<T> | undefined;

    if (query) return query;

    const destroy$ = new Subject();
    let subscriberCount = 0;
    let timeout: NodeJS.Timeout | undefined;

    query = queryFactory().pipe(
      takeUntil(destroy$),
      shareReplay(1),
      tapOnSubscribe(() => {
        if (timeout) clearTimeout(timeout);
        subscriberCount++;
      }),
      finalize(() => {
        subscriberCount--;

        if (subscriberCount === 0) {
          // If there are no subscribers, hold onto any cached queries
          // for `HOLD_CACHED_QUERIES_DURATION` milliseconds and then
          // clean them up if there still aren't any new
          // subscribers
          timeout = setTimeout(() => {
            destroy$.next();
            destroy$.complete();
            this.cache.delete(key);
          }, options.expirationTime ?? HOLD_CACHED_QUERIES_DURATION);
        }
      }),
      // Without this delay, very large queries are executed synchronously
      // which can introduce some pauses/jank in the UI. This causes
      // subscribers to use the `async` schedular which overs a miniscule amount of
      // delay for queries but keeps UI performance speedy. I also tried the
      // `asap` schedular but it still has jank.
      delay(0),
    );

    this.cache.set(key, query);

    return query;
  }
}
