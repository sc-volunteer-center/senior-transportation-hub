import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import {
  tap,
  switchMap,
  map,
  distinctUntilChanged,
  filter,
  takeUntil,
  shareReplay,
} from 'rxjs/operators';
import { IUser, IOrganization, IDispatcher, Dispatcher } from '@local/models';
import { of, interval, Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { CookieService } from '@local/ngx-cookie';
import { AuthService } from '../auth.service';
import { QueryCacheService } from './query-cache.service';

interface IDispatcherShiftCookie {
  shiftId: string;
  organizationId: string;
  dispatcherId: string;
}

interface IUserState {
  user?: IUser;
  organization?: IOrganization;
  dispatcher?: Dispatcher;
  dispatcherShiftCookie?: IDispatcherShiftCookie;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private userLoaded = false;
  private organizationLoaded = false;
  private dispatcherLoaded = false;

  private user$ = new BehaviorSubject<IUser | undefined>(undefined);
  private organization$ = new BehaviorSubject<IOrganization | undefined>(
    undefined,
  );
  private dispatcher$ = new BehaviorSubject<IDispatcher | undefined>(undefined);
  private dispatcherShiftCookie$ = new BehaviorSubject<
    IDispatcherShiftCookie | undefined
  >(undefined);

  constructor(
    cookieService: CookieService,
    private fstore: AngularFirestore,
    private authService: AuthService,
    private queryCache: QueryCacheService,
  ) {
    // need to load the cookie immediately
    const dispatcherShiftCookie = cookieService.getObject(
      'current_dispatcher_shift',
    ) as IDispatcherShiftCookie | undefined;

    this.dispatcherShiftCookie$.next(dispatcherShiftCookie);

    interval(1000)
      .pipe(
        map(
          () =>
            cookieService.getObject('current_dispatcher_shift') as
              | IDispatcherShiftCookie
              | undefined,
        ),
        distinctUntilChanged((x, y) => {
          if (!x && y) {
            return false;
          } else if (x && !y) {
            return false;
          } else if (!x && !y) {
            return true;
          } else if (x!.shiftId !== y!.shiftId) {
            return false;
          } else if (x!.organizationId !== y!.organizationId) {
            return false;
          } else if (x!.dispatcherId !== y!.dispatcherId) {
            return false;
          } else {
            return true;
          }
        }),
      )
      .subscribe(this.dispatcherShiftCookie$);

    this.authService.firebaseUser
      .pipe(
        switchMap((user) =>
          !user
            ? of(undefined)
            : this.fstore
                .doc<IUser>(`users/${user.email}`)
                .valueChanges()
                .pipe(
                  takeUntil(
                    this.authService.firebaseUser.pipe(filter((user) => !user)),
                  ),
                ),
        ),
        tap((user) => {
          this.userLoaded = true;
          this.user$.next(user);
        }),
        switchMap((user) =>
          !(user && user.organizationId)
            ? of(undefined)
            : this.fstore
                .doc<IOrganization>(`organizations/${user.organizationId}`)
                .valueChanges()
                .pipe(
                  takeUntil(
                    this.authService.firebaseUser.pipe(filter((user) => !user)),
                  ),
                ),
        ),
        tap((organization) => {
          this.organizationLoaded = true;
          this.organization$.next(organization);
        }),
      )
      .subscribe();

    this.dispatcherShiftCookie$
      .pipe(
        switchMap((cookie) =>
          !cookie
            ? of(undefined)
            : this.authService.firebaseUser.pipe(
                filter((user) => !!user),
                switchMap(() =>
                  this.fstore
                    .doc<IDispatcher>(
                      `organizations/${cookie.organizationId}/dispatchers/` +
                        cookie.dispatcherId,
                    )
                    .valueChanges()
                    .pipe(
                      takeUntil(
                        this.authService.firebaseUser.pipe(
                          filter((user) => !user),
                        ),
                      ),
                    ),
                ),
              ),
        ),
      )
      .subscribe((dispatcher) => {
        this.dispatcherLoaded = true;
        this.dispatcher$.next(dispatcher);
      });
  }

  getUserState(): Observable<IUserState> {
    const query = () =>
      combineLatest([
        this.dispatcher$,
        this.organization$,
        this.user$,
        this.dispatcherShiftCookie$,
      ]).pipe(
        filter(
          () =>
            this.userLoaded && this.organizationLoaded && this.dispatcherLoaded,
        ),
        map(([dispatcher, organization, user, dispatcherShiftCookie]) => ({
          user,
          organization,
          dispatcher: dispatcher && new Dispatcher(dispatcher),
          dispatcherShiftCookie,
        })),
        shareReplay(1),
      );

    return this.queryCache.resolve('UserService', 'getUserState', [], query, {
      expirationTime: Number.POSITIVE_INFINITY,
    });
  }

  getCurrentDispatcherInfo(): Observable<{
    user: IUser;
    organization: IOrganization;
    dispatcher?: Dispatcher;
    dispatcherShiftCookie?: IDispatcherShiftCookie;
  } | null>;

  getCurrentDispatcherInfo<A, B>(
    base: A,
    fn: (info: {
      user: IUser;
      organization: IOrganization;
      dispatcher?: Dispatcher;
      dispatcherShiftCookie?: IDispatcherShiftCookie;
    }) => Observable<B>,
  ): Observable<B | A>;

  getCurrentDispatcherInfo(
    base?: unknown,
    fn?: (info: {
      user: IUser;
      organization: IOrganization;
      dispatcher?: Dispatcher;
      dispatcherShiftCookie?: IDispatcherShiftCookie;
    }) => Observable<unknown>,
  ) {
    const obs = this.getUserState().pipe(
      map((state) => (state.user && state.organization ? state : null)),
    ) as Observable<{
      user: IUser;
      organization: IOrganization;
      dispatcher?: Dispatcher;
      dispatcherShiftCookie?: IDispatcherShiftCookie;
    } | null>;

    if (!fn) return obs;

    return obs.pipe(switchMap((info) => (!info ? of(base) : fn(info))));
  }
}
