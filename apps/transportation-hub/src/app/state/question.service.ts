import { Injectable } from '@angular/core';
import { switchMap, filter, toArray, map, share } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IQuestion, instantiateClientQuestion } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction, OperatorFunction } from 'rxjs';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';

export interface IGetQuestionsParams {
  retired?: boolean;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getQuestion(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IQuestion>(
                  `organizations/${info.organization.uid}/clientQuestions/` +
                    id,
                )
                .valueChanges(),
        ),
        map((question) => instantiateClientQuestion(question)),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'QuestionService',
      'getQuestion',
      [id],
      query,
      cacheOptions,
    );
  }

  getQuestions(args: IGetQuestionsParams = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of([])
            : this.fstore
                .collection<IQuestion>(
                  `organizations/${info.organization.uid}/clientQuestions`,
                  (ref) =>
                    args.retired
                      ? ref
                          .where('retiredAt', '>', new Date(1970, 1, 1))
                          .orderBy('retiredAt')
                      : ref.where('retiredAt', '==', null).orderBy('order'),
                )
                .valueChanges(),
        ),
        map((questions) => {
          // we use an observable to filter because it is more performant than
          // chaining `.filter()` calls.

          const operators: (
            | MonoTypeOperatorFunction<any>
            | OperatorFunction<any, any>
          )[] = [];

          if (typeof args.retired !== 'undefined') {
            operators.push(filter((question) => !!question.retiredAt));
            operators.push(toArray());
            operators.push(
              map((quess: IQuestion[]) =>
                quess.sort((a, b) => {
                  const x = a.retiredAt!.toDate().valueOf();
                  const y = b.retiredAt!.toDate().valueOf();

                  if (x > y) return 1;
                  if (x < y) return -1;
                  return 0;
                }),
              ),
            );
          } else {
            operators.push(filter((question) => !question.retiredAt));
            operators.push(toArray());
            operators.push(
              map((quess: IQuestion[]) =>
                quess.sort((a, b) => {
                  const x = a.order;
                  const y = b.order;

                  if (x > y) return 1;
                  if (x < y) return -1;
                  return 0;
                }),
              ),
            );
          }

          let obs = from(questions);

          operators.forEach((filt) => (obs = obs.pipe(filt)));

          let filteredQuestions: IQuestion[];

          obs.subscribe((vals) => (filteredQuestions = vals as any));

          return filteredQuestions!;
        }),
        map((questions) =>
          questions.map((question) => instantiateClientQuestion(question)!),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'QuestionService',
      'getQuestions',
      [args],
      query,
      cacheOptions,
    );
  }
}
