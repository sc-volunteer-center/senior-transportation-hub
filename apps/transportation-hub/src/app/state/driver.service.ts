import { Injectable } from '@angular/core';
import { switchMap, filter, toArray, map, take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IDriver, Driver } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction, OperatorFunction } from 'rxjs';
import type firebase from 'firebase/compat';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';
import { isDefined } from './utilities';

export interface IGetDriversParams {
  retired?: boolean;
  hasFlag?: boolean;
  getOnce?: boolean;
  onVacation?: boolean;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class DriverService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getDriver(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IDriver>(
                  `organizations/${info.organization.uid}/drivers/` + id,
                )
                .valueChanges(),
        ),
        map((driver) => (driver && new Driver(driver)) || null),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'DriverService',
      'getDriver',
      [id],
      query,
      cacheOptions,
    );
  }

  getDrivers(args: IGetDriversParams = {}) {
    const query = () =>
      this.userService
        .getCurrentDispatcherInfo([], (info) => {
          const base = this.fstore.collection<IDriver>(
            `organizations/${info.organization.uid}/drivers`,
            (ref) => {
              let result: firebase.firestore.Query = ref;

              if (isDefined(args.retired)) {
                result = args.retired
                  ? result.where('retiredAt', '!=', null)
                  : result.where('retiredAt', '==', null);
              }

              return result;
            },
          );

          if (args.getOnce) {
            return base.get({ source: 'server' }).pipe(
              take(1),
              map((snaps) => snaps.docs.map((snap) => snap.data())),
            );
          } else {
            return base.valueChanges();
          }
        })
        .pipe(
          map((drivers) => {
            // we use an observable to filter because it is more performant than
            // chaining `.filter()` calls.

            const operators: Array<
              MonoTypeOperatorFunction<IDriver> | OperatorFunction<any, any>
            > = [];

            if (isDefined(args.retired)) {
              operators.push(
                filter((driver) => !!driver.retiredAt === args.retired!),
              );
            }

            if (args.hasFlag !== undefined) {
              operators.push(
                filter((driver) => {
                  if (args.hasFlag) {
                    if (driver.flags === undefined) return false;
                    return driver.flags > 0;
                  } else {
                    if (driver.flags === undefined) return true;
                    return driver.flags === 0;
                  }
                }),
              );
            }

            if (args.onVacation !== undefined) {
              const now = Date.now();

              operators.push(
                filter((driver: IDriver) => {
                  const onVacation =
                    driver.vacationStartsAt.toDate().valueOf() <= now &&
                    driver.vacationEndsAt.toDate().valueOf() > now;

                  return args.onVacation ? onVacation : !onVacation;
                }),
              );
            }

            operators.push(toArray());

            let filteredDrivers: IDriver[];

            from(drivers)
              .pipe(...(operators as []))
              .subscribe((vals) => (filteredDrivers = vals as any));

            return filteredDrivers!;
          }),
          map((drivers) => drivers.map((driver) => new Driver(driver))),
        );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'DriverService',
      'getDrivers',
      [args],
      query,
      cacheOptions,
    );
  }
}
