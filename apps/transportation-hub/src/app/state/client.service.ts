import { Injectable } from '@angular/core';
import { filter, toArray, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IClient, Client } from '@local/models';
import { UserService } from './user.service';
import { from, MonoTypeOperatorFunction } from 'rxjs';
import type firebase from 'firebase/compat';
import {
  extractQueryCacheOptions,
  IQueryCacheOptions,
  QueryCacheService,
} from './query-cache.service';
import { isDefined } from './utilities';

export interface IGetClientsParams {
  retired?: boolean;
  hasFlag?: boolean;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getClient(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService
        .getCurrentDispatcherInfo(null, (info) => {
          return this.fstore
            .doc<IClient>(
              `organizations/${info.organization.uid}/clients/${id}`,
            )
            .valueChanges();
        })
        .pipe(map((client) => (client && new Client(client)) || null));

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'ClientService',
      'getClient',
      [id],
      query,
      cacheOptions,
    );
  }

  getClients(args: IGetClientsParams = {}) {
    const query = () =>
      this.userService
        .getCurrentDispatcherInfo([], (info) =>
          this.fstore
            .collection<IClient>(
              `organizations/${info.organization.uid}/clients`,
              (ref) => {
                let result: firebase.firestore.Query = ref;

                if (isDefined(args.retired)) {
                  result = args.retired
                    ? result.where('retiredAt', '!=', null)
                    : result.where('retiredAt', '==', null);
                }

                return result;
              },
            )
            .valueChanges(),
        )
        .pipe(
          map((clients) => {
            // we use an observable to filter because it is theoretically more performant than
            // chaining `.filter()` calls.

            const filters: MonoTypeOperatorFunction<IClient>[] = [];

            if (typeof args.retired !== 'undefined') {
              filters.push(
                filter((client) => !!client.retiredAt === args.retired!),
              );
            }

            if (args.hasFlag !== undefined) {
              filters.push(
                filter((client) => {
                  if (args.hasFlag) {
                    if (client.flags === undefined) return false;
                    return client.flags > 0;
                  } else {
                    if (client.flags === undefined) return true;
                    return client.flags === 0;
                  }
                }),
              );
            }

            let obs = from(clients);

            filters.forEach((filt) => (obs = obs.pipe(filt)));

            let filteredClients: IClient[];

            obs.pipe(toArray()).subscribe((vals) => (filteredClients = vals));

            return filteredClients!;
          }),
          map((clients) => clients.map((client) => new Client(client))),
        );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'ClientService',
      'getClients',
      [args],
      query,
      cacheOptions,
    );
  }
}
