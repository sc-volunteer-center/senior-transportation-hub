import { Injectable } from '@angular/core';
import { switchMap, filter, toArray, map, take } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IDispatcher, Dispatcher } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction, OperatorFunction } from 'rxjs';
import {
  IQueryCacheOptions,
  QueryCacheService,
  extractQueryCacheOptions,
} from './query-cache.service';

export interface IGetDispatchersParams {
  retired?: boolean;
  getOnce?: boolean;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class DispatcherService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getDispatcher(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IDispatcher>(
                  `organizations/${info.organization.uid}/dispatchers/` + id,
                )
                .valueChanges(),
        ),
        map((dispatcher) => (dispatcher && new Dispatcher(dispatcher)) || null),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'DispatcherService',
      'getDispatcher',
      [id],
      query,
      cacheOptions,
    );
  }

  getDispatchers(args: IGetDispatchersParams = {}) {
    const query = () =>
      this.userService
        .getCurrentDispatcherInfo([], (info) => {
          const base = this.fstore.collection<IDispatcher>(
            `organizations/${info.organization.uid}/dispatchers`,
            (ref) => {
              const result = args.retired
                ? ref.where('retiredAt', '>', new Date(1970, 1, 1))
                : ref.where('retiredAt', '==', null);

              return result;
            },
          );

          if (args.getOnce) {
            return base.get({ source: 'server' }).pipe(
              take(1),
              map((snaps) => snaps.docs.map((snap) => snap.data())),
            );
          } else {
            return base.valueChanges();
          }
        })
        .pipe(
          map((dispatchers) => {
            // we use an observable to filter because it is more performant than
            // chaining `.filter()` calls.

            const operators: Array<
              MonoTypeOperatorFunction<IDispatcher> | OperatorFunction<any, any>
            > = [];

            if (typeof args.retired !== 'undefined') {
              operators.push(
                filter(
                  (dispatcher) => !!dispatcher.retiredAt === args.retired!,
                ),
              );
            }

            operators.push(toArray());

            let filteredDispatchers: IDispatcher[];

            from(dispatchers)
              .pipe(...(operators as []))
              .subscribe((vals) => (filteredDispatchers = vals as any));

            return filteredDispatchers!;
          }),
          map((dispatchers) =>
            dispatchers.map((dispatcher) => new Dispatcher(dispatcher)),
          ),
        );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'DispatcherService',
      'getDispatchers',
      [args],
      query,
      cacheOptions,
    );
  }
}
