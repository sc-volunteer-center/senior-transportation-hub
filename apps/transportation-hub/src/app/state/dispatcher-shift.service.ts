import { Injectable } from '@angular/core';
import { switchMap, take, filter, toArray, map, skip } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { IDispatcherShift, DispatcherShift } from '@local/models';
import { UserService } from './user.service';
import { of, from, MonoTypeOperatorFunction } from 'rxjs';
import { isAfter, isBefore } from 'date-fns';
import {
  extractQueryCacheOptions,
  IQueryCacheOptions,
  QueryCacheService,
} from './query-cache.service';

export interface IGetDispatcherShiftsParams {
  dispatcherId?: string;
  startDate?: Date;
  endDate?: Date;
  limit?: number;
  startAfter?: AngularFirestoreDocument<IDispatcherShift>;
  cache?: IQueryCacheOptions;
}

@Injectable({
  providedIn: 'root',
})
export class DispatcherShiftService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getDispatcherShift(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IDispatcherShift>(
                  `organizations/${info.organization.uid}/dispatcherShifts/` +
                    id,
                )
                .valueChanges(),
        ),
        map(
          (dispatcherShift) =>
            (dispatcherShift && new DispatcherShift(dispatcherShift)) || null,
        ),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'DispatcherShiftService',
      'getDispatcherShift',
      [id],
      query,
      cacheOptions,
    );
  }

  getDispatcherShifts(args: IGetDispatcherShiftsParams = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of([])
            : this.fstore
                .collection<IDispatcherShift>(
                  `organizations/${info.organization.uid}/` +
                    'dispatcherShifts',
                  (ref) => {
                    let result = ref.orderBy('startAt');

                    if (args.dispatcherId) {
                      result = result.where(
                        'dispatcherId',
                        '==',
                        args.dispatcherId,
                      );
                    }

                    if (args.startDate) {
                      result = result.where('startAt', '>=', args.startDate);
                    }

                    if (args.endDate) {
                      result = result.where('startAt', '<=', args.endDate);
                    }

                    if (args.limit) {
                      result = result.limit(args.limit);
                    }

                    if (args.startAfter) {
                      result = result.startAfter(args.startAfter);
                    }

                    return result;
                  },
                )
                .valueChanges(),
        ),
        map((dispatcherShifts) => {
          // we use an observable to filter because it is more performant than
          // chaining `.filter()` calls.

          const filters: MonoTypeOperatorFunction<IDispatcherShift>[] = [];

          if (args.dispatcherId) {
            filters.push(
              filter(
                (dispatcherShift) =>
                  dispatcherShift.dispatcherId === args.dispatcherId,
              ),
            );
          }

          if (args.startAfter) {
            const afterId = args.startAfter.ref.id;

            const skipNumber =
              1 + dispatcherShifts.findIndex((ds) => ds.uid === afterId);

            filters.push(skip(skipNumber));
          }

          if (args.startDate) {
            filters.push(
              filter((dispatcherShift) =>
                isAfter(dispatcherShift.startAt.toDate(), args.startDate!),
              ),
            );
          }

          if (args.endDate) {
            filters.push(
              filter((dispatcherShift) =>
                isBefore(dispatcherShift.startAt.toDate(), args.endDate!),
              ),
            );
          }

          if (args.limit) {
            filters.push(take(args.limit));
          }

          let obs = from(dispatcherShifts);

          filters.forEach((filt) => (obs = obs.pipe(filt)));

          let filteredDispatcherShifts: IDispatcherShift[];

          obs
            .pipe(toArray())
            .subscribe((vals) => (filteredDispatcherShifts = vals));

          return filteredDispatcherShifts!;
        }),
        map((dispatcherShifts) =>
          dispatcherShifts.map(
            (dispatcherShift) => new DispatcherShift(dispatcherShift),
          ),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'DispatcherShiftService',
      'getDispatcherShifts',
      [args],
      query,
      cacheOptions,
    );
  }
}
