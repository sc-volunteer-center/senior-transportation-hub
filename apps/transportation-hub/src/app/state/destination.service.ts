import { Injectable } from '@angular/core';
import { switchMap, filter, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IDestination, Destination } from '@local/models';
import { UserService } from './user.service';
import { of } from 'rxjs';
import {
  extractQueryCacheOptions,
  IQueryCacheOptions,
  QueryCacheService,
} from './query-cache.service';

@Injectable({
  providedIn: 'root',
})
export class DestinationService {
  constructor(
    private fstore: AngularFirestore,
    private userService: UserService,
    private queryCache: QueryCacheService,
  ) {}

  getDestination(id: string, options: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of(undefined)
            : this.fstore
                .doc<IDestination>(
                  `organizations/${info.organization.uid}/destinations/` + id,
                )
                .valueChanges(),
        ),
        filter((destination) => !!(destination && destination.updatedAt)),
        map(
          (destination) =>
            (destination && new Destination(destination)) || null,
        ),
      );

    const cacheOptions = extractQueryCacheOptions(options);

    return this.queryCache.resolve(
      'DestinationService',
      'getDestination',
      [id],
      query,
      cacheOptions,
    );
  }

  getDestinations(args: { cache?: IQueryCacheOptions } = {}) {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        switchMap((info) =>
          !info
            ? of([])
            : this.fstore
                .collection<IDestination>(
                  `organizations/${info.organization.uid}/destinations`,
                )
                .valueChanges(),
        ),
        // the server is returning updatedAt === null after an update
        // and then immediately sending down a new document with the actual updatedAt value
        map((destinations) =>
          destinations
            .filter((destination) => !!(destination && destination.updatedAt))
            .map((destination) => new Destination(destination)),
        ),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.queryCache.resolve(
      'DestinationService',
      'getDestinations',
      [],
      query,
      cacheOptions,
    );
  }
}
