# Transportation Hub App State

The initial production version of this app didn't cache any firestore documents, instead retreiving them directly from the database each time. Shortly after initially going into production, we found that users were reading _a lot_ of documents from the database and actually hit the daily "free tier" read limit of 50,000 documents on one day.

Given that the number of documents in the database will only increase with time (making this problem more likely to pop up in the future), the decision was made to update the app to only fetch documents once, cache them in ngrx/store, and then subscribe to future updates to those documents. The app (almost) never reads documents directly from the database. Instead, it reads docs from ngrx/store.

Each store reducer has a service associated with it. The service handles subscribing to store data. The service also tracks if a particular document has been loaded yet. If a document is requested which hasn't been loaded, then the service will fetch the document from firestore and cache it. It only ever does this once for a session. Each service also subscribes to all changes to it's associated collections. As individual changes come in, the service dispatches actions to keep the store up to date.

This also means that all querying in components is processed locally. This could become problematic in the future if there are thousands of documents loaded into a particular reducer. If it does become problematic in the future, a possible solution would be to move the ngrx/store into a shared web worker or service worker. A shared web worker is probably the ideal choice for this app, but, if for some reason the limited browser support for shared web workers is a problem, a service worker could probably be used instead (the fact that the service worker would persist between sessions would be undesireable in this case, however).

The app's components themselves generally don't interact with reducer services directly. Instead, the FirestoreService (`firestore.service.ts`) uses the reducer services internally and provides all the public CRUD methods this app's components rely on.
