import { Injectable } from '@angular/core';
import { Address, IAddress, getAddressURLString } from '@local/models';
import { directionsMapLink } from '@local/utilities';

@Injectable({
  providedIn: 'root',
})
export class MapsService {
  private readonly apiKey = 'AIzaSyB82hOzLB1hEYpz1xedcdEmr3TLV7glRMw';
  // the global `google` object is added via a script tag in `index.html`
  private readonly distanceService = new google.maps.DistanceMatrixService();
  private readonly geocodeService = new google.maps.Geocoder();

  constructor() {}

  async getLatLong(address: {
    street: string;
    city: string;
    zip: string;
    state: string;
  }) {
    const results = await new Promise<google.maps.GeocoderResult[] | null>(
      (resolve, reject) => {
        this.geocodeService.geocode(
          {
            address: getAddressURLString(address),
          },
          (results, status: any) => {
            if (status === 'OK') {
              resolve(results);
            } else {
              reject(status);
            }
          },
        );
      },
    );

    if (!results) {
      throw new Error('GeocoderResult is null');
    }

    return {
      latitude: results[0].geometry.location.lat(),
      longitude: results[0].geometry.location.lng(),
    };
  }

  addressMapUrl(address: Address) {
    return (
      'https://www.google.com/maps/embed/v1/place' +
      `?key=${this.apiKey}` +
      `&q=${getAddressURLString(address)}`
    );
  }

  directionsMapLink = directionsMapLink;

  directionsMapUrl(addresses: Address[]) {
    if (addresses.length < 2) {
      throw new Error('Must provide a origin and destination address');
    }

    const origin = addresses.shift()!;
    const destination = addresses.pop()!;
    const waypoints = addresses;

    let waypointString = '';

    if (waypoints && waypoints.length > 0) {
      waypointString = `&waypoints=${waypoints
        .map((waypoint) => getAddressURLString(waypoint))
        .join('|')}`;
    }

    return (
      'https://www.google.com/maps/embed/v1/directions' +
      `?key=${this.apiKey}` +
      `&origin=${getAddressURLString(origin)}` +
      `&destination=${getAddressURLString(destination)}` +
      '&mode=driving' +
      '&zoom=13' +
      waypointString
    );
  }

  /**
   * @return meters and seconds
   */
  async directionsDistance(addresses: Address[]): Promise<{
    seconds: number;
    meters: number;
  }> {
    if (addresses.length < 2) {
      throw new Error('Must provide at least two addresses');
    }

    const origins: string[] = [];
    const destinations: string[] = [];

    let prev = getAddressURLString(addresses[0]);

    addresses.forEach((address, index) => {
      if (index === 0) {
        return;
      }

      origins.push(prev);
      prev = getAddressURLString(address);
      destinations.push(prev);
    });

    const response =
      await new Promise<google.maps.DistanceMatrixResponse | null>(
        (resolve, reject) => {
          this.distanceService.getDistanceMatrix(
            {
              travelMode: google.maps.TravelMode.DRIVING,
              unitSystem: google.maps.UnitSystem.METRIC,
              origins,
              destinations,
            },
            (response, status: any) => {
              if (status == 'OK') {
                resolve(response);
              } else {
                reject(status);
              }
            },
          );
        },
      );

    if (!response) {
      throw new Error('DistanceMatrixResponse was null');
    }

    let seconds = 0;
    let meters = 0;

    response.rows.forEach((row) => {
      row.elements.forEach((element) => {
        seconds += element.duration.value;
        meters += element.distance.value;
      });
    });

    return { seconds, meters };
  }

  /** Bird's distance in miles */
  birdsDistance(addresses: Address[]) {
    if (addresses.length < 2) {
      throw new Error('Must provide at least two addresses');
    }

    if (addresses.some((address) => !address.latitude || !address.longitude)) {
      console.error(
        'Provided addresses must have known latitute and longitude',
      );
      return;
    }

    let prev = addresses[0];
    let distance = 0;

    addresses.forEach((address, index) => {
      if (index === 0) {
        return;
      }

      distance =
        distance +
        getDistanceFromLatLonInMi(
          prev.latitude!,
          prev.longitude!,
          address.latitude!,
          address.longitude!,
        );

      prev = address;
    });

    return distance;
  }
}

function getDistanceFromLatLonInMi(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number,
) {
  return getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) * 0.6213712;
}

function getDistanceFromLatLonInKm(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number,
) {
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1); // deg2rad below
  const dLon = deg2rad(lon2 - lon1);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return d;
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}
