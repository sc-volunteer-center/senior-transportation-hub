import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GoogleService {
  private gapiAuth?: Promise<gapi.auth2.GoogleAuth>;

  constructor() {
    // Chrome lets us load the SDK on demand, but firefox will block the popup
    // when loaded on demand. If we preload in the constructor,
    // then firefox won't block the popup.
    this.googleSDK();
  }

  async signinGoogle() {
    const authClient = (await this.googleSDK()) as gapi.auth2.GoogleAuth;
    const googleUser = await authClient.signIn();
    const profile = googleUser.getBasicProfile();

    return {
      type: 'GOOGLE',
      token: googleUser.getAuthResponse().id_token as string,
      uid: profile.getId() as string,
      firstName: profile.getGivenName() as string,
      lastName: profile.getFamilyName() as string,
      photoUrl: profile.getImageUrl() as string,
      emailAddress: profile.getEmail() as string,
    };
  }

  // async grantOfflineAccess() {
  //   const authClient: gapi.auth2.GoogleAuth = (await this.googleSDK()) as any;

  //   try {
  //     const { code } = await authClient.grantOfflineAccess();

  //     return code;
  //   } catch (e) {
  //     // access was denied
  //     return null;
  //   }
  // }

  // annoyingly there is some sort of bug with typescript or the `gapi.auth2`
  // typings that seems to prohibit awaiting a promise of type `Promise<gapi.auth2.GoogleAuth>`
  // https://stackoverflow.com/questions/54299128/type-is-referenced-directly-or-indirectly-in-the-fulfillment-callback-of-its-own
  private googleSDK(): Promise<unknown> {
    if (this.gapiAuth) return this.gapiAuth;

    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.defer = true;
    script.src = 'https://apis.google.com/js/api.js?onload=gapiClientLoaded';

    this.gapiAuth = new Promise<void>((res, rej) => {
      (window as any)['gapiClientLoaded'] = res;
      script.onerror = rej;
    })
      .then(() => new Promise((res) => gapi.load('client:auth2', res)))
      .then(() =>
        gapi.client.init({
          apiKey: environment.google.apiKey,
          clientId: environment.google.clientId,
          discoveryDocs: environment.google.discoveryDocs,
          scope: environment.google.scopes.join(' '),
        }),
      )
      .catch((err) => {
        console.error('there was an error initializing the client', err);
        return Promise.reject(err);
      })
      .then(() => gapi.auth2.getAuthInstance());

    document.body.appendChild(script);

    return this.gapiAuth;
  }
}
