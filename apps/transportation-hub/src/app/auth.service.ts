import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
// import { GoogleService } from './google.service';
import { take, switchMap, delay } from 'rxjs/operators';
import firebase from 'firebase/compat/app';
import { MatDialog } from '@angular/material/dialog';
import { PersistancePopupComponent } from './persistance-popup.component';
import { Router } from '@angular/router';
import { BehaviorSubject, of, Subject, Observable } from 'rxjs';
import { API_VERSION } from './utilities';

const FIRESTORE_LOCALSTORAGE_KEY = 'firestorePersistenceSetting';

/**
 * A recurring problem was that the persisted local firestore DB
 * schema would be out of date when a new version of the app
 * was released and would cause crashes because the new code
 * would expect firestore documents with a different schema than what
 * was located in the persisted DB (because the "real" DB had received
 * a migration that the local DB hadn't).
 *
 * To fix, we save (in localstore) what version of the API is saved locally
 * in the firestore IndexedDB and if the wrong version is present we clear the local DB.
 */
export const CURRENTLY_RUNNING_API_KEY = 'firestorePersistenceAPI';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  readonly persistenceSetting$ = new BehaviorSubject<boolean | null>(null);

  readonly firebaseUser = this.persistenceSetting$.pipe(
    switchMap((p) => (typeof p === 'boolean' ? this.fireAuth.user : of(null))),
  );

  constructor(
    private fireAuth: AngularFireAuth,
    // private googleService: GoogleService,
    private dialog: MatDialog,
    private router: Router,
  ) {
    this.persistenceSetting$.next(getPersistenceSetting());
  }

  /**
   * **Logs out of the Hub**
   *
   * - This method is patched by the FirestoreService when it
   *   is created.
   */
  logout() {
    return this.authLogout();
  }

  /**
   * user signs in
   * get related organization
   * see if organization has access token
   *  - if not, prompt for offline access
   *  - if yes, finish login
   *
   * - if organizatoin has access token error,
   *   show error and re-prompt for offline access
   */
  async startLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();

    provider.addScope('email');

    const userCredential = await this.fireAuth.signInWithPopup(provider);

    // const googleUser = await this.googleService.signinGoogle();

    const firebaseUser = userCredential.user;

    // // Check to see if user is already signed into firebase
    // if (!firebaseUserIsGoogleUser(firebaseUser, googleUser.uid)) {
    //   // Build Firebase credential with the Google ID token.
    //   const credential = firebase.auth.GoogleAuthProvider.credential(
    //     googleUser.token,
    //   );

    //   // Sign in with credential from the Google user.
    //   await this.fireAuth.signInWithCredential(credential);

    //   // Fetch firebase user
    //   firebaseUser = await this.fireAuth.user.pipe(take(1)).toPromise();

    if (!firebaseUser || !firebaseUser!.email) {
      throw new Error('Failed to signin to firebase');
    }
    // }

    await this.setPersistence();

    return firebaseUser;
  }

  async setPersistence(): Promise<boolean> {
    // NOTE: I tried replacing firestore persistance with just my new query cache
    // but I found that $$$ costs seemed to double. Even doubling, costs are still
    // very small but ultimately it seems better to just stick with persistance.
    // Because of this, I decided to keep local persistance turned on.
    const persist: boolean | undefined = await this.dialog
      .open(PersistancePopupComponent, {
        width: '600px',
        disableClose: true,
        closeOnNavigation: false,
      })
      .afterClosed()
      .toPromise();

    this.persistenceSetting$.next(!!persist);

    localStorage.setItem(
      FIRESTORE_LOCALSTORAGE_KEY,
      JSON.stringify(this.persistenceSetting$.value),
    );

    if (persist) {
      localStorage.setItem(
        CURRENTLY_RUNNING_API_KEY,
        JSON.stringify(API_VERSION),
      );
    }

    return this.persistenceSetting$.value!;
  }

  private async authLogout() {
    this.persistenceSetting$.next(null);
    localStorage.removeItem(FIRESTORE_LOCALSTORAGE_KEY);
    localStorage.removeItem(CURRENTLY_RUNNING_API_KEY);

    await Promise.all([
      // gapi.auth2.getAuthInstance()?.signOut(),
      this.fireAuth.signOut(),
    ]);

    await this.router.navigate(['/']);
  }
}

function firebaseUserIsGoogleUser(
  firebaseUser: firebase.User | null,
  googleID: string,
) {
  return (
    firebaseUser &&
    firebaseUser.providerData.some(
      (provider) =>
        !!provider &&
        provider.providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
        provider.uid === googleID,
    )
  );
}

export function getPersistenceSetting() {
  const persistence = localStorage.getItem(FIRESTORE_LOCALSTORAGE_KEY);

  if (typeof persistence === 'string') {
    return JSON.parse(persistence) as boolean;
  }

  return persistence;
}
