import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceWarningComponent } from './maintenance-warning.component';

const routes: Routes = [{ path: '', component: MaintenanceWarningComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceWarningRoutingModule { }
