import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceWarningRoutingModule } from './maintenance-warning-routing.module';
import { MaintenanceWarningComponent } from './maintenance-warning.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [CommonModule, MaintenanceWarningRoutingModule, MatButtonModule],
  declarations: [MaintenanceWarningComponent],
})
export class MaintenanceWarningModule {}
