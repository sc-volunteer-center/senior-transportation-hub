import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { API_VERSION, IApiDoc } from '../../utilities';
import { Subscription } from 'rxjs';
import { AppUpdateService } from '../shared/update-available/app-update.service';
import { take, filter } from 'rxjs/operators';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-maintenance-warning',
  templateUrl: './maintenance-warning.component.html',
  styleUrls: ['./maintenance-warning.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MaintenanceWarningComponent implements OnInit, OnDestroy {
  message = `The Senior Transportation Hub is currently being updated and will be
  available again shortly. You won't be able to use it until the update is
  complete.`;

  apiDoc?: IApiDoc;

  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private fs: AngularFirestore,
    private updateService: AppUpdateService,
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.fs
        .doc<IApiDoc>('apiVersion/latest')
        .valueChanges()
        .subscribe((doc) => {
          if (doc?.message) {
            this.message = doc.message;
          }

          this.apiDoc = doc;
          this.cdr.markForCheck();

          if (doc?.version !== API_VERSION) return;

          this.router.navigate(['/']);
        }),
      this.updateService.updateAvailable
        .pipe(
          filter((u) => u),
          take(1),
        )
        .subscribe(() => {
          this.updateService.updateNow();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  signOut() {
    return this.authService.logout();
  }
}
