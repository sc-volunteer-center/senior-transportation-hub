import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome.component';
import { WelcomeGuard } from './welcome.guard';
import { WelcomeBreadcrumbComponent } from './welcome-breadcrumb.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [WelcomeGuard],
    component: WelcomeComponent,
    data: {
      breadcrumb: WelcomeBreadcrumbComponent,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WelcomeRoutingModule {}
