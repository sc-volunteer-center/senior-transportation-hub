import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-welcome-breadcrumb',
  template: `
    Welcome
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WelcomeBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
