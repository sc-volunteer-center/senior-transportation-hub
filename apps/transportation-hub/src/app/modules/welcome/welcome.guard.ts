import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserService } from '../../state';
import { AuthService } from '../../auth.service';

@Injectable({
  providedIn: 'root',
})
export class WelcomeGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.getUserState().pipe(
      tap((info) => {
        if (!info.organization) return;

        if (
          info.organization.adminEmailAddresses.includes(
            info.user!.emailAddress,
          )
        ) {
          return this.router.navigate(['/admin/overview']);
        }

        return this.router.navigate(['/dispatcher/signin']);
      }),
      map((info) => !info.organization),
    );
  }
}
