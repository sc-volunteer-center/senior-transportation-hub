import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './welcome.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { WelcomeBreadcrumbComponent } from './welcome-breadcrumb.component';
import { IsLoadingModule } from '@service-work/is-loading';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        IsLoadingModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatToolbarModule,
        MatTooltipModule,
        ReactiveFormsModule,
        WelcomeRoutingModule,
        SharedModule,
    ],
    declarations: [WelcomeComponent, WelcomeBreadcrumbComponent]
})
export class WelcomeModule {}
