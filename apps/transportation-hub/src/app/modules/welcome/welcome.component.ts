import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, from } from 'rxjs';
import { FirestoreService } from '../../firestore.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IsLoadingService } from '@service-work/is-loading';
import { IUser } from '@local/models';
import { UserService } from '../../state';
import { Title } from '@angular/platform-browser';
import { filter, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-welcome',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  user: IUser | null = null;

  organizationForm: FormGroup;

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private loadingService: IsLoadingService,
    private router: Router,
    private userService: UserService,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.titleService.setTitle(`Welcome | Hub`);

    this.organizationForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      dispatcherEmailAddress: ['', [Validators.required, Validators.email]],
      rideRequestEmailPreface: [''],
    });

    this.subscriptions.push(
      this.authService.firebaseUser
        .pipe(
          tap((user) => {
            if (user) return;
            this.router.navigate(['/']);
          }),
          filter((user) => !!user),
          distinctUntilChanged((x, y) => x!.email === y!.email),
          switchMap(() => this.fs.completeLogin()),
          switchMap(() => this.userService.getUserState()),
        )
        .subscribe((info) => {
          this.user = info.user || null;
          this.cdr.markForCheck();

          if (info.user) {
            if (info.dispatcher) {
              this.router.navigate(['/dispatcher/overview']);
            } else if (
              info.organization &&
              info.organization.adminEmailAddresses.includes(
                info.user.emailAddress,
              )
            ) {
              this.router.navigate(['/admin/overview']);
            } else if (
              info.organization &&
              info.organization.dispatcherEmailAddress ===
                info.user.emailAddress
            ) {
              this.router.navigate(['/dispatcher/signin']);
            }
          } else {
            this.router.navigate(['/']);
          }
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  logout() {
    return this.authService.logout().catch((e) => console.error(e));
  }

  async submit() {
    if (this.organizationForm.invalid) {
      return;
    }

    const form = this.organizationForm.value;

    if (!this.user || form.dispatcherEmailAddress === this.user.emailAddress) {
      return;
    }

    await this.loadingService.add(
      this.fs.addOrganization({
        name: form.name,
        dispatcherEmailAddress: form.dispatcherEmailAddress,
        rideRequestEmailPreface: form.rideRequestEmailPreface,
      }),
      { key: 'submit' },
    );

    this.router.navigate(['/admin/overview']);
  }
}
