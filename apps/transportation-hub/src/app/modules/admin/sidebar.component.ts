import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewEncapsulation,
} from '@angular/core';
import { AppUpdateService } from '../shared/update-available/app-update.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="flex-column flex-1" style="overflow-y: auto">
      <a mat-button routerLink="/admin/overview" routerLinkActive="active">
        Overview
      </a>

      <a mat-button routerLink="/admin/dispatchers" routerLinkActive="active">
        Dispatchers
      </a>

      <a
        mat-button
        routerLink="/admin/client-questions"
        routerLinkActive="active"
      >
        Client Questions
      </a>

      <a mat-button routerLink="/admin/settings" routerLinkActive="active">
        Settings
      </a>

      <a mat-button routerLink="/admin/help" routerLinkActive="active">
        Help
      </a>
      <button mat-button (click)="logout()">Logout</button>
    </div>

    <!-- Update available notification -->
    <app-update-available
      *ngIf="appUpdateService.updateAvailable | async"
      class="padding-12 user-box"
    >
    </app-update-available>
  `,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'app-sidebar' },
})
export class SidebarComponent implements OnInit {
  constructor(
    private authService: AuthService,
    public appUpdateService: AppUpdateService,
  ) {}

  ngOnInit() {}

  logout() {
    return this.authService.logout().catch((e) => console.error(e));
  }
}
