import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Question, clientQuestionIs } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { toObservable, timestamp, Timestamp } from '../../../../utilities';
import { MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { EditClientQuestionComponent } from '../edit-client-question/edit-client-question.component';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-client-question-profile',
  templateUrl: './client-question-profile.component.html',
  styleUrls: ['./client-question-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientQuestionProfileComponent implements OnInit {
  question: Question | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private loadingService: IsLoadingService,
    private dialog: MatDialog,
    private router: Router,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.questionId)
        .pipe(
          switchMap((id) => (!id ? of(null) : this.fs.getClientQuestion(id))),
        )
        .subscribe((question) => {
          this.question = question;
          this.titleService.setTitle(
            `"${question?.type}" Question | Admin | Hub`,
          );
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editQuestion() {
    this.dialog.open(EditClientQuestionComponent, {
      width: '600px',
    });
  }

  toggleRetireQuestion() {
    if (!this.question) {
      return;
    }

    let sub: Subscription;

    if (clientQuestionIs(this.question, 'select')) {
      sub = toObservable(
        this.fs.updateClientSelectQuestion(this.question.uid, {
          retiredAt: this.question.retiredAt
            ? null
            : (timestamp() as Timestamp),
        }),
      ).subscribe();
    } else if (clientQuestionIs(this.question, 'text')) {
      sub = toObservable(
        this.fs.updateClientTextQuestion(this.question.uid, {
          retiredAt: this.question.retiredAt
            ? null
            : (timestamp() as Timestamp),
        }),
      ).subscribe();
    } else if (clientQuestionIs(this.question, 'boolean')) {
      sub = toObservable(
        this.fs.updateClientBooleanQuestion(this.question.uid, {
          retiredAt: this.question.retiredAt
            ? null
            : (timestamp() as Timestamp),
        }),
      ).subscribe();
    } else if (clientQuestionIs(this.question, 'section-break')) {
      sub = toObservable(
        this.fs.destroyClientQuestion(this.question.uid),
      ).subscribe(() => this.router.navigate(['/admin/client-questions']));
    } else {
      throw new Error('Unexpected question type');
    }

    this.loadingService.add(sub);

    this.cdr.markForCheck();
  }
}
