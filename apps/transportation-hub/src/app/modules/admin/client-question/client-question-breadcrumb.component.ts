import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Question } from '@local/models';
import { FormBuilder } from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from '../../../state/router-store.service';

@Component({
  selector: 'app-client-question-breadcrumb',
  template: `
    Question
    <!--
    <mat-icon class='side-margin-16'>chevron_right</mat-icon>
    <span class='truncate-text' style='max-width: 200px'>{{question?.text}}</span>
    -->
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientQuestionBreadcrumbComponent implements OnInit {
  question: Question | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private routerStore: RouterStoreService,
    private cdr: ChangeDetectorRef,
    private fs: FirestoreService,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.questionId)
        .pipe(
          switchMap((id) => (!id ? of(null) : this.fs.getClientQuestion(id))),
        )
        .subscribe((question) => {
          this.question = question;
          this.cdr.markForCheck();
        }),
    );
  }
}
