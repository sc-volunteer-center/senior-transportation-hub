import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Question, clientQuestionIs } from '@local/models';
import { Subscription, of, Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-edit-client-question',
  templateUrl: './edit-client-question.component.html',
  styleUrls: ['./edit-client-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditClientQuestionComponent implements OnInit {
  question: Question | null = null;
  questionForm?: FormGroup;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private loadingService: IsLoadingService,
    private ref: MatDialogRef<EditClientQuestionComponent>,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.questionId)
        .pipe(
          switchMap((id) => (!id ? of(null) : this.fs.getClientQuestion(id))),
        )
        .subscribe((question) => {
          this.question = question;
          this.questionForm = undefined;

          if (clientQuestionIs(question, 'select')) {
            this.questionForm = this.fb.group({
              text: [question.text, Validators.required],
              required: [question.required],
              options: [question.options, Validators.required],
              allowOther: [question.allowOther],
            });
          } else if (clientQuestionIs(question, 'text')) {
            this.questionForm = this.fb.group({
              text: [question.text, Validators.required],
              required: [question.required],
              valueType: [question.valueType, Validators.required],
            });
          } else if (clientQuestionIs(question, 'boolean')) {
            this.questionForm = this.fb.group({
              text: [question.text, Validators.required],
              required: [question.required],
            });
          } else if (clientQuestionIs(question, 'section-break')) {
            this.questionForm = this.fb.group({
              text: [question.text, Validators.required],
              body: question.body,
            });
          }

          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async submit() {
    if (!this.question || !this.questionForm || this.questionForm.invalid) {
      return;
    }

    const form = this.questionForm.value;
    let promise: Promise<unknown>;

    if (clientQuestionIs(this.question, 'select')) {
      promise = this.fs.updateClientSelectQuestion(this.question.uid, form);
    } else if (clientQuestionIs(this.question, 'text')) {
      promise = this.fs.updateClientTextQuestion(this.question.uid, form);
    } else if (clientQuestionIs(this.question, 'boolean')) {
      promise = this.fs.updateClientBooleanQuestion(this.question.uid, form);
    } else if (clientQuestionIs(this.question, 'section-break')) {
      promise = this.fs.updateClientQuestionSectionBreak(
        this.question.uid,
        form,
      );
    } else {
      throw new Error('Unexpected question type');
    }

    await this.loadingService.add(promise);

    this.ref.close();
    this.cdr.markForCheck();
  }

  removeSelectOption(option: string) {
    if (!this.questionForm) {
      return;
    }

    let options = this.questionForm.get('options')!.value as string[];
    options = options.filter((opt) => opt !== option);
    this.questionForm.get('options')!.patchValue(options);
  }

  addSelectOption(event: MatChipInputEvent) {
    if (!event.value || !this.questionForm) {
      return;
    }

    const options = this.questionForm.get('options')!.value as string[];
    options.push(event.value);
    this.questionForm.get('options')!.patchValue(options);
    event.input.value = '';
  }
}
