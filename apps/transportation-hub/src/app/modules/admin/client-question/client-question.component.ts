import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-client-question',
  templateUrl: './client-question.component.html',
  styleUrls: ['./client-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientQuestionComponent implements OnInit {
  private subscriptions: Subscription[] = [];

  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
