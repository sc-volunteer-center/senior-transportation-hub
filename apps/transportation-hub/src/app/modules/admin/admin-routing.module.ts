import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from './admin.guard';
import { SettingsComponent } from './settings/settings.component';
import { OverviewComponent } from './overview/overview.component';
import { DispatcherComponent } from '../shared/dispatcher/dispatcher.component';
import { DispatchersComponent } from '../shared/dispatchers/dispatchers.component';
import { ClientQuestionsComponent } from './client-questions/client-questions.component';
import { ClientQuestionGuard } from './client-question.guard';
import { ClientQuestionComponent } from './client-question/client-question.component';
import { OverviewBreadcrumbComponent } from './overview/overview-breadcrumb.component';
import { DispatchersBreadcrumbComponent } from '../shared/dispatchers/dispatchers-breadcrumb.component';
import { DispatcherBreadcrumbComponent } from '../shared/dispatcher/dispatcher-breadcrumb.component';
import { ClientQuestionsBreadcrumbComponent } from './client-questions/client-questions-breadcrumb.component';
import { ClientQuestionBreadcrumbComponent } from './client-question/client-question-breadcrumb.component';
import { SettingsBreadcrumbComponent } from './settings/settings-breadcrumb.component';
import { DispatcherProfileComponent } from '../shared/dispatcher/dispatcher-profile/dispatcher-profile.component';
import { ClientQuestionProfileComponent } from './client-question/client-question-profile/client-question-profile.component';
import { HelpComponent } from './help/help.component';
import { HelpBreadcrumbComponent } from './help/help-breadcrumb.component';
import { DispatcherProfileGuard } from '../shared/dispatcher-profile.guard';
import { AdminRootComponent } from './admin-root.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AdminGuard],
    component: AdminRootComponent,
    children: [
      {
        path: 'overview',
        component: OverviewComponent,
        data: {
          breadcrumb: OverviewBreadcrumbComponent,
        },
      },
      {
        path: 'dispatchers',
        component: DispatchersComponent,
        data: {
          breadcrumb: DispatchersBreadcrumbComponent,
        },
      },
      {
        path: 'dispatcher/:dispatcherId',
        component: DispatcherComponent,
        canActivate: [DispatcherProfileGuard],
        data: {
          breadcrumb: DispatcherBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: DispatcherProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'client-questions',
        component: ClientQuestionsComponent,
        data: {
          breadcrumb: ClientQuestionsBreadcrumbComponent,
        },
      },
      {
        path: 'client-question/:questionId',
        canActivate: [ClientQuestionGuard],
        component: ClientQuestionComponent,
        data: {
          breadcrumb: ClientQuestionBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: ClientQuestionProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: {
          breadcrumb: SettingsBreadcrumbComponent,
        },
      },
      {
        path: 'help',
        component: HelpComponent,
        data: {
          breadcrumb: HelpBreadcrumbComponent,
        },
      },
      {
        path: '**',
        redirectTo: '/admin/overview',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
