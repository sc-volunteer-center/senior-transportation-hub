import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../firestore.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClientQuestionGuard implements CanActivate {
  constructor(private fs: FirestoreService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!next.params['questionId']) {
      return false;
    }

    return this.fs
      .getClientQuestion(next.params['questionId'])
      .pipe(map(question => !!question));
  }
}
