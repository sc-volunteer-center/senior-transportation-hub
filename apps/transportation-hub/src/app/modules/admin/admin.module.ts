import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings/settings.component';
import { OverviewComponent } from './overview/overview.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';

import { NgChartsModule } from 'ng2-charts';
import { IsLoadingModule } from '@service-work/is-loading';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { UpdateAdminComponent } from './settings/update-admin/update-admin.component';
import { ClientQuestionsComponent } from './client-questions/client-questions.component';
import { ClientQuestionComponent } from './client-question/client-question.component';
import { OverviewBreadcrumbComponent } from './overview/overview-breadcrumb.component';
import { ClientQuestionBreadcrumbComponent } from './client-question/client-question-breadcrumb.component';
import { ClientQuestionsBreadcrumbComponent } from './client-questions/client-questions-breadcrumb.component';
import { SettingsBreadcrumbComponent } from './settings/settings-breadcrumb.component';
import { SharedModule } from '../shared/shared.module';
import { EditClientQuestionComponent } from './client-question/edit-client-question/edit-client-question.component';
import { ClientQuestionProfileComponent } from './client-question/client-question-profile/client-question-profile.component';
import { HelpBreadcrumbComponent } from './help/help-breadcrumb.component';
import { HelpComponent } from './help/help.component';
import { AdminRootComponent } from './admin-root.component';
import { HeaderComponent } from './header.component';
import { SidebarComponent } from './sidebar.component';
import { AddDispatcherComponent } from './add-dispatcher/add-dispatcher.component';
import { AddQuestionComponent } from './add-question/add-question.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    NgChartsModule,
    CommonModule,
    DragDropModule,
    IsLoadingModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatTooltipModule,
    MatToolbarModule,
    MatTreeModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AdminRootComponent,
    HeaderComponent,
    SidebarComponent,
    SettingsComponent,
    OverviewComponent,
    UpdateAdminComponent,
    ClientQuestionsComponent,
    ClientQuestionComponent,
    OverviewBreadcrumbComponent,
    ClientQuestionBreadcrumbComponent,
    ClientQuestionsBreadcrumbComponent,
    SettingsBreadcrumbComponent,
    EditClientQuestionComponent,
    ClientQuestionProfileComponent,
    HelpBreadcrumbComponent,
    HelpComponent,
    AddDispatcherComponent,
    AddQuestionComponent,
  ],
})
export class AdminModule {}
