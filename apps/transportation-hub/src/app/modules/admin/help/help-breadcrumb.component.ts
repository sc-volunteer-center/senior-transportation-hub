import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-help-breadcrumb',
  template: `
    Help
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelpBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
