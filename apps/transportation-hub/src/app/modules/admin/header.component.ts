import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  AfterViewInit,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AddDispatcherComponent } from './add-dispatcher/add-dispatcher.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { ExportService } from '../../export.service';
import { IsLoadingService } from '@service-work/is-loading';
import { subYears } from 'date-fns';
import { RouterStoreService } from '../../state/router-store.service';

@Component({
  selector: 'app-header',
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <div id="app-menu-outlet">
          <div #menuOutlet></div>
        </div>

        <div class="flex-1"></div>

        <div style="display: flex; flex-shrink: 0;">
          <button mat-button (click)="addDispatcher()">Add Dispatcher</button>

          <button mat-button (click)="addClientQuestion()">
            Add Client Question
          </button>

          <!-- 
            The "download CSV" is being disabled in favor of Google Sheets syncing.
            John has setup several Google Sheets which automatically pull data from
            the Hub for generating reports.

            <span
              matTooltip="Processing data. This might take a minute or two."
              [matTooltipDisabled]="!('download-data' | swIsLoading | async)"
            >
              <button
                mat-button
                swIsLoading="download-data"
                [matMenuTriggerFor]="downloadOptions"
              >
                Download Data
              </button>
            </span>
          -->
        </div>
      </mat-toolbar-row>
    </mat-toolbar>

    <!-- <mat-menu #downloadOptions="matMenu">
      <button
        *ngFor="let fiscalYear of fiscalYears"
        mat-menu-item
        (click)="downloadData(fiscalYear)"
      >
        {{ fiscalYear }}
      </button>
      <button mat-menu-item>Cancel</button>
    </mat-menu> -->
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'app-header' },
  styles: [
    `
      .TESTING-APP .mat-toolbar.mat-primary {
        background: #c2185b;
      }
    `,
  ],
})
export class HeaderComponent implements AfterViewInit {
  @ViewChild('menuOutlet', { read: ViewContainerRef, static: true })
  menuOutlet: ViewContainerRef;

  fiscalYears = [
    this.exportService.getFiscalYear(new Date()),
    this.exportService.getFiscalYear(subYears(new Date(), 1)),
    this.exportService.getFiscalYear(subYears(new Date(), 2)),
    this.exportService.getFiscalYear(subYears(new Date(), 3)),
    this.exportService.getFiscalYear(subYears(new Date(), 4)),
    this.exportService.getFiscalYear(subYears(new Date(), 5)),
  ];

  constructor(
    private isLoadingService: IsLoadingService,
    private dialog: MatDialog,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private factory: ComponentFactoryResolver,
    private exportService: ExportService,
    private routerStore: RouterStoreService,
  ) {}

  ngAfterViewInit() {
    this.routerStore
      .get$((store) => store.state.breadcrumbs)
      .subscribe((breadcrumbs) => {
        this.menuOutlet.clear();

        if (!breadcrumbs || breadcrumbs.length === 0) {
          return;
        }

        const factory = this.factory.resolveComponentFactory(
          breadcrumbs[breadcrumbs.length - 1],
        );
        this.menuOutlet.createComponent(factory);
        this.cdr.markForCheck();
      });
  }

  addDispatcher() {
    this.dialog
      .open(AddDispatcherComponent, {
        width: '600px',
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.router.navigate([`/admin/dispatchers`]);
        }
      });
  }

  addClientQuestion() {
    this.dialog
      .open(AddQuestionComponent, {
        width: '600px',
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.router.navigate([`/admin/client-questions`]);
        }
      });
  }

  async downloadData(fiscalYear: string) {
    try {
      await this.isLoadingService.add(this.exportService.export(fiscalYear), {
        key: 'download-data',
      });
    } catch (e) {
      console.error(e);
    }
  }
}
