import {
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { FirestoreService } from '../../firestore.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-root',
  template: `
    <app-header class="mat-elevation-z8"> </app-header>

    <mat-sidenav-container class="flex-1">
      <mat-sidenav
        mode="side"
        opened
        disableClose
        position="start"
        class="mat-elevation-z8"
        style="width: 200px;"
      >
        <app-sidebar></app-sidebar>
      </mat-sidenav>

      <mat-sidenav-content>
        <router-outlet></router-outlet>
      </mat-sidenav-content>
    </mat-sidenav-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class AdminRootComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private router: Router,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.fs
        .getOrganization()
        .pipe(
          switchMap(org =>
            this.fs.getCurrentUser().pipe(
              tap(user => {
                if (
                  !(
                    org &&
                    user &&
                    org.adminEmailAddresses.includes(user!.emailAddress)
                  )
                ) {
                  this.router.navigate(['']);
                }
              }),
            ),
          ),
        )
        .subscribe(),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
