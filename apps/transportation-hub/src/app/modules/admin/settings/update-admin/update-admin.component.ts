import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  FormArray,
  Validators,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { toObservable } from '../../../../utilities';
import { FirestoreService } from '../../../../firestore.service';
import { IsLoadingService } from '@service-work/is-loading';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { withLatestFrom, take } from 'rxjs/operators';
import { IUser } from '@local/models';

@Component({
  selector: 'app-update-admin',
  templateUrl: './update-admin.component.html',
  styleUrls: ['./update-admin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateAdminComponent implements OnInit {
  adminForm?: FormArray;
  currentUser: IUser | null = null;

  readonly separatorKeysCodes = [ENTER, COMMA];

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<UpdateAdminComponent>,
  ) {}

  ngOnInit() {
    this.fs
      .getOrganization()
      .pipe(withLatestFrom(this.fs.getCurrentUser().pipe(take(1))))
      .subscribe(([organization, currentUser]) => {
        this.currentUser = currentUser;
        this.adminForm = undefined;

        this.cdr.markForCheck();

        if (!organization || !currentUser) {
          return;
        }

        this.adminForm = this.fb.array(
          organization.adminEmailAddresses.map(email => [
            { value: email, disabled: currentUser.emailAddress === email },
            [Validators.required, Validators.email],
          ]),
        );
      });
  }

  add(event: MatChipInputEvent) {
    if (
      !this.adminForm ||
      !event.value ||
      this.adminForm.value.includes(event.value)
    ) {
      return;
    }

    this.adminForm.push(
      new FormControl(event.value, [Validators.required, Validators.email]),
    );

    event.input.value = '';
  }

  remove(control: FormControl) {
    if (!this.adminForm) {
      return;
    }

    const index = this.adminForm.controls.indexOf(control);

    this.adminForm.removeAt(index);
  }

  submit() {
    if (!this.adminForm || !this.currentUser || this.adminForm.invalid) {
      return;
    }

    const adminEmailAddresses = this.adminForm.value.filter(
      (email: string) => email !== this.currentUser!.emailAddress,
    );

    adminEmailAddresses.push(this.currentUser.emailAddress);

    this.loadingService.add(
      this.fs.updateOrganizationAdminEmailAddresses(adminEmailAddresses),
      { key: 'update-admin' },
    );
  }
}
