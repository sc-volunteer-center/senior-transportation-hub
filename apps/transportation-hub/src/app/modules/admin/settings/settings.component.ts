import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewContainerRef,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { IsLoadingService } from '@service-work/is-loading';
import { MatDialog } from '@angular/material/dialog';
import { IUser, IOrganization } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { toObservable } from '../../../utilities';
import { UpdateAdminComponent } from './update-admin/update-admin.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsComponent implements OnInit {
  organizationForm?: FormGroup;
  organization: IOrganization | null = null;

  dispatcherForms: FormGroup[] = [];

  @ViewChild('activeDispatchers', { read: ViewContainerRef })
  activeDispatchersContainer?: ViewContainerRef;

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private loadingService: IsLoadingService,
    private dialog: MatDialog,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.titleService.setTitle(`Settings | Admin | Hub`);

    let currentUser: IUser | null = null;

    this.subscriptions.push(
      this.fs
        .getCurrentUser()
        .pipe(
          switchMap(user => {
            currentUser = user;

            return this.fs.getOrganization();
          }),
        )
        .subscribe(organization => {
          this.organization = organization;
          this.cdr.markForCheck();

          if (!currentUser || !organization) {
            return;
          }

          this.organizationForm = this.fb.group({
            name: [
              organization.name,
              [Validators.required, Validators.minLength(5)],
            ],
            dispatcherEmailAddress: [
              organization.dispatcherEmailAddress,
              [Validators.required, Validators.email],
            ],
            // rideRequestEmailPreface: [organization.rideRequestEmailPreface],
          });
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submitOrganizationForm() {
    if (!this.organizationForm || this.organizationForm.invalid) {
      return;
    }

    const form = this.organizationForm.value;

    this.loadingService.add(this.fs.updateOrganization(form), {
      key: 'submit-organization-form',
    });
  }

  updateAdminEmailAddresses() {
    this.dialog.open(UpdateAdminComponent, {
      width: '600px',
    });
  }
}
