import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-settings-breadcrumb',
  template: `
    Settings
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
