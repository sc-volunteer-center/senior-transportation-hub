import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { toObservable } from '../../../utilities';
import { FirestoreService } from '../../../firestore.service';
import { IsLoadingService } from '@service-work/is-loading';
import { MatChipInput, MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddQuestionComponent implements OnInit {
  questionType = new FormControl();

  questionForm: FormGroup;
  selectQuestionForm: FormGroup;
  textQuestionForm: FormGroup;
  booleanQuestionForm: FormGroup;
  questionSectionBreakForm: FormGroup;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<AddQuestionComponent>,
  ) {}

  ngOnInit() {
    this.questionForm = this.fb.group({
      temp: ['', Validators.required],
    });

    this.subscriptions.push(
      this.questionType.valueChanges.subscribe(value => {
        switch (value) {
          case 'select':
            this.questionForm = this.selectQuestionForm;
            break;
          case 'text':
            this.questionForm = this.textQuestionForm;
            break;
          case 'boolean':
            this.questionForm = this.booleanQuestionForm;
            break;
          case 'section-break':
            this.questionForm = this.questionSectionBreakForm;
            break;
        }

        this.cdr.markForCheck();
      }),
    );

    this.selectQuestionForm = this.fb.group({
      text: ['', Validators.required],
      required: [false],
      options: [[], Validators.required],
      allowOther: [false],
    });

    this.textQuestionForm = this.fb.group({
      text: ['', Validators.required],
      required: [false],
      valueType: ['text', Validators.required],
    });

    this.booleanQuestionForm = this.fb.group({
      text: ['', Validators.required],
      required: [false],
    });

    this.questionSectionBreakForm = this.fb.group({
      text: ['', Validators.required],
      body: '',
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.questionForm.invalid) return;

    let promise: Promise<unknown>;

    switch (this.questionType.value) {
      case 'select':
        promise = this.fs.addClientSelectQuestion(this.questionForm.value);
        break;
      case 'text':
        promise = this.fs.addClientTextQuestion(this.questionForm.value);
        break;
      case 'boolean':
        promise = this.fs.addClientBooleanQuestion(this.questionForm.value);
        break;
      case 'section-break':
        promise = this.fs.addClientQuestionSectionBreak(
          this.questionForm.value,
        );
        break;
      default:
        throw new Error('Ack! Unexpected value');
    }

    const id = await this.loadingService.add(promise);
    this.ref.close(id);
    this.cdr.markForCheck();
  }

  removeSelectOption(option: string) {
    let options = this.selectQuestionForm.get('options')!.value as string[];
    options = options.filter(opt => opt !== option);
    this.selectQuestionForm.get('options')!.patchValue(options);
  }

  addSelectOption(event: MatChipInputEvent) {
    if (!event.value) {
      return;
    }

    const options = this.selectQuestionForm.get('options')!.value as string[];
    options.push(event.value);
    this.selectQuestionForm.get('options')!.patchValue(options);
    event.input.value = '';
  }
}
