import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Question } from '@local/models';
import { Subscription, BehaviorSubject, of } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatTable } from '@angular/material/table';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { IsLoadingService } from '@service-work/is-loading';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-client-questions',
  templateUrl: './client-questions.component.html',
  styleUrls: ['./client-questions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientQuestionsComponent implements OnInit, OnDestroy {
  allQuestions: Question[] = [];
  questions: Question[] = [];
  retiredQuestions: Question[] = [];
  showRetiredQuestions = new BehaviorSubject(false);

  filterText = '';
  filteredQuestions: Question[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private firestore: FirestoreService,
    private loadingService: IsLoadingService,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.titleService.setTitle(`Client Questions | Admin | Hub`);

    this.subscriptions.push(
      this.loadingService
        .add(this.firestore.getClientQuestions(), { key: 'loading-questions' })
        .subscribe((questions) => {
          this.questions = questions;
          this.processQuestions();
        }),
    );

    this.subscriptions.push(
      this.showRetiredQuestions
        .pipe(
          distinctUntilChanged(),
          switchMap((show) =>
            !show
              ? of([])
              : this.firestore.getClientQuestions({ retired: true }),
          ),
        )
        .subscribe((questions) => {
          this.retiredQuestions = questions;
          this.processQuestions();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  displayActiveStatus(question: Question) {
    return question.active ? 'Yes' : 'Retired';
  }

  filterQuestions(event?: Event) {
    if (event && event.target) {
      this.filterText = (event.target as HTMLInputElement).value.toLowerCase();
    }

    const searchText = this.filterText;

    this.filteredQuestions = this.allQuestions.filter((question) =>
      question.text.toLowerCase().includes(searchText),
    );

    this.cdr.detectChanges();
  }

  toggleRetiredQuestions(event: MatCheckboxChange) {
    this.showRetiredQuestions.next(event.checked);
  }

  reorderQuestions(event: CdkDragDrop<Question[]>) {
    if (event.previousIndex === event.currentIndex) return;

    // `this.questions` has already been reordered by `sortingQuestions()`
    this.loadingService.add(
      this.firestore.updateClientQuestionOrder(
        this.questions.map((question, index) => ({
          questionId: question.uid,
          order: index,
        })),
      ),
    );

    this.cdr.detectChanges();
  }

  sortingQuestions(event: CdkDragDrop<Question[]>) {
    moveItemInArray(this.questions, event.previousIndex, event.currentIndex);

    this.questions.forEach((question, index) => {
      question.order = index;
    });

    this.cdr.detectChanges();
  }

  private processQuestions() {
    this.allQuestions = [...this.questions, ...this.retiredQuestions];
    this.filteredQuestions = this.allQuestions;
    this.filterQuestions();
  }
}
