import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-client-questions-breadcrumb',
  template: `
    Questions
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientQuestionsBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
