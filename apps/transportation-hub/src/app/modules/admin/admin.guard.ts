import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../firestore.service';
import { map, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private fs: FirestoreService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.fs.getCurrentUser().pipe(
      switchMap(user =>
        this.fs
          .getOrganization()
          .pipe(
            map(
              org =>
                !!(
                  org &&
                  user &&
                  org.adminEmailAddresses.includes(user.emailAddress)
                ),
            ),
          ),
      ),
      tap(passing => {
        if (!passing) {
          this.router.navigate(['/welcome']);
        }
      }),
    );
  }
}
