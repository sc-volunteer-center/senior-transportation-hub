import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Subscription, of, combineLatest } from 'rxjs';
import { Client, Driver, Dispatcher, RideRequest } from '@local/models';
import {
  subMonths,
  differenceInMinutes,
  format,
  startOfMonth,
  endOfMonth,
} from 'date-fns';
import { switchMap, take } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { ExportService } from '../../../export.service';
import { IsLoadingService } from '@service-work/is-loading';

const METERS_TO_MILES_CONVERSION = 0.0006213712;
const DEFAULT_DISPATCHER_SHIFT_MINUTES = 3 * 60; // 3 hours

interface INGChartsData {
  data: unknown[];
  label: string;
}

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'profile-component',
  },
})
export class OverviewComponent implements OnInit {
  clients: Client[] = [];
  drivers: Driver[] = [];
  dispatchers: Dispatcher[] = [];

  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  chartMonthLabels: string[] = [];

  requestsChartData?: INGChartsData[];
  ridesChartData?: INGChartsData[];
  driverMilesChartData?: INGChartsData[];
  dispatcherHoursChartData?: INGChartsData[];

  private requestData?: Promise<{
    totalRequests: RideRequest[][];
    totalFulfilledRequests: RideRequest[][];
    totalUnfulfilledRequests: RideRequest[][];
    totalCancelledRequests: RideRequest[][];
  }>;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private titleService: Title,
    private exportService: ExportService,
  ) {}

  ngOnInit() {
    this.titleService.setTitle(`Overview | Admin | Hub`);

    this.subscriptions.push(
      combineLatest([
        this.fs.getClients({ retired: false }),
        this.fs.getDrivers({ retired: false }),
        this.fs.getDispatchers({ retired: false }),
      ]).subscribe((results) => {
        this.clients = results[0];
        this.drivers = results[1];
        this.dispatchers = results[2];
        this.cdr.markForCheck();
      }),
    );
  }

  initChartData() {
    const endDate = endOfMonth(new Date());
    const startDate = subMonths(startOfMonth(endDate), 11);

    this.chartMonthLabels = this.exportService
      .getMonths(startDate, endDate)
      .map((d) => format(d, 'MMM yyyy'));

    this.requestData = this.exportService
      .allRideRequestDocs(startDate, endDate)
      .then((value) => {
        const totalRequests = value;

        const totalFulfilledRequests = value.map((requests) =>
          requests.filter((r) => !r.requestCancelledAt && r.driverId),
        );

        const totalUnfulfilledRequests = value.map((requests) =>
          requests.filter((r) => !r.requestCancelledAt && !r.driverId),
        );

        const totalCancelledRequests = value.map((requests) =>
          requests.filter((r) => r.requestCancelledAt),
        );

        return {
          totalRequests,
          totalFulfilledRequests,
          totalUnfulfilledRequests,
          totalCancelledRequests,
        };
      });
  }

  async showRidesChart() {
    if (!this.requestData) {
      this.initChartData();
    }

    this.ridesChartData = [];
    this.cdr.markForCheck();

    const {
      totalRequests,
      totalFulfilledRequests,
      totalUnfulfilledRequests,
      totalCancelledRequests,
    } = await this.requestData!;

    const totalRidesData = totalRequests.map((requests) =>
      requests.reduce(
        (prev, curr) => prev + this.exportService.numberOfRides(curr),
        0,
      ),
    );

    const totalFulfilledRidesData = totalFulfilledRequests.map((requests) =>
      requests.reduce(
        (prev, curr) => prev + this.exportService.numberOfRides(curr),
        0,
      ),
    );

    const totalUnfulfilledRidesData = totalUnfulfilledRequests.map((requests) =>
      requests.reduce(
        (prev, curr) => prev + this.exportService.numberOfRides(curr),
        0,
      ),
    );

    const totalCancelledRidesData = totalCancelledRequests.map((requests) =>
      requests.reduce(
        (prev, curr) => prev + this.exportService.numberOfRides(curr),
        0,
      ),
    );

    this.ridesChartData = [
      { data: totalRidesData, label: 'Total rides' },
      { data: totalFulfilledRidesData, label: 'Total fulfilled rides' },
      { data: totalUnfulfilledRidesData, label: 'Total unfulfulled rides' },
      { data: totalCancelledRidesData, label: 'Total cancelled rides' },
    ];

    this.cdr.markForCheck();
  }

  async showRequestsChart() {
    if (!this.requestData) {
      this.initChartData();
    }

    this.requestsChartData = [];
    this.cdr.markForCheck();

    const {
      totalRequests,
      totalFulfilledRequests,
      totalUnfulfilledRequests,
      totalCancelledRequests,
    } = await this.requestData!;

    const totalRequestsData = totalRequests.map((requests) => requests.length);

    const totalFulfilledRequestsData = totalFulfilledRequests.map(
      (requests) => requests.length,
    );

    const totalUnfulfilledRequestsData = totalUnfulfilledRequests.map(
      (requests) => requests.length,
    );

    const totalCancelledRequestsData = totalCancelledRequests.map(
      (requests) => requests.length,
    );

    this.requestsChartData = [
      { data: totalRequestsData, label: 'Total requests' },
      {
        data: totalFulfilledRequestsData,
        label: 'Total fulfilled requests',
      },
      {
        data: totalUnfulfilledRequestsData,
        label: 'Total unfulfulled requests',
      },
      {
        data: totalCancelledRequestsData,
        label: 'Total cancelled requests',
      },
    ];

    this.cdr.markForCheck();
  }

  async showDriverMilesChart() {
    if (!this.requestData) {
      this.initChartData();
    }

    this.driverMilesChartData = [];

    this.cdr.markForCheck();

    const { totalFulfilledRequests } = await this.requestData!;

    const estimatedMilesDrivenData = totalFulfilledRequests.map((requests) =>
      requests.reduce(
        (prev, curr) =>
          prev + curr.estimatedRideMeters! * METERS_TO_MILES_CONVERSION,
        0,
      ),
    );

    this.driverMilesChartData = [
      {
        data: estimatedMilesDrivenData.map((d) => d.toFixed(1)),
        label: 'Estimated driver miles driven',
      },
    ];

    this.cdr.markForCheck();
  }

  async showDispatcherHoursChart() {
    const endDate = endOfMonth(new Date());
    const startDate = subMonths(startOfMonth(endDate), 11);

    if (!this.requestData) {
      this.chartMonthLabels = this.exportService
        .getMonths(startDate, endDate)
        .map((d) => format(d, 'MMM yyyy'));
    }

    this.dispatcherHoursChartData = [];

    this.cdr.markForCheck();

    const dispatcherShifts = await this.fs
      .getDispatchers()
      .pipe(
        switchMap((dispatchers) =>
          dispatchers.length === 0
            ? of([])
            : combineLatest(
                dispatchers.map((disp) =>
                  this.fs.getDispatcherShifts({
                    dispatcherId: disp.uid,
                    startDate,
                  }),
                ),
              ),
        ),
        take(1),
      )
      .toPromise();

    const months = new Map(
      this.exportService
        .getMonths(startDate, endDate)
        .map((d) => [startOfMonth(d).valueOf(), 0]),
    );

    dispatcherShifts.forEach((shifts) => {
      shifts.forEach((shift) => {
        const month = startOfMonth(shift.startAt).valueOf();

        const curr = months.get(month);

        if (typeof curr !== 'number') {
          throw new Error('Error calculating dispatcher shift hours');
        }

        let minutes = DEFAULT_DISPATCHER_SHIFT_MINUTES;

        if (shift.endAt) {
          minutes = differenceInMinutes(shift.endAt, shift.startAt);
        }

        months.set(month, curr + minutes);
      });
    });

    this.dispatcherHoursChartData = [
      {
        data: Array.from(months.values()).map((m) => (m / 60).toFixed(1)),
        label: 'Estimated dispatcher volunteer hours',
      },
    ];

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
