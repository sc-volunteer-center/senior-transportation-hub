import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FirestoreService } from '../../../firestore.service';
import { IsLoadingService } from '@service-work/is-loading';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-dispatcher',
  templateUrl: './add-dispatcher.component.html',
  styleUrls: ['./add-dispatcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddDispatcherComponent implements OnInit {
  dispatcherForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<AddDispatcherComponent>,
  ) {}

  ngOnInit() {
    this.dispatcherForm = this.fb.group({
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: ['', Validators.required],
      phoneNumber: '',
      emailAddress: '',
      notes: '',
    });
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.dispatcherForm.invalid) {
      return;
    }

    const id = await this.loadingService.add(
      this.fs.addDispatcher(this.dispatcherForm.value),
    );
    this.ref.close(id);
    this.cdr.markForCheck();
  }
}
