import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ElementRef,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
  ValidatorFn,
} from '@angular/forms';
import { combineLatest, of, Subscription } from 'rxjs';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import {
  Client,
  Destination,
  RideRequest,
  createRecurringRideRequestSchedule,
  setTime,
  NoteThread,
  setToStartOf,
  convertDateToSameWallclockTimeInCaliZone,
} from '@local/models';
import { AddClientComponent } from '../add-client/add-client.component';
import {
  take,
  debounceTime,
  distinctUntilChanged,
  map,
  filter,
  switchMap,
  tap,
} from 'rxjs/operators';
import {
  stringComparer,
  startOfWeek,
  endOfWeek,
  WeekRangeSelectionStrategy,
} from '../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { addDays } from 'date-fns';
import * as moment from 'moment-timezone';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { isEqual } from '@local/isEqual';

const addressValidator: ValidatorFn = (control) => {
  const v = control.value;
  const controls = (control as FormGroup).controls;

  if (v.address === 'other' && controls.label.disabled) {
    controls.label.enable();
    controls.street.enable();
    controls.apartmentNumber.enable();
    controls.city.enable();
    controls.zip.enable();
    controls.comments.enable();
  } else if (v.address !== 'other' && controls.label.enabled) {
    controls.label.disable();
    controls.street.disable();
    controls.apartmentNumber.disable();
    controls.city.disable();
    controls.zip.disable();
    controls.comments.disable();
  }

  return null;
};

const disabledValue = { value: '', disabled: true };

@Component({
  selector: 'app-add-ride-request',
  templateUrl: './add-ride-request.component.html',
  styleUrls: ['./add-ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: WeekRangeSelectionStrategy,
    },
  ],
})
export class AddRideRequestComponent implements OnInit {
  clients: Client[] = [];
  filteredClients: Client[] = [];
  flaggedThreads: NoteThread[] = [];
  requestTooFarInAdvance = false;

  private prevControlValue?: { [key: string]: any };

  control = this.fb.group(
    {
      client: ['', Validators.required],
      requestMadeBy: ['client', Validators.required],
      requestMadeByName: [disabledValue, Validators.required],
      requestType: ['', Validators.required],
      requestSchedule: ['', Validators.required],
      scheduleOnce: this.fb.group({
        date: [null, Validators.required],
        time: ['', Validators.required],
      }),
      scheduleRecurring: this.fb.group({
        startDate: [null, Validators.required],
        time: ['', Validators.required],
        scheduleName: ['', Validators.required],
        endDate: [''], // end date is currently disabled in the UI
        occurrenceCount: [0, [Validators.required, Validators.min(1)]],
      }),
      flexible: [null, Validators.required],
      flexibleSchedule: [disabledValue, Validators.required],
      priority: ['', Validators.required],
      pickupAddress: this.fb.group(
        {
          address: ['', Validators.required],
          label: [disabledValue],
          street: [disabledValue, Validators.required],
          apartmentNumber: [disabledValue],
          city: [disabledValue, Validators.required],
          zip: [disabledValue, Validators.required],
          comments: [disabledValue],
        },
        {
          validators: addressValidator,
        },
      ),
      destinations: this.fb.array([
        this.fb.group(
          {
            address: ['', Validators.required],
            tripPurpose: ['', Validators.required],
            label: [disabledValue],
            street: [disabledValue, Validators.required],
            apartmentNumber: [disabledValue],
            city: [disabledValue, Validators.required],
            zip: [disabledValue, Validators.required],
            comments: [disabledValue],
          },
          {
            validators: addressValidator,
          },
        ),
      ]),
      peopleImpacted: ['', Validators.required],
      peopleImpactedDescription: [''],
      comments: [''],
    },
    {
      validators: (control) => {
        const c = control as FormGroup;
        const v = c.getRawValue();
        const prev = this.prevControlValue;
        this.prevControlValue = v;
        const destControls = (c.controls.destinations as FormArray)
          .controls as FormGroup[];

        if (
          (typeof v.client === 'string' || !v.client) &&
          v.peopleImpacted !== 1
        ) {
          c.controls.peopleImpacted.setValue(1);
          c.controls.peopleImpactedDescription.setValue('');
        } else if (
          typeof v.client === 'object' &&
          (typeof prev?.client !== 'object' || v.client.uid !== prev.client.uid)
        ) {
          c.controls.peopleImpacted.setValue(
            (v.client as Client).defaultPeopleImpacted,
          );
          c.controls.peopleImpactedDescription.setValue(
            (v.client as Client).defaultPeopleImpactedDescription,
          );
        }

        // disable/enable `flexibleSchedule` as appropriate
        if (!prev?.flexible && v.flexible) {
          c.controls.flexibleSchedule.enable();
        } else if (prev?.flexible && !v.flexible) {
          c.controls.flexibleSchedule.disable();
        }

        // set value of scheduleOnce.time as appropriate
        if (
          (!prev?.flexible && v.flexible && v.requestSchedule === 'ONCE') ||
          (prev?.requestSchedule !== 'ONCE' &&
            v.flexible &&
            v.requestSchedule === 'ONCE')
        ) {
          c.get('scheduleOnce.date')?.setValue(null);
          c.get('scheduleOnce.time')?.setValue('00:00');
        } else if (
          (prev?.flexible && !v.flexible && v.requestSchedule === 'ONCE') ||
          (prev?.requestSchedule === 'ONCE' &&
            !v.flexible &&
            v.requestSchedule !== 'ONCE') ||
          (v.requestSchedule === 'ONCE' && prev?.flexible && !v.flexible)
        ) {
          c.get('scheduleOnce.date')?.setValue(null);
          c.get('scheduleOnce.time')?.setValue(null);
        }

        // set value of scheduleRecurring.time as appropriate
        if (
          (!prev?.flexible &&
            v.flexible &&
            v.requestSchedule === 'RECURRING') ||
          (prev?.requestSchedule !== 'RECURRING' &&
            v.flexible &&
            v.requestSchedule === 'RECURRING')
        ) {
          c.get('scheduleRecurring.startDate')?.setValue(null);
          c.get('scheduleRecurring.endDate')?.setValue(null);
          c.get('scheduleRecurring.time')?.setValue('00:00');
        } else if (
          (prev?.flexible &&
            !v.flexible &&
            v.requestSchedule !== 'RECURRING') ||
          (prev?.requestSchedule === 'RECURRING' &&
            !v.flexible &&
            v.requestSchedule !== 'RECURRING') ||
          (v.requestSchedule === 'RECURRING' && prev?.flexible && !v.flexible)
        ) {
          c.get('scheduleRecurring.startDate')?.setValue(null);
          c.get('scheduleRecurring.endDate')?.setValue(null);
          c.get('scheduleRecurring.time')?.setValue(null);
        }

        if (
          v.requestMadeBy === 'other' &&
          c.controls.requestMadeByName.disabled
        ) {
          c.controls.requestMadeByName.enable();
        } else if (
          v.requestMadeBy === 'client' &&
          c.controls.requestMadeByName.enabled
        ) {
          c.controls.requestMadeByName.disable();
        }

        if (v.requestType === 'GSP' && prev?.requestType !== 'GSP') {
          destControls.forEach((ac) => {
            ac.controls.tripPurpose.setValue('Grocery Shopper Program');
          });
        } else if (v.requestType === 'STP' && prev?.requestType !== 'STP') {
          destControls.forEach((ac) => {
            ac.controls.tripPurpose.setValue('');
          });
          c.controls.peopleImpacted.setValue(1);
          c.controls.peopleImpactedDescription.setValue('');
        }

        if (v.requestSchedule === 'ONCE' && c.controls.scheduleOnce.disabled) {
          c.controls.scheduleOnce.enable(); // needs to come first
          c.controls.scheduleRecurring.disable();
        } else if (
          v.requestSchedule === 'RECURRING' &&
          c.controls.scheduleRecurring.disabled
        ) {
          c.controls.scheduleRecurring.enable(); // needs to come first
          c.controls.scheduleOnce.disable();
        }
      },
    },
  );

  favoriteDestinations: Destination[] = [];
  filteredFavoriteDestinations: Destination[][] = [[], [], [], [], [], []];

  possibleDuplicate = '';
  otherRideRequestsThatWeek: RideRequest[] = [];
  otherRecurringRequestsCount = 0;
  selectedClient: Client | null = null; // <-- this may not be used. don't have time to check at the moment.

  recurringRideRequestSchedule: Date[] | null = null;

  private subscriptions: Subscription[] = [];
  private currentFavoriteDestinationsFilter: string[] = [
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  clientDisplayFn(client?: Client): string | undefined {
    return client?.nameWithRetiredStatus;
  }

  destinationAddressDisplayFn(destination?: Destination | string): string {
    if (!destination) {
      return '';
    }

    switch (typeof destination) {
      case 'string':
        return 'A custom address';
      case 'object':
        return (destination as Destination).label!;
      default:
        return '';
    }
  }

  constructor(
    public fs: FirestoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<AddRideRequestComponent>,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private isLoadingService: IsLoadingService,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {}

  ngOnInit() {
    this.control.controls.scheduleOnce.disable();
    this.control.controls.scheduleRecurring.disable();

    // subscribe to favorite destinations
    this.subscriptions.push(
      this.fs.getDestinations().subscribe((destinations) => {
        this.favoriteDestinations = destinations;
        this.filterFavoriteDestinations();
      }),
    );

    // Generate a preview of the requests associated with a recurring
    // ride request
    const { scheduleRecurring, flexible } = this.control.controls;

    // generate preview of recurring request schedule and make sure
    // the schedule produces events
    this.subscriptions.push(
      combineLatest([
        scheduleRecurring.valueChanges,
        flexible.valueChanges,
      ]).subscribe(
        ([sr, flex]: [
          {
            startDate: Date | null;
            time: string;
            scheduleName:
              | 'Once a month'
              | 'Once every 3 weeks'
              | 'Once every 2 weeks'
              | 'Once a week';
            endDate: Date | null;
            occurrenceCount: number;
          },
          boolean | null,
        ]) => {
          this.cdr.markForCheck();

          if (!(sr.startDate && sr.time && sr.scheduleName)) {
            this.recurringRideRequestSchedule = null;

            if (sr.occurrenceCount !== 0) {
              scheduleRecurring.patchValue({
                occurrenceCount: 0,
              });
            }
            return;
          }

          const start = moment(setTime(sr.startDate!, sr.time));

          const end =
            (sr.endDate && moment(setToStartOf(sr.endDate, 'day'))) || null;

          const schedule = createRecurringRideRequestSchedule({
            start,
            end,
            scheduleName: sr.scheduleName,
          });

          this.recurringRideRequestSchedule = schedule
            .occurrences({ take: 5 })
            .toArray()
            .map(
              ({ date }) =>
                (flex && setToStartOf(date.toDate(), 'week')) || date.toDate(),
            );

          const scheduleOccurrenceCount =
            this.recurringRideRequestSchedule?.length ?? 0;

          if (scheduleOccurrenceCount !== sr.occurrenceCount) {
            scheduleRecurring.patchValue({
              occurrenceCount: scheduleOccurrenceCount,
            });
          }
        },
      ),
    );

    this.subscriptions.push(
      this.control.get('peopleImpacted')!.valueChanges.subscribe((value) => {
        if (value > 1) {
          this.control.get('peopleImpactedDescription')!.enable();
          return;
        } else if (value === 1) {
          this.control.get('peopleImpactedDescription')!.disable();
          return;
        }

        // prevent entering a number less than 1
        this.control.get('peopleImpacted')!.patchValue(1);
      }),
    );

    // detect if the new, non-recurring ride request might be a duplicate
    this.subscriptions.push(
      this.control.valueChanges
        .pipe(
          filter(
            (v) =>
              typeof v.client === 'object' &&
              v.scheduleOnce &&
              v.scheduleOnce.date &&
              v.scheduleOnce.time &&
              v.requestType,
          ),
          map((v) => {
            return {
              clientId: v.client.uid as string,
              date: v.scheduleOnce.date as Date,
              time: v.scheduleOnce.time as string,
              type: v.requestType as 'STP' | 'GSP' | '',
            };
          }),
          distinctUntilChanged(isEqual),
          tap(() => {
            this.otherRideRequestsThatWeek = [];
            this.otherRecurringRequestsCount = 0;
            this.possibleDuplicate = '';
            this.renderer.removeClass(
              this.el.nativeElement.parentElement!,
              'warn',
            );
            this.cdr.markForCheck();
          }),
          debounceTime(1000),
          switchMap((d) => {
            const { clientId, date, time, type } = d!;

            const intermediate = setTime(date, time);

            const startOfWeekDate = convertDateToSameWallclockTimeInCaliZone(
              startOfWeek(intermediate),
            );

            const endOfWeekDate = convertDateToSameWallclockTimeInCaliZone(
              endOfWeek(intermediate),
            );

            const datetime =
              convertDateToSameWallclockTimeInCaliZone(intermediate);

            return combineLatest([
              this.isLoadingService.isLoading$({ key: 'add-ride-request' }),
              this.fs
                .getRideRequests({
                  clientId,
                  startAfterOrOnDate: datetime,
                  endBeforeOrOnDate: datetime,
                  isFlexible: false,
                })
                .pipe(
                  map((requests) =>
                    requests.filter((request) => request.category === type),
                  ),
                ),
              this.fs.getRideRequests({
                clientId,
                startAfterOrOnDate: startOfWeekDate,
                endBeforeOrOnDate: endOfWeekDate,
                requestCancelledAt: null,
              }),
            ]);
          }),
        )
        .subscribe(([submitPending, dupRideRequests, otherRideRequests]) => {
          if (submitPending) return;

          this.otherRideRequestsThatWeek = otherRideRequests;

          if (dupRideRequests.length > 0) {
            this.possibleDuplicate = dupRideRequests[0].uid;

            this.renderer.addClass(
              this.el.nativeElement.parentElement!,
              'warn',
            );
          } else {
            this.possibleDuplicate = '';

            this.renderer.removeClass(
              this.el.nativeElement.parentElement!,
              'warn',
            );
          }

          this.cdr.markForCheck();
        }),
    );

    // get any other active recurring requests for this client
    this.subscriptions.push(
      this.control.valueChanges
        .pipe(
          filter(
            (v) =>
              typeof v.client === 'object' &&
              v.scheduleRecurring?.scheduleName &&
              v.scheduleRecurring?.startDate,
          ),
          map((v) => {
            return [v.client.uid, v.scheduleRecurring.startDate] as [
              string,
              Date,
            ];
          }),
          distinctUntilChanged(isEqual),
          switchMap((data) => {
            this.cdr.markForCheck();
            this.otherRecurringRequestsCount = 0;
            this.otherRideRequestsThatWeek = [];

            const [clientId, startDate] = data;

            const startOfWeekDate = convertDateToSameWallclockTimeInCaliZone(
              startOfWeek(startDate),
            );

            const endOfWeekDate = convertDateToSameWallclockTimeInCaliZone(
              endOfWeek(startDate),
            );

            return combineLatest([
              this.fs.getRideRequests({
                clientId,
                startAfterOrOnDate: startOfWeekDate,
                endBeforeOrOnDate: endOfWeekDate,
                requestCancelledAt: null,
              }),
              this.fs.getRecurringRideRequests({
                clientId,
                requestIsCancelled: false,
              }),
            ]);
          }),
        )
        .subscribe(([otherRideRequests, otherRecurringRideRequests]) => {
          this.cdr.markForCheck();
          this.otherRecurringRequestsCount = otherRecurringRideRequests.length;
          this.otherRideRequestsThatWeek = otherRideRequests;
        }),
    );

    // clear otherRideRequestsThatWeek and otherRecurringRequestsCount if relevant
    // form values are incomplete
    this.subscriptions.push(
      this.control.valueChanges
        .pipe(
          map((v) => {
            if (typeof v.client !== 'object') return true;
            if (!v.requestSchedule) return true;

            return v.requestSchedule === 'ONCE'
              ? !(
                  v.client.uid &&
                  v.scheduleOnce?.date &&
                  v.scheduleOnce?.time &&
                  v.requestType
                )
              : !(
                  v.scheduleRecurring?.scheduleName &&
                  v.scheduleRecurring?.startDate
                );
          }),
          filter((v) => v),
          distinctUntilChanged(),
        )
        .subscribe(() => {
          this.cdr.markForCheck();
          this.otherRecurringRequestsCount = 0;
          this.otherRideRequestsThatWeek = [];
        }),
    );

    this.subscriptions.push(
      this.fs.getClients({ retired: false }).subscribe((clients) => {
        this.clients = clients;
        this.filterClients();
      }),
    );

    this.subscriptions.push(
      this.control.get('client')!.valueChanges.subscribe((value) => {
        if (typeof value !== 'string') {
          return;
        }

        this.filterClients(value);
      }),
    );

    this.subscriptions.push(
      this.control
        .get('client')!
        .valueChanges.pipe(
          map((value) =>
            typeof value === 'object' ? (value.uid as string) : null,
          ),
          distinctUntilChanged(isEqual),
          switchMap((clientId) =>
            !clientId
              ? of([])
              : this.fs.getNoteThreads({
                  clientId: clientId,
                  flag: true,
                }),
          ),
        )
        .subscribe((noteThreads) => {
          this.flaggedThreads = noteThreads;
          this.cdr.markForCheck();
        }),
    );

    // check to see if selected start date is less than 33 days from now
    this.subscriptions.push(
      this.control
        .get('requestSchedule')!
        .valueChanges.pipe(
          switchMap((rs) => {
            switch (rs) {
              case 'ONCE': {
                return this.control.get('scheduleOnce.date')!.valueChanges;
              }
              case 'RECURRING': {
                return this.control.get('scheduleRecurring.startDate')!
                  .valueChanges;
              }
              default: {
                return of(null);
              }
            }
          }),
        )
        .subscribe((date: Date | null) => {
          this.cdr.markForCheck();

          if (!date) {
            this.requestTooFarInAdvance = false;
            return;
          }

          this.requestTooFarInAdvance =
            addDays(new Date(), 33).valueOf() < date.valueOf();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  filterFavoriteDestinations(input: { value: string }, index: number): void;
  filterFavoriteDestinations(): void;
  filterFavoriteDestinations(input?: { value: string }, index?: number) {
    if (index !== undefined && input) {
      this.currentFavoriteDestinationsFilter[index] = input.value.toLowerCase();

      this.filteredFavoriteDestinations[index] =
        this.favoriteDestinations.filter((destination) =>
          `${destination.label} ${destination.street}`
            .toLowerCase()
            .includes(this.currentFavoriteDestinationsFilter[index]),
        );
    } else {
      for (const index of [0, 1, 2, 3, 4, 5]) {
        this.filteredFavoriteDestinations[index] =
          this.favoriteDestinations.filter((destination) =>
            `${destination.label} ${destination.street}`
              .toLowerCase()
              .includes(this.currentFavoriteDestinationsFilter[index]),
          );
      }
    }

    this.cdr.markForCheck();
  }

  destinationAddressInputBlurred(index: number) {
    const destinations = this.control.controls.destinations as FormArray;
    const control = destinations.at(index) as FormControl;
    const value = control.value;

    if (typeof value === 'string' && value !== 'other') {
      control.patchValue('');
      this.filterFavoriteDestinations({ value: '' }, index);
    }
  }

  addDestination() {
    const destinations = this.control.controls.destinations as FormArray;
    const defaultTripPurpose =
      this.control.value.requestType === 'GSP' ? 'Grocery Shopper Program' : '';

    destinations.push(
      this.fb.group(
        {
          address: ['', Validators.required],
          tripPurpose: [defaultTripPurpose, Validators.required],
          label: [''],
          street: ['', Validators.required],
          apartmentNumber: [''],
          city: ['', Validators.required],
          zip: ['', Validators.required],
          comments: [''],
        },
        {
          validators: addressValidator,
        },
      ),
    );
  }

  removeDestination(index: number) {
    const destinations = this.control.controls.destinations as FormArray;
    destinations.removeAt(index);
    this.cdr.markForCheck();
  }

  get formInvalid() {
    return (
      this.control.invalid || typeof this.control.value.client === 'string'
    );
  }

  async submit() {
    // run clientSelectionCallback() without the timeout
    const client = this.control.value.client as string | Client;

    if (typeof client === 'string') {
      this.control.get('client')!.patchValue('');
      this.selectedClient = null;
    }

    if (this.formInvalid) return;

    const { value } = this.control;

    const clientId = value.client.uid;

    const requestMadeBy =
      value.requestMadeBy === 'other'
        ? value.requestMadeByName
        : value.requestMadeBy;

    const priority = value.priority;
    const peopleImpacted = value.peopleImpacted;
    const peopleImpactedDescription = value.peopleImpactedDescription || null;
    const comments = value.comments;

    const requestSchedule =
      value.requestSchedule === 'ONCE'
        ? value.scheduleOnce
        : value.scheduleRecurring;

    if (value.requestSchedule === 'RECURRING') {
      requestSchedule.endDate ||= null;
    }

    const pickupAddress =
      value.pickupAddress.address === 'other' ? value.pickupAddress : undefined;

    const destinationAddresses = value.destinations.map(
      (d: Record<string, any>, i: number) => {
        if (d.address === 'other') return d;

        const dest = this.favoriteDestinations.find(
          (dest) => dest.label === (d.address as Destination).label,
        );

        if (!dest) {
          throw new Error(`Cannot find destination #${i + 1}`);
        }

        return {
          label: dest.label,
          street: dest.street,
          apartmentNumber: dest.apartmentNumber,
          city: dest.city,
          zip: dest.zip,
          comments: dest.comments,
          tripPurpose: d.tripPurpose,
        };
      },
    );

    const id = await this.isLoadingService.add(
      this.fs.addRideRequest({
        clientId,
        requestMadeBy,
        pickupAddress,
        destinationAddresses,
        priority,
        peopleImpacted,
        peopleImpactedDescription,
        comments,
        flexible: value.flexible,
        flexibleSchedule: value.flexibleSchedule,
        ...requestSchedule,
        requestType: value.requestSchedule,
      }),
      { key: 'add-ride-request' },
    );
    this.ref.close(id);
  }

  addClient() {
    this.dialog
      .open(AddClientComponent, {
        maxWidth: '600px',
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.fs
            .getClient(id)
            .pipe(take(1))
            .subscribe((client) => {
              this.control.get('client')!.patchValue(client);
              this.control.get('client')!.updateValueAndValidity();
            });
        }
      });
  }

  clientSelectionCallback() {
    // without setTimeout, if someone starts to filter the list,
    // then uses the mouse to make a selection, a blur event will
    // fire before the selection is made and the control will be
    // inadvertently cleared
    setTimeout(() => {
      const client = this.control.get('client')!.value as string | Client;

      if (typeof client === 'string') {
        this.control.get('client')!.patchValue('');
        this.selectedClient = null;
      } else {
        this.selectedClient = client;
      }

      this.cdr.markForCheck();
    }, 500);
  }

  addressNumber(index: number) {
    switch (index) {
      case 0:
        return 'First';
      case 1:
        return 'Second';
      case 2:
        return 'Third';
      case 3:
        return 'Fourth';
      case 4:
        return 'Fifth';
      case 5:
        return 'Sixth';
      default:
        throw new Error('Too many destination addresses');
    }
  }

  renderNote(text: string) {
    return text.replaceAll('\n', '<br>');
  }

  private filterClients(val = '') {
    const text = val.toLowerCase();

    this.filteredClients = this.clients
      .filter((client) => client.matchName(text))
      .sort((a: Client, b: Client) => stringComparer(a.name, b.name));

    this.cdr.markForCheck();
  }
}
