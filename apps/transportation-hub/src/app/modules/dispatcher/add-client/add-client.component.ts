import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
} from '@angular/forms';
import { Subscription, combineLatest } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { toPromise } from '../../../utilities';
import { Client, Tag } from '@local/models';
import { debounceTime, map, distinctUntilChanged } from 'rxjs/operators';
import { isEqual } from 'lodash-es';
import { WarningService } from '../../shared/warning/warning.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddClientComponent implements OnInit {
  clientForm: FormGroup;

  questions: any[] = [];

  clientTags: Tag[] = [];

  howDidYouHearAboutUsOther = new FormControl('');

  hasEmergencyContactControl = new FormControl();
  hasSocialWorkerControl = new FormControl();

  possibleDuplicates: Client[] = [];

  otherClientContacts = new FormArray([]);

  private existingClients: Client[] = [];
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<AddClientComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private warningService: WarningService,
  ) {}

  async ngOnInit() {
    this.subscriptions.push(
      this.fs.getTags({ type: 'client' }).subscribe((tags) => {
        this.clientTags = tags;
        this.cdr.markForCheck();
      }),
    );

    const questions = await toPromise(this.fs.getClientQuestions());

    const questionsForm = this.fb.array([]);

    this.questions = questions.map((question) => {
      if (question.type === 'section-break') {
        return question;
      }

      const form = this.fb.group({
        questionId: question.uid,
        answer: ['', question.required ? Validators.required : []],
      });

      questionsForm.push(form);

      return {
        ...question,
        answer: form,
      };
    });

    this.clientForm = this.fb.group({
      program: ['', Validators.required],
      nickname: [''],
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      emailAddress: ['', [Validators.email]],
      address: this.fb.group({
        street: ['', Validators.required],
        apartmentNumber: [''],
        city: ['', Validators.required],
        zip: ['', Validators.required],
        comments: [''],
      }),
      emergencyContact: this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        relationshipToPerson: ['', Validators.required],
      }),
      socialWorker: this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        relationshipToPerson: ['', Validators.required],
      }),
      mediCalOrSsi: ['', Validators.required],
      ethnicity: ['Decline to state', Validators.required],
      birthdate: ['', Validators.required],
      questions: questionsForm,
      authorizeSharingInfoWithDrivers: [false, Validators.requiredTrue],
      comments: [''],
      restrictions: [''],
      tagIds: [[], Validators.minLength(1)],
      otherClientContacts: this.otherClientContacts,
      defaultPeopleImpacted: [1, [Validators.required, Validators.min(1)]],
      defaultPeopleImpactedDescription: [{ value: '', disabled: true }],
    });

    // prevent entering a number less than 1
    this.subscriptions.push(
      this.clientForm
        .get('defaultPeopleImpacted')!
        .valueChanges.subscribe((value) => {
          if (value > 1) {
            this.clientForm.get('defaultPeopleImpactedDescription')!.enable();
            return;
          } else if (value === 1) {
            this.clientForm.get('defaultPeopleImpactedDescription')!.disable();
            return;
          }

          this.clientForm.get('defaultPeopleImpacted')!.patchValue(1);
        }),
    );

    // prevent entering a number less than 1 and make sure
    // appropriate tags are added
    this.subscriptions.push(
      this.clientForm.get('program')!.valueChanges.subscribe((value) => {
        const addTag = (uid: string) => {
          const currTags = this.clientForm.value.tagIds as string[];
          this.clientForm.patchValue({ tagIds: [...currTags, uid] });
        };

        const rmvTag = (uid: string) => {
          const currTags = this.clientForm.value.tagIds as string[];
          this.clientForm.patchValue({
            tagIds: currTags.filter((t) => t !== uid),
          });
        };

        const stpTag = this.clientTags.find((t) => t.label === 'STP');
        const gspTag = this.clientTags.find((t) => t.label === 'GSP');

        // ensure defaultPeopleImpacted = 1
        // ensure STP tag added and GSP tag removed
        if (value === 'STP') {
          this.clientForm.patchValue({ defaultPeopleImpacted: 1 });

          if (stpTag && !this.clientForm.value.tagIds.includes(stpTag.uid)) {
            addTag(stpTag.uid);
          }

          if (gspTag && this.clientForm.value.tagIds.includes(gspTag.uid)) {
            rmvTag(gspTag.uid);
          }
        }

        // ensure GSP tag added and STP tag removed
        if (value === 'GSP') {
          if (stpTag && this.clientForm.value.tagIds.includes(stpTag.uid)) {
            rmvTag(stpTag.uid);
          }

          if (gspTag && !this.clientForm.value.tagIds.includes(gspTag.uid)) {
            addTag(gspTag.uid);
          }
        }

        // ensure STP and GSP tag added
        if (value === 'Both') {
          if (stpTag && !this.clientForm.value.tagIds.includes(stpTag.uid)) {
            addTag(stpTag.uid);
          }

          if (gspTag && !this.clientForm.value.tagIds.includes(gspTag.uid)) {
            addTag(gspTag.uid);
          }
        }
      }),
    );

    this.subscriptions.push(
      this.hasEmergencyContactControl.valueChanges.subscribe((value) => {
        if (value) {
          this.clientForm.get('emergencyContact')!.enable();
        } else {
          this.clientForm.get('emergencyContact')!.disable();
        }
      }),
      this.hasSocialWorkerControl.valueChanges.subscribe((value) => {
        if (value) {
          this.clientForm.get('socialWorker')!.enable();
        } else {
          this.clientForm.get('socialWorker')!.disable();
        }
      }),
      combineLatest([
        this.fs.getClients({ retired: false }),
        this.fs.getClients({ retired: true }),
      ]).subscribe(([clients, retiredClients]) => {
        this.existingClients = [...clients, ...retiredClients];
      }),
      // detect duplicate clients
      this.clientForm.valueChanges
        .pipe(
          map(({ firstName, lastName, birthdate }) => ({
            firstName,
            lastName,
            birthdate,
          })),
          distinctUntilChanged(isEqual),
          debounceTime(1000),
        )
        .subscribe((value) => {
          const provFirstName: string | undefined =
            value.firstName && value.firstName.toLowerCase().trim();
          const provLastName: string | undefined =
            value.lastName && value.lastName.toLowerCase().trim();
          const provBirthdate: string | undefined =
            value.birthdate && value.birthdate.toDateString();

          if (!(provFirstName && provLastName && provBirthdate)) {
            this.possibleDuplicates = [];
          } else {
            this.possibleDuplicates = this.existingClients.filter((client) => {
              const firstName = client.firstName.toLowerCase().trim();
              const lastName = client.lastName.toLowerCase().trim();
              const birthdate = client.birthdate.toDateString();

              return (
                firstName.includes(provFirstName) &&
                lastName.includes(provLastName) &&
                birthdate === provBirthdate
              );
            });
          }

          this.cdr.markForCheck();
        }),
    );

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.clientForm.invalid) {
      return;
    }

    const form = this.clientForm.value;

    if (form.defaultPeopleImpacted === 1) {
      form.defaultPeopleImpactedDescription = null;
    }

    form.answers = form.questions.map(
      (question: { questionId: string; answer: any }) => ({
        ...question,
        updatedAt: new Date(),
      }),
    );

    form.contacts = [];

    if (this.hasEmergencyContactControl.value) {
      form.contacts.push({
        label: 'Emergency Contact',
        firstName: form.emergencyContact.firstName,
        lastName: form.emergencyContact.lastName,
        phoneNumber: form.emergencyContact.phoneNumber,
        relationshipToPerson: form.emergencyContact.relationshipToPerson,
      });
    }

    if (this.hasSocialWorkerControl.value) {
      form.contacts.push({
        label: 'Social Worker',
        firstName: form.socialWorker.firstName,
        lastName: form.socialWorker.lastName,
        phoneNumber: form.socialWorker.phoneNumber,
        relationshipToPerson: form.socialWorker.relationshipToPerson,
      });
    }

    this.otherClientContacts.value.forEach((contact: any) => {
      form.contacts.push({
        label: contact.label,
        firstName: contact.firstName,
        lastName: contact.lastName,
        phoneNumber: contact.phoneNumber,
        relationshipToPerson: contact.relationshipToPerson,
      });
    });

    delete form.program;
    delete form.otherClientContacts;
    delete form.questions;
    delete form.emergencyContact;
    delete form.socialWorker;

    const id = await this.loadingService.add(this.fs.addClient(form), {
      key: 'add-client',
    });

    await this.warningService
      .popup({
        title: 'Checklist',
        text: `After adding a new client, <strong>don't forget to also create a Google Contacts entry for them</strong> so that their name shows up next to their phone number when they call us.

        To do this, open up “Google Contacts” (<em>Important! Make sure you are using Google Contacts with the transportation google account and not another google account</em>). Add a contact entry for this client if one doesn’t already exist for them. Be sure the contact entry includes the client’s name, email address, and phone number. Last, apply the “GSP Client” label to the client’s contact entry if they are participating in the GSP program and add the “STP Client” label to their contact entry if they are participating in the STP program.`,
        affirmative: 'Gotcha',
      })
      .afterClosed()
      .toPromise();

    this.ref.close(id);
  }

  addContact() {
    this.otherClientContacts.push(
      this.fb.group({
        label: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        relationshipToPerson: ['', Validators.required],
      }),
    );
  }

  removeContact(index: number) {
    this.otherClientContacts.removeAt(index);
    this.cdr.markForCheck();
  }

  contactNumber(index: number) {
    switch (index) {
      case 0:
        return 'First';
      case 1:
        return 'Second';
      case 2:
        return 'Third';
      case 3:
        return 'Fourth';
      case 4:
        return 'Fifth';
      case 5:
        return 'Sixth';
      default:
        throw new Error('Too many client contacts');
    }
  }
}
