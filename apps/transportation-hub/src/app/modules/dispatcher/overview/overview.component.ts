import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  TrackByFunction,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Subscription, combineLatest, of, Observable } from 'rxjs';
import { RideRequest, Driver, Client } from '@local/models';
import { map, switchMap, share, distinctUntilChanged } from 'rxjs/operators';
import { startOfToday, compareAsc, addMonths } from 'date-fns';
import uniqBy from 'lodash-es/uniqBy';
import flatten from 'lodash-es/flatten';
import { Title } from '@angular/platform-browser';
import {
  navigateToPathFn,
  ngForTrackByID,
  isNonNullable,
} from '../../../utilities';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { isEqual } from '@local/isEqual';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'padding-24',
  },
})
export class OverviewComponent implements OnInit, OnDestroy {
  // rideRequestsToday: RideRequest[] = [];
  rideRequestsWithoutDriver: RideRequest[] = [];
  ridesWithTasks: RideRequest[] = [];
  driversWithFlags: Driver[] = [];
  clientsWithFlags: Client[] = [];

  todos: Array<{
    request?: RideRequest;
    type: string;
    person: Client | Driver;
    text: string;
    url: string;
  }> = [];

  trackByTodo: TrackByFunction<{
    request?: RideRequest;
    type: string;
    person: Client | Driver;
  }> = (_, item) =>
    item.person.uid + item.type + (item.request?.datetime.toISOString() || '');

  trackByID = ngForTrackByID;

  private subscriptions: Subscription[] = [];
  private isLoadingObs: Array<{ obs: Observable<unknown>; key: string }> = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private titleService: Title,
    private router: Router,
    private isLoadingService: IsLoadingService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    this.titleService.setTitle(`Overview | Hub`);

    function combineFlatten<T>(arr: Observable<T[]>[]) {
      return combineLatest(arr).pipe(map((v) => flatten(v)));
    }

    const trackLoadingOfFirstEmit = <T>(a: Observable<T>, key: string) => {
      this.isLoadingObs.push({ obs: a, key });
      return this.isLoadingService.add(a, { key });
    };

    // ride requests without a driver
    const rideRequestsWithoutDriver = this.fs.getRideRequests(
      {
        driverId: null,
        startAfterOrOnDate: startOfToday(),
        // only show the next month's worth of requests
        endBeforeDate: addMonths(startOfToday(), 1),
        requestIsCancelled: false,
      },
      { driver: false },
    );

    this.subscriptions.push(
      trackLoadingOfFirstEmit(
        rideRequestsWithoutDriver,
        'ride-requests-without-driver',
      ).subscribe((rideRequests) => {
        this.rideRequestsWithoutDriver = rideRequests;
        this.cdr.markForCheck();
      }),
    );

    const flaggedProfileIds = this.fs
      .getNoteThreads({
        flag: true,
        suppressTodo: false,
        limit: 100,
      })
      .pipe(
        distinctUntilChanged(isEqual),
        map((threads) => {
          const driverIds = new Set(threads.map((t) => t.driverId));
          const clientIds = new Set(threads.map((t) => t.clientId));
          const requestIds = new Set(threads.map((t) => t.rideRequestId));

          driverIds.delete(undefined);
          clientIds.delete(undefined);
          requestIds.delete(undefined);

          return {
            driverIds: Array.from(driverIds as Set<string>),
            clientIds: Array.from(clientIds as Set<string>),
            requestIds: Array.from(requestIds as Set<string>),
          };
        }),
        share(),
      );

    const driverTodos = flaggedProfileIds.pipe(
      switchMap(({ driverIds }) =>
        driverIds.length === 0
          ? of([])
          : combineLatest(driverIds.map((id) => this.fs.getDriver(id))),
      ),
      map((drivers) => drivers.filter(isNonNullable)),
      distinctUntilChanged(isEqual),
    );

    const clientTodos = flaggedProfileIds.pipe(
      switchMap(({ clientIds }) =>
        clientIds.length === 0
          ? of([])
          : combineLatest(clientIds.map((id) => this.fs.getClient(id))),
      ),
      map((clients) => clients.filter(isNonNullable)),
      distinctUntilChanged(isEqual),
    );

    // has flag
    const requestHasFlag = flaggedProfileIds.pipe(
      switchMap(({ requestIds }) =>
        requestIds.length === 0
          ? of([])
          : combineLatest(requestIds.map((id) => this.fs.getRideRequest(id))),
      ),
      map((requests) => requests.filter(isNonNullable)),
      distinctUntilChanged(isEqual),
    );

    const filterOutRecurringRequestInstancesWithSameVolunteerAfterTheFirst = (
      requests: RideRequest[],
    ) => {
      // Map<recurring ride request ID, next upcoming instance of the recurring request>()
      const store = new Map<string, RideRequest>();

      requests.forEach((request) => {
        if (request.recurringRideRequest) {
          if (request.driverId! === request.recurringRideRequest.driverId) {
            if (store.has(request.recurringRideRequestId!)) return;

            store.set(request.recurringRideRequestId!, request);
          } else {
            store.set(request.uid!, request);
          }
        } else {
          store.set(request.uid!, request);
        }
      });

      return Array.from(store.values());
    };

    // has driver which has not been notified
    const hasDriverWhichHasNotBeenNotified = this.fs
      .getRideRequests(
        {
          driverIdIsNotNull: true,
          startAfterOrOnDate: startOfToday(),
          driverNotifiedAt: null,
          requestIsCancelled: false,
        },
        {
          recurringRideRequest: true,
        },
      )
      .pipe(
        // If a driver is assigned as the ongoing volunteer for a recurring request,
        // all future requests will initially pop up as "has driver which has not been
        // notified". All the instances after the first are just noise so we want to
        // filter them out
        map(filterOutRecurringRequestInstancesWithSameVolunteerAfterTheFirst),
      );

    // has driver but client not notified
    const hasDriverButClientNotNotified = this.fs
      .getRideRequests(
        {
          driverIdIsNotNull: true,
          startAfterOrOnDate: startOfToday(),
          clientNotifiedOfDriverAt: null,
          requestIsCancelled: false,
        },
        {
          recurringRideRequest: true,
        },
      )
      .pipe(
        // If a driver is assigned as the ongoing volunteer for a recurring request,
        // all future requests will initially pop up as "has driver but client not notified".
        // All the instances after the first are just noise so we want to
        // filter them out
        map(filterOutRecurringRequestInstancesWithSameVolunteerAfterTheFirst),
      );

    // request cancelled but driver not notified
    const requestCancelledButDriverNotNotified = this.fs.getRideRequests({
      driverIdIsNotNull: true,
      startAfterOrOnDate: startOfToday(),
      requestIsCancelled: true,
      driverNotifiedOfCancellationAt: null,
    });

    /*
      It looks like the client was notified that a volunteer had agreed to
      drive them. However, after that happened, the driver was removed from
      this ride request.
    */
    const driverRemovedFromRideRequest = this.fs
      .getRideRequests(
        {
          driverId: null,
          startAfterOrOnDate: startOfToday(),
          requestIsCancelled: false,
          clientNotifiedOfDriverAtIsNotNull: true,
        },
        {
          recurringRideRequest: true,
        },
      )
      .pipe(
        // If a driver is assigned as the ongoing volunteer for a recurring request,
        // all future requests will initially pop up as "Need to notify client that driver cancelled".
        // All the instances after the first are just noise so we want to
        // filter them out
        map(filterOutRecurringRequestInstancesWithSameVolunteerAfterTheFirst),
      );

    const rideRequestTodos = combineFlatten([
      hasDriverWhichHasNotBeenNotified,
      hasDriverButClientNotNotified,
      requestCancelledButDriverNotNotified,
      requestCancelledButDriverNotNotified,
      driverRemovedFromRideRequest,
      requestHasFlag,
    ]).pipe(
      map((responses) =>
        responses.sort((a, b) => {
          const x = a.datetime.valueOf();
          const y = b.datetime.valueOf();

          if (x > y) return 1;
          if (x < y) return -1;
          return 0;
        }),
      ),
      map((rr) => uniqBy(rr, 'uid')),
      distinctUntilChanged(isEqual),
    );

    this.subscriptions.push(
      trackLoadingOfFirstEmit(
        combineLatest([clientTodos, driverTodos, rideRequestTodos]),
        'overview-todos',
      ).subscribe(([clients, drivers, requests]) => {
        this.clientsWithFlags = clients;
        this.driversWithFlags = drivers;
        this.ridesWithTasks = requests;
        this.generateTodos();
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.isLoadingObs.forEach(({ obs, key }) =>
      this.isLoadingService.remove(obs, { key }),
    );

    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  requestTodoText(rr: RideRequest): string {
    const notifyDriverCancellation =
      rr.driverId &&
      rr.requestCancelledAt &&
      !rr.driverNotifiedOfCancellationAt;
    const notifyDriver = rr.driverId && !rr.driverNotifiedAt;
    const notifyClient =
      rr.driverId && !rr.clientNotifiedOfDriverAt && !rr.requestCancelledAt;
    const willingDriver = !rr.driverId && rr.hasWillingDriver;
    const hasFlag = (rr.flags ?? 0) > 0;
    const notifyClientThatDriverCancelled =
      rr.driverId === null && rr.clientNotifiedOfDriverAt !== null;

    let todo = '';

    if (notifyDriverCancellation) {
      todo = 'Need to notify volunteer of cancellation.';
    } else if (notifyClientThatDriverCancelled) {
      todo = 'Need to notify client that volunteer cancelled.';
    } else if (notifyDriver && notifyClient) {
      todo = 'Need to notify volunteer and client.';
    } else if (notifyDriver) {
      todo = 'Need to notify volunteer.';
    } else if (notifyClient) {
      todo = 'Need to notify client.';
    } else if (willingDriver) {
      todo = 'Has people willing to volunteer but no one is assigned.';
    }

    if (hasFlag) return `Flagged as important. ${todo}`.trim();

    return todo;
  }

  generateTodos() {
    this.todos = [
      ...this.ridesWithTasks.map((r) => {
        return {
          request: r,
          type: 'Request',
          person: r.client,
          text: this.requestTodoText(r),
          url: `/dispatcher/ride-request/${r.uid}/overview`,
        };
      }),

      ...this.clientsWithFlags.map((c) => {
        return {
          type: 'Client',
          person: c,
          text: 'Flagged as important.',
          url: `/dispatcher/client/${c.uid}/profile`,
        };
      }),

      ...this.driversWithFlags.map((d) => {
        return {
          type: 'Driver',
          person: d,
          text: 'Flagged as important.',
          url: `/dispatcher/driver/${d.uid}/profile`,
        };
      }),
    ];

    this.todos.sort((a, b) => {
      if (a.type === 'Client') {
        if (b.type === 'Client') {
          return a.person.name.toLowerCase() > b.person.name.toLowerCase()
            ? 1
            : -1;
        }

        return -1;
      } else if (b.type === 'Client') {
        return 1;
      } else if (a.type === 'Driver') {
        if (b.type === 'Driver') {
          return a.person.name.toLowerCase() > b.person.name.toLowerCase()
            ? 1
            : -1;
        }

        return -1;
      } else if (b.type === 'Driver') {
        return 1;
      } else {
        return compareAsc(a.request!.datetime, b.request!.datetime);
      }
    });
  }
}
