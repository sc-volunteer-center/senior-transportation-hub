import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-overview-breadcrumb',
  template: `
    Overview
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverviewBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
