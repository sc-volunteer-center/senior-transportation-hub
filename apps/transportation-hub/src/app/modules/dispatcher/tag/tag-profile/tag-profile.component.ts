import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Tag, Dispatcher } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { EditTagComponent } from '../edit-tag/edit-tag.component';
import { MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { WarningComponent } from '../../../shared/warning/warning.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tag-profile',
  templateUrl: './tag-profile.component.html',
  styleUrls: ['./tag-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'profile-component',
  },
})
export class TagProfileComponent implements OnInit {
  tag: Tag | null = null;

  private dispatcher: Dispatcher | null = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private dialog: MatDialog,
    private isLoadingService: IsLoadingService,
    private router: Router,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.tagId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getTag(id))))
        .subscribe((tag) => {
          this.tag = tag;
          this.titleService.setTitle(`${tag?.label} Tag | Hub`);
          this.cdr.markForCheck();
        }),
    );

    this.subscriptions.push(
      this.fs.getCurrentDispatcher().subscribe((dispatcher) => {
        this.dispatcher = dispatcher;
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editTag() {
    this.dialog.open(EditTagComponent, {
      width: '600px',
    });
  }

  async deleteTag() {
    if (!this.tag || !this.dispatcher) {
      return;
    }

    const type = this.tag.type;

    const response = await this.dialog
      .open(WarningComponent, {
        data: {
          title: 'Heads up!',
          text: `Deleting this tag will remove it from
          every ${type} it was applied to. If you just want to
          remove this tag from a specific ${type}, instead navigate
          to that ${type}'s profile and edit it.`,
          affirmative: 'delete tag',
        },
      })
      .afterClosed()
      .toPromise();

    if (!response) return;

    await this.router.navigate(['/dispatcher/tags', type]);

    this.isLoadingService.add(this.fs.destroyTag(this.tag.uid));
  }
}
