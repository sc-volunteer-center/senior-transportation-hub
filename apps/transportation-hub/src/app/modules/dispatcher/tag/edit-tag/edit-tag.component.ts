import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../../../../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { Subscription, of } from 'rxjs';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { switchMap } from 'rxjs/operators';
import { Tag } from '@local/models';
import { TAG_COLORS } from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-edit-tag',
  templateUrl: './edit-tag.component.html',
  styleUrls: ['./edit-tag.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditTagComponent implements OnInit {
  tag: Tag | null = null;
  tagForm?: FormGroup;

  colors = Array.from(TAG_COLORS.keys());

  TAG_COLORS = TAG_COLORS;

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private ref: MatDialogRef<EditTagComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.tagId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getTag(id))))
        .subscribe((tag) => {
          this.tag = tag;
          this.tagForm = undefined;
          this.cdr.markForCheck();

          if (!tag) return;

          this.tagForm = this.fb.group({
            label: [
              tag.label,
              [
                Validators.required,
                Validators.maxLength(50),
                Validators.minLength(1),
              ],
            ],
            description: [tag.description, Validators.required],
            color: [tag.color, Validators.required],
            favorite: [tag.favorite, Validators.required],
          });
        }),
    );
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (!this.tagForm || this.tagForm.invalid) {
      return;
    }

    await this.loadingService.add(
      this.fs.updateTag(this.tag!.uid, this.tagForm.value),
      { key: 'edit-tag' },
    );

    this.ref.close();
  }
}
