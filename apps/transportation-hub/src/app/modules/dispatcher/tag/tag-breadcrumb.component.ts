import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Tag } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';

@Component({
  selector: 'app-tag-breadcrumb',
  template: ` {{ tag?.type | titlecase }} Tag `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagBreadcrumbComponent implements OnInit {
  tag: Tag | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.tagId)
        .pipe(switchMap((id) => this.fs.getTag(id!)))
        .subscribe((tag) => {
          this.tag = tag;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
