import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../../../../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { Subscription } from 'rxjs';
import { toObservable } from '../../../../utilities';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-destination',
  templateUrl: './add-destination.component.html',
  styleUrls: ['./add-destination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddDestinationComponent implements OnInit {
  destinationForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private firestore: FirestoreService,
    private ref: MatDialogRef<AddDestinationComponent>,
    private loadingService: IsLoadingService,
    private router: Router,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.destinationForm = this.fb.group({
      label: [
        '',
        [
          Validators.required,
          Validators.maxLength(40),
          Validators.minLength(3),
        ],
      ],
      street: ['', Validators.required],
      apartmentNumber: [''],
      city: ['', Validators.required],
      zip: ['', Validators.required],
      comments: [''],
    });
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.destinationForm.invalid) {
      return;
    }

    const id = await this.loadingService.add(
      this.firestore.addDestination(this.destinationForm.value),
      { key: 'add-destination' },
    );

    this.ref.close(id);
  }
}
