import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Subscription } from 'rxjs';
import { Destination } from '@local/models';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { AddDestinationComponent } from './add-destination/add-destination.component';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { stringComparer, navigateToPathFn } from '../../../utilities';
import { DestinationsBreadcrumbService } from './destinations-breadcrumb.component';
import { Title } from '@angular/platform-browser';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DestinationsComponent implements OnInit {
  destinations: Destination[] = [];

  searchControl = new FormControl('');

  filterText = '';
  filteredDestinations: Destination[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private destinationsBreadcrumbService: DestinationsBreadcrumbService,
    private titleService: Title,
    private isLoadingService: IsLoadingService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get('search') || '';

    this.titleService.setTitle(`Destinations | Hub`);

    this.searchControl.patchValue(query);

    this.subscriptions.push(
      this.isLoadingService
        .add(this.fs.getDestinations(), { key: 'loading-destinations' })
        .subscribe((destinations) => {
          this.destinations = destinations;
          this.filteredDestinations = destinations.slice();
          this.filterDestinations();
        }),
      this.searchControl.valueChanges
        .pipe(debounceTime(500))
        .subscribe((value) => {
          this.router.navigate([], {
            queryParams: {
              search: value.toLowerCase(),
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
      this.route.queryParamMap.subscribe((params) => {
        const search = params.get('search') || '';
        this.searchControl.patchValue(search, { emitEvent: false });
        this.filterDestinations(search);
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  addDestination() {
    this.dialog
      .open(AddDestinationComponent, {
        width: '600px',
      })
      .afterClosed()
      .subscribe((id) => {
        if (!id) return;

        this.router.navigate(['/dispatcher/destination', id, 'profile']);
      });
  }

  filterDestinations(text?: string) {
    this.filterText = (text && text.toLowerCase()) || '';

    this.filteredDestinations = this.destinations.filter((destination) =>
      `${destination.label} ${destination.street}`
        .toLowerCase()
        .includes(this.filterText),
    );

    this.filteredDestinations
      .sort((a, b) => {
        return stringComparer(a.label!, b.label!);
      })
      .reverse();

    this.destinationsBreadcrumbService.destinationCount.next(
      this.filteredDestinations.length,
    );
    this.cdr.detectChanges();
  }
}
