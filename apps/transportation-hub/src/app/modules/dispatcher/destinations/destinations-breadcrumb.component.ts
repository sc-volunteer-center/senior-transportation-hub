import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Injectable,
  ChangeDetectorRef,
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DestinationsBreadcrumbService {
  destinationCount = new BehaviorSubject(0);
}

@Component({
  selector: 'app-destinations-breadcrumb',
  template: `
    <div
      *ngIf="
        'loading-destinations' | swIsLoading | async;
        else destinationsLoaded
      "
      class="flex-row flex-center"
    >
      <mat-spinner
        [diameter]="22"
        color="accent"
        class="vertical-margin-16"
        style="margin-right: 0.5rem"
      ></mat-spinner>
      <h3>Favorite Destinations</h3>
    </div>

    <ng-template #destinationsLoaded>
      {{ count }}
      {{ count === 1 ? 'Favorite Destination' : 'Favorite Destinations' }}
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DestinationsBreadcrumbComponent implements OnInit {
  count = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    public destinationsBreadcrumbService: DestinationsBreadcrumbService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.destinationsBreadcrumbService.destinationCount.subscribe((count) => {
        this.count = count;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
