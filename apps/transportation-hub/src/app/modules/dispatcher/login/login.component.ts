import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Dispatcher } from '@local/models';
import { FirestoreService } from '../../../firestore.service';
import { Subscription } from 'rxjs';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { toObservable, toPromise } from '../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { Router } from '@angular/router';
import { filter, take, switchMap, tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { wait } from '@local/utilities';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  dispatchers: Dispatcher[] = [];
  filteredDispatchers: Dispatcher[] = [];

  loginForm: FormGroup;

  private subscriptions: Subscription[] = [];

  dispatcherDisplayFn(dispatcher?: Dispatcher): string | undefined {
    return dispatcher && dispatcher.name;
  }

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private isLoadingService: IsLoadingService,
    private router: Router,
    private dialog: MatDialog,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.fs.getDispatchers({ retired: false }).subscribe((dispatchers) => {
        this.dispatchers = dispatchers;
        this.filteredDispatchers = dispatchers;
        this.cdr.markForCheck();
      }),
      this.fs
        .getOrganization()
        .pipe(
          switchMap((org) =>
            this.fs.getCurrentDispatcher().pipe(
              tap((disp) => {
                if (org && disp) {
                  this.router.navigate(['dispatcher/overview']);
                }
              }),
            ),
          ),
        )
        .subscribe(),
    );

    this.loginForm = this.fb.group({
      dispatcherId: ['', [Validators.required]],
    });

    this.subscriptions.push(
      this.loginForm.get('dispatcherId')!.valueChanges.subscribe((value) => {
        if (typeof value !== 'string') {
          return;
        }

        this.filterDispatchers(value);
      }),
    );

    this.titleService.setTitle(`Dispatcher Login | Hub`);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  get isFormInvalid() {
    return (
      this.loginForm.invalid ||
      typeof this.loginForm.get('dispatcherId')!.value !== 'object'
    );
  }

  async submit() {
    // run dispatcherSelectionCallback() without the timeout
    const dispatcher = this.loginForm.get('dispatcherId')!.value as
      | string
      | Dispatcher;

    if (typeof dispatcher === 'string') {
      this.loginForm.get('dispatcherId')!.patchValue('');
    }

    if (this.isFormInvalid) {
      return;
    }

    await this.isLoadingService.add(
      this.fs
        .startDispatcherShift(this.loginForm.get('dispatcherId')!.value.uid)
        // need to wait for the cookieService to pick up the change.
        .then(() => wait(1010)),
      { key: 'dispatcher-login' },
    );

    const disp = await toPromise(this.fs.getCurrentDispatcher());

    if (!disp) return;

    await this.router.navigate(['/dispatcher/overview']);
  }

  dispatcherSelectionCallback() {
    // without setTimeout, if someone starts to filter the list,
    // then uses the mouse to make a selection, a blur event will
    // fire before the selection is made and the control will be
    // inadvertently cleared
    setTimeout(() => {
      const dispatcher = this.loginForm.get('dispatcherId')!.value as
        | string
        | Dispatcher;

      if (typeof dispatcher === 'string') {
        this.loginForm.get('dispatcherId')!.patchValue('');
      }
    }, 500);
  }

  private filterDispatchers(val: string) {
    this.filteredDispatchers = this.dispatchers.filter((dispatcher) =>
      dispatcher.name.toLowerCase().includes(val.toLowerCase()),
    );

    this.cdr.markForCheck();
  }
}
