import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { IOrganization } from '@local/models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-breadcrumb',
  template: `
    {{ organization?.name }}
    <mat-icon class="side-margin-16">chevron_right</mat-icon> Signin
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginBreadcrumbComponent implements OnInit {
  organization: IOrganization | null = null;

  private subscriptions: Subscription[] = [];

  constructor(private fs: FirestoreService, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.subscriptions.push(
      this.fs.getOrganization().subscribe(org => {
        this.organization = org;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
