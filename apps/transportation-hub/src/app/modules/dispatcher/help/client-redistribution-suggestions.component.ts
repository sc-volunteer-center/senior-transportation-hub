import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { subWeeks, addWeeks, startOfDay, endOfDay } from 'date-fns';
import { take, map, filter } from 'rxjs/operators';
import { IsLoadingService } from '@service-work/is-loading';
import { RideRequest, Driver, RecurringRideRequest } from '@local/models';
import { MapsService } from '../../../maps.service';
import {
  comparer,
  navigateToPathFn,
  stringComparer,
  isRideRequestInstanceDifferentFromRecurringParent,
} from '../../../utilities';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ISimpleTag } from '../../shared/display-tags/display-tags.component';

@Component({
  selector: 'app-client-redistribution-suggestions',
  template: `
    <h3 mat-dialog-title>
      <span
        *ngIf="
          'redistribution-suggestions' | swIsLoading | async;
          else defaultTitle
        "
      >
        This could take a minute to load...
      </span>

      <ng-template #defaultTitle>
        Drivers who are inactive for the next 3 weeks
      </ng-template>
    </h3>

    <mat-dialog-content
      style="overflow-y: auto; max-height: 60vh;"
      class="vertical-padding-8"
    >
      <div
        *ngIf="
          'redistribution-suggestions' | swIsLoading | async;
          else suggestions
        "
        class="flex-1 flex-center"
        style="height: 300px;"
      >
        <mat-spinner></mat-spinner>
      </div>

      <ng-template #suggestions>
        <mat-accordion>
          <mat-expansion-panel *ngFor="let driver of inactiveDrivers">
            <mat-expansion-panel-header (click)="driver.__resultsCount = 10">
              <mat-panel-title>
                <app-display-tags
                  [label]="driver.nameWithRetiredStatus"
                  [tagIds]="driver.tagIds"
                  [tags]="driver.__customTags"
                >
                </app-display-tags>
              </mat-panel-title>
            </mat-expansion-panel-header>

            <table style="width: 100%">
              <thead>
                <tr>
                  <th>Dist. (mi)</th>
                  <th>Date</th>
                  <th>Client</th>
                  <th>Driver (upcoming reqs)</th>
                </tr>
              </thead>

              <tbody>
                <tr
                  *ngFor="
                    let request of driver.nearbyRideRequests
                      | slice: 0:driver.__resultsCount
                  "
                  class="clickable"
                  (click)="
                    navigateToPath($event, [
                      '/dispatcher/ride-request',
                      request.uid,
                      'overview'
                    ])
                  "
                >
                  <td>{{ request.distanceFromDriver }}</td>
                  <td>{{ request.datetime | date: 'shortDate' }}</td>
                  <td>{{ request.client.nameWithRetiredStatus }}</td>
                  <td>
                    <span *ngIf="request.driver; else noDriver">
                      {{ request.driver!.nameWithRetiredStatus }}
                      ({{ driversRequestMap.get(request.driverId) }})
                    </span>

                    <ng-template #noDriver>
                      <span style="color: red;"> none </span>
                    </ng-template>
                  </td>
                </tr>
              </tbody>
            </table>

            <div class="flex-row" style="margin-top: 1rem;">
              <button
                *ngIf="driver.nearbyRideRequests.length > driver.__resultsCount"
                mat-button
                type="button"
                (click)="driver.__resultsCount = driver.__resultsCount + 10"
              >
                Show 10 more results
              </button>

              <div class="flex-1"></div>

              <a
                mat-button
                [routerLink]="['/dispatcher/driver', driver.uid, 'profile']"
              >
                View profile
              </a>
            </div>
          </mat-expansion-panel>
        </mat-accordion>
      </ng-template>
    </mat-dialog-content>

    <mat-dialog-actions>
      <div class="flex-1"></div>
      <button mat-button matDialogClose>Done</button>
    </mat-dialog-actions>
  `,
  styles: [
    `
      th {
        text-align: left;
      }

      th,
      td {
        padding: 0 1rem;
      }

      th:first-child,
      td:first-child {
        padding-left: 0;
      }

      th:last-child,
      td:last-child {
        padding-right: 0;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientRedistributionSuggestionsComponent implements OnInit {
  inactiveDrivers: Array<
    Driver & {
      nearbyRideRequests: Array<RideRequest & { distanceFromDriver: number }>;
      __resultsCount: number;
      __customTags: ISimpleTag[];
    }
  > = [];

  driversRequestMap = new Map<string, number>();

  drivers: Array<
    Driver & {
      rideRequestsInNextThreeWeeks: RideRequest[];
    }
  > = [];

  rideRequestsInNextThreeWeeks: RideRequest[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private isLoadingService: IsLoadingService,
    private mapsService: MapsService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private ref: MatDialogRef<ClientRedistributionSuggestionsComponent>,
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    this.subscriptions.push(
      this.router.events
        .pipe(filter((e) => e instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.isLoadingService.add(this.calculate(), {
      key: 'redistribution-suggestions',
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async calculate() {
    // Intentionally performing these requests serially rather than in parallel
    // in attempt to squash some kind of bug that I think is coming from
    // the firestore sdk itself
    const recurringRideRequests = await this.fs
      .getRecurringRideRequests({
        endAfterDate: startOfDay(new Date()),
        startsBeforeOrOnDate: addWeeks(endOfDay(new Date()), 3),
        requestIsCancelled: false,
        // getOnce: true,
      })
      .pipe(take(1))
      .toPromise();

    this.rideRequestsInNextThreeWeeks = await this.fs
      .getRideRequests({
        startAfterOrOnDate: endOfDay(new Date()),
        endBeforeDate: addWeeks(startOfDay(new Date()), 3),
        requestIsCancelled: false,
        // getOnce: true,
      })
      .pipe(take(1))
      .toPromise();

    this.rideRequestsInNextThreeWeeks.forEach((rr) => {
      if (!rr.recurringRideRequestId) return;

      rr.recurringRideRequest = recurringRideRequests.find(
        (rrr) => rrr.uid === rr.recurringRideRequestId,
      )!;
    });

    this.drivers = await this.fs
      .getDrivers({ retired: false, getOnce: true, onVacation: false })
      .pipe(
        take(1),
        map((_drivers) => {
          const drivers = _drivers as Array<
            Driver & {
              rideRequestsInNextThreeWeeks: RideRequest[];
            }
          >;

          drivers.forEach((driver) => {
            driver.rideRequestsInNextThreeWeeks =
              this.rideRequestsInNextThreeWeeks.filter(
                (r) => r.driverId === driver.uid,
              );
          });

          return drivers;
        }),
      )
      .toPromise();

    this.drivers.forEach((driver) => {
      this.driversRequestMap.set(
        driver.uid,
        driver.rideRequestsInNextThreeWeeks.length,
      );
    });

    const rideRequestsInNextThreeWeeksWithImpactedDrivers =
      this.rideRequestsInNextThreeWeeks.filter((r) => {
        if (!r.driverId) return true;

        return this.driversRequestMap.get(r.driverId)! > 4;
      });

    // we're fetching the drivers down here in order to reduce the number of individual
    // driver doc requests we make to firestore because firestore is *really* unhappy
    // when we fetch the drivers in the call to fs.getRideRequests() above.
    await Promise.all(
      rideRequestsInNextThreeWeeksWithImpactedDrivers.map((r) =>
        r.driverId
          ? this.fs.getDriver(r.driverId).pipe(take(1)).toPromise()
          : null,
      ),
    ).then((drivers) => {
      drivers.forEach((d, i) => {
        rideRequestsInNextThreeWeeksWithImpactedDrivers[i].driver = d;
      });
    });

    this.inactiveDrivers = this.drivers
      .filter((d) => d.rideRequestsInNextThreeWeeks.length <= 4)
      .map((d) => {
        const driver = d.clone() as unknown as Driver & {
          rideRequestsInNextThreeWeeks: RideRequest[];
          nearbyRideRequests: Array<
            RideRequest & { distanceFromDriver: number }
          >;
          __resultsCount: number;
          __customTags: ISimpleTag[];
        };

        driver.rideRequestsInNextThreeWeeks =
          d.rideRequestsInNextThreeWeeks.slice();
        driver.__resultsCount = 10;
        driver.__customTags = this.getCustomTags(driver);

        // this is used to ensure that we only grab a single instance
        // of each recurring ride request.
        const tempRecurringRRIdSet = new Set<string>();

        driver.nearbyRideRequests =
          rideRequestsInNextThreeWeeksWithImpactedDrivers
            .map((r) => {
              const request = r.clone() as RideRequest & {
                distanceFromDriver: number;
              };

              request.distanceFromDriver =
                this.mapsService.birdsDistance([
                  driver.address,
                  request.pickupAddress,
                ]) || -1;

              request.distanceFromDriver = parseFloat(
                request.distanceFromDriver.toFixed(1),
              );

              return request;
            })
            .filter((r) => r.distanceFromDriver >= 0)
            .sort(
              comparer((a, b) => {
                if (a.distanceFromDriver === b.distanceFromDriver) {
                  return [a.datetime.valueOf(), b.datetime.valueOf()];
                }

                return [a.distanceFromDriver, b.distanceFromDriver];
              }),
            )
            .filter((r) => {
              if (!r.recurringRideRequestId) return true;
              if (
                isRideRequestInstanceDifferentFromRecurringParent(
                  r,
                  r.recurringRideRequest!,
                )
              ) {
                return true;
              }

              if (tempRecurringRRIdSet.has(r.recurringRideRequestId)) {
                return false;
              }

              tempRecurringRRIdSet.add(r.recurringRideRequestId);

              return true;
            });

        return driver;
      })
      // I think the only way "nearbyRideRequests === 0" is if we don't have proper
      // address information for the driver
      .filter((d) => d.nearbyRideRequests.length > 0)
      .sort((a, b) => stringComparer(a.name, b.name));

    this.cdr.markForCheck();
  }

  private getCustomTags(
    driver: Driver & {
      rideRequestsInNextThreeWeeks: RideRequest[];
    },
  ) {
    const tags: ISimpleTag[] = [];

    if (driver.rideRequestsInNextThreeWeeks.length > 0) {
      tags.push({
        label: driver.rideRequestsInNextThreeWeeks.length.toString(),
        description: `Has ${
          driver.rideRequestsInNextThreeWeeks.length
        } request${
          driver.rideRequestsInNextThreeWeeks.length === 1 ? '' : 's'
        } in the next 3 weeks.`,
        color: 'Gray',
      });
    }

    return tags;
  }
}
