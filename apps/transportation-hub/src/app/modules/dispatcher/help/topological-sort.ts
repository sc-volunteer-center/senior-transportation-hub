// THIS CODE IS ADAPTED FROM
// https://www.geeksforgeeks.org/all-topological-sorts-of-a-directed-acyclic-graph/

// # class to represent a graph object
// class Graph:

// 	# Constructor
// 	def __init__(self, edges, N):

// 		# A List of Lists to represent an adjacency list
// 		self.adjList = [[] for _ in range(N)]

// 		# stores in-degree of a vertex
// 		# initialize in-degree of each vertex by 0
// 		self.indegree = [0] * N

// 		# add edges to the undirected graph
// 		for (src, dest) in edges:

// 			# add an edge from source to destination
// 			self.adjList[src].append(dest)

// 			# increment in-degree of destination vertex by 1
// 			self.indegree[dest] = self.indegree[dest] + 1

// class Graph {
//   public adjacencyList: number[][];
//   public indegree: number[];

//   constructor(edges: [number, number][], public numberOfNodes: number) {
//     this.adjacencyList = Array(this.numberOfNodes)
//       .fill(0) // fill([]) will use the same array instance for each element
//       .map(() => []);

//     this.indegree = Array(this.numberOfNodes).fill(0);

//     for (const [src, dest] of edges) {
//       this.adjacencyList[src].push(dest);
//       this.indegree[dest]++;
//     }
//   }
// }

// // # Recursive function to find
// // # all topological orderings of a given DAG
// // def findAllTopologicalOrders(graph, path, discovered, N):

// function findAllTopologicalOrders(
//   graph: Graph,
//   path: number[],
//   discovered: boolean[],
// ) {
//   const { numberOfNodes } = graph;
//   // 	# do for every vertex
//   // 	for v in range(N):
//   for (let nodeNumber = 0; nodeNumber < numberOfNodes; nodeNumber++) {
//     // 		# proceed only if in-degree of current node is 0 and
//     // 		# current node is not processed yet
//     // 		if graph.indegree[v] == 0 and not discovered[v]:
//     if (graph.indegree[nodeNumber] !== 0 || discovered[nodeNumber]) continue;

//     // 			# for every adjacent vertex u of v,
//     // 			# reduce in-degree of u by 1
//     // 			for u in graph.adjList[v]:
//     // 				graph.indegree[u] = graph.indegree[u] - 1
//     for (const u of graph.adjacencyList[nodeNumber]) {
//       graph.indegree[u]--;
//     }

//     // 			# include current node in the path
//     // 			# and mark it as discovered
//     // 			path.append(v)
//     path.push(nodeNumber);
//     // 			discovered[v] = True
//     discovered[nodeNumber] = true;

//     // 			# recur
//     // 			findAllTopologicalOrders(graph, path, discovered, N)
//     findAllTopologicalOrders(graph, path, discovered);

//     // 			# backtrack: reset in-degree
//     // 			# information for the current node
//     // 			for u in graph.adjList[v]:
//     // 				graph.indegree[u] = graph.indegree[u] + 1
//     for (const u of graph.adjacencyList[nodeNumber]) {
//       graph.indegree[u]++;
//     }

//     // 			# backtrack: remove current node from the path and
//     // 			# mark it as undiscovered
//     // 			path.pop()
//     // 			discovered[v] = False
//     path.pop();
//     discovered[nodeNumber] = false;
//   }

//   // 	# print the topological order if
//   // 	# all vertices are included in the path
//   // 	if len(path) == N:
//   // 		print(path)
//   // result.push(path);
//   if (path.length === numberOfNodes) {
//     console.log('path', path);
//   }
// }

// // # Print all topological orderings of a given DAG
// // def printAllTopologicalOrders(graph):
// export function getAllTopologicalOrdersForGraph(
//   edges: [number, number][],
//   numberOfNodes: number,
// ) {
//   const graph = new Graph(edges, numberOfNodes);

//   console.log('graph init', JSON.stringify(graph));

//   // 	# get number of nodes in the graph
//   // 	N = len(graph.adjList)
//   // const numberOfNodes = graph.adjacencyList.length;

//   // 	# create an auxiliary space to keep track of whether vertex is discovered
//   // 	discovered = [False] * N
//   const discovered = new Array(graph.numberOfNodes).fill(false);
//   // 	# list to store the topological order
//   // 	path = []
//   const path: number[] = [];

//   // 	# find all topological ordering and print them
//   // 	findAllTopologicalOrders(graph, path, discovered, N)
//   findAllTopologicalOrders(graph, path, discovered);

//   console.log('graph', JSON.stringify(graph));
// }

// THIS CODE IS ADAPTED FROM
// https://www.geeksforgeeks.org/all-topological-sorts-of-a-directed-acyclic-graph/

export class Graph {
  public readonly nodes = new Set<string>();
  public readonly adjacencyList = new Map<string, string[]>();
  public readonly indegree = new Map<string, number>();

  get size() {
    return this.nodes.size;
  }

  addNode(node: string) {
    if (this.nodes.has(node)) return;
    this.nodes.add(node);
    this.adjacencyList.set(node, []);
    this.indegree.set(node, 0);
  }

  addEdge(from: string, to: string) {
    if (!this.nodes.has(from)) {
      throw new Error(
        `Oops, tried to add an edge from non-existant node ${from}`,
      );
    }

    if (!this.nodes.has(to)) {
      throw new Error(`Oops, tried to add an edge to non-existant node ${to}`);
    }

    this.adjacencyList.get(from)!.push(to);
    this.incrementIndegree(to);
  }

  incrementIndegree(node: string) {
    if (!this.nodes.has(node)) {
      throw new Error(`Node "${node}" doesn't exist.`);
    }

    this.indegree.set(node, this.indegree.get(node)! + 1);
  }

  decrementIndegree(node: string) {
    if (!this.nodes.has(node)) {
      throw new Error(`Node "${node}" doesn't exist.`);
    }

    this.indegree.set(node, this.indegree.get(node)! - 1);
  }
}

function findAllTopologicalOrders(
  graph: Graph,
  discovered = new Set<string>(),
  path: string[] = [],
  results: string[][] = [],
) {
  const numberOfNodes = graph.size;

  for (const node of graph.nodes) {
    if (graph.indegree.get(node)! !== 0 || discovered.has(node)) continue;

    for (const u of graph.adjacencyList.get(node)!) {
      graph.decrementIndegree(u);
    }

    path.push(node);
    discovered.add(node);
    findAllTopologicalOrders(graph, discovered, path, results);

    for (const u of graph.adjacencyList.get(node)!) {
      graph.incrementIndegree(u);
    }

    path.pop();
    discovered.delete(node);
  }

  if (path.length === numberOfNodes) {
    results.push(path.slice());
  }

  return results;
}

// # Print all topological orderings of a given DAG
// def printAllTopologicalOrders(graph):
export function getAllTopologicalOrdersForGraph(graph: Graph) {
  return findAllTopologicalOrders(graph);
}
