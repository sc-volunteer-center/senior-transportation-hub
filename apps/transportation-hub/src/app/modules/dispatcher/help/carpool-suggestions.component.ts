import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { filter } from 'rxjs/operators';
import { IsLoadingService } from '@service-work/is-loading';
import { RideRequest, Address } from '@local/models';
import { MapsService } from '../../../maps.service';
import { navigateToPathFn } from '../../../utilities';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { startOfWeek, endOfWeek, numberComparer } from '../../../utilities';
import {
  addHours,
  addWeeks,
  format,
  isSameDay,
  isWithinInterval,
  subHours,
} from 'date-fns';
import { getCarpoolSuggestionsForRequest } from './getCarpoolSuggestionsForRequest';

@Component({
  selector: 'app-carpool-suggestions',
  template: `
    <h3 mat-dialog-title>
      <span
        *ngIf="'carpool-suggestions' | swIsLoading | async; else defaultTitle"
      >
        This could take a minute to load...
      </span>

      <ng-template #defaultTitle>
        Requests with carpools suggestions for {{ displayTimeRange() }}
      </ng-template>
    </h3>

    <mat-dialog-content
      style="overflow-y: auto; max-height: 60vh;"
      class="vertical-padding-8"
    >
      <div
        *ngIf="
          'carpool-suggestions' | swIsLoading | async;
          else showSuggestions
        "
        class="flex-1 flex-center"
        style="height: 300px;"
      >
        <mat-spinner></mat-spinner>
      </div>

      <ng-template #showSuggestions>
        <p *ngIf="requestsWithCarpools.length === 0; else thereAreSuggestions">
          There are no requests with carpool suggestions
        </p>

        <ng-template #thereAreSuggestions>
          <p>
            Below is a list of requests which have carpool suggestions. Each of
            these requests has a sublist of requests which they are suggested to
            carpool with.
          </p>

          <ul>
            <li *ngFor="let suggestion of requestsWithCarpools">
              <a
                [routerLink]="[
                  '/dispatcher/ride-request',
                  suggestion.original.uid,
                  'overview'
                ]"
              >
                {{
                  suggestion.original.flexible
                    ? (suggestion.original.datetime | flexibleDatetime)
                    : (suggestion.original.datetime | date: 'shortDate')
                }}

                for

                {{ suggestion.original.client.nameWithRetiredStatus }}
              </a>

              <ul>
                <li *ngFor="let pairingRequest of suggestion.pairings">
                  <a
                    [routerLink]="[
                      '/dispatcher/ride-request',
                      pairingRequest.uid,
                      'overview'
                    ]"
                  >
                    {{
                      pairingRequest.flexible
                        ? (pairingRequest.datetime | flexibleDatetime)
                        : (pairingRequest.datetime | date: 'shortDate')
                    }}

                    for

                    {{ pairingRequest.client.nameWithRetiredStatus }}
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </ng-template>
      </ng-template>
    </mat-dialog-content>

    <mat-dialog-actions>
      <div class="flex-1"></div>
      <button mat-button matDialogClose>Done</button>
    </mat-dialog-actions>
  `,
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientCarpoolSuggestionsComponent implements OnInit {
  requestsWithCarpools: Array<{
    original: RideRequest;
    pairings: RideRequest[];
  }> = [];

  private start = startOfWeek(new Date());
  private end = endOfWeek(addWeeks(new Date(), 1));
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private isLoadingService: IsLoadingService,
    private mapsService: MapsService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private ref: MatDialogRef<ClientCarpoolSuggestionsComponent>,
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    this.subscriptions.push(
      // close the dialog if the user navigates to a different page
      this.router.events
        .pipe(filter((e) => e instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.subscriptions.push(
      this.isLoadingService
        .add(
          this.fs.getRideRequests({
            startAfterOrOnDate: this.start,
            endBeforeDate: this.end,
            requestIsCancelled: false,
          }),
          {
            key: 'carpool-suggestions',
          },
        )
        .subscribe((requests) => {
          this.requestsWithCarpools = this.getRequestsWithCarpools(requests);
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  getRequestsWithCarpools(requests: RideRequest[]) {
    const stpRequests = requests.filter((r) => r.category === 'STP');
    const flexibleRequests = stpRequests.filter((r) => r.flexible);
    const fixedRequests = stpRequests.filter((r) => !r.flexible);

    const flexibleAndFlexibleSuggestions = flexibleRequests.flatMap(
      (request) => {
        const candidateRequests = flexibleRequests.filter(
          (r) => r.uid !== request.uid,
        );

        const suggestions = getCarpoolSuggestionsForRequest({
          request,
          candidateRequests,
          mapsService: this.mapsService,
        });

        return suggestions.map((s) => ({ ...s, request }));
      },
    );

    const flexibleAndFixedSuggestions = flexibleRequests.flatMap((request) => {
      const suggestions = getCarpoolSuggestionsForRequest({
        request,
        candidateRequests: fixedRequests,
        mapsService: this.mapsService,
      });

      return suggestions.map((s) => ({ ...s, request }));
    });

    const fixedAndFixedSuggestions = fixedRequests.flatMap((request) => {
      const candidateRequests = fixedRequests.filter(
        (r) => r.uid !== request.uid,
      );

      const suggestions = getCarpoolSuggestionsForRequest({
        request,
        candidateRequests,
        mapsService: this.mapsService,
        onSameDay: true,
      });

      return suggestions.map((s) => ({ ...s, request }));
    });

    const suggestions = [
      ...flexibleAndFlexibleSuggestions,
      ...flexibleAndFixedSuggestions,
      ...fixedAndFixedSuggestions,
    ];

    const store = new Map<
      string,
      { original: RideRequest; pairings: RideRequest[] }
    >();

    const getKey = (r: RideRequest) => r.recurringRideRequestId || r.uid;

    suggestions.forEach((s) => {
      if (!store.has(getKey(s.request))) {
        store.set(getKey(s.request), { original: s.request, pairings: [] });
      }

      if (!store.has(getKey(s.otherRequest))) {
        store.set(getKey(s.otherRequest), {
          original: s.otherRequest,
          pairings: [],
        });
      }

      store.get(getKey(s.request))!.pairings.push(s.otherRequest);
      store.get(getKey(s.otherRequest))!.pairings.push(s.request);
    });

    return Array.from(store.values())
      .map((suggestion) => {
        return {
          ...suggestion,
          pairings: Array.from(
            new Map(
              suggestion.pairings.map(
                (r) => [r.recurringRideRequestId || r.uid, r] as const,
              ),
            ).values(),
          ),
        };
      })
      .sort((a, b) =>
        numberComparer(
          a.original.datetime.valueOf(),
          b.original.datetime.valueOf(),
        ),
      );
  }

  displayTimeRange() {
    const start = format(this.start, 'MMM d');
    const end = format(this.end, 'MMM d, yyyy');

    return `${start} - ${end}`;
  }

  totalDistance(addresses: Address[]) {
    return this.mapsService.birdsDistance(addresses)!;
  }

  maxSectionDistance(addresses: Address[]) {
    addresses = addresses.slice();
    let maxDistance = 0;
    let addressA = addresses.shift()!;
    let addressB: Address;

    while ((addressB = addresses.shift()!)) {
      const distance = this.mapsService.birdsDistance([addressA, addressB])!;

      if (maxDistance < distance) {
        maxDistance = distance;
      }

      addressA = addressB;
    }

    return maxDistance;
  }
}

const pickup = (r: RideRequest) => r.pickupAddress;
const destination = (r: RideRequest) => r.destinationAddresses[0];
const clientName = (r: RideRequest) => r.client.name;
