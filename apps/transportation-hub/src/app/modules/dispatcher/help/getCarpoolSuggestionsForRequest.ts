import { RideRequest } from '@local/models';
import { addHours, isWithinInterval, subHours } from 'date-fns';
import { MapsService } from '../../../maps.service';
import { Graph, getAllTopologicalOrdersForGraph } from './topological-sort';

/**
 * What makes a carpool candidate?
 *
 * - Because, in practice, shopping for multiple people at the same time is
 *   difficult, we should limit carpool suggestions to STP requests for now.
 * - Because the logic for carpools is probably complex:
 *   - Limit carpools to two requests.
 *   - Can be two flexible requests, one flexible request and one fixed request,
 *     or two fixed requests within two hours of one another.
 *   - Instances of recurring requests are treated as the same
 *   - Limit timespan analyzed to 2 weeks for simplicity
 *
 * - Close by via any of these:
 *   1. Request only has one destination, pickup locations should be within 1 mile,
 *      destinations should be within 3 miles.
 *   2. Pickup location of A is within 1 mile of final destination of B.
 */
export function getCarpoolSuggestionsForRequest(args: {
  request: RideRequest;
  candidateRequests: RideRequest[];
  mapsService: MapsService;
  onSameDay?: boolean;
}) {
  const { request, candidateRequests, mapsService } = args;

  const pairings = getCarpoolPairingsForRequest(args);

  const mapper = (requestIdB: string) => {
    const requestB = candidateRequests.find((r) => r.uid === requestIdB)!;

    const graph = new Graph();
    indexAddresses(graph, request);
    indexAddresses(graph, requestB);

    const options = getAllTopologicalOrdersForGraph(graph).map(
      (path: string[]) =>
        path.map((pathSegment) => {
          const [type, id, destinationIndex] = pathSegment.split(':');

          if (type === 'request') {
            const r = id === request.uid ? request : requestB;

            return {
              request: r,
              address: {
                type: 'pickup',
                address: r.pickupAddress,
              },
            };
          } else {
            const r = id === request.uid ? request : requestB;

            return {
              request: r,
              address: {
                type: 'destination',
                index: Number(destinationIndex),
                address: r.destinationAddresses[Number(destinationIndex)],
              },
            };
          }
        }),
    );

    const bestOption = options
      .sort((a, b) => {
        const maxDistA = mapsService.birdsDistance(
          a.map((o) => o.address.address),
        )!;
        const maxDistB = mapsService.birdsDistance(
          b.map((o) => o.address.address),
        )!;

        if (maxDistA === maxDistB) return 0;
        if (maxDistA > maxDistB) return 1;
        return -1;
      })
      .shift()!;

    const mapDescription = bestOption
      .map((dest) => {
        if (dest.address.type === 'pickup') {
          return `${clientName(dest.request)}'s pickup`;
        } else {
          return `${clientName(dest.request)}'s ${convertIndexToWord(
            dest.address.index!,
          )} destination`;
        }
      })
      .join(', to ');

    const mapURL = mapsService.directionsMapUrl(
      bestOption.map((dest) => dest.address.address),
    );

    return {
      otherRequest: requestB,
      mapURL,
      mapDescription,
    };
  };

  return Array.from(pairings).map(mapper);
}

function getCarpoolPairingsForRequest({
  request,
  candidateRequests,
  mapsService,
  onSameDay = false,
}: {
  request: RideRequest;
  candidateRequests: RideRequest[];
  mapsService: MapsService;
  onSameDay?: boolean;
}) {
  const dedupStore = new Set<string>(); // used to dedup instances of the same recurring request
  const store = new Set<string>();
  const comparerInterval = {
    start: subHours(request.datetime, 2),
    end: addHours(request.datetime, 2),
  };

  const currentRequestId = request.recurringRideRequestId || request.uid;

  candidateRequests.forEach((candidateRequest) => {
    if (request.clientId === candidateRequest.clientId) return;

    const candidateRequestId =
      candidateRequest.recurringRideRequestId || candidateRequest.uid;

    if (currentRequestId === candidateRequestId) return;

    const dedupKey = [currentRequestId, candidateRequestId].sort().join('+');

    if (dedupStore.has(dedupKey)) return;

    if (
      onSameDay &&
      !isWithinInterval(candidateRequest.datetime, comparerInterval)
    ) {
      return;
    }

    let satisfiesConditionsForCarpool = false;

    // lets test for option (1):
    // Requests only have one destination, pickup locations should be within 1 mile,
    // destinations should be within 3 miles.

    if (
      !satisfiesConditionsForCarpool &&
      candidateRequest.destinationAddresses.length === 1 &&
      request.destinationAddresses.length === 1
    ) {
      const pickupDistanceInMiles = mapsService.birdsDistance([
        pickup(request),
        pickup(candidateRequest),
      ])!;

      const destinationDistanceInMiles = mapsService.birdsDistance([
        finalDestination(request),
        finalDestination(candidateRequest),
      ])!;

      satisfiesConditionsForCarpool =
        pickupDistanceInMiles <= 1 && destinationDistanceInMiles <= 3;
    }

    // lets test for option (2):
    // Pickup location of A is within 1 mile of final destination of B.

    if (!satisfiesConditionsForCarpool) {
      const destinationDistanceInMiles = mapsService.birdsDistance([
        pickup(request),
        finalDestination(candidateRequest),
      ])!;

      satisfiesConditionsForCarpool = destinationDistanceInMiles <= 1;
    }

    if (!satisfiesConditionsForCarpool) return;

    dedupStore.add(dedupKey);

    store.add(candidateRequest.uid);
  });

  return store;
}

const pickup = (r: RideRequest) => r.pickupAddress;

const finalDestination = (r: RideRequest) =>
  r.destinationAddresses[r.destinationAddresses.length - 1];

const clientName = (r: RideRequest) => r.client.name;

function indexAddresses(graph: Graph, r: RideRequest) {
  const requestNode = `request:${r.uid}`;
  const getAddressNode = (i: number) => `address:${r.uid}:${i}`;

  graph.addNode(requestNode);

  r.destinationAddresses.forEach((_, i) => {
    const currentAddressNode = getAddressNode(i);

    graph.addNode(currentAddressNode);

    if (i === 0) {
      graph.addEdge(requestNode, currentAddressNode);
    } else {
      graph.addEdge(getAddressNode(i - 1), currentAddressNode);
    }
  });
}

function convertIndexToWord(index: number) {
  switch (index) {
    case 0:
      return 'first';
    case 1:
      return 'second';
    case 2:
      return 'third';
    case 3:
      return 'fourth';
    case 4:
      return 'fifth';
    default: {
      throw new Error(
        `unsupported index number for convertIndexToWord: ${index}`,
      );
    }
  }
}
