import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Title } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { ClientRedistributionSuggestionsComponent } from './client-redistribution-suggestions.component';
import { ClientCarpoolSuggestionsComponent } from './carpool-suggestions.component';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelpComponent implements OnInit {
  dispatcher = this.fs.dispatcher;

  constructor(
    private fs: FirestoreService,
    private titleService: Title,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.titleService.setTitle(`Help Page | Hub`);
  }

  redistributionSuggestions() {
    return this.dialog.open(ClientRedistributionSuggestionsComponent, {
      width: '600px',
      maxHeight: '90vh',
      disableClose: true,
    });
  }

  suggestCarpools() {
    return this.dialog.open(ClientCarpoolSuggestionsComponent, {
      width: '600px',
      maxHeight: '90vh',
      disableClose: true,
    });
  }
}
