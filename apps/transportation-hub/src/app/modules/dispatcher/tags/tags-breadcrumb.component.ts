import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { filter, map } from 'rxjs/operators';
import { isTruthy } from '../../../utilities';

@Component({
  selector: 'app-client-tags-breadcrumb',
  template: `
    <div
      *ngIf="'loading-tags' | swIsLoading | async; else tagsLoaded"
      class="flex-row flex-center"
    >
      <mat-spinner
        [diameter]="22"
        color="accent"
        class="vertical-margin-16"
        style="margin-right: 0.5rem"
      ></mat-spinner>
      <h3>Tags</h3>
    </div>

    <ng-template #tagsLoaded> {{ type | async }} Tags </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsBreadcrumbComponent implements OnInit {
  type: Observable<string>;

  constructor(private routerStore: RouterStoreService) {}

  ngOnInit() {
    this.type = this.routerStore
      .get$((store) => store.state.params.tagType)
      .pipe(
        filter(isTruthy),
        map((type) => (type === 'driver' ? 'Volunteer' : 'Client')),
      );
  }
}
