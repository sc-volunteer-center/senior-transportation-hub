import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../../../../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { TAG_COLORS } from '../../../../utilities';
import { Router } from '@angular/router';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddTagComponent implements OnInit {
  tagForm: FormGroup;

  colors = Array.from(TAG_COLORS.keys());

  TAG_COLORS = TAG_COLORS;

  constructor(
    private fb: FormBuilder,
    private firestore: FirestoreService,
    private ref: MatDialogRef<AddTagComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    const type = this.routerStore.get((store) => store.state.params.tagType);

    this.tagForm = this.fb.group({
      type,
      label: [
        '',
        [
          Validators.required,
          Validators.maxLength(50),
          Validators.minLength(1),
        ],
      ],
      description: ['', Validators.required],
      color: ['', Validators.required],
      favorite: [true, Validators.required],
    });

    this.cdr.markForCheck();
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.tagForm.invalid) {
      return;
    }

    const id = await this.loadingService.add(
      this.firestore.addTag(this.tagForm.value),
      { key: 'add-tag' },
    );

    this.ref.close(id);
  }
}
