import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Subscription, Observable } from 'rxjs';
import { Tag } from '@local/models';
import { MatDialog } from '@angular/material/dialog';
import { AddTagComponent } from './add-tag/add-tag.component';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime, switchMap, filter } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { navigateToPathFn, isTruthy } from '../../../utilities';
import { Title } from '@angular/platform-browser';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsComponent implements OnInit {
  tagType: Observable<string>;
  tags: Tag[] = [];

  searchControl = new FormControl('');

  filterText = '';
  filteredTags: Tag[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private routerStore: RouterStoreService,
    private titleService: Title,
    private isLoadingService: IsLoadingService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    this.tagType = this.routerStore
      .get$((store) => store.state.params.tagType)
      .pipe(filter(isTruthy));

    const query = this.route.snapshot.queryParamMap.get('search') || '';

    this.searchControl.patchValue(query);

    this.titleService.setTitle(`Tags | Hub`);

    // this.test
    //   .collection(
    //     `organizations/${this.fs.user!.organizationId}/drivers`,
    //     ref => {
    //       return ref.where('tagIds', 'array-contains', 'n4FoFGGXaqAFAp9rllnv');
    //     },
    //   )
    //   .valueChanges()
    //   .subscribe(value => {
    //     console.log('values', value);
    //   });

    this.subscriptions.push(
      this.isLoadingService
        .add(
          this.tagType.pipe(
            switchMap((type) =>
              this.fs.getTags({
                type,
                cache: { expirationTime: Number.POSITIVE_INFINITY },
              }),
            ),
          ),
          { key: 'loading-tags' },
        )
        .subscribe((tags) => {
          this.tags = tags;
          this.filteredTags = tags.slice();
          this.filterTags();
        }),
      this.searchControl.valueChanges
        .pipe(debounceTime(500))
        .subscribe((value) => {
          this.router.navigate([], {
            queryParams: {
              search: value.toLowerCase(),
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
      this.route.queryParamMap.subscribe((params) => {
        const search = params.get('search') || '';
        this.searchControl.patchValue(search, { emitEvent: false });
        this.filterTags(search);
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  addTag() {
    this.dialog.open(AddTagComponent, {
      width: '600px',
    });
    // .afterClosed()
    // .subscribe(id => {
    //   if (id) {
    //     this.router.navigate(['/dispatcher/tag', id, 'profile']);
    //   }
    // });
  }

  filterTags(text?: string) {
    this.filterText = (text && text.toLowerCase()) || '';

    this.filteredTags = this.tags.filter((tag) =>
      tag.label.toLowerCase().includes(this.filterText),
    );

    this.cdr.detectChanges();
  }
}
