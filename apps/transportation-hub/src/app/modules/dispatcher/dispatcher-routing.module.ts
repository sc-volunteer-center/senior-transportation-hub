import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ValidUserGuard } from './valid-user.guard';
import { OverviewComponent } from './overview/overview.component';
import { ClientsComponent } from './clients/clients.component';
import { RideRequestsComponent } from './ride-requests/ride-requests.component';
import { DriversComponent } from './drivers/drivers.component';
import { LoginComponent } from './login/login.component';
import { DispatcherSignedInGuard } from './dispatcher-signed-in.guard';
import { DispatcherSignedOutGuard } from './dispatcher-signed-out.guard';
import { ClientGuard } from './client/client.guard';
import { ClientComponent } from './client/client.component';
import { RideRequestGuard } from './ride-request/ride-request.guard';
import { RideRequestComponent } from './ride-request/ride-request.component';
import { DriverGuard } from './driver/driver.guard';
import { DriverComponent } from './driver/driver.component';
import { OverviewBreadcrumbComponent } from './overview/overview-breadcrumb.component';
import { ClientsBreadcrumbComponent } from './clients/clients-breadcrumb.component';
import { ClientBreadcrumbComponent } from './client/client-breadcrumb.component';
import { RideRequestBreadcrumbComponent } from './ride-request/ride-request-breadcrumb.component';
import { RideRequestsBreadcrumbComponent } from './ride-requests/ride-requests-breadcrumb.component';
import { DriversBreadcrumbComponent } from './drivers/drivers-breadcrumb.component';
import { DriverBreadcrumbComponent } from './driver/driver-breadcrumb.component';
import { LoginBreadcrumbComponent } from './login/login-breadcrumb.component';
import { ClientProfileComponent } from './client/client-profile/client-profile.component';
import { ClientRideRequestsComponent } from './client/client-ride-requests/client-ride-requests.component';
import { ClientContactsComponent } from './client/client-contacts/client-contacts.component';
import { RideRequestProfileComponent } from './ride-request/ride-request-profile/ride-request-profile.component';
import { DriverProfileComponent } from './driver/driver-profile/driver-profile.component';
import { DriverRideRequestsComponent } from './driver/driver-ride-requests/driver-ride-requests.component';
import { DispatcherComponent } from '../shared/dispatcher/dispatcher.component';
import { DispatcherBreadcrumbComponent } from '../shared/dispatcher/dispatcher-breadcrumb.component';
import { DispatcherProfileComponent } from '../shared/dispatcher/dispatcher-profile/dispatcher-profile.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { DestinationsBreadcrumbComponent } from './destinations/destinations-breadcrumb.component';
import { DestinationComponent } from './destination/destination.component';
import { DestinationBreadcrumbComponent } from './destination/destination-breadcrumb.component';
import { DestinationGuard } from './destination/destination.guard';
import { DestinationProfileComponent } from './destination/destination-profile/destination-profile.component';
import { RideRequestsMonthViewComponent } from './ride-requests/ride-requests-view/ride-requests-month-view.component';
import { RideRequestsDayViewComponent } from './ride-requests/ride-requests-view/ride-requests-day-view.component';
import { RideRequestsWeekViewComponent } from './ride-requests/ride-requests-view/ride-requests-week-view.component';
import { DispatchersComponent } from '../shared/dispatchers/dispatchers.component';
import { DispatchersBreadcrumbComponent } from '../shared/dispatchers/dispatchers-breadcrumb.component';
import { HelpBreadcrumbComponent } from './help/help-breadcrumb.component';
import { HelpComponent } from './help/help.component';
import { DispatcherProfileGuard } from '../shared/dispatcher-profile.guard';
import { DispatcherRootComponent } from './dispatcher-root.component';
import { SharedNotesComponent } from './shared-notes/shared-notes.component';
import { SharedNotesBreadcrumbComponent } from './shared-notes/shared-notes-breadcrumb.component';
import { TagsComponent } from './tags/tags.component';
import { TagsBreadcrumbComponent } from './tags/tags-breadcrumb.component';
import { TagGuard } from './tag/tag.guard';
import { TagComponent } from './tag/tag.component';
import { TagBreadcrumbComponent } from './tag/tag-breadcrumb.component';
import { TagProfileComponent } from './tag/tag-profile/tag-profile.component';
import {
  FindRequestForVolunteerDayViewComponent,
  FindRequestForVolunteerMonthViewComponent,
  FindRequestForVolunteerWeekViewComponent,
} from './driver/find-request-for-volunteer/find-request-for-volunteer.component';

const routes: Routes = [
  {
    path: 'signin',
    canActivate: [ValidUserGuard, DispatcherSignedOutGuard],
    component: DispatcherRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: LoginComponent,
        data: {
          breadcrumb: LoginBreadcrumbComponent,
        },
      },
    ],
  },
  {
    path: '',
    canActivate: [DispatcherSignedInGuard],
    component: DispatcherRootComponent,
    children: [
      {
        path: 'overview',
        component: OverviewComponent,
        data: {
          breadcrumb: OverviewBreadcrumbComponent,
        },
      },
      {
        path: 'clients',
        component: ClientsComponent,
        data: {
          breadcrumb: ClientsBreadcrumbComponent,
        },
      },
      {
        path: 'client/:clientId',
        canActivate: [ClientGuard],
        component: ClientComponent,
        data: {
          breadcrumb: ClientBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: ClientProfileComponent,
          },
          {
            path: 'ride-requests/:rideRequestsDate',
            component: ClientRideRequestsComponent,
          },
          {
            path: 'client-contacts',
            component: ClientContactsComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'ride-requests/:date',
        component: RideRequestsComponent,
        data: {
          breadcrumb: RideRequestsBreadcrumbComponent,
        },
        children: [
          {
            path: 'month',
            component: RideRequestsMonthViewComponent,
          },
          {
            path: 'week',
            component: RideRequestsWeekViewComponent,
          },
          {
            path: 'day',
            component: RideRequestsDayViewComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './month',
          },
          {
            path: '**',
            redirectTo: '../month',
          },
        ],
      },
      {
        path: 'ride-request/:rideRequestId',
        canActivate: [RideRequestGuard],
        component: RideRequestComponent,
        data: {
          breadcrumb: RideRequestBreadcrumbComponent,
        },
        children: [
          {
            path: 'overview',
            component: RideRequestProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './overview',
          },
          {
            path: '**',
            redirectTo: '../overview',
          },
        ],
      },
      {
        path: 'drivers',
        component: DriversComponent,
        data: {
          breadcrumb: DriversBreadcrumbComponent,
        },
      },
      {
        path: 'driver/:driverId',
        canActivate: [DriverGuard],
        component: DriverComponent,
        data: {
          breadcrumb: DriverBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: DriverProfileComponent,
          },
          {
            path: 'ride-requests/:rideRequestsDate',
            component: DriverRideRequestsComponent,
          },
          {
            path: 'suggest-requests/:date',
            children: [
              {
                path: 'month',
                component: FindRequestForVolunteerMonthViewComponent,
              },
              {
                path: 'week',
                component: FindRequestForVolunteerWeekViewComponent,
              },
              {
                path: 'day',
                component: FindRequestForVolunteerDayViewComponent,
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: './month',
              },
              {
                path: '**',
                redirectTo: '../month',
              },
            ],
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'destinations',
        component: DestinationsComponent,
        data: {
          breadcrumb: DestinationsBreadcrumbComponent,
        },
      },
      {
        path: 'destination/:destinationId',
        canActivate: [DestinationGuard],
        component: DestinationComponent,
        data: {
          breadcrumb: DestinationBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: DestinationProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'tags/:tagType',
        component: TagsComponent,
        data: {
          breadcrumb: TagsBreadcrumbComponent,
        },
      },
      {
        path: 'tag/:tagId',
        canActivate: [TagGuard],
        component: TagComponent,
        data: {
          breadcrumb: TagBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: TagProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'dispatchers',
        component: DispatchersComponent,
        data: {
          breadcrumb: DispatchersBreadcrumbComponent,
        },
      },
      {
        path: 'dispatcher/:dispatcherId',
        component: DispatcherComponent,
        canActivate: [DispatcherProfileGuard],
        data: {
          breadcrumb: DispatcherBreadcrumbComponent,
        },
        children: [
          {
            path: 'profile',
            component: DispatcherProfileComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: './profile',
          },
          {
            path: '**',
            redirectTo: '../profile',
          },
        ],
      },
      {
        path: 'notes',
        component: SharedNotesComponent,
        data: {
          breadcrumb: SharedNotesBreadcrumbComponent,
        },
      },
      {
        path: 'help',
        component: HelpComponent,
        data: {
          breadcrumb: HelpBreadcrumbComponent,
        },
      },
      {
        path: '**',
        redirectTo: 'overview',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispatcherRoutingModule {}
