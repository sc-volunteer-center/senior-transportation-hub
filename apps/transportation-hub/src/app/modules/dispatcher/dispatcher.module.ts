import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DispatcherRoutingModule } from './dispatcher-routing.module';
import { OverviewComponent } from './overview/overview.component';
import { ClientsComponent } from './clients/clients.component';
import { RideRequestsComponent } from './ride-requests/ride-requests.component';
import { DriversComponent } from './drivers/drivers.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactiveFormsModule } from '@angular/forms';
import { IsLoadingModule } from '@service-work/is-loading';
import { GoogleMapsModule } from '@angular/google-maps';
import { MatRadioModule } from '@angular/material/radio';

import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { DriverComponent } from './driver/driver.component';
import { RideRequestComponent } from './ride-request/ride-request.component';
import { OverviewBreadcrumbComponent } from './overview/overview-breadcrumb.component';
import { DriversBreadcrumbComponent } from './drivers/drivers-breadcrumb.component';
import { RideRequestBreadcrumbComponent } from './ride-request/ride-request-breadcrumb.component';
import { RideRequestsBreadcrumbComponent } from './ride-requests/ride-requests-breadcrumb.component';
import { LoginBreadcrumbComponent } from './login/login-breadcrumb.component';
import { DriverBreadcrumbComponent } from './driver/driver-breadcrumb.component';
import { ClientsBreadcrumbComponent } from './clients/clients-breadcrumb.component';
import { ClientBreadcrumbComponent } from './client/client-breadcrumb.component';
import { ClientRideRequestsComponent } from './client/client-ride-requests/client-ride-requests.component';
import { ClientProfileComponent } from './client/client-profile/client-profile.component';
import { ClientContactsComponent } from './client/client-contacts/client-contacts.component';
import { RideRequestProfileComponent } from './ride-request/ride-request-profile/ride-request-profile.component';
import { FindDriverForRideRequestComponent } from './ride-request/find-driver-for-ride-request/find-driver-for-ride-request.component';
import { DriverProfileComponent } from './driver/driver-profile/driver-profile.component';
import { DriverRideRequestsComponent } from './driver/driver-ride-requests/driver-ride-requests.component';
import { EditDriverComponent } from './driver/edit-driver/edit-driver.component';
import { EditClientComponent } from './client/edit-client/edit-client.component';
import { EditClientContactsComponent } from './client/edit-client-contacts/edit-client-contacts.component';
import { CancelRideRequestComponent } from './ride-request/cancel-ride-request/cancel-ride-request.component';
import { EditRideRequestComponent } from './ride-request/edit-ride-request/edit-ride-request.component';
import { SharedModule } from '../shared/shared.module';
import { EditDriverVacationComponent } from './driver/edit-driver-vacation/edit-driver-vacation.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { DestinationComponent } from './destination/destination.component';
import { DestinationsBreadcrumbComponent } from './destinations/destinations-breadcrumb.component';
import { DestinationBreadcrumbComponent } from './destination/destination-breadcrumb.component';
import { DestinationProfileComponent } from './destination/destination-profile/destination-profile.component';
import { EditDestinationComponent } from './destination/edit-destination/edit-destination.component';
import { AddDestinationComponent } from './destinations/add-destination/add-destination.component';
import { RideRequestsMonthViewComponent } from './ride-requests/ride-requests-view/ride-requests-month-view.component';
import { RideRequestsDayViewComponent } from './ride-requests/ride-requests-view/ride-requests-day-view.component';
import { RideRequestsWeekViewComponent } from './ride-requests/ride-requests-view/ride-requests-week-view.component';
import { RideRequestsEditFilterComponent } from './ride-requests/ride-requests-edit-filter/ride-requests-edit-filter.component';
import { HelpBreadcrumbComponent } from './help/help-breadcrumb.component';
import { HelpComponent } from './help/help.component';
import { DeleteRideRequestComponent } from './ride-request/delete-ride-request/delete-ride-request.component';
import { DispatcherRootComponent } from './dispatcher-root.component';
import {
  HeaderComponent,
  NonSecureModeWarningComponent,
} from './header.component';
import { SidebarComponent } from './sidebar.component';
import { SharedNotesComponent } from './shared-notes/shared-notes.component';
import { SharedNotesBreadcrumbComponent } from './shared-notes/shared-notes-breadcrumb.component';
import { TagsComponent } from './tags/tags.component';
import { TagsBreadcrumbComponent } from './tags/tags-breadcrumb.component';
import { AddTagComponent } from './tags/add-tag/add-tag.component';
import { TagBreadcrumbComponent } from './tag/tag-breadcrumb.component';
import { TagProfileComponent } from './tag/tag-profile/tag-profile.component';
import { EditTagComponent } from './tag/edit-tag/edit-tag.component';
import { TagComponent } from './tag/tag.component';
import { DeleteDriverComponent } from './driver/delete-driver/delete-driver.component';
import { DeleteClientComponent } from './client/delete-client/delete-client.component';
import { RetireClientComponent } from './client/retire-client/retire-client.component';
import { RetireDriverComponent } from './driver/retire-driver/retire-driver.component';
import { AddRideRequestComponent } from './add-ride-request/add-ride-request.component';
import { AddClientComponent } from './add-client/add-client.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { WhichRideRequestComponent } from './ride-request/which-ride-request.component';
import { ClientRedistributionSuggestionsComponent } from './help/client-redistribution-suggestions.component';
import {
  FindRequestForVolunteerDayViewComponent,
  FindRequestForVolunteerMonthViewComponent,
  FindRequestForVolunteerWeekViewComponent,
} from './driver/find-request-for-volunteer/find-request-for-volunteer.component';
import { ClientCarpoolSuggestionsComponent } from './help/carpool-suggestions.component';
import { RequestCarpoolSuggestionsComponent } from './ride-request/carpool-suggestions/carpool-suggestions.component';

@NgModule({
  imports: [
    CommonModule,
    IsLoadingModule,
    GoogleMapsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatRadioModule,
    SharedModule,
    DispatcherRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DispatcherRootComponent,
    HeaderComponent,
    SidebarComponent,
    OverviewComponent,
    ClientsComponent,
    RideRequestsComponent,
    DriversComponent,
    LoginComponent,
    ClientComponent,
    DriverComponent,
    RideRequestComponent,
    OverviewBreadcrumbComponent,
    DriversBreadcrumbComponent,
    RideRequestBreadcrumbComponent,
    RideRequestsBreadcrumbComponent,
    LoginBreadcrumbComponent,
    DriverBreadcrumbComponent,
    ClientsBreadcrumbComponent,
    ClientBreadcrumbComponent,
    ClientRideRequestsComponent,
    ClientProfileComponent,
    ClientContactsComponent,
    RideRequestProfileComponent,
    FindDriverForRideRequestComponent,
    DriverProfileComponent,
    DriverRideRequestsComponent,
    FindRequestForVolunteerDayViewComponent,
    FindRequestForVolunteerWeekViewComponent,
    FindRequestForVolunteerMonthViewComponent,
    EditDriverComponent,
    EditClientComponent,
    EditClientContactsComponent,
    CancelRideRequestComponent,
    EditRideRequestComponent,
    EditDriverVacationComponent,
    DestinationsComponent,
    DestinationComponent,
    DestinationsBreadcrumbComponent,
    DestinationBreadcrumbComponent,
    DestinationProfileComponent,
    TagsComponent,
    TagComponent,
    TagsBreadcrumbComponent,
    TagBreadcrumbComponent,
    TagProfileComponent,
    AddTagComponent,
    EditTagComponent,
    EditDestinationComponent,
    AddDestinationComponent,
    RideRequestsMonthViewComponent,
    RideRequestsDayViewComponent,
    RideRequestsWeekViewComponent,
    RideRequestsEditFilterComponent,
    HelpComponent,
    HelpBreadcrumbComponent,
    DeleteRideRequestComponent,
    DeleteDriverComponent,
    DeleteClientComponent,
    SharedNotesComponent,
    SharedNotesBreadcrumbComponent,
    RetireClientComponent,
    RetireDriverComponent,
    AddRideRequestComponent,
    AddClientComponent,
    AddDriverComponent,
    NonSecureModeWarningComponent,
    WhichRideRequestComponent,
    ClientRedistributionSuggestionsComponent,
    ClientCarpoolSuggestionsComponent,
    RequestCarpoolSuggestionsComponent,
  ],
})
export class DispatcherModule {}
