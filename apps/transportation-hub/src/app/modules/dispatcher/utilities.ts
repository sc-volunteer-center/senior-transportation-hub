import { RideRequest } from '@local/models';
import { numberComparer, stringComparer } from '../../utilities';

function requestComparer(a: RideRequest, b: RideRequest) {
  const order = numberComparer(a.datetime.valueOf(), b.datetime.valueOf());

  return order === 0 ? stringComparer(a.uid, b.uid) : order;
}

function findRequest(rideRequest: RideRequest, reverse: boolean) {
  return (nextRequests: RideRequest[]) => {
    const ordered = nextRequests.sort(requestComparer);

    if (reverse) ordered.reverse();

    const index = ordered.findIndex((a) => a.uid === rideRequest.uid);

    return ordered.slice(index + 1)[0] || null;
  };
}

export function findNextRequest(rideRequest: RideRequest) {
  return findRequest(rideRequest, false);
}

export function findPreviousRequest(rideRequest: RideRequest) {
  return findRequest(rideRequest, true);
}
