import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../../../../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { Subscription, of } from 'rxjs';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { switchMap } from 'rxjs/operators';
import { Destination } from '@local/models';

@Component({
  selector: 'app-edit-destination',
  templateUrl: './edit-destination.component.html',
  styleUrls: ['./edit-destination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditDestinationComponent implements OnInit {
  destination: Destination | null = null;
  destinationForm?: FormGroup;

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private ref: MatDialogRef<EditDestinationComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.destinationId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDestination(id))))
        .subscribe((destination) => {
          this.destination = destination;
          this.destinationForm = undefined;
          this.cdr.markForCheck();

          if (!destination) {
            return;
          }

          this.destinationForm = this.fb.group({
            label: [
              destination.label,
              [
                Validators.required,
                Validators.maxLength(40),
                Validators.minLength(3),
              ],
            ],
            street: [destination.street, Validators.required],
            apartmentNumber: [destination.apartmentNumber],
            city: [destination.city, Validators.required],
            zip: [destination.zip, Validators.required],
            comments: [destination.comments],
          });
        }),
    );
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (!this.destinationForm || this.destinationForm.invalid) {
      return;
    }

    await this.loadingService.add(
      this.fs.updateDestination(
        this.destination!.uid,
        this.destinationForm.value,
      ),
      { key: 'edit-destination' },
    );

    this.ref.close();
  }
}
