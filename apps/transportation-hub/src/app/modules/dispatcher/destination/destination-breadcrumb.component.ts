import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Destination } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';

@Component({
  selector: 'app-destination-breadcrumb',
  template: ` Favorite Destination `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DestinationBreadcrumbComponent implements OnInit {
  destination: Destination | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.destinationId)
        .pipe(switchMap((id) => this.fs.getDestination(id!)))
        .subscribe((destination) => {
          this.destination = destination;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
