import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Destination, Dispatcher } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { EditDestinationComponent } from '../edit-destination/edit-destination.component';
import { MatDialog } from '@angular/material/dialog';
import { SafeResourceUrl, Title } from '@angular/platform-browser';
import { toObservable, areSafeResourceUrlsEqual } from '../../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { MapsService } from '../../../../maps.service';

@Component({
  selector: 'app-destination-profile',
  templateUrl: './destination-profile.component.html',
  styleUrls: ['./destination-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'profile-component',
  },
})
export class DestinationProfileComponent implements OnInit {
  destination: Destination | null = null;
  mapUrl: SafeResourceUrl = '';

  private dispatcher: Dispatcher | null = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private dialog: MatDialog,
    private router: Router,
    private loadingService: IsLoadingService,
    private mapsService: MapsService,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.destinationId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDestination(id))))
        .subscribe((destination) => {
          this.destination = destination;
          this.cdr.markForCheck();

          if (destination) {
            const newUrl = this.mapsService.addressMapUrl(destination);
            this.titleService.setTitle(
              `Destination | ${destination.label} | Hub`,
            );

            if (!areSafeResourceUrlsEqual(this.mapUrl, newUrl)) {
              this.mapUrl = newUrl;
            }
          } else {
            this.mapUrl = '';
          }
        }),
    );

    this.subscriptions.push(
      this.fs.getCurrentDispatcher().subscribe((dispatcher) => {
        this.dispatcher = dispatcher;
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editDestination() {
    this.dialog.open(EditDestinationComponent, {
      width: '600px',
    });
  }

  deleteDestination() {
    if (!this.destination || !this.dispatcher) {
      return;
    }

    const sub = toObservable(
      this.fs.destroyDestination(this.destination.uid),
    ).subscribe(() => this.router.navigate(['/dispatcher/destinations']));

    this.loadingService.add(sub);
  }
}
