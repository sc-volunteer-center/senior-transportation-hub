import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DestinationGuard implements CanActivate {
  constructor(private fs: FirestoreService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!next.params['destinationId']) {
      return false;
    }

    return this.fs
      .getDestination(next.params['destinationId'])
      .pipe(map(destination => !!destination));
  }
}
