import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { FirestoreService } from '../../firestore.service';

@Injectable({
  providedIn: 'root',
})
export class ValidUserGuard implements CanActivate {
  constructor(private fs: FirestoreService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.fs.getCurrentUser().pipe(
      switchMap(user =>
        this.fs
          .getOrganization()
          .pipe(
            map(
              org =>
                !!(
                  org &&
                  user &&
                  (org.dispatcherEmailAddress === user.emailAddress ||
                    org.adminEmailAddresses.includes(user.emailAddress))
                ),
            ),
          ),
      ),
      tap(isValidUser => {
        if (!isValidUser) {
          this.router.navigate(['/welcome']);
        }
      }),
    );
  }
}
