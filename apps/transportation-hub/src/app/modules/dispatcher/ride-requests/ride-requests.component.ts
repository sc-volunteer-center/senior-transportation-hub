import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';
import { format, startOfDay } from 'date-fns';
import { RideRequestsViewService } from './ride-requests-view.service';
import { Router } from '@angular/router';
import { map, startWith, switchMap, tap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import {
  MatDatepicker,
  MatDatepickerInputEvent,
} from '@angular/material/datepicker';
import { IsLoadingService } from '@service-work/is-loading';
import { endOfWeek, startOfWeek } from '../../../utilities';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ride-requests',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './ride-requests.component.html',
  styleUrls: ['./ride-requests.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RideRequestsComponent implements OnDestroy {
  selectedDate = startOfDay(new Date());
  selectedView = 'month';

  filterOptions: Array<
    'unassignedOnly' | 'map' | 'stp' | 'gsp' | 'fixed' | 'flexible'
  > = [];
  filterOptionsControl = new FormControl([]);

  private subscriptions: Subscription[] = [];

  constructor(
    public rideRequestsViewService: RideRequestsViewService,
    private routerStore: RouterStoreService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private isLoadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.filterOptionsControl.patchValue([
      ...this.rideRequestsViewService.viewOptions$.value,
    ]);

    this.subscriptions.push(
      this.filterOptionsControl.valueChanges
        .pipe(startWith(this.filterOptionsControl.value))
        .subscribe((options: typeof this['filterOptions']) => {
          this.rideRequestsViewService.viewOptions$.next([...options]);
        }),
    );
  }

  ngOnDestroy() {
    // In the off chance that someone navigates in the middle of loading ride
    // requests, this clears the loading stack. Hacky and bad, I know.
    this.isLoadingService.remove({ key: 'ride-requests' });
    this.isLoadingService.remove({ key: 'ride-requests' });
    this.isLoadingService.remove({ key: 'ride-requests' });
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  displaySelectedDate() {
    return this.rideRequestsViewService.selectedDate$.pipe(
      tap((date) => {
        this.selectedDate = date;
        this.cdr.markForCheck();
      }),
      switchMap((date) =>
        this.routerStore
          .get$((store) => store.url)
          .pipe(
            map((url) => {
              const fragments = url.split('/');
              const view = fragments[fragments.length - 1]?.split('?')[0];

              this.selectedView = view;

              if (view === 'month') {
                return `Month of ${format(date, 'MMMM yyyy')}`;
              } else if (view === 'week') {
                const start = format(startOfWeek(date), 'MMM d');
                const end = format(endOfWeek(date), 'MMM d, yyyy');

                return `Week of ${start} - ${end}`;
              } else {
                return format(date, 'EEEE, MMM d, yyyy');
              }
            }),
          ),
      ),
    );
  }

  selectDate(datepicker: MatDatepicker<Date>) {
    datepicker.opened ? datepicker.close() : datepicker.open();
  }

  dateSelected(event: MatDatepickerInputEvent<Date>) {
    if (!event.value) {
      return;
    }
    this.isLoadingService.remove({ key: 'ride-requests' });

    this.router.navigate([
      '/dispatcher/ride-requests',
      event.value.valueOf(),
      this.selectedView,
    ]);
  }
}
