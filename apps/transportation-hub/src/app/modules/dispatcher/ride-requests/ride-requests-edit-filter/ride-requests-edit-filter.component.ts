import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-ride-requests-edit-filter',
  templateUrl: './ride-requests-edit-filter.component.html',
  styleUrls: ['./ride-requests-edit-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideRequestsEditFilterComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
