import { Injectable, OnDestroy } from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import {
  of,
  BehaviorSubject,
  combineLatest,
  Observable,
  Subscription,
} from 'rxjs';
import {
  switchMap,
  map,
  distinctUntilChanged,
  take,
  filter,
} from 'rxjs/operators';
import {
  startOfMonth,
  endOfMonth,
  isSameMonth,
  addMonths,
  subMonths,
  startOfDay,
  endOfDay,
  addWeeks,
  addDays,
  subWeeks,
  subDays,
  isSameWeek,
  isSameDay,
} from 'date-fns';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { EmailService } from '../../shared/email.service';
import { IsLoadingService } from '@service-work/is-loading';
import { RideRequest } from '@local/models';
import { formatDate } from '@angular/common';
import { Sort } from '@angular/material/sort';
import {
  stringComparer,
  isTruthy,
  startOfWeek,
  endOfWeek,
} from '../../../utilities';
import { Router } from '@angular/router';
import { IRideRequestsGetParams } from '../../../state';

@Injectable({
  providedIn: 'root',
})
export class RideRequestsViewService implements OnDestroy {
  selectedDate$ = new BehaviorSubject(startOfDay(new Date()));
  rideRequestCount$ = new BehaviorSubject(0);

  viewOptions$ = new BehaviorSubject<
    Array<'unassignedOnly' | 'map' | 'stp' | 'gsp' | 'fixed' | 'flexible'>
  >([]);

  unassignedOnly$ = this.viewOptions$.pipe(
    map((options) => options.includes('unassignedOnly')),
    distinctUntilChanged(),
  );

  mapView$ = this.viewOptions$.pipe(
    map((options) => options.includes('map')),
    distinctUntilChanged(),
  );

  protected readonly loadingKey: string = 'ride-requests';
  protected subscriptions: Subscription[] = [];

  constructor(
    protected fs: FirestoreService,
    protected routerStore: RouterStoreService,
    protected emailService: EmailService,
    protected isLoadingService: IsLoadingService,
    protected router: Router,
  ) {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.date)
        .pipe(
          filter(isTruthy),
          map((timestamp: string) => new Date(parseInt(timestamp))),
        )
        .subscribe((date) => this.selectedDate$.next(date)),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  getRideRequests(view: 'month' | 'week' | 'day'): Observable<RideRequest[]> {
    let requestFilterFn: (
      date: Date,
      unassignedOnly: boolean,
    ) => IRideRequestsGetParams;
    let isDateChangedFn: (x: Date, y: Date) => boolean;

    switch (view) {
      case 'month':
        requestFilterFn = (date: Date, unassignedOnly: boolean) => ({
          startAfterOrOnDate: startOfMonth(date),
          endBeforeDate: endOfMonth(date),
          driverId: unassignedOnly ? null : undefined,
          requestIsCancelled: unassignedOnly ? false : undefined,
        });
        isDateChangedFn = (x: Date, y: Date) => isSameMonth(x, y);
        break;
      case 'week':
        requestFilterFn = (date: Date, unassignedOnly: boolean) => ({
          startAfterOrOnDate: startOfWeek(date),
          endBeforeDate: endOfWeek(date),
          driverId: unassignedOnly ? null : undefined,
          requestIsCancelled: unassignedOnly ? false : undefined,
        });
        isDateChangedFn = (x: Date, y: Date) => isSameWeek(x, y);
        break;
      case 'day':
        requestFilterFn = (date: Date, unassignedOnly: boolean) => ({
          startAfterOrOnDate: startOfDay(date),
          endBeforeDate: endOfDay(date),
          driverId: unassignedOnly ? null : undefined,
          requestIsCancelled: unassignedOnly ? false : undefined,
        });
        isDateChangedFn = (x: Date, y: Date) => isSameDay(x, y);
        break;
      default:
        throw new Error('invalid method argument');
    }

    const obs$ = this.selectedDate$.pipe(
      distinctUntilChanged(isDateChangedFn),
      switchMap((date) =>
        this.unassignedOnly$.pipe(map((u) => [date, u] as const)),
      ),
      switchMap(([date, unassignedOnly]) => {
        return this.fs.getRideRequests(requestFilterFn(date, unassignedOnly), {
          driver: true,
        });
      }),
      switchMap((requests) =>
        this.viewOptions$.pipe(
          map((options) => {
            let filteredRequests = requests.slice();

            const stp = options.includes('stp');
            const gsp = options.includes('gsp');

            if (stp !== gsp) {
              const fn = stp
                ? (r: RideRequest) => r.category === 'STP'
                : (r: RideRequest) => r.category === 'GSP';

              filteredRequests = filteredRequests.filter(fn);
            }

            const fixed = options.includes('fixed');
            const flexible = options.includes('flexible');

            if (fixed !== flexible) {
              const fn = fixed
                ? (r: RideRequest) => !r.flexible
                : (r: RideRequest) => r.flexible;

              filteredRequests = filteredRequests.filter(fn);
            }

            return filteredRequests;
          }),
        ),
      ),
    );

    return this.isLoadingService.add(obs$, { key: this.loadingKey });
  }

  nextDate() {
    const view = this.getSelectedView();

    const fn =
      view === 'month' ? addMonths : view === 'week' ? addWeeks : addDays;

    this.router.navigate(
      [
        '/dispatcher/ride-requests/',
        fn(this.selectedDate$.value, 1).valueOf(),
        view,
      ],
      {
        replaceUrl: true,
        queryParamsHandling: 'preserve',
      },
    );
  }

  prevDate() {
    const view = this.getSelectedView();

    const fn =
      view === 'month' ? subMonths : view === 'week' ? subWeeks : subDays;

    this.router.navigate(
      [
        '/dispatcher/ride-requests/',
        fn(this.selectedDate$.value, 1).valueOf(),
        view,
      ],
      {
        replaceUrl: true,
        queryParamsHandling: 'preserve',
      },
    );
  }

  isRideRequestSelected(id: string) {
    return this.emailService.isRideRequestSelected(id);
  }

  requestSelected(r: RideRequest) {
    return this.emailService.requestSelected(r);
  }

  stopEventPropagation(event: MouseEvent) {
    event.stopPropagation();
  }

  sortRideRequests(rideRequests: RideRequest[], event: Sort) {
    rideRequests = rideRequests.slice();

    switch (event.active) {
      case 'date':
        rideRequests.sort(sortByDate);
        break;
      case 'time':
        rideRequests.sort(sortByTime);
        break;
      case 'priority':
        rideRequests.sort(sortByPriority);
        break;
      case 'tag':
        rideRequests.sort(sortByCategory);
        break;
      case 'client':
        rideRequests.sort(sortByClient);
        break;
      case 'driver':
        rideRequests.sort(sortByDriver);
        break;
    }

    if (event.direction === 'desc') {
      rideRequests.reverse();
    }

    return rideRequests;
  }

  protected getSelectedView() {
    const path = this.routerStore.get((store) => store.url);
    const fragments = path.split('/');
    return fragments[fragments.length - 1]?.split('?')[0];
  }
}

function sortByDate(a: RideRequest, b: RideRequest) {
  const first = formatDate(a.datetime, 'yyyy-LL-dd', 'en-US');
  const second = formatDate(b.datetime, 'yyyy-LL-dd', 'en-US');

  if (first === second) return sortByTime(a, b);
  else if (first < second) return 1;
  else return -1;
}

function sortByTime(a: RideRequest, b: RideRequest) {
  const first = formatDate(a.datetime, 'HH-mm', 'en-US');
  const second = formatDate(b.datetime, 'HH-mm', 'en-US');

  return stringComparer(second, first);
}

function sortByPriority(a: RideRequest, b: RideRequest) {
  const first = a.priority === 'low' ? 1 : a.priority === 'medium' ? 2 : 3;
  const second = b.priority === 'low' ? 1 : b.priority === 'medium' ? 2 : 3;

  if (first === second) return 1;
  else if (first < second) return 1;
  else return -1;
}

function sortByCategory(a: RideRequest, b: RideRequest) {
  const first = a.category === 'GSP' ? 1 : 2;
  const second = b.category === 'GSP' ? 1 : 2;

  if (a.category === b.category) return sortByDate(a, b);
  return stringComparer(a.category, b.category);
}

function sortByClient(a: RideRequest, b: RideRequest) {
  const first = a.client.name.toLowerCase();
  const second = b.client.name.toLowerCase();

  return stringComparer(first, second);
}

function sortByDriver(a: RideRequest, b: RideRequest) {
  const first = (a.driver && a.driver.name.toLowerCase()) || '';
  const second = (b.driver && b.driver.name.toLowerCase()) || '';

  if (first === second) return sortByDate(a, b);
  else return stringComparer(first, second);
}
