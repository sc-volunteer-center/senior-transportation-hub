import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { RideRequestsViewService } from './ride-requests-view.service';

@Component({
  selector: 'app-ride-requests-breadcrumb',
  template: `
    <div
      *ngIf="'ride-requests' | swIsLoading | async; else requestsLoaded"
      class="flex-row flex-center"
    >
      <mat-spinner
        [diameter]="22"
        color="accent"
        class="vertical-margin-16"
        style="margin-right: 0.5rem"
      ></mat-spinner>
      <h3>Requests</h3>
    </div>

    <ng-template #requestsLoaded>
      {{ count }} {{ count === 1 ? 'Request' : 'Requests' }}
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideRequestsBreadcrumbComponent implements OnInit {
  count = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    public rideRequestsViewService: RideRequestsViewService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.rideRequestsViewService.rideRequestCount$.subscribe((count) => {
        this.count = count;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
