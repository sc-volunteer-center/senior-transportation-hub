import {
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
} from '@angular/core';
import { formatDate } from '@angular/common';
import { BaseRideRequestsViewComponent } from './base-ride-requests-view.component';

@Component({
  selector: 'app-ride-requests-month-view',
  templateUrl: './ride-requests-view.component.html',
  styleUrls: ['./ride-requests-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'ride-requests-view',
  },
})
export class RideRequestsMonthViewComponent extends BaseRideRequestsViewComponent {
  readonly viewTimespan = 'month';

  setTitle(date: Date): void {
    this.titleService.setTitle(
      `Ride Requests, ${formatDate(date, 'MMMM yyyy', 'en-US')} | Hub`,
    );
  }
}
