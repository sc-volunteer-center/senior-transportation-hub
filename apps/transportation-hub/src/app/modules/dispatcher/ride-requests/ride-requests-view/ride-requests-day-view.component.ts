import {
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
} from '@angular/core';
import { formatDate } from '@angular/common';
import { BaseRideRequestsViewComponent } from './base-ride-requests-view.component';

@Component({
  selector: 'app-ride-requests-day-view',
  templateUrl: './ride-requests-view.component.html',
  styleUrls: ['./ride-requests-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'ride-requests-view',
  },
})
export class RideRequestsDayViewComponent extends BaseRideRequestsViewComponent {
  readonly viewTimespan = 'day';

  setTitle(date: Date): void {
    this.titleService.setTitle(
      `Ride Requests, ${formatDate(date, 'shortDate', 'en-US')} | Hub`,
    );
  }
}
