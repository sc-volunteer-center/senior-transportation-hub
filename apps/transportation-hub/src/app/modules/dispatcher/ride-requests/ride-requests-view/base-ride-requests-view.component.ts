import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewEncapsulation,
  AfterViewInit,
  ViewChildren,
  QueryList,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { RideRequest } from '@local/models';
import { EmailService } from '../../../shared/email.service';
import { RideRequestsViewService } from '../ride-requests-view.service';
import { Title } from '@angular/platform-browser';
import { format } from 'date-fns';
import {
  endOfWeek,
  navigateToPathFn,
  ngForTrackByID,
  startOfWeek,
} from 'apps/transportation-hub/src/app/utilities';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-base-ride-requests-view',
  templateUrl: './ride-requests-view.component.html',
  styleUrls: ['./ride-requests-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'ride-requests-view',
  },
})
export abstract class BaseRideRequestsViewComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  rideRequests: RideRequest[] = [];

  sortBy = 'date';
  sortDir = 'desc';

  abstract readonly viewTimespan: 'day' | 'week' | 'month';

  selectedRequestOnMap: RideRequest | null = null;
  @ViewChildren(MapInfoWindow) private infoWindow: QueryList<MapInfoWindow>;

  protected subscriptions: Subscription[] = [];

  constructor(
    private emailService: EmailService,
    private cdr: ChangeDetectorRef,
    public rideRequestsViewService: RideRequestsViewService,
    protected titleService: Title,
    private router: Router,
    private isLoadingService: IsLoadingService,
    private routerStore: RouterStoreService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    // need to mark 'ride-requests' as loading from OnInit to
    // AfterViewInit because otherwise
    // ('ride-requests' | swIsLoading | async) === false briefly
    // in the template which causes the mat-table to initialize
    // and then a tick later it is destroyed which causes
    // swScrollPosition to erase the "good" scroll position
    // and set a scroll position of 0.
    this.isLoadingService.add({ key: 'ride-requests' });

    this.subscriptions.push(
      this.routerStore
        .get$((s) => s.state.queryParams)
        .subscribe((queryParams) => {
          this.sortBy = queryParams.sortBy || 'date';
          this.sortDir = queryParams.sortDir || 'desc';
          this.sortRideRequests();
        }),
    );
  }

  ngAfterViewInit() {
    this.subscriptions.push(
      this.rideRequestsViewService.selectedDate$.subscribe((date) => {
        this.setTitle(date);
      }),
      this.rideRequestsViewService
        .getRideRequests(this.viewTimespan)
        .subscribe((rideRequests) => {
          this.rideRequests = rideRequests;

          this.rideRequestsViewService.rideRequestCount$.next(
            rideRequests.length,
          );

          this.sortRideRequests();
        }),
    );

    this.subscriptions.push(
      this.emailService.selectedRideRequests.subscribe(() => {
        this.cdr.detectChanges();
      }),
    );

    // must come at the end of AfterViewInit to ensure proper loading state
    this.isLoadingService.remove({ key: 'ride-requests' });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  abstract setTitle(date: Date): void;

  openInfoWindow(marker: MapMarker, request: RideRequest) {
    this.selectedRequestOnMap = request;
    this.infoWindow.first.open(marker);
    this.cdr.markForCheck();
  }

  protected sortRideRequests() {
    this.rideRequests = this.rideRequestsViewService.sortRideRequests(
      this.rideRequests,
      { active: this.sortBy, direction: this.sortDir as 'asc' | 'desc' },
    );

    this.cdr.detectChanges();
  }
}
