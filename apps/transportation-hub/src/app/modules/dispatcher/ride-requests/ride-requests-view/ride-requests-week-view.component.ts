import {
  Component,
  ChangeDetectionStrategy,
  ViewEncapsulation,
} from '@angular/core';
import { format } from 'date-fns';
import { BaseRideRequestsViewComponent } from './base-ride-requests-view.component';
import {
  endOfWeek,
  startOfWeek,
} from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-ride-requests-week-view',
  templateUrl: './ride-requests-view.component.html',
  styleUrls: ['./ride-requests-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'ride-requests-view',
  },
})
export class RideRequestsWeekViewComponent extends BaseRideRequestsViewComponent {
  readonly viewTimespan = 'week';

  setTitle(date: Date): void {
    const start = format(startOfWeek(date), 'MMM d');
    const end = format(endOfWeek(date), 'MMM d, yyyy');
    this.titleService.setTitle(`Ride Requests, ${start} - ${end} | Hub`);
  }
}
