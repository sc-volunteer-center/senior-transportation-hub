import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterViewInit,
  ViewChildren,
  QueryList,
  OnInit,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Subscription, of, combineLatest, NEVER } from 'rxjs';
import { Client, Tag } from '@local/models';
import {
  distinctUntilChanged,
  switchMap,
  debounceTime,
  map,
  filter,
  startWith,
  skip,
  takeUntil,
} from 'rxjs/operators';
import {
  stringComparer,
  navigateToPathFn,
  numberComparer,
  ngForTrackByID,
  isNonNullable,
} from '../../../utilities';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ClientsBreadcrumbService } from './clients-breadcrumb.component';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { Title } from '@angular/platform-browser';
import { ISimpleTag } from '../../shared/display-tags/display-tags.component';
import { IsLoadingService } from '@service-work/is-loading';
import * as moment from 'moment-timezone';
import { formatDate } from '@angular/common';
import { addWeeks, endOfDay, startOfDay } from 'date-fns';
import { isEqual } from '@local/isEqual';
import Fuse from 'fuse.js';

const SEARCH_DEBOUNCE_TIME = 300;

type ModifiedClient = Client & {
  customTags: ISimpleTag[];
  activeRecurringRequests: number;
  upcomingRequests: number;
};

@Component({
  selector: 'app-clients',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit, OnDestroy {
  private clients: ModifiedClient[] = [];

  searchControl = new FormControl('');

  filterText = '';
  filteredClients: ModifiedClient[] = [];

  clientTags: Tag[] = [];
  tagsControl = new FormControl([]);
  private filterTags: string[] = [];

  filterOptions: string[] = [];
  filterOptionsControl = new FormControl([]);

  selectedClientOnMap: ModifiedClient | null = null;

  sortBy = 'name';
  sortDir = 'desc';

  @ViewChildren(MapInfoWindow) private infoWindow: QueryList<MapInfoWindow>;
  private subscriptions: Subscription[] = [];

  private filterQuery$ = this.route.queryParamMap.pipe(
    map((params) => ({
      // the sortBy and sortDir params are set by the MatSortRouterSyncDirective
      sortBy: params.get('sortBy') || 'name',
      sortDir: params.get('sortDir') || 'desc',
      searchText: params.get('search')?.trim() || '',
      // params.get('tags') might equal "" or null
      tags: !params.get('tags') ? [] : params.get('tags')!.split(','),
      // params.get('options') might equal "" or null
      options: !params.get('options') ? [] : params.get('options')!.split(','),
    })),
    distinctUntilChanged(isEqual),
  );

  constructor(
    private cdr: ChangeDetectorRef,
    private fs: FirestoreService,
    private route: ActivatedRoute,
    private router: Router,
    private clientsBreadcrumbService: ClientsBreadcrumbService,
    private titleService: Title,
    private isLoadingService: IsLoadingService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    this.titleService.setTitle(`Clients | Hub`);

    this.subscriptions.push(
      this.fs.getTags({ type: 'client' }).subscribe((tags) => {
        this.clientTags = tags;
        this.cdr.markForCheck();
      }),
    );

    const params = this.route.snapshot.queryParamMap;

    // params.get('search') might equal null
    this.searchControl.patchValue(params.get('search') || '');
    // params.get('tags') might equal "" or null
    this.tagsControl.patchValue(
      !params.get('tags') ? [] : params.get('tags')!.split(','),
    );
    // params.get('options') might equal "" or null
    this.filterOptionsControl.patchValue(
      !params.get('options') ? [] : params.get('options')!.split(','),
    );

    this.subscriptions.push(
      combineLatest([
        this.searchControl.valueChanges.pipe(
          startWith(this.searchControl.value),
        ),
        this.tagsControl.valueChanges.pipe(startWith(this.tagsControl.value)),
        this.filterOptionsControl.valueChanges.pipe(
          startWith(this.filterOptionsControl.value),
        ),
      ])
        .pipe(debounceTime(SEARCH_DEBOUNCE_TIME), distinctUntilChanged(isEqual))
        .subscribe(([search, tags, options]) => {
          if (tags.includes(undefined)) {
            // user has selected the "Edit tags" option
            this.router.navigate(['/dispatcher/tags/client']);
            return;
          }

          this.router.navigate([], {
            queryParams: {
              search: search.trim(),
              tags: tags.join(','),
              options: options.join(','),
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
    );

    // when searching reset results to top of list
    this.subscriptions.push(
      this.searchControl.valueChanges
        .pipe(debounceTime(SEARCH_DEBOUNCE_TIME), distinctUntilChanged(isEqual))
        .subscribe(() => {
          document.getElementById('clients-table')?.scrollTo({
            top: 0,
          });
        }),
    );

    const includeRetiredClients$ = this.filterQuery$.pipe(
      map(
        ({ searchText, options }) =>
          !!(options.includes('retired') || searchText),
      ),
      distinctUntilChanged(),
    );

    // respond to filtering changes
    this.subscriptions.push(
      this.isLoadingService
        .add(
          combineLatest([
            // get clients
            includeRetiredClients$.pipe(
              switchMap((includeRetiredClients) =>
                this.fs.getClients({
                  // if includeRetiredClients then all clients else just non-retired clients
                  retired: includeRetiredClients && undefined,
                  cache: { expirationTime: Number.POSITIVE_INFINITY },
                }),
              ),
            ),
            // get recurring ride requests
            this.fs.getRecurringRideRequests({
              endAfterDate: endOfDay(new Date()),
              cache: { expirationTime: 1000 * 60 * 10 },
            }),
            // get ride requests
            this.fs.getRideRequests({
              startAfterOrOnDate: startOfDay(new Date()),
              endBeforeDate: addWeeks(endOfDay(new Date()), 3),
              requestIsCancelled: false,
              cache: { expirationTime: 1000 * 60 * 10 },
            }),
          ]).pipe(
            distinctUntilChanged(isEqual),
            map(([clients, rrr, rr]) => {
              return clients.map((client) => {
                const c = client as ModifiedClient;

                c.activeRecurringRequests = rrr.filter(
                  (r) => r.clientId === client.uid,
                ).length;

                c.upcomingRequests = rr.filter(
                  (r) => r.clientId === client.uid,
                ).length;

                // need to add customTags to the client objects because dynamically
                // generating them in the template messes with MatTooltip's change
                // detection and prevents the tooltips from showing
                c.customTags = this.generateCustomTags(c);

                return c;
              });
            }),
            switchMap((clients) =>
              // here we ensure that filtering doesn't happen until our clients array
              // has been updated
              this.filterQuery$.pipe(
                takeUntil(includeRetiredClients$.pipe(skip(1))),
                map((p) => [clients, p] as const),
              ),
            ),
          ),
          { key: 'loading-clients' },
        )
        .subscribe(([clients, p]) => {
          this.sortBy = p.sortBy;
          this.sortDir = p.sortDir;
          this.filterText = p.searchText;
          this.filterTags = p.tags;
          this.filterOptions = p.options;
          this.clients = clients;

          this.filterAndSortClients();
        }),
    );

    // respond to filtering and sorting changes
    // this.subscriptions.push(
    //   this.filterQuery$.subscribe((p) => {
    //     this.sortBy = p.sortBy;
    //     this.sortDir = p.sortDir;
    //     this.filterText = p.searchText;
    //     this.filterTags = p.tags;
    //     this.filterOptions = p.options;
    //     this.filterAndSortClients();
    //   }),
    // );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  displayActiveStatus(client: Client) {
    return client.active ? 'Yes' : 'Retired';
  }

  filterAndSortClients() {
    let filteredClients = this.clients.slice();

    if (this.filterTags.length > 0) {
      // filter by selected tags
      const handleTagResult = (res: boolean) =>
        this.filterOptions.includes('hideTags') ? !res : res;

      filteredClients = filteredClients.filter((client) => {
        if (!client.tagIds) return handleTagResult(false);

        if (this.filterOptions.includes('allTags')) {
          return handleTagResult(
            this.filterTags.every((t) => client.tagIds.includes(t)),
          );
        } else {
          return handleTagResult(
            this.filterTags.some((t) => client.tagIds.includes(t)),
          );
        }
      });
    }

    if (this.filterText) {
      // filter and sort by search text
      const fuse = new Fuse(
        filteredClients.map((client) => ({
          nicknameFullName: client.name,
          realFullName: client.nameSecondaryIndex,
          phone: client.phoneNumber.replace(/\D/g, ''), // remove non-numbers
          email: client.emailAddress,
          restrictions: client.restrictions,
          comments: client.comments,
          client,
        })),
        {
          keys: [
            {
              name: 'nicknameFullName',
              weight: '0.8',
            },
            {
              name: 'realFullName',
              weight: '0.5',
            },
            {
              name: 'phone',
              weight: '0.7',
            },
            {
              name: 'email',
              weight: '0.5',
            },
            {
              name: 'restrictions',
              weight: '0.3',
            },
            {
              name: 'comments',
              weight: '0.3',
            },
          ] as any,
          isCaseSensitive: false,
          shouldSort: true,
          // includeScore: true,
          // includeMatches: true,
          ignoreLocation: true,
          // Only the matches whose length exceeds this value will be returned.
          // (For instance, if you want to ignore single character matches in the result, set it to 2).
          // minMatchCharLength: 2,
        },
      );

      const searchResults = fuse.search(this.filterText);

      filteredClients = searchResults.map((result) => result.item.client);
    } else {
      // sort results as appropriate by selected column

      let fn: (a: ModifiedClient, b: ModifiedClient) => 1 | 0 | -1;

      if (this.sortBy === 'recurring') {
        fn = (a, b) => {
          if (a.activeRecurringRequests === b.activeRecurringRequests) {
            const v = stringComparer(a.name, b.name);
            return v === 0 ? 0 : (-v as 1 | -1);
          }

          return numberComparer(
            a.activeRecurringRequests,
            b.activeRecurringRequests,
          );
        };
      } else if (this.sortBy === 'upcoming') {
        fn = (a, b) => {
          if (a.upcomingRequests === b.upcomingRequests) {
            const v = stringComparer(a.name, b.name);
            return v === 0 ? 0 : (-v as 1 | -1);
          }

          return numberComparer(a.upcomingRequests, b.upcomingRequests);
        };
      } else if (this.sortBy === 'active') {
        fn = (a, b) => {
          if (a.active === b.active) return stringComparer(a.name, b.name);
          else if (a.active) return 1;
          else return -1;
        };
      } else {
        fn = (a, b) => stringComparer(a.name, b.name);
      }

      filteredClients.sort(fn);

      if (this.sortDir === 'asc') {
        filteredClients.reverse();
      }
    }

    this.filteredClients = filteredClients;
    this.clientsBreadcrumbService.clientCount.next(filteredClients.length);
    this.cdr.markForCheck();
  }

  openInfoWindow(marker: MapMarker, client: ModifiedClient) {
    this.selectedClientOnMap = client;
    this.infoWindow.first.open(marker);
    this.cdr.markForCheck();
  }

  private generateCustomTags(client: Client): ISimpleTag[] {
    const tags: ISimpleTag[] = [];

    if (
      moment(new Date(client.createdAt)).isAfter(
        moment().subtract(3, 'weeks'),
        'day',
      )
    ) {
      tags.push({
        label: `NEW`,
        description: `This is a relatively new client that was added on ${formatDate(
          client.createdAt,
          'fullDate',
          'en-US',
        )}.`,
        color: 'Lime',
      });
    }

    return tags;
  }
}
