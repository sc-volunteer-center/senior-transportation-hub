import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Injectable,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientsBreadcrumbService {
  clientCount = new BehaviorSubject(0);
}

@Component({
  selector: 'app-clients-breadcrumb',
  template: `
    <div
      *ngIf="'loading-clients' | swIsLoading | async; else clientsLoaded"
      class="flex-row flex-center"
    >
      <mat-spinner
        [diameter]="22"
        color="accent"
        class="vertical-margin-16"
        style="margin-right: 0.5rem"
      ></mat-spinner>
      <h3>Clients</h3>
    </div>

    <ng-template #clientsLoaded>
      {{ count }} {{ count === 1 ? 'Client' : 'Clients' }}
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientsBreadcrumbComponent implements OnInit, OnDestroy {
  count = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    public clientsBreadcrumbService: ClientsBreadcrumbService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.clientsBreadcrumbService.clientCount.subscribe((count) => {
        this.count = count;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
