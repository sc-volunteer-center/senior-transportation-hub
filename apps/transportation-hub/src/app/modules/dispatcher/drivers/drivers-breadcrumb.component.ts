import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Injectable,
  ChangeDetectorRef,
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DriversBreadcrumbService {
  driverCount = new BehaviorSubject(0);
}

@Component({
  selector: 'app-drivers-breadcrumb',
  template: `
    <div
      *ngIf="'loading-drivers' | swIsLoading | async; else driversLoaded"
      class="flex-row flex-center"
    >
      <mat-spinner
        [diameter]="22"
        color="accent"
        class="vertical-margin-16"
        style="margin-right: 0.5rem"
      ></mat-spinner>
      <h3>Volunteers</h3>
    </div>

    <ng-template #driversLoaded>
      {{ count }} {{ count === 1 ? 'Volunteer' : 'Volunteers' }}
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DriversBreadcrumbComponent implements OnInit {
  count = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    public driversBreadcrumbService: DriversBreadcrumbService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.driversBreadcrumbService.driverCount.subscribe((count) => {
        this.count = count;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
