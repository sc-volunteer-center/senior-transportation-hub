import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  ChangeDetectorRef,
  AfterViewInit,
  ViewChildren,
  QueryList,
  OnInit,
} from '@angular/core';
import { Subscription, of, combineLatest, NEVER } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { Driver, Tag } from '@local/models';
import {
  switchMap,
  distinctUntilChanged,
  debounceTime,
  map,
  skip,
  startWith,
  takeUntil,
} from 'rxjs/operators';
import {
  stringComparer,
  navigateToPathFn,
  numberComparer,
  ngForTrackByID,
} from '../../../utilities';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DriversBreadcrumbService } from './drivers-breadcrumb.component';
import { MapMarker, MapInfoWindow } from '@angular/google-maps';
import { Title } from '@angular/platform-browser';
import { ISimpleTag } from '../../shared/display-tags/display-tags.component';
import { IsLoadingService } from '@service-work/is-loading';
import * as moment from 'moment-timezone';
import { formatDate } from '@angular/common';
import { addWeeks, endOfDay, startOfDay } from 'date-fns';
import { isEqual } from '@local/isEqual';
import Fuse from 'fuse.js';

const SEARCH_DEBOUNCE_TIME = 300;

type ModifiedDriver = Driver & {
  customTags: ISimpleTag[];
  activeRecurringRequests: number;
  upcomingRequests: number;
};

@Component({
  selector: 'app-drivers',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss'],
})
export class DriversComponent implements OnInit, OnDestroy {
  private drivers: ModifiedDriver[] = [];

  searchControl = new FormControl('');

  filterText = '';
  filteredDrivers: ModifiedDriver[] = [];

  driverTags: Tag[] = [];
  tagsControl = new FormControl([]);
  private filterTags: string[] = [];

  filterOptions: string[] = [];
  filterOptionsControl = new FormControl([]);

  selectedDriverOnMap: ModifiedDriver | null = null;

  sortBy = 'name';
  sortDir = 'desc';

  @ViewChildren(MapInfoWindow) private infoWindow: QueryList<MapInfoWindow>;
  private subscriptions: Subscription[] = [];

  private filterQuery$ = this.route.queryParamMap.pipe(
    map((params) => ({
      // the sortBy and sortDir params are set by the MatSortRouterSyncDirective
      sortBy: params.get('sortBy') || 'name',
      sortDir: params.get('sortDir') || 'desc',
      searchText: params.get('search')?.trim() || '',
      // params.get('tags') might equal "" or null
      tags: !params.get('tags') ? [] : params.get('tags')!.split(','),
      // params.get('options') might equal "" or null
      options: !params.get('options') ? [] : params.get('options')!.split(','),
    })),
    distinctUntilChanged(isEqual),
  );

  constructor(
    private cdr: ChangeDetectorRef,
    private fs: FirestoreService,
    private route: ActivatedRoute,
    private router: Router,
    private driversBreadcrumbService: DriversBreadcrumbService,
    private titleService: Title,
    private isLoadingService: IsLoadingService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    this.titleService.setTitle(`Drivers | Hub`);

    this.subscriptions.push(
      this.fs.getTags({ type: 'driver' }).subscribe((tags) => {
        this.driverTags = tags;
        this.cdr.markForCheck();
      }),
    );

    const params = this.route.snapshot.queryParamMap;

    // params.get('search') might equal null
    this.searchControl.patchValue(params.get('search') || '');
    // params.get('tags') might equal "" or null
    this.tagsControl.patchValue(
      !params.get('tags') ? [] : params.get('tags')!.split(','),
    );
    // params.get('options') might equal "" or null
    this.filterOptionsControl.patchValue(
      !params.get('options') ? [] : params.get('options')!.split(','),
    );

    this.subscriptions.push(
      combineLatest([
        this.searchControl.valueChanges.pipe(
          startWith(this.searchControl.value),
        ),
        this.tagsControl.valueChanges.pipe(startWith(this.tagsControl.value)),
        this.filterOptionsControl.valueChanges.pipe(
          startWith(this.filterOptionsControl.value),
        ),
      ])
        .pipe(debounceTime(SEARCH_DEBOUNCE_TIME), distinctUntilChanged(isEqual))
        .subscribe(([search, tags, options]) => {
          if (tags.includes(undefined)) {
            // user has selected the "Edit tags" option
            this.router.navigate(['/dispatcher/tags/driver']);
            return;
          }

          this.router.navigate([], {
            queryParams: {
              search: search.trim(),
              tags: tags.join(','),
              options: options.join(','),
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
    );

    // when searching reset results to top of list
    this.subscriptions.push(
      this.searchControl.valueChanges
        .pipe(debounceTime(SEARCH_DEBOUNCE_TIME), distinctUntilChanged(isEqual))
        .subscribe(() => {
          document.getElementById('volunteers-table')?.scrollTo({
            top: 0,
          });
        }),
    );

    const includeRetiredDrivers$ = this.filterQuery$.pipe(
      map(
        ({ searchText, options }) =>
          !!(options.includes('retired') || searchText),
      ),
      distinctUntilChanged(),
    );

    // respond to filtering changes
    this.subscriptions.push(
      this.isLoadingService
        .add(
          combineLatest([
            // get drivers
            includeRetiredDrivers$.pipe(
              switchMap((includeRetiredDrivers) =>
                this.fs.getDrivers({
                  // if includeRetiredDrivers then all drivers else just non-retired drivers
                  retired: includeRetiredDrivers && undefined,
                  cache: { expirationTime: Number.POSITIVE_INFINITY },
                }),
              ),
            ),
            // get recurring ride requests
            this.fs.getRecurringRideRequests({
              endAfterDate: endOfDay(new Date()),
              cache: { expirationTime: 1000 * 60 * 10 },
            }),
            // get ride requests
            this.fs.getRideRequests({
              startAfterOrOnDate: startOfDay(new Date()),
              endBeforeDate: addWeeks(endOfDay(new Date()), 3),
              requestIsCancelled: false,
              cache: { expirationTime: 1000 * 60 * 10 },
            }),
          ]).pipe(
            distinctUntilChanged(isEqual),
            map(([drivers, rrr, rr]) => {
              return drivers.map((driver) => {
                const c = driver as ModifiedDriver;

                c.activeRecurringRequests = rrr.filter(
                  (r) => r.driverId === driver.uid,
                ).length;

                c.upcomingRequests = rr.filter(
                  (r) => r.driverId === driver.uid,
                ).length;

                // need to add customTags to the client objects because dynamically
                // generating them in the template messes with MatTooltip's change
                // detection and prevents the tooltips from showing
                c.customTags = this.generateCustomTags(c);

                return c;
              });
            }),
            switchMap((drivers) =>
              // here we ensure that filtering doesn't happen until our drivers array
              // has been updated
              this.filterQuery$.pipe(
                takeUntil(includeRetiredDrivers$.pipe(skip(1))),
                map((p) => [drivers, p] as const),
              ),
            ),
          ),
          { key: 'loading-drivers' },
        )
        .subscribe(([drivers, p]) => {
          this.sortBy = p.sortBy;
          this.sortDir = p.sortDir;
          this.filterText = p.searchText;
          this.filterTags = p.tags;
          this.filterOptions = p.options;
          this.drivers = drivers;

          this.filterAndSortDrivers();
        }),
    );

    // respond to filtering and sorting changes
    // this.subscriptions.push(
    //   this.filterQuery$.subscribe((p) => {
    //     this.sortBy = p.sortBy;
    //     this.sortDir = p.sortDir;
    //     this.filterText = p.searchText;
    //     this.filterTags = p.tags;
    //     this.filterOptions = p.options;
    //     this.filterAndSortDrivers();
    //   }),
    // );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  displayActiveStatus(driver: Driver) {
    return driver.isOnVacation()
      ? 'Currently on vacation'
      : driver.active
      ? 'Yes'
      : 'Retired';
  }

  filterAndSortDrivers() {
    let filteredDrivers = this.drivers.slice();

    if (this.filterTags.length > 0) {
      // filter by selected tags
      const handleTagResult = (res: boolean) =>
        this.filterOptions.includes('hideTags') ? !res : res;

      filteredDrivers = filteredDrivers.filter((driver) => {
        if (!driver.tagIds) return handleTagResult(false);

        if (this.filterOptions.includes('allTags')) {
          return handleTagResult(
            this.filterTags.every((t) => driver.tagIds.includes(t)),
          );
        } else {
          return handleTagResult(
            this.filterTags.some((t) => driver.tagIds.includes(t)),
          );
        }
      });
    }

    if (this.filterText) {
      // filter and sort by search text
      const fuse = new Fuse(
        filteredDrivers.map((driver) => ({
          nicknameFullName: driver.name,
          realFullName: driver.nameSecondaryIndex,
          phone: driver.phoneNumber.replace(/\D/g, ''), // remove non-numbers
          email: driver.emailAddress,
          comments: driver.comments,
          driver,
        })),
        {
          keys: [
            {
              name: 'nicknameFullName',
              weight: '0.8',
            },
            {
              name: 'realFullName',
              weight: '0.5',
            },
            {
              name: 'phone',
              weight: '0.7',
            },
            {
              name: 'email',
              weight: '0.5',
            },
            {
              name: 'comments',
              weight: '0.3',
            },
          ] as any,
          isCaseSensitive: false,
          shouldSort: true,
          // includeScore: true,
          // includeMatches: true,
          ignoreLocation: true,
          // Only the matches whose length exceeds this value will be returned.
          // (For instance, if you want to ignore single character matches in the result, set it to 2).
          // minMatchCharLength: 2,
        },
      );

      const searchResults = fuse.search(this.filterText);

      filteredDrivers = searchResults.map((result) => result.item.driver);
    } else {
      // sort results as appropriate by selected column

      let fn: (a: ModifiedDriver, b: ModifiedDriver) => 1 | 0 | -1;

      if (this.sortBy === 'recurring') {
        fn = (a, b) => {
          if (a.activeRecurringRequests === b.activeRecurringRequests) {
            const v = stringComparer(a.name, b.name);
            return v === 0 ? 0 : (-v as 1 | -1);
          }

          return numberComparer(
            a.activeRecurringRequests,
            b.activeRecurringRequests,
          );
        };
      } else if (this.sortBy === 'upcoming') {
        fn = (a, b) => {
          if (a.upcomingRequests === b.upcomingRequests) {
            const v = stringComparer(a.name, b.name);
            return v === 0 ? 0 : (-v as 1 | -1);
          }

          return numberComparer(a.upcomingRequests, b.upcomingRequests);
        };
      } else if (this.sortBy === 'active') {
        fn = (a, b) => {
          if (a.active === b.active) return stringComparer(a.name, b.name);
          else if (a.active) return 1;
          else return -1;
        };
      } else {
        fn = (a, b) => stringComparer(a.name, b.name);
      }

      filteredDrivers.sort(fn);

      if (this.sortDir === 'asc') {
        filteredDrivers.reverse();
      }
    }

    this.filteredDrivers = filteredDrivers;
    this.driversBreadcrumbService.driverCount.next(filteredDrivers.length);
    this.cdr.markForCheck();
  }

  openInfoWindow(marker: MapMarker, driver: ModifiedDriver) {
    this.selectedDriverOnMap = driver;
    this.infoWindow.first.open(marker);
    this.cdr.markForCheck();
  }

  private generateCustomTags(driver: Driver): ISimpleTag[] {
    const tags: ISimpleTag[] = [];

    if (
      moment(new Date(driver.createdAt)).isAfter(
        moment().subtract(3, 'weeks'),
        'day',
      )
    ) {
      tags.push({
        label: `NEW`,
        description: `This is a relatively new driver that was added on ${formatDate(
          driver.createdAt,
          'fullDate',
          'en-US',
        )}.`,
        color: 'Lime',
      });
    }

    return tags;
  }
}
