import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../firestore.service';
import { Dispatcher, IOrganization } from '@local/models';
import { Subscription } from 'rxjs';
import { IsLoadingService } from '@service-work/is-loading';
import { toObservable } from '../../utilities';
import {
  Router,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
} from '@angular/router';
import { filter, switchMap, take } from 'rxjs/operators';
import { AppUpdateService } from '../shared/update-available/app-update.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      [ngSwitch]="render()"
      class="flex-column flex-1"
      style="overflow-y: auto"
    >
      <ng-template [ngSwitchCase]="'welcome'">
        <a mat-button routerLink="/welcome" routerLinkActive="active">
          Welcome
        </a>

        <button mat-button (click)="logout()">Logout</button>
      </ng-template>

      <ng-template [ngSwitchCase]="'dispatcher'">
        <a
          mat-button
          routerLink="/dispatcher/overview"
          routerLinkActive="active"
        >
          Overview
        </a>

        <a
          mat-button
          [routerLink]="[
            '/dispatcher/ride-requests',
            rideRequestViewDate,
            'week'
          ]"
          [ngClass]="{ active: rideRequestsActive }"
        >
          Requests
        </a>

        <a
          mat-button
          routerLink="/dispatcher/clients"
          routerLinkActive="active"
        >
          Clients
        </a>

        <a
          mat-button
          routerLink="/dispatcher/drivers"
          routerLinkActive="active"
        >
          Volunteers
        </a>

        <a
          mat-button
          routerLink="/dispatcher/destinations"
          routerLinkActive="active"
        >
          Destinations
        </a>

        <a
          mat-button
          routerLink="/dispatcher/dispatchers"
          routerLinkActive="active"
        >
          Dispatchers
        </a>

        <a mat-button routerLink="/dispatcher/notes" routerLinkActive="active">
          Shared Notes
        </a>

        <a mat-button routerLink="/dispatcher/help" routerLinkActive="active">
          Help
        </a>

        <button mat-button (click)="dispatcherLogout()">Signout</button>
      </ng-template>

      <ng-template [ngSwitchCase]="'dispatcher signin'">
        <a mat-button routerLink="/dispatcher/signin" routerLinkActive="active">
          Signin
        </a>

        <button mat-button (click)="logout()">Exit Website</button>
      </ng-template>
    </div>

    <!-- Update available notification -->
    <app-update-available
      *ngIf="appUpdateService.updateAvailable | async; else userInfo"
      class="padding-12 user-box"
    >
    </app-update-available>

    <!-- Signed in user info -->
    <ng-template #userInfo>
      <div *ngIf="dispatcher && organization" class="padding-12 user-box">
        <h5>Signed in as</h5>

        <h3
          class="truncate-text"
          [matTooltip]="dispatcher!.name"
          [matTooltipShowDelay]="500"
        >
          {{ dispatcher!.name }}
        </h3>

        <h5
          class="truncate-text"
          [matTooltip]="organizationTooltip()"
          [matTooltipShowDelay]="500"
          style="margin-top: .5rem;"
        >
          @ {{ organization!.name }}
        </h5>

        <h5 style="margin-top: .5rem;">
          Today's date: {{ today | date: 'shortDate' }}
        </h5>
      </div>
    </ng-template>
  `,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'app-sidebar' },
})
export class SidebarComponent implements OnInit {
  @Input() organization?: IOrganization;
  @Input() dispatcher?: Dispatcher;

  pendingDispatcherSignout?: Subscription = new Subscription();

  rideRequestsActive = false;

  rideRequestViewDate = new Date().valueOf();

  today = new Date();

  constructor(
    private fs: FirestoreService,
    private isLoadingService: IsLoadingService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
    public appUpdateService: AppUpdateService,
  ) {}

  ngOnInit() {
    this.rideRequestsActive = !!this.router.url.match(
      'dispatcher/ride-requests',
    );

    this.router.events
      .pipe(
        filter(
          (event) =>
            event instanceof NavigationEnd ||
            event instanceof NavigationCancel ||
            event instanceof NavigationError,
        ),
      )
      .subscribe(() => {
        this.rideRequestsActive = !!this.router.url.match(
          'dispatcher/ride-requests',
        );
        this.cdr.markForCheck();
      });
  }

  logout() {
    return this.authService.logout().catch((e) => console.error(e));
  }

  dispatcherLogout() {
    this.pendingDispatcherSignout = toObservable(this.fs.endDispatcherShift())
      .pipe(
        switchMap(() => this.fs.getCurrentDispatcher()),
        filter((disp) => !disp),
        take(1),
      )
      .subscribe(() => {
        this.router.navigate(['dispatcher/signin']);
      });

    this.isLoadingService.add(this.pendingDispatcherSignout);
  }

  render() {
    if (!this.organization) {
      return 'welcome';
    } else if (this.dispatcher) {
      return 'dispatcher';
    } else {
      return 'dispatcher signin';
    }
  }

  organizationTooltip() {
    if (!this.organization) return '';

    return `${this.organization.name} [PERSISTANCE ${
      this.fs.persistanceEnabled ? 'ON' : 'OFF'
    }]`;
  }
}
