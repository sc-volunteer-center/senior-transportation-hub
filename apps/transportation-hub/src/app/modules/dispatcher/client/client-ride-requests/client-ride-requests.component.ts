import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { RideRequest, Driver, Destination } from '@local/models';
import { Subscription, of, combineLatest } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import {
  switchMap,
  map,
  tap,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
import { startOfMonth, endOfMonth, addMonths, subMonths } from 'date-fns';
import {
  MatDatepicker,
  MatDatepickerInputEvent,
} from '@angular/material/datepicker';
import { IsLoadingService } from '@service-work/is-loading';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailService } from 'apps/transportation-hub/src/app/modules/shared/email.service';
import { Title } from '@angular/platform-browser';
import {
  navigateToPathFn,
  isTruthy,
  ngForTrackByID,
} from 'apps/transportation-hub/src/app/utilities';
import { isEqual } from '@local/isEqual';

@Component({
  selector: 'app-client-ride-requests',
  templateUrl: './client-ride-requests.component.html',
  styleUrls: ['./client-ride-requests.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientRideRequestsComponent implements OnInit {
  rideRequests: RideRequest[] = [];
  rideRequestsDate?: Date;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private isLoadingService: IsLoadingService,
    private router: Router,
    private route: ActivatedRoute,
    public emailService: EmailService,
    private titleService: Title,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getClient(id))))
        .subscribe((client) => {
          this.titleService.setTitle(
            `Client Ride Requests | ${client?.nameWithRetiredStatus} | Hub`,
          );
        }),
      this.routerStore
        .get$((store) => store.state.params.rideRequestsDate)
        .pipe(
          filter(isTruthy),
          map((timestamp: string) => new Date(parseInt(timestamp, 10))),
          tap((date) => {
            this.rideRequestsDate = new Date(date);
          }),
          switchMap((date) => {
            const obs = this.routerStore
              .get$((store) => store.state.params.clientId)
              .pipe(
                switchMap((clientId) =>
                  !clientId
                    ? of([])
                    : this.fs.getRideRequests({
                        clientId,
                        startAfterOrOnDate: startOfMonth(date),
                        endBeforeDate: endOfMonth(date),
                      }),
                ),
                switchMap((rideRequests) =>
                  rideRequests.length === 0
                    ? of([])
                    : combineLatest(
                        rideRequests.map((request) =>
                          request.driverId
                            ? this.fs.getDriver(request.driverId)
                            : of(null),
                        ),
                      ).pipe(
                        map((drivers) => {
                          rideRequests.forEach(
                            (request, index) =>
                              (request.driver = drivers[
                                index
                              ] as Driver | null),
                          );
                          return rideRequests;
                        }),
                      ),
                ),
                distinctUntilChanged(isEqual),
              );

            return this.isLoadingService.add(obs, {
              key: 'client-ride-requests',
            });
          }),
        )
        .subscribe((rideRequests) => {
          this.rideRequests = rideRequests;
          this.cdr.markForCheck();
        }),
      this.emailService.selectedRideRequests.subscribe(() => {
        this.cdr.detectChanges();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  nextDate() {
    if (!this.rideRequestsDate) return;

    const date = addMonths(startOfMonth(this.rideRequestsDate), 1);

    this.router.navigate(['../', date.valueOf()], {
      relativeTo: this.route,
      replaceUrl: true,
    });
  }

  prevDate() {
    if (!this.rideRequestsDate) return;

    const date = subMonths(startOfMonth(this.rideRequestsDate), 1);

    this.router.navigate(['../', date.valueOf()], {
      relativeTo: this.route,
      replaceUrl: true,
    });
  }

  openDateSelector(datepicker: MatDatepicker<Date>) {
    datepicker.opened ? datepicker.close() : datepicker.open();
  }

  dateSelected(event: MatDatepickerInputEvent<Date>) {
    if (!event.value) {
      return;
    }
    this.router.navigate(['../', event.value.valueOf()], {
      relativeTo: this.route,
    });
  }

  destinationSummary: (destinations: Destination[]) => string = (() => {
    const prevDestinations: Destination[] = [];
    let summary = '';

    return (destinations: Destination[]) => {
      if (
        prevDestinations.length === destinations.length &&
        prevDestinations[0] === destinations[0]
      ) {
        return summary;
      }

      summary = destinations[0].toString();

      if (destinations.length === 2) {
        summary = `${summary} and 1 other location`;
      } else if (destinations.length > 2) {
        summary = `${summary} and ${destinations.length - 1} other locations`;
      }

      return summary;
    };
  })();

  driverName(rideRequest: RideRequest) {
    if (rideRequest.requestCancelledAt) return 'CANCELLED';
    if (rideRequest.driver) return rideRequest.driver.nameWithRetiredStatus;
    return 'none';
  }
}
