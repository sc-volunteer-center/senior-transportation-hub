import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  TrackByFunction,
} from '@angular/core';
import { Client } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { EditClientContactsComponent } from '../edit-client-contacts/edit-client-contacts.component';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-client-contact',
  templateUrl: './client-contacts.component.html',
  styleUrls: ['./client-contacts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientContactsComponent implements OnInit {
  client: Client | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private dialog: MatDialog,
    private titleService: Title,
  ) {}

  trackByContact: TrackByFunction<Client['contacts'][number]> = (_, item) =>
    item.label + item.firstName + item.lastName + item.relationshipToPerson;

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getClient(id))))
        .subscribe((client) => {
          this.client = client;
          this.cdr.markForCheck();

          if (!this.client) return;

          this.titleService.setTitle(
            `Client Contacts | ${this.client.nameWithRetiredStatus} | Hub`,
          );
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editContacts() {
    this.dialog.open(EditClientContactsComponent, {
      width: '600px',
    });
  }
}
