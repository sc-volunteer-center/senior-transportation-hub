import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { Client } from '@local/models';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, NavigationEnd } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { timestamp } from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-retire-client',
  templateUrl: './retire-client.component.html',
  styleUrls: ['./retire-client.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RetireClientComponent implements OnInit {
  client: Client | null = null;
  retirementForm: FormGroup | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<RetireClientComponent>,
    private router: Router,
    private loadingService: IsLoadingService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    private data: { clientId: string; dispatcherId: string },
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.subscriptions.push(
      this.fs.getClient(this.data.clientId).subscribe((client) => {
        this.client = client;
        this.cdr.markForCheck();

        if (!client) {
          this.ref.close();
          return;
        }

        this.retirementForm = this.fb.group({
          retiredReason: ['', Validators.required],
        });
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (!this.retirementForm || this.retirementForm.invalid || !this.client) {
      return;
    }

    const form = this.retirementForm.value as {
      retiredReason: string;
    };

    await this.loadingService.add(
      Promise.all([
        this.fs.updateClient(this.client.uid, {
          ...form,
          retiredAt: timestamp(),
          retiredById: this.data.dispatcherId,
        }),
        this.fs.addThread({
          clientId: this.client.uid,
          flag: false,
          text: `CLIENT RETIRED:\n` + form.retiredReason,
        }),
      ]),
      { key: 'retire-client' },
    );

    this.ref.close();
  }
}
