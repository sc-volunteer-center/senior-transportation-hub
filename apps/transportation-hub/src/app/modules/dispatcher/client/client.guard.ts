import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClientGuard implements CanActivate {
  constructor(private fs: FirestoreService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!next.params['clientId']) {
      return false;
    }

    return this.fs
      .getClient(next.params['clientId'])
      .pipe(map(client => !!client));
  }
}
