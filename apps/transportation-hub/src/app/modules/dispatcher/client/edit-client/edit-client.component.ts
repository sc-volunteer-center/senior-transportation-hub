import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription, of, combineLatest } from 'rxjs';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { toPromise } from '../../../../utilities';
import { Question, Client, Tag } from '@local/models';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { switchMap, map, tap, take, startWith } from 'rxjs/operators';
import { WarningComponent } from '../../../shared/warning/warning.component';

interface DisplayedAnswer {
  question: Question;
  answer?: FormGroup;
}

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditClientComponent implements OnInit {
  client: Client | null = null;
  clientForm?: FormGroup;

  currentAnswers: DisplayedAnswer[] = [];
  oldAnswers: DisplayedAnswer[] = [];

  clientTags: Tag[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<EditClientComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private dialog: MatDialog,
  ) {}

  async ngOnInit() {
    this.clientTags = await toPromise(this.fs.getTags({ type: 'client' }));

    this.subscriptions.push(
      this.fs.getTags({ type: 'client' }).subscribe((tags) => {
        this.clientTags = tags;
        this.cdr.markForCheck();
      }),
    );

    this.client = await this.routerStore
      .get$((store) => store.state.params.clientId)
      .pipe(
        switchMap((id) => (!id ? of(null) : this.fs.getClient(id))),
        switchMap((client) =>
          !client
            ? of(null)
            : client.answers.length === 0
            ? of(client)
            : combineLatest(
                client.answers.map((answer) =>
                  this.fs.getClientQuestion(answer.questionId),
                ),
              ).pipe(
                map((questions) => {
                  questions.forEach(
                    (question, index) =>
                      (client.answers[index].question = question!),
                  );
                  return client;
                }),
              ),
        ),
        take(1),
      )
      .toPromise();

    const standardQuestions = await toPromise(this.fs.getClientQuestions());

    if (!this.client) return;

    const currentQuestionsForm = this.fb.array([]);

    standardQuestions.forEach((question) => {
      const answer = this.client!.answers.find(
        (answer) => answer.questionId === question.uid,
      );

      const form = this.fb.group({
        questionId: question.uid,
        answer: answer?.answer,
      });

      if (question.type !== 'section-break') {
        currentQuestionsForm.push(form);
      }

      this.currentAnswers.push({
        question,
        answer: form,
      });
    });

    const oldQuestionsForm = this.fb.array([]);

    this.client.answers.forEach((answer) => {
      if (answer.question!.type === 'section-break') {
        return;
      }

      if (
        this.currentAnswers.some(
          (currentAnswer) => currentAnswer.question!.uid === answer.questionId,
        )
      ) {
        return;
      }

      const form = this.fb.group({
        questionId: answer.questionId,
        answer: answer.answer,
      });

      oldQuestionsForm.push(form);

      this.oldAnswers.push({
        question: answer.question!,
        answer: form,
      });
    });

    this.clientForm = this.fb.group({
      nickname: [this.client.nickname],
      firstName: [this.client.firstName, Validators.required],
      middleName: [this.client.middleName],
      lastName: [this.client.lastName, Validators.required],
      phoneNumber: [this.client.phoneNumber, Validators.required],
      emailAddress: [this.client.emailAddress ?? '', Validators.email],
      address: this.fb.group({
        street: [this.client.address.street, Validators.required],
        apartmentNumber: [this.client.address.apartmentNumber],
        city: [this.client.address.city, Validators.required],
        zip: [this.client.address.zip, Validators.required],
        comments: [this.client.address.comments],
      }),
      mediCalOrSsi: [this.client.mediCalOrSsi, Validators.required],
      ethnicity: [this.client.ethnicity, Validators.required],
      birthdate: [this.client.birthdate, Validators.required],
      currentQuestions: currentQuestionsForm,
      oldQuestions: oldQuestionsForm,
      comments: this.client.comments,
      restrictions: this.client.restrictions,
      tagIds: [this.client.tagIds || []],
      defaultPeopleImpacted: [
        this.client.defaultPeopleImpacted || 1,
        [Validators.required, Validators.min(1)],
      ],
      defaultPeopleImpactedDescription: [
        {
          value: this.client.defaultPeopleImpactedDescription || '',
          disabled: this.client.defaultPeopleImpacted === 1,
        },
      ],
    });

    // prevent entering a number less than 1
    this.subscriptions.push(
      this.clientForm
        .get('defaultPeopleImpacted')!
        .valueChanges.pipe(startWith(this.client.defaultPeopleImpacted))
        .subscribe((value) => {
          if (value > 1) {
            this.clientForm!.get('defaultPeopleImpactedDescription')!.enable();
            return;
          } else if (value === 1) {
            this.clientForm!.get('defaultPeopleImpactedDescription')!.disable();
            return;
          }

          this.clientForm!.get('defaultPeopleImpacted')!.patchValue(1);
        }),
    );

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (!this.clientForm || this.clientForm.invalid) {
      return;
    }

    const form = this.clientForm.value;

    form.answers = form.currentQuestions.map(
      (question: { questionId: string; answer: any }) => ({
        ...question,
        updatedAt: new Date(),
      }),
    );

    form.answers.push(
      ...form.oldQuestions
        .filter(
          (question: { questionId: string; answer: any }) =>
            ![undefined, '', null].includes(question.answer),
        )
        .map((question: { questionId: string; answer: any }) => ({
          ...question,
          updatedAt: new Date(),
        })),
    );

    delete form.currentQuestions;
    delete form.oldQuestions;

    const id = await this.loadingService.add(
      this.fs.updateClient(this.client!.uid, form),
      { key: 'edit-client' },
    );

    this.ref.close(id);
  }
}
