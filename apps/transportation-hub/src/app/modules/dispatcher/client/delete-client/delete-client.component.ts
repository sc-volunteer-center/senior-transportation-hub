import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { Observable } from 'rxjs';
import { Dispatcher } from '@local/models';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { toPromise } from '../../../../utilities';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-delete-client',
  templateUrl: './delete-client.component.html',
  styleUrls: ['./delete-client.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteClientComponent implements OnInit {
  dispatcher: Observable<Dispatcher | null>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: { clientId: string },
    private fs: FirestoreService,
    private router: Router,
    private ref: MatDialogRef<DeleteClientComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.dispatcher = this.fs.getCurrentDispatcher();
  }

  async delete() {
    const client = await toPromise(this.fs.getClient(this.data.clientId));

    if (!this.fs.dispatcher || !client || !client.retiredAt) {
      return;
    }

    await this.loadingService.add(this.fs.destroyClient(this.data.clientId), {
      key: 'delete-client',
    });

    this.router.navigate(['/dispatcher/clients', new Date().valueOf(), 'week']);

    this.ref.close();
  }
}
