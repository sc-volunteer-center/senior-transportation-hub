import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Client } from '@local/models';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-edit-client-contacts',
  templateUrl: './edit-client-contacts.component.html',
  styleUrls: ['./edit-client-contacts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditClientContactsComponent implements OnInit {
  client: Client | null = null;
  clientContacts?: FormArray;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<EditClientContactsComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getClient(id))))
        .subscribe((client) => {
          this.client = client;
          this.clientContacts = undefined;
          this.cdr.markForCheck();

          if (!client) {
            return;
          }

          this.clientContacts = this.fb.array(
            client.contacts.map((contact) =>
              this.fb.group({
                label: [contact.label, Validators.required],
                firstName: [contact.firstName, Validators.required],
                lastName: [contact.lastName, Validators.required],
                phoneNumber: [contact.phoneNumber, Validators.required],
                relationshipToPerson: [
                  contact.relationshipToPerson,
                  Validators.required,
                ],
              }),
            ),
          );
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async submit() {
    if (!this.clientContacts || this.clientContacts.invalid) {
      return;
    }

    this.loadingService.add(
      this.fs
        .updateClient(this.client!.uid, {
          contacts: this.clientContacts.value,
        })
        .catch((e) => console.error(e)),
      { key: 'edit-client-contacts' },
    );

    this.ref.close();
  }

  addContact() {
    if (!this.clientContacts) return;

    this.clientContacts.push(
      this.fb.group({
        label: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phoneNumber: ['', Validators.required],
        relationshipToPerson: ['', Validators.required],
      }),
    );
  }

  removeContact(index: number) {
    if (!this.clientContacts) return;

    this.clientContacts.removeAt(index);
    this.cdr.markForCheck();
  }

  contactNumber(index: number) {
    switch (index) {
      case 0:
        return 'First';
      case 1:
        return 'Second';
      case 2:
        return 'Third';
      case 3:
        return 'Fourth';
      case 4:
        return 'Fifth';
      case 5:
        return 'Sixth';
      case 6:
        return 'Seventh';
      case 7:
        return 'Eighth';
      default:
        throw new Error('Too many client contacts');
    }
  }
}
