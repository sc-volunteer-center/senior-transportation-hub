import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Client } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';

@Component({
  selector: 'app-client-breadcrumb',
  template: ` Client - {{ client?.nameWithRetiredStatus }} `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientBreadcrumbComponent implements OnInit {
  client: Client | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(switchMap((id) => this.fs.getClient(id!)))
        .subscribe((client) => {
          this.client = client;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
