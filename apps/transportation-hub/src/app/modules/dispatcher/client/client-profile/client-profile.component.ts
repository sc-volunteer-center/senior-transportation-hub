import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  TrackByFunction,
} from '@angular/core';
import {
  Client,
  Question,
  Dispatcher,
  NoteThread,
  RecurringRideRequest,
  RideRequest,
} from '@local/models';
import { Subscription, of, combineLatest } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap, tap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { addWeeks, endOfDay, format, startOfDay } from 'date-fns';
import { EditClientComponent } from '../edit-client/edit-client.component';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { MapsService } from '../../../../maps.service';
import { IsLoadingService } from '@service-work/is-loading';
import { DeleteClientComponent } from '../delete-client/delete-client.component';
import { RetireClientComponent } from '../retire-client/retire-client.component';
import {
  EditNoteComponent,
  IEditNoteComponentData,
  IEditNoteOutput,
  IFlagNoteOutput,
  IDeleteNoteOutput,
  IReplyToNoteOutput,
  IEditNoteResponse,
} from '../../../shared/notes/edit-note/edit-note.component';
import { ngForTrackByID } from 'apps/transportation-hub/src/app/utilities';
import { WarningService } from '../../../shared/warning/warning.service';

interface DisplayedAnswer {
  question?: Question;
  answer?: any;
  renderedAnswer?: string;
}

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientProfileComponent implements OnInit, OnDestroy {
  client: Client | null = null;
  mapUrl = '';

  currentAnswers: DisplayedAnswer[] = [];
  oldAnswers: DisplayedAnswer[] = [];

  noteThreads: NoteThread[] = [];
  flaggedNoteThreads: NoteThread[] = [];

  upcomingRideRequests: RideRequest[] | null = null;
  activeRecurringRideRequests: RecurringRideRequest[] | null = null;
  previousRideRequest: RideRequest | null = null;
  nextRideRequest: RideRequest | null = null;

  private dispatcher: Dispatcher | null = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private dialog: MatDialog,
    private mapsService: MapsService,
    private loadingService: IsLoadingService,
    private titleService: Title,
    private warningService: WarningService,
  ) {}

  trackByID = ngForTrackByID;
  trackByQuestionID: TrackByFunction<DisplayedAnswer> = (index, item) =>
    item.question ? item.question.uid : index;

  ngOnInit() {
    // get client, set page title, and set client map location
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(
          switchMap((id) =>
            !id ? of(null) : this.fs.getClient(id, { full: true }),
          ),
          tap((client) => {
            if (client) {
              client.answers.forEach((answer) => {
                answer.renderedAnswer = this.renderAnswer(answer.answer);
              });
            }

            this.client = client;

            if (client) {
              this.mapUrl = this.mapsService.addressMapUrl(client.address);
            }
          }),
          switchMap(() => this.fs.getClientQuestions()),
        )
        .subscribe((standardQuestions) => {
          this.currentAnswers = [];
          this.oldAnswers = [];
          this.cdr.markForCheck();

          if (!this.client) {
            return;
          }

          standardQuestions.forEach((question) => {
            if (question.type === 'section-break') {
              return;
            }

            const answer = this.client!.answers.find(
              (ans) => ans.questionId === question.uid,
            );

            if (answer) {
              this.currentAnswers.push(answer);
              return;
            }

            this.currentAnswers.push({ question });
          });

          this.client.answers.forEach((answer) => {
            if (answer.question!.type === 'section-break') {
              return;
            }

            if (
              this.currentAnswers.some(
                (currentAnswer) =>
                  currentAnswer.question!.uid === answer.questionId,
              )
            ) {
              return;
            }

            this.oldAnswers.push(answer);
          });

          this.titleService.setTitle(
            `Client | ${this.client.nameWithRetiredStatus} | Hub`,
          );
        }),
    );

    // get notes and flagged notes
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(
          switchMap((clientId) =>
            !clientId
              ? of([])
              : this.loadingService.add(this.fs.getNoteThreads({ clientId }), {
                  key: 'client-note-threads',
                }),
          ),
        )
        .subscribe((threads) => {
          this.noteThreads = threads;
          this.flaggedNoteThreads = threads.filter((t) => t.flag);
          this.cdr.markForCheck();
        }),
    );

    // get active recurring ride requests
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.clientId)
        .pipe(
          switchMap((clientId) =>
            !clientId
              ? of([])
              : this.fs.getRecurringRideRequests({
                  clientId,
                  // startsBeforeOrOn: new Date(),
                  endAfterDate: startOfDay(new Date()),
                }),
          ),
        )
        .subscribe((rrr) => {
          this.activeRecurringRideRequests = rrr;
          this.cdr.markForCheck();
        }),
    );

    // get upcoming requests in the next 3 weeks
    this.subscriptions.push(
      combineLatest([
        this.fs.getRideRequests({
          startAfterOrOnDate: startOfDay(new Date()),
          endBeforeDate: addWeeks(endOfDay(new Date()), 3),
          requestIsCancelled: false,
          cache: { expirationTime: 1000 * 60 * 10 },
        }),
        this.routerStore.get$((store) => store.state.params.clientId),
      ]).subscribe(([rrr, clientId]) => {
        this.upcomingRideRequests = rrr.filter((r) => r.clientId === clientId);
        this.cdr.markForCheck();
      }),
    );

    // get next and previous non-cancelled requests
    this.subscriptions.push(
      this.routerStore
        .get$((s) => s.state.params.clientId)
        .pipe(
          switchMap((clientId) =>
            !clientId
              ? of([null, null])
              : this.loadingService.add(
                  combineLatest([
                    this.fs.getRideRequests({
                      clientId,
                      endBeforeDate: startOfDay(new Date()),
                      order: 'desc',
                      limit: 2,
                      requestIsCancelled: false,
                    }),
                    this.fs.getRideRequests({
                      clientId,
                      startAfterOrOnDate: startOfDay(new Date()),
                      limit: 1,
                      requestIsCancelled: false,
                    }),
                  ]),
                  {
                    key: 'next/previous requests',
                  },
                ),
          ),
        )
        .subscribe(([previous, next]) => {
          this.previousRideRequest = previous?.[0] || null;
          this.nextRideRequest = next?.[0] || null;

          if (
            this.previousRideRequest &&
            this.previousRideRequest.uid === this.nextRideRequest?.uid
          ) {
            this.previousRideRequest = previous![1] || null;
          }

          this.cdr.markForCheck();
        }),
    );

    // get current dispatcher
    this.subscriptions.push(
      this.fs.getCurrentDispatcher().subscribe((dispatcher) => {
        this.dispatcher = dispatcher;
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editClient() {
    this.dialog.open(EditClientComponent, {
      width: '600px',
      disableClose: true,
    });
  }

  async toggleRetireClient() {
    if (!this.client || !this.dispatcher) {
      return;
    }

    if (this.client.retiredAt) {
      await this.loadingService.add(
        Promise.all([
          this.fs.updateClient(this.client.uid, {
            retiredAt: null,
            retiredById: null,
            retiredReason: null,
          }),
          this.fs.addThread({
            clientId: this.client.uid,
            flag: false,
            text: `CLIENT UN-RETIRED`,
          }),
        ]),
      );

      await this.warningService
        .popup({
          title: 'Checklist',
          text: `After un-retiring a client, <strong>don't forget to also go over the participant onboarding script with them (to make sure they have the necessary, up-to-date knowledge) and also create a Google Contacts entry for them</strong> so that their name shows up next to their phone number when they call us.

        To do this, open up “Google Contacts” (<em>Important! Make sure you are using Google Contacts with the transportation google account and not another google account</em>). Add a contact entry for this client if one doesn’t already exist for them. Be sure the contact entry includes the client’s name, email address, and phone number. Last, apply the “GSP Client” label to the client’s contact entry if they are participating in the GSP program and add the “STP Client” label to their contact entry if they are participating in the STP program.`,
          affirmative: 'Gotcha',
        })
        .afterClosed()
        .toPromise();
    } else {
      this.retireClient();
    }
  }

  retireClient() {
    if (!this.client || !this.dispatcher) {
      return;
    }

    this.dialog
      .open(RetireClientComponent, {
        width: '600px',
        data: {
          clientId: this.client.uid,
          dispatcherId: this.dispatcher.uid,
        },
      })
      .afterClosed()
      .subscribe();
  }

  deleteClient() {
    if (!this.client || !this.dispatcher || !this.client.retiredAt) {
      return;
    }

    this.dialog.open(DeleteClientComponent, {
      width: '600px',
      closeOnNavigation: true,
      data: {
        clientId: this.client.uid,
      },
    });
  }

  renderAnswer(val?: string | boolean | Date | null) {
    if (val === null) {
      return '';
    } else if (typeof val === 'object') {
      return format(val, 'EEE, MMM M, YYYY');
    } else if (typeof val === 'boolean') {
      return val ? 'yes' : 'no';
    }

    return `${val}`;
  }

  renderNote(text: string) {
    return text.replaceAll('\n', '<br>');
  }

  async replyToFlag(thread: NoteThread) {
    const note = thread.notes[thread.notes.length - 1];

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            reply: note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.reply) return;

    return this.replyToNote(result.reply);
  }

  async addNote() {
    if (!this.client?.uid) return;

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.new) return;

    return this.loadingService.add(
      this.fs.addThread({
        clientId: this.client.uid,
        flag: result.new.flaggedAsImportant,
        text: result.new.text,
      }),
    );
  }

  replyToNote(args: IReplyToNoteOutput) {
    if (!this.client?.uid) return;

    return this.loadingService.add(this.fs.addNote(args));
  }

  editNote(args: IEditNoteOutput) {
    if (!this.client?.uid) return;

    return this.loadingService.add(this.fs.updateNote(args));
  }

  deleteNote(args: IDeleteNoteOutput) {
    if (!this.client?.uid) return;

    if (args.deleteThread) {
      return this.loadingService.add(this.fs.deleteThread(args));
    }

    return this.loadingService.add(this.fs.deleteNote(args));
  }

  flagNote(args: Omit<IFlagNoteOutput, 'noteId'>) {
    if (!this.client?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }

  suppressFlagTodo(args: {
    noteThreadId: string;
    flag: boolean;
    suppressTodo: boolean;
  }) {
    if (!this.client?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }
}
