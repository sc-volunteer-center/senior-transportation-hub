import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../../../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { Subscription } from 'rxjs';
import { toObservable, toPromise } from '../../../utilities';
import { Router } from '@angular/router';
import { Tag } from '@local/models';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddDriverComponent implements OnInit {
  driverForm: FormGroup;
  driverTags: Tag[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private firestore: FirestoreService,
    private ref: MatDialogRef<AddDriverComponent>,
    private loadingService: IsLoadingService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    this.driverTags = await toPromise(
      this.firestore.getTags({ type: 'driver' }),
    );

    this.subscriptions.push(
      this.firestore.getTags({ type: 'driver' }).subscribe((tags) => {
        this.driverTags = tags;
        this.cdr.markForCheck();
      }),
    );

    this.driverForm = this.fb.group({
      nickname: [''],
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      emailAddress: ['', [Validators.email]],
      ethnicity: ['Decline to state', Validators.required],
      birthdate: [null, Validators.required],
      genderIdentity: '',
      address: this.fb.group({
        street: ['', Validators.required],
        apartmentNumber: [''],
        city: ['', Validators.required],
        zip: ['', Validators.required],
        comments: [''],
      }),
      comments: '',
      driversLicenseExpirationDate: ['', Validators.required],
      carInsuranceExpirationDate: ['', Validators.required],
      blacklistedClientIds: [[]],
      availability: this.fb.group({
        sun: false,
        mon: false,
        tue: false,
        wed: false,
        thu: false,
        fri: false,
        sat: false,
      }),
      carInfo: [''],
      tagIds: [[]],
    });

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (this.driverForm.invalid) {
      return;
    }

    const id = await this.loadingService.add(
      this.firestore.addDriver(this.driverForm.value),
      { key: 'add-driver' },
    );

    this.ref.close(id);
  }
}
