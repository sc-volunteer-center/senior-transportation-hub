import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-shared-notes-breadcrumb',
  template: `
    Shared Notes
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedNotesBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
