import { Component, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-which-ride-request',
  template: `
    <div mat-dialog-title>
      <h3 style="margin-top: 0;">Edit recurring request</h3>
    </div>

    <mat-dialog-content style="height: 75px;">
      <mat-radio-group
        [formControl]="form"
        (change)="submit()"
        class="flex-column"
        color="primary"
      >
        <mat-radio-button value="ONCE" style="margin-bottom: 1rem">
          Just this instance
        </mat-radio-button>

        <mat-radio-button
          value="RECURRING"
          [disabled]="data.inPast"
          matTooltip="Can only edit instances of recurring requests which haven't happened yet"
          [matTooltipDisabled]="!data.inPast"
        >
          This and all future instances of the request
        </mat-radio-button>
      </mat-radio-group>
    </mat-dialog-content>
  `,
  styles: [''],
})
export class WhichRideRequestComponent {
  form = new FormControl('', Validators.required);

  constructor(
    private ref: MatDialogRef<WhichRideRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { inPast: boolean },
  ) {}

  submit() {
    if (!this.form.valid) return;
    this.ref.close(this.form.value);
  }
}
