import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RideRequestGuard implements CanActivate {
  constructor(private fs: FirestoreService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!next.params['rideRequestId']) {
      return false;
    }

    return this.fs
      .getRideRequest(next.params['rideRequestId'])
      .pipe(map(rideRequest => !!rideRequest));
  }
}
