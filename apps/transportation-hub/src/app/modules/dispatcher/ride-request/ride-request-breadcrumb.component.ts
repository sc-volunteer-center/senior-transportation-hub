import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { RideRequest } from '@local/models';
import { switchMap, tap, filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { isTruthy } from '../../../utilities';

@Component({
  selector: 'app-ride-request-breadcrumb',
  template: `
    {{ rideRequest?.category }} Request -
    {{ rideRequest?.client.nameWithRetiredStatus }}
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideRequestBreadcrumbComponent implements OnInit {
  rideRequest: RideRequest | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.rideRequestId)
        .pipe(
          filter(isTruthy),
          tap(() => {
            this.rideRequest = null;
            this.cdr.markForCheck();
          }),
          switchMap((id) => this.fs.getRideRequest(id)),
        )
        .subscribe((rideRequest) => {
          this.rideRequest = rideRequest;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  printDate(date?: Date) {
    if (!date) {
      return '';
    }

    return `(${date.getMonth()}/${date.getDate()}/${date.getFullYear()})`;
  }
}
