import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  TrackByFunction,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import {
  Driver,
  RideRequest,
  Dispatcher,
  Tag,
  NoteThread,
} from '@local/models';
import { Subscription, of, combineLatest, BehaviorSubject } from 'rxjs';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import {
  switchMap,
  tap,
  filter,
  take,
  startWith,
  debounceTime,
  map,
  pairwise,
  distinctUntilChanged,
} from 'rxjs/operators';
import { MapsService } from '../../../../maps.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import {
  format,
  addHours,
  subHours,
  subWeeks,
  addWeeks,
  isBefore,
  subDays,
  startOfDay,
  endOfDay,
} from 'date-fns';
import { Router, NavigationEnd } from '@angular/router';
import { stringComparer, toPromise } from '../../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { WarningComponent } from '../../../shared/warning/warning.component';
import { FormControl } from '@angular/forms';
import {
  EditNoteComponent,
  IEditNoteComponentData,
  IEditNoteResponse,
  IReplyToNoteOutput,
  IEditNoteOutput,
  IDeleteNoteOutput,
  IFlagNoteOutput,
} from '../../../shared/notes/edit-note/edit-note.component';
import { WhichRideRequestComponent } from '../which-ride-request.component';
import { ISimpleTag } from '../../../shared/display-tags/display-tags.component';
import { isEqual } from '@local/isEqual';

interface RideRequestContactInfo {
  driver: Driver;
  distance?: number;
  accepted: boolean;
  availability: boolean;
  otherTags: Array<{
    label: string;
    description: string;
    color: string;
  }>;
}

@Component({
  selector: 'app-find-driver-for-ride-request',
  templateUrl: './find-driver-for-ride-request.component.html',
  styleUrls: ['./find-driver-for-ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FindDriverForRideRequestComponent implements OnInit {
  rideRequest: RideRequest | null = null;
  closeRideRequests: RideRequest[] = [];
  rideRequestContactInfo: RideRequestContactInfo[] = [];
  drivers: Driver[] = [];
  mapUrl = '';

  filteredRideRequestContactInfo: RideRequestContactInfo[] = [];
  filterText = '';
  searchControl = new FormControl('');

  driverTags: Tag[] = [];
  stpTag: Tag | null = null;
  gspTag: Tag | null = null;
  tagsControl = new FormControl([]);
  filterTags: string[] = [];

  get selectedDriver() {
    return this.selectedDriver$.value;
  }

  selectedDriver$ = new BehaviorSubject<Driver | null>(null);

  noteThreads: NoteThread[] = [];

  driverNoteThreads: NoteThread[] = [];
  driverNoteThreadsSub: Subscription | undefined;

  format = format;
  trackBy: TrackByFunction<RideRequestContactInfo> = (_, item) =>
    item.driver.uid;

  private dispatcher: Dispatcher | null = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private mapsService: MapsService,
    private ref: MatDialogRef<FindDriverForRideRequestComponent>,
    private router: Router,
    private dialog: MatDialog,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.fs.getTags({ type: 'driver' }).subscribe((tags) => {
        this.driverTags = tags;
        this.stpTag = tags.find((t) => t.label === 'STP') || null;
        this.gspTag = tags.find((t) => t.label === 'GSP') || null;
        this.cdr.markForCheck();
      }),
    );

    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
      this.routerStore
        .get$((store) => store.state.params.rideRequestId)
        .pipe(
          switchMap((rideRequestId) =>
            !rideRequestId ? of([]) : this.fs.getNoteThreads({ rideRequestId }),
          ),
        )
        .subscribe((threads) => {
          this.noteThreads = threads;
          this.cdr.markForCheck();
        }),
    );

    this.subscriptions.push(
      combineLatest([
        this.searchControl.valueChanges.pipe(
          startWith(this.searchControl.value),
        ),
        this.tagsControl.valueChanges.pipe(startWith(this.tagsControl.value)),
      ]).subscribe(([search, tags]) => {
        this.filterText = search.toLowerCase();
        this.filterTags = tags;
        this.filterRideRequestContactInfo();
      }),
    );

    const sub1 = combineLatest([
      this.fs.getCurrentDispatcher(),
      this.routerStore
        .get$((store) => store.state.params.rideRequestId)
        .pipe(
          switchMap((id) =>
            !id
              ? of(null)
              : this.fs.getRideRequest(id, { recurringRideRequest: true }),
          ),
          switchMap((rr) =>
            !rr
              ? of([])
              : this.fs.getRideRequests({
                  startAfterOrOnDate: startOfDay(rr.datetime),
                  endBeforeDate: addWeeks(endOfDay(rr.datetime), 2),
                  driverIdIsNotNull: true,
                }),
          ),
        ),
      combineLatest([
        this.routerStore
          .get$((store) => store.state.params.rideRequestId)
          .pipe(
            switchMap((id) =>
              !id
                ? of(null)
                : this.fs.getRideRequest(id, { recurringRideRequest: true }),
            ),
          ),
        this.fs.getDrivers({ retired: false }),
      ]).pipe(
        switchMap(([rr, drivers]) =>
          !rr?.driverId
            ? of([rr, drivers] as const)
            : this.fs.getDriver(rr.driverId).pipe(
                map((driver) => {
                  if (!driver) return [rr, drivers] as const;

                  const store = new Map(
                    drivers.map((d) => [d.uid, d] as const),
                  );

                  store.set(driver.uid, driver);

                  return [rr, Array.from(store.values())] as const;
                }),
              ),
        ),
      ),
    ])
      .pipe(
        startWith([null, [] as RideRequest[], [null, [] as Driver[]]] as const),
        pairwise(),
        distinctUntilChanged(isEqual),
      )
      .subscribe(([prev, curr]) => {
        const [
          prevDispatcher,
          prevRideRequests,
          [prevRideRequest, prevDrivers],
        ] = prev;

        const [dispatcher, rideRequests, [rideRequest, drivers]] = curr;

        if (!isEqual(prevDispatcher, dispatcher)) {
          this.dispatcher = dispatcher;
        }

        if (!isEqual(prevRideRequests, rideRequests)) {
          this.closeRideRequests = rideRequests;
        }

        if (!isEqual(prevRideRequest, rideRequest)) {
          this.rideRequest = rideRequest;
        }

        if (
          !isEqual(prevRideRequest, rideRequest) ||
          !isEqual(prevDrivers, drivers)
        ) {
          if (!rideRequest) {
            this.drivers = [];
          } else {
            this.drivers = drivers.filter(
              (driver) =>
                rideRequest!.driverId === driver.uid ||
                (!driver.isOnVacation(rideRequest && rideRequest.datetime) &&
                  !driver.blacklistedClientIds.includes(rideRequest!.clientId)),
            );
          }
        }

        if (
          !isEqual(prevRideRequests, rideRequests) ||
          !isEqual(prevRideRequest, rideRequest) ||
          !isEqual(prevDrivers, drivers)
        ) {
          if (!rideRequest) {
            this.rideRequestContactInfo = [];
          } else {
            this.rideRequestContactInfo = this.drivers
              .map((driver) => {
                const distance = this.tripDistance(driver);

                const closeRequestsCount = this.closeRideRequests.filter(
                  (r) => r.driverId === driver.uid,
                ).length;

                return {
                  driver,
                  distance: distance && parseFloat(distance.toFixed(1)),
                  accepted:
                    driver.uid ===
                    (this.rideRequest && this.rideRequest.driverId),
                  availability: driver.availableOn(this.rideRequest!.datetime),
                  otherTags: this.otherDriverTags(driver, {
                    closeRequestsCount,
                  }),
                };
              })
              .sort((a, b) => {
                if (a.accepted) {
                  return -1;
                } else if (b.accepted) {
                  return 1;
                  // disabling this sorting because it isn't applicable to GSP
                  // } else if (
                  //   a.driver.availableOn(this.rideRequest!.datetime) &&
                  //   !b.driver.availableOn(this.rideRequest!.datetime)
                  // ) {
                  //   return -1;
                  // } else if (
                  //   !a.driver.availableOn(this.rideRequest!.datetime) &&
                  //   b.driver.availableOn(this.rideRequest!.datetime)
                  // ) {
                  //   return 1;
                } else if (!a.distance && b.distance) {
                  return 1;
                } else if (a.distance && !b.distance) {
                  return -1;
                } else if (!a.distance && !b.distance) {
                  return stringComparer(a.driver.uid, b.driver.uid);
                } else if (a.distance! > b.distance!) {
                  return 1;
                } else if (a.distance! < b.distance!) {
                  return -1;
                } else {
                  return stringComparer(a.driver.uid, b.driver.uid);
                }
              });
          }
        }

        this.filterRideRequestContactInfo();
      });

    this.subscriptions.push(sub1);

    this.subscriptions.push(
      this.selectedDriver$
        .pipe(
          filter((d) => !!d),
          debounceTime(10),
          tap((driver) => {
            this.mapUrl = this.buildTripUrl(driver!);
            this.cdr.markForCheck();
          }),
          switchMap((driver) =>
            this.loadingService.add(
              this.fs.getNoteThreads({
                driverId: driver!.uid,
              }),
              {
                key: 'driver-note-threads',
              },
            ),
          ),
        )
        .subscribe((threads) => {
          this.driverNoteThreads = threads;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
    if (this.driverNoteThreadsSub) this.driverNoteThreadsSub.unsubscribe();
  }

  close() {
    this.ref.close();
  }

  filterRideRequestContactInfo() {
    const searchText = this.filterText;
    const tags = this.filterTags;

    if (!this.rideRequest) return;

    if (!this.gspTag) {
      console.warn('Could not find the "GSP" tag');
      return;
    }

    if (!this.stpTag) {
      console.warn('Could not find the "STP" tag');
      return;
    }

    this.filteredRideRequestContactInfo = this.rideRequestContactInfo.filter(
      (info) => {
        if (this.rideRequest!.category === 'GSP') {
          if (!info.driver.tagIds.includes(this.gspTag!.uid)) return false;
        } else if (this.rideRequest!.category === 'STP') {
          if (!info.driver.tagIds.includes(this.stpTag!.uid)) return false;
        } else {
          throw new Error('Only expected "GSP"/"STP" categories');
        }

        if (
          tags.length > 0 &&
          !info.driver.tagIds.some((id) => tags.includes(id))
        ) {
          return false;
        }

        if (
          info.driver.nickname &&
          info.driver.nameSecondaryIndex.toLowerCase().includes(searchText)
        ) {
          return true;
        }

        return info.driver.name.toLowerCase().includes(searchText);
      },
    );

    this.cdr.markForCheck();
  }

  tripDistance(driver: Driver) {
    if (!this.rideRequest) {
      return;
    }

    return this.mapsService.birdsDistance([
      driver.address,
      this.rideRequest.pickupAddress,
    ]);
  }

  buildTripUrl(driver: Driver) {
    if (!this.rideRequest) {
      return '';
    }

    return this.mapsService.directionsMapUrl([
      driver.address,
      this.rideRequest.pickupAddress,
      ...this.rideRequest.destinationAddresses,
    ]);
  }

  async assignDriver() {
    if (!this.rideRequest || !this.selectedDriver) {
      return;
    }

    const requestType = await this.editWhichRideRequest('assignDriver');

    if (!requestType) return;

    if (requestType === 'ONCE') {
      const otherRideRequestsForDriver = await this.loadingService.add(
        toPromise(
          this.fs.getRideRequests({
            driverId: this.selectedDriver.uid,
            startAfterOrOnDate: subHours(this.rideRequest.datetime, 2),
            endBeforeDate: addHours(this.rideRequest.datetime, 2),
            requestCancelledAt: null,
            isFlexible: false,
          }),
        ),
      );

      if (otherRideRequestsForDriver.length > 0) {
        const plural = otherRideRequestsForDriver.length === 1 ? '' : 's';

        const proceed = await this.dialog
          .open(WarningComponent, {
            data: {
              title: 'Heads up!',
              text: `This volunteer is already assigned to
            ${otherRideRequestsForDriver.length}
            request${plural} within two hours of this one.
            Are you sure you want to assign them to this ride request?`,
              affirmative: 'Yes',
            },
          })
          .afterClosed()
          .toPromise();

        if (!proceed) return;
      }
    }

    if (this.rideRequest.driverId) {
      // If there is currently someone else assigned to this request, first
      // create a note indicating that they are being unassigned
      await this.loadingService.add(
        this.fs.addThread({
          rideRequestId: this.rideRequest.uid,
          driverId: this.rideRequest.driverId,
          flag: false,
          text: `UN-ASSIGNED VOLUNTEER`,
        }),
      );
    }

    await this.loadingService.add(
      Promise.all([
        this.fs.assignDriver(
          this.rideRequest.uid,
          this.selectedDriver.uid,
          requestType,
        ),
        this.fs.addThread({
          rideRequestId: this.rideRequest.uid,
          driverId: this.selectedDriver.uid,
          flag: false,
          text: `ASSIGNED VOLUNTEER`,
        }),
      ]),
    );

    this.ref.close();
  }

  async unassignDriver() {
    if (!this.rideRequest) {
      return;
    }

    const requestType = await this.editWhichRideRequest('unassignDriver');

    if (!requestType) return;

    await this.loadingService.add(
      Promise.all([
        this.fs.addThread({
          rideRequestId: this.rideRequest.uid,
          driverId: this.rideRequest.driverId!,
          flag: false,
          text: `UN-ASSIGNED VOLUNTEER`,
        }),
        this.fs.unassignDriver(this.rideRequest.uid, requestType),
      ]),
    );

    this.ref.close();
  }

  async replyToFlag(thread: NoteThread) {
    const note = thread.notes[thread.notes.length - 1];

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            reply: note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.reply) return;

    return this.replyToNote(result.reply);
  }

  async addNote(driverId: string) {
    if (!this.rideRequest?.uid) return;

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.new) return;

    return this.loadingService.add(
      this.fs.addThread({
        driverId,
        rideRequestId: this.rideRequest.uid,
        flag: result.new.flaggedAsImportant,
        text: result.new.text,
      }),
    );
  }

  replyToNote(args: IReplyToNoteOutput) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.addNote(args));
  }

  editNote(args: IEditNoteOutput) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.updateNote(args));
  }

  deleteNote(args: IDeleteNoteOutput) {
    if (!this.rideRequest?.uid) return;

    if (args.deleteThread) {
      return this.loadingService.add(this.fs.deleteThread(args));
    }

    return this.loadingService.add(this.fs.deleteNote(args));
  }

  flagNote(args: IFlagNoteOutput) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }

  handleExpansionClick(driver: Driver) {
    if (this.selectedDriver?.uid === driver.uid) {
      this.selectedDriver$.next(null);
    } else {
      this.selectedDriver$.next(driver);
    }

    this.cdr.markForCheck();
  }

  private async editWhichRideRequest(
    changeType: 'assignDriver' | 'unassignDriver',
  ): Promise<'ONCE' | 'RECURRING' | undefined> {
    if (!this.rideRequest?.recurringRideRequest) return 'ONCE';

    const r = this.rideRequest;
    const rr = this.rideRequest.recurringRideRequest;

    if (changeType === 'unassignDriver' && r.driverId !== rr.driverId) {
      // If the current ride request instance has been edited to be different
      // then the parent recurring request, then these operations should only
      // be applied to this specific instance
      return 'ONCE';
    }

    return this.dialog
      .open(WhichRideRequestComponent, {
        width: '600px',
        data: {
          inPast: isBefore(
            this.rideRequest.datetime,
            subDays(startOfDay(new Date()), 4),
          ),
        },
      })
      .afterClosed()
      .toPromise();
  }

  private otherDriverTags(
    _driver: Driver,
    options: {
      closeRequestsCount: number;
    },
  ) {
    const tags: ISimpleTag[] = [];

    if (options.closeRequestsCount > 0) {
      tags.push({
        label: options.closeRequestsCount.toString(),
        description: `Has ${options.closeRequestsCount} other request${
          options.closeRequestsCount === 1 ? '' : 's'
        } in the next 2 weeks.`,
        color: 'Gray',
      });
    }

    return tags;
  }
}
