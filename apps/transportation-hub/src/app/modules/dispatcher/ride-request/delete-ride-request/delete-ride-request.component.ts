import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ChangeDetectorRef,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { Observable, Subscription } from 'rxjs';
import { Dispatcher } from '@local/models';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { toObservable, toPromise } from '../../../../utilities';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-delete-ride-request',
  templateUrl: './delete-ride-request.component.html',
  styleUrls: ['./delete-ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteRideRequestComponent implements OnInit {
  dispatcher: Observable<Dispatcher | null>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: { rideRequestId: string },
    private fs: FirestoreService,
    private router: Router,
    private cdk: ChangeDetectorRef,
    private ref: MatDialogRef<DeleteRideRequestComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.dispatcher = this.fs.getCurrentDispatcher();
  }

  async delete() {
    const rideRequest = await toPromise(
      this.fs.getRideRequest(this.data.rideRequestId),
    );

    if (
      !this.fs.dispatcher ||
      !rideRequest ||
      !rideRequest.requestCancelledAt
    ) {
      return;
    }

    await this.loadingService.add(
      this.fs.destroyRideRequest(this.data.rideRequestId),
      { key: 'delete-ride-request' },
    );

    this.router.navigate([
      '/dispatcher/ride-requests',
      new Date().valueOf(),
      'week',
    ]);

    this.ref.close();
  }
}
