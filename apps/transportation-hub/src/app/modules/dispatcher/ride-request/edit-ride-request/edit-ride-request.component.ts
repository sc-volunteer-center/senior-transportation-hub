import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
} from '@angular/core';
import { RideRequest, IAddress, Destination } from '@local/models';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap, take, startWith } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IsLoadingService } from '@service-work/is-loading';
import { Router } from '@angular/router';
import {
  MatCalendar,
  MAT_DATE_RANGE_SELECTION_STRATEGY,
} from '@angular/material/datepicker';
import { isSameDay } from 'date-fns';
import { wait } from '@local/utilities';
import {
  endOfWeek,
  WeekRangeSelectionStrategy,
} from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-edit-ride-request',
  templateUrl: './edit-ride-request.component.html',
  styleUrls: ['./edit-ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: WeekRangeSelectionStrategy,
    },
  ],
})
export class EditRideRequestComponent implements OnInit {
  rideRequest: RideRequest | null = null;
  rideRequestForm?: FormGroup;
  rideRequestDatetimeForm?: FormGroup;
  rideRequestRecurringScheduleForm?: FormGroup;

  pickupAddressChooser: FormControl;
  pickupAddress: FormGroup;

  destinationAddresses: FormArray;

  otherDestinationAddresses: (null | FormGroup)[] = [
    null,
    null,
    null,
    null,
    null,
    null,
  ];

  favoriteDestinations: Destination[] = [];
  filteredFavoriteDestinations: Destination[][] = [[], [], [], [], [], []];

  isDuplicate = false;

  // just used to properly display the week of a one-time flexible request
  endOfFlexibleRequestControl = new FormControl(null);

  selectedDupDates: Date[] = [];
  isDupDateSelected = (date: Date) => {
    if (isSameDay(date, this.rideRequest!.datetime)) return 'dup-start-date';

    return this.selectedDupDates.some((d) => d.valueOf() === date.valueOf())
      ? 'dup-date-selected'
      : null;
  };
  dupDateFilter = (date: Date) => !isSameDay(date, this.rideRequest!.datetime);

  private subscriptions: Subscription[] = [];
  private currentFavoriteDestinationsFilter: string[] = [
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  destinationAddressDisplayFn(destination?: Destination | string): string {
    if (!destination) {
      return '';
    }

    switch (typeof destination) {
      case 'string':
        return 'A custom address';
      case 'object':
        return (destination as Destination).label!;
      default:
        return '';
    }
  }

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private loadingService: IsLoadingService,
    private ref: MatDialogRef<EditRideRequestComponent>,
    private router: Router,
    private matSnackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      requestType: 'ONCE' | 'RECURRING';
      duplicate: boolean;
      overviewEl?: HTMLElement;
    },
  ) {}

  async ngOnInit() {
    this.isDuplicate = !!this.data.duplicate;

    const rideRequest = await this.routerStore
      .get$((store) => store.state.params.rideRequestId)
      .pipe(
        switchMap((id) =>
          !id
            ? of(null)
            : this.fs.getRideRequest(id, { recurringRideRequest: true }),
        ),
        take(1),
      )
      .toPromise();

    if (rideRequest?.flexible) {
      this.endOfFlexibleRequestControl.setValue(
        endOfWeek(rideRequest.datetime),
      );
    }

    this.favoriteDestinations = await this.fs
      .getDestinations()
      .pipe(take(1))
      .toPromise();

    this.rideRequest = rideRequest;
    this.rideRequestForm = undefined;
    this.cdr.markForCheck();

    if (!rideRequest) return;

    const pickupAddress = rideRequest.pickupAddress;
    const clientAddress = rideRequest.client.address;

    this.pickupAddressChooser = this.fb.control(
      areAddressesEqual(pickupAddress, clientAddress) ? 'standard' : 'other',
      Validators.required,
    );

    this.pickupAddress = this.fb.group({
      label: [pickupAddress.label],
      street: [pickupAddress.street, Validators.required],
      apartmentNumber: [pickupAddress.apartmentNumber],
      city: [pickupAddress.city, Validators.required],
      zip: [pickupAddress.zip, Validators.required],
      comments: [pickupAddress.comments],
    });

    const addressValidator: ValidatorFn = (control) => {
      if (typeof control.value === 'object' || control.value === 'other') {
        return null;
      }

      return {
        invalidAddress: 'invalid address',
      };
    };

    this.destinationAddresses = this.fb.array(
      rideRequest.destinationAddresses.map((a) => {
        const favoriteDestination = this.favoriteDestinations.find(
          (d) => d.label === a.label && areAddressesEqual(d, a),
        );

        return this.fb.group({
          address: [
            favoriteDestination || 'other',
            [Validators.required, addressValidator],
          ],
          tripPurpose: [a.tripPurpose, Validators.required],
        });
      }),
    );

    this.otherDestinationAddresses = rideRequest.destinationAddresses.map(
      (a) => {
        const favoriteDestination = this.favoriteDestinations.find(
          (d) => d.label === a.label && areAddressesEqual(d, a),
        );

        console.log('favoriteDest', favoriteDestination);

        return favoriteDestination
          ? null
          : this.fb.group({
              label: [a.label],
              street: [a.street, Validators.required],
              apartmentNumber: [a.apartmentNumber],
              city: [a.city, Validators.required],
              zip: [a.zip, Validators.required],
              comments: [a.comments],
            });
      },
    );

    this.subscriptions.push(
      this.destinationAddresses.valueChanges.subscribe(
        (
          values: Array<{
            address: string | Destination;
            tripPurpose: string;
          }>,
        ) => {
          values.forEach((value, index) => {
            if (!value.address) {
              this.otherDestinationAddresses[index] = null;
            } else if (value.address === 'other') {
              if (!this.otherDestinationAddresses[index]) {
                this.otherDestinationAddresses[index] = this.fb.group({
                  label: [''],
                  street: ['', Validators.required],
                  apartmentNumber: [''],
                  city: ['', Validators.required],
                  zip: ['', Validators.required],
                  comments: [''],
                });
              }
            } else if (this.otherDestinationAddresses[index]) {
              this.otherDestinationAddresses[index] = null;
            }
          });

          this.cdr.markForCheck();
        },
      ),
    );

    this.rideRequestForm = this.fb.group({
      flexible: [
        { value: rideRequest.flexible, disabled: true },
        Validators.required,
      ],
      flexibleSchedule: [
        {
          value: rideRequest.flexibleSchedule,
          disabled: !rideRequest.flexible,
        },
        Validators.required,
      ],
      requestMadeBy: [rideRequest.requestMadeBy, Validators.required],
      requestType: this.data.requestType,
      priority: [rideRequest.priority, Validators.required],
      comments: [rideRequest.comments],
      peopleImpacted: [
        rideRequest.peopleImpacted,
        [Validators.required, Validators.min(1)],
      ],
      peopleImpactedDescription: [
        { value: rideRequest.peopleImpactedDescription, disabled: true },
      ],
    });

    if (this.data.requestType === 'RECURRING') {
      const r = rideRequest.recurringRideRequest!;

      this.rideRequestRecurringScheduleForm = this.fb.group({
        date: [
          { value: rideRequest.datetime, disabled: true },
          Validators.required,
        ],
        time: [r.time, Validators.required],
        scheduleName: [
          {
            value: r.scheduleName,
            disabled: true,
          },
          Validators.required,
        ],
        endDate: r.endDate,
      });
    } else {
      this.rideRequestDatetimeForm = this.fb.group({
        date: [rideRequest.datetime, Validators.required],
        time: [
          normalizeTime(rideRequest.datetime.getHours()) +
            ':' +
            normalizeTime(rideRequest.datetime.getMinutes()),
          Validators.required,
        ],
      });
    }

    // prevent entering a number less than 1
    this.subscriptions.push(
      this.rideRequestForm
        .get('peopleImpacted')!
        .valueChanges.pipe(startWith(rideRequest.peopleImpacted))
        .subscribe((value) => {
          if (value > 1) {
            this.rideRequestForm!.get('peopleImpactedDescription')!.enable();
            return;
          } else if (value === 1) {
            this.rideRequestForm!.get('peopleImpactedDescription')!.disable();
            return;
          }

          this.rideRequestForm!.get('peopleImpacted')!.patchValue(1);
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  filterFavoriteDestinations(input: { value: string }, index: number): void;
  filterFavoriteDestinations(): void;
  filterFavoriteDestinations(input?: { value: string }, index?: number) {
    if (index !== undefined && input) {
      this.currentFavoriteDestinationsFilter[index] = input.value.toLowerCase();

      this.filteredFavoriteDestinations[index] =
        this.favoriteDestinations.filter((destination) =>
          `${destination.label} ${destination.street}`
            .toLowerCase()
            .includes(this.currentFavoriteDestinationsFilter[index]),
        );
    } else {
      for (const index of [0, 1, 2, 3, 4, 5]) {
        this.filteredFavoriteDestinations[index] =
          this.favoriteDestinations.filter((destination) =>
            `${destination.label} ${destination.street}`
              .toLowerCase()
              .includes(this.currentFavoriteDestinationsFilter[index]),
          );
      }
    }

    this.cdr.markForCheck();
  }

  destinationAddressInputBlurred(index: number) {
    const control = this.destinationAddresses.at(index) as FormControl;
    const value = control.value;

    if (typeof value === 'string' && value !== 'other') {
      control.patchValue('');
      this.filterFavoriteDestinations({ value: '' }, index);
    }
  }

  addDestination() {
    this.destinationAddresses.push(
      this.fb.group({
        address: ['', Validators.required],
        tripPurpose: ['', Validators.required],
      }),
    );
  }

  removeDestination(index: number) {
    this.otherDestinationAddresses[index] = null;
    this.destinationAddresses.removeAt(index);
    this.cdr.markForCheck();
  }

  addressNumber(index: number) {
    switch (index) {
      case 0:
        return 'First';
      case 1:
        return 'Second';
      case 2:
        return 'Third';
      case 3:
        return 'Fourth';
      case 4:
        return 'Fifth';
      case 5:
        return 'Sixth';
      default:
        throw new Error('Too many destination addresses');
    }
  }

  async submit() {
    if (this.invalid) return;

    const baseForm = this.rideRequestForm!.value;

    if (this.pickupAddressChooser.value === 'standard') {
      const address = this.rideRequest!.client.address;

      this.pickupAddress.setValue({
        label: "Client's standard address",
        street: address.street,
        apartmentNumber: address.apartmentNumber,
        city: address.city,
        zip: address.zip,
        comments: address.comments,
      });
    }

    const form =
      this.data.requestType === 'ONCE'
        ? {
            ...baseForm,
            ...this.rideRequestDatetimeForm!.value,
          }
        : {
            ...baseForm,
            ...this.rideRequestRecurringScheduleForm!.value,
          };

    const destinationAddresses: {
      label: string | null;
      street: string;
      apartmentNumber: string | null;
      city: string;
      zip: string;
      comments: string | null;
      tripPurpose: string;
    }[] = [];

    this.destinationAddresses.controls
      .map((control) => control.value)
      .forEach(
        (
          value: { address: 'other' | Destination; tripPurpose: string },
          index,
        ) => {
          if (value.address === 'other') {
            destinationAddresses.push({
              ...this.otherDestinationAddresses[index]!.value,
              tripPurpose: value.tripPurpose,
            });
          } else {
            const dest = this.favoriteDestinations.find(
              (dest) => dest.uid === (value.address as Destination).uid,
            );

            if (!dest) {
              throw new Error(`Cannot find destination #${index + 1}`);
            }

            destinationAddresses.push({
              label: dest.label,
              street: dest.street,
              apartmentNumber: dest.apartmentNumber,
              city: dest.city,
              zip: dest.zip,
              comments: dest.comments,
              tripPurpose: value.tripPurpose,
            });
          }
        },
      );

    await this.loadingService.add(
      this.fs.updateRideRequest(this.rideRequest!.uid, {
        ...form,
        pickupAddress: this.pickupAddress.value,
        destinationAddresses,
      }),
      { key: 'edit-ride-request' },
    );

    this.ref.close();
  }

  async duplicate(dates: Date[]) {
    if (!this.rideRequest) return;

    const ids = await this.loadingService.add(
      this.fs.duplicateRideRequest(this.rideRequest.uid, dates),
      { key: 'edit-ride-request' },
    );

    if (!ids) return;

    this.ref.close();

    this.matSnackbar.open(
      `Success! ${ids.length} duplicate ride request${
        ids.length === 1 ? '' : 's'
      } created.`,
      undefined,
      { duration: 5000 },
    );

    await this.router.navigate([
      '/dispatcher/ride-request/',
      ids[0],
      'overview',
    ]);

    if (ids.length > 1) return;

    await wait(1000);

    this.dialog.open(EditRideRequestComponent, {
      width: '600px',
      data: { duplicate: true },
    });
  }

  selectDupDate(date: Date, calendar: MatCalendar<Date>) {
    if (this.selectedDupDates.some((d) => d.valueOf() === date.valueOf())) {
      this.selectedDupDates = this.selectedDupDates.filter(
        (d) => d.valueOf() !== date.valueOf(),
      );
      calendar.updateTodaysDate();
      return;
    }

    this.selectedDupDates.push(date);
    calendar.updateTodaysDate();
  }

  get invalid() {
    if (!this.rideRequestForm) return true;
    if (
      this.data.requestType === 'RECURRING' &&
      !this.rideRequestRecurringScheduleForm
    ) {
      return true;
    }
    if (
      this.pickupAddressChooser.value === 'other' &&
      !this.pickupAddress.valid
    ) {
      return true;
    }
    if (
      this.otherDestinationAddresses.some(
        (addressForm) => !!addressForm && !addressForm.valid,
      )
    ) {
      return true;
    }

    return (
      !this.rideRequestForm.valid ||
      !this.destinationAddresses.valid ||
      (this.data.requestType === 'ONCE'
        ? !this.rideRequestDatetimeForm!.valid
        : !this.rideRequestRecurringScheduleForm!.valid)
    );
  }

  get pristine() {
    return (
      this.rideRequestForm!.pristine &&
      (this.data.requestType === 'ONCE'
        ? this.rideRequestDatetimeForm?.pristine
        : this.rideRequestRecurringScheduleForm?.pristine) &&
      (this.pickupAddressChooser.value === 'other'
        ? this.pickupAddressChooser.pristine && this.pickupAddress.pristine
        : this.pickupAddressChooser.pristine) &&
      this.destinationAddresses.controls.every((c, index) => {
        if (c.value.address === 'other') {
          return c.pristine && this.otherDestinationAddresses[index]!.pristine;
        }

        return c.pristine;
      })
    );
  }
}

function normalizeTime(input: number) {
  if (input < 10) return `0${input}`;

  return input.toString();
}

function areAddressesEqual(a: IAddress, b: IAddress): boolean {
  return (
    coerce(a.street) === coerce(b.street) &&
    coerce(a.apartmentNumber) === coerce(b.apartmentNumber) &&
    coerce(a.city) === coerce(b.city) &&
    coerce(a.zip) === coerce(b.zip) &&
    coerce(a.comments) === coerce(b.comments)
  );
}

function coerce(v: unknown) {
  return v || null;
}
