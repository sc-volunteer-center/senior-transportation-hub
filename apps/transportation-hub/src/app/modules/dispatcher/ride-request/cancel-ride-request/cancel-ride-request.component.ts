import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Input,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { RideRequest } from '@local/models';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, NavigationEnd } from '@angular/router';
import { toObservable, timestamp, Timestamp } from '../../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cancel-ride-request',
  templateUrl: './cancel-ride-request.component.html',
  styleUrls: ['./cancel-ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CancelRideRequestComponent implements OnInit {
  rideRequest: RideRequest | null = null;
  cancellationForm: FormGroup | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<CancelRideRequestComponent>,
    private router: Router,
    private loadingService: IsLoadingService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      requestType: 'ONCE' | 'RECURRING';
      rideRequestId: string;
      dispatcherId: string;
    },
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.subscriptions.push(
      this.fs
        .getRideRequest(this.data.rideRequestId)
        .subscribe((rideRequest) => {
          this.rideRequest = rideRequest;
          this.cdr.markForCheck();

          if (!rideRequest) {
            this.ref.close();
            return;
          }
        }),
    );

    this.cancellationForm = this.fb.group({
      requestCancelledBy: ['Client', Validators.required],
      cancellationReason: ['', Validators.required],
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (
      !this.cancellationForm ||
      this.cancellationForm.invalid ||
      !this.rideRequest
    ) {
      return;
    }

    const form = this.cancellationForm.value as {
      requestCancelledBy: string;
      cancellationReason: string;
    };

    await this.loadingService.add(
      Promise.all([
        this.fs.updateRideRequest(this.rideRequest.uid, {
          ...form,
          requestType: this.data.requestType,
          requestCancelled: true,
          requestCancelledAt: timestamp() as Timestamp,
          idOfDispatcherWhoCancelledRequest: this.data.dispatcherId,
          updatedAt: timestamp() as Timestamp,
        }),
        this.fs.addThread({
          rideRequestId: this.rideRequest.uid,
          flag: false,
          text: `REQUEST CANCELLED:\n` + form.cancellationReason,
        }),
      ]),
      { key: 'cancel-ride-request' },
    );

    this.ref.close();
  }
}
