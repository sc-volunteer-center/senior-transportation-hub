import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { RideRequest } from '@local/models';
import { Subscription, of, combineLatest } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { switchMap, filter, tap, map } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { Router } from '@angular/router';
import { isNavigationOver } from '../../../utilities';
import { EmailService } from '../../shared/email.service';
import { IsLoadingService } from '@service-work/is-loading';
import { findNextRequest, findPreviousRequest } from '../utilities';
import { addMinutes, addWeeks, subMinutes } from 'date-fns';
import { startOfWeek, endOfWeek } from '../../../utilities';

@Component({
  selector: 'app-ride-request',
  templateUrl: './ride-request.component.html',
  styleUrls: ['./ride-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideRequestComponent implements OnInit {
  rideRequest: RideRequest | null = null;
  activeRoute = 'overview';
  previousRideRequest: RideRequest | null = null;
  nextRideRequest: RideRequest | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private router: Router,
    private isLoadingService: IsLoadingService,
    public emailService: EmailService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.rideRequestId)
        .pipe(
          switchMap((id) => (!id ? of(null) : this.fs.getRideRequest(id))),
          tap((rideRequest) => {
            this.rideRequest = rideRequest;
            this.cdr.markForCheck();
          }),
          switchMap((rideRequest) => {
            if (!rideRequest) return of([null, null]);

            const prev = combineLatest([
              // get requests happening at the same time in decending order
              this.fs
                .getRideRequests({
                  clientId: rideRequest.clientId,
                  endBeforeOrOnDate: rideRequest.datetime,
                  startAfterOrOnDate: rideRequest.datetime,
                  order: 'desc',
                })
                .pipe(
                  map(
                    (requests) =>
                      requests
                        .filter(
                          (r) =>
                            // Normally, flexible requests happening this same week would be returned. We don't
                            // want them unless they are really happening at the same time.
                            r.datetime.valueOf() ===
                            rideRequest.datetime.valueOf(),
                        )
                        .find((r) => r.uid < rideRequest.uid) || null,
                  ),
                ),
              // get the previous request not happening at the same time
              this.fs.getRideRequests({
                clientId: rideRequest.clientId,
                endBeforeOrOnDate: subMinutes(rideRequest.datetime, 1),
                order: 'desc',
                limit: 1,
              }),
            ]).pipe(
              map(
                ([sameTime, prevMinute]) => sameTime || prevMinute[0] || null,
              ),
            );

            const next = combineLatest([
              // get requests happening at the same time in asc order
              this.fs
                .getRideRequests({
                  clientId: rideRequest.clientId,
                  endBeforeOrOnDate: rideRequest.datetime,
                  startAfterOrOnDate: rideRequest.datetime,
                })
                .pipe(
                  map(
                    (requests) =>
                      requests
                        .filter(
                          (r) =>
                            // Normally, flexible requests happening this same week would be returned. We don't
                            // want them unless they are really happening at the same time.
                            r.datetime.valueOf() ===
                            rideRequest.datetime.valueOf(),
                        )
                        .find((r) => r.uid > rideRequest.uid) || null,
                  ),
                ),
              // get next fixed requests happening this week in the future.
              this.fs.getRideRequests({
                clientId: rideRequest.clientId,
                startAfterOrOnDate: addMinutes(rideRequest.datetime, 1),
                endBeforeOrOnDate: endOfWeek(rideRequest.datetime),
                isFlexible: false,
                limit: 1,
              }),
              // get next request starting next week
              this.fs.getRideRequests({
                clientId: rideRequest.clientId,
                startAfterOrOnDate: startOfWeek(
                  addWeeks(rideRequest.datetime, 1),
                ),
                limit: 1,
              }),
            ]).pipe(
              map(
                ([sameTime, sameWeek, nextWeek]) =>
                  sameTime || sameWeek[0] || nextWeek[0] || null,
              ),
            );

            return this.isLoadingService.add(combineLatest([prev, next]), {
              key: 'next/previous requests',
            });
          }),
        )
        .subscribe(([previous, next]) => {
          if (!this.rideRequest) return;
          this.previousRideRequest = previous;
          this.nextRideRequest = next;
          this.cdr.markForCheck();
        }),
    );

    this.subscriptions.push(
      this.router.events.pipe(filter(isNavigationOver)).subscribe(() => {
        this.activeRoute = this.router.url.split('/').pop()!;

        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  submit() {}
}
