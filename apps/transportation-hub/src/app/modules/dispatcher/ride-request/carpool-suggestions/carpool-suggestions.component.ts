import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { filter, map, switchMap } from 'rxjs/operators';
import { IsLoadingService } from '@service-work/is-loading';
import { RideRequest } from '@local/models';
import { MapsService } from '../../../../maps.service';
import { navigateToPathFn } from '../../../../utilities';
import { Router, NavigationEnd } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of, Subscription } from 'rxjs';
import { startOfWeek, endOfWeek } from '../../../../utilities';
import { addWeeks, format } from 'date-fns';
import { getCarpoolSuggestionsForRequest } from '../../help/getCarpoolSuggestionsForRequest';

@Component({
  selector: 'app-carpool-suggestions',
  templateUrl: './carpool-suggestions.component.html',
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequestCarpoolSuggestionsComponent implements OnInit {
  carpoolSuggestions: Array<{
    otherRequest: RideRequest;
    mapURL: string;
    mapDescription: string;
  }> = [];

  private start?: Date;
  private end?: Date;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private isLoadingService: IsLoadingService,
    private mapsService: MapsService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private ref: MatDialogRef<RequestCarpoolSuggestionsComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { requestId: string },
  ) {}

  navigateToPath = navigateToPathFn(this.router);

  ngOnInit() {
    this.subscriptions.push(
      // close the dialog if the user navigates to a different page
      this.router.events
        .pipe(filter((e) => e instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.subscriptions.push(
      this.isLoadingService
        .add(
          this.fs.getRideRequest(this.data.requestId).pipe(
            switchMap((request) => {
              if (!request) return of([]);

              this.start = startOfWeek(request.datetime);
              this.end = endOfWeek(addWeeks(request.datetime, 1));

              return this.fs
                .getRideRequests({
                  startAfterOrOnDate: this.start,
                  endBeforeDate: this.end,
                  requestIsCancelled: false,
                })
                .pipe(
                  map((otherRequests) => {
                    return [
                      request,
                      otherRequests.filter(
                        (r) =>
                          r.category === request.category &&
                          (r.recurringRideRequestId || r.uid) !==
                            (request.recurringRideRequestId || request.uid),
                      ),
                    ] as const;
                  }),
                );
            }),
          ),
          {
            key: 'carpool-suggestions',
          },
        )
        .subscribe(([request, otherRequests]) => {
          this.cdr.markForCheck();

          if (!request) {
            this.carpoolSuggestions = [];
            return;
          }

          this.carpoolSuggestions = this.calculateCarpools(
            request,
            otherRequests,
          );
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  calculateCarpools(
    request: RideRequest,
    otherRequests: RideRequest[],
  ): Array<{
    otherRequest: RideRequest;
    mapURL: string;
    mapDescription: string;
  }> {
    const flexibleRequests = otherRequests.filter((r) => r.flexible);
    const fixedRequests = otherRequests.filter((r) => !r.flexible);

    const flexiblePairings = getCarpoolSuggestionsForRequest({
      request,
      candidateRequests: flexibleRequests,
      mapsService: this.mapsService,
    });

    const fixedPairings = getCarpoolSuggestionsForRequest({
      request,
      candidateRequests: fixedRequests,
      mapsService: this.mapsService,
      // only need to worry about the time if both requests are fixed
      onSameDay: !request.flexible,
    });

    return [...flexiblePairings, ...fixedPairings];
  }

  displayTimeRange() {
    if (!this.start || !this.end) return '';

    const start = format(this.start, 'MMM d');
    const end = format(this.end, 'MMM d, yyyy');

    return `${start} - ${end}`;
  }
}
