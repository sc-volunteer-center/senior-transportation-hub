import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ElementRef,
} from '@angular/core';
import {
  RideRequest,
  Dispatcher,
  Address,
  Driver,
  NoteThread,
  setTime,
} from '@local/models';
import { FormControl } from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap, tap, share } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MapsService } from '../../../../maps.service';
import { FindDriverForRideRequestComponent } from '../find-driver-for-ride-request/find-driver-for-ride-request.component';
import { MatDialog } from '@angular/material/dialog';
import {
  timestamp,
  Timestamp,
  isRideRequestInstanceDifferentFromRecurringParent,
  ngForTrackByID,
  startOfWeek,
} from '../../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { CancelRideRequestComponent } from '../cancel-ride-request/cancel-ride-request.component';
import { EditRideRequestComponent } from '../edit-ride-request/edit-ride-request.component';
import { SafeResourceUrl, Title } from '@angular/platform-browser';
import { DeleteRideRequestComponent } from '../delete-ride-request/delete-ride-request.component';
import { startOfDay, endOfDay, isBefore } from 'date-fns';
import { formatDate } from '@angular/common';
import {
  EditNoteComponent,
  IEditNoteComponentData,
  IEditNoteResponse,
  IReplyToNoteOutput,
  IEditNoteOutput,
  IDeleteNoteOutput,
  IFlagNoteOutput,
} from '../../../shared/notes/edit-note/edit-note.component';
import { WhichRideRequestComponent } from '../which-ride-request.component';
import { formatFlexibleRequestDatetime } from '../../../shared/flexible-request-datetime.pipe';
import { RequestCarpoolSuggestionsComponent } from '../carpool-suggestions/carpool-suggestions.component';

@Component({
  selector: 'app-ride-request-profile',
  templateUrl: './ride-request-profile.component.html',
  styleUrls: ['./ride-request-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RideRequestProfileComponent implements OnInit {
  rideRequest: RideRequest | null = null;

  dispatcher: Dispatcher | null = null;

  driverNotified = new FormControl(false);
  clientNotified = new FormControl(false);
  driverNotifiedOfCancellation = new FormControl(false);

  possibleDuplicate: RideRequest | null = null;

  tripUrl: SafeResourceUrl = '';

  noteThreads: NoteThread[] = [];
  flaggedRideRequestNoteThreads: NoteThread[] = [];
  flaggedDriverNoteThreads: NoteThread[] = [];
  flaggedClientNoteThreads: NoteThread[] = [];

  get cancelled() {
    return !!(this.rideRequest && this.rideRequest.requestCancelledAt);
  }

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private mapsService: MapsService,
    private dialog: MatDialog,
    private loadingService: IsLoadingService,
    private el: ElementRef<HTMLElement>,
    private titleService: Title,
  ) {}

  trackByID = ngForTrackByID;

  ngOnInit() {
    const rideRequest$ = this.routerStore
      .get$((store) => store.state.params.rideRequestId)
      .pipe(
        switchMap((id) =>
          !id
            ? of(null)
            : this.fs.getRideRequest(id, {
                dispatcher: true,
                driver: true,
                recurringRideRequest: true,
              }),
        ),
        share(),
      );

    this.subscriptions.push(
      rideRequest$
        .pipe(
          // Handle ride request
          tap((rideRequest) => {
            this.rideRequest = rideRequest;

            this.driverNotified.patchValue(
              !!(rideRequest && rideRequest.driverNotifiedAt),
            );

            this.clientNotified.patchValue(
              !!(rideRequest && rideRequest.clientNotifiedOfDriverAt),
            );

            this.driverNotifiedOfCancellation.patchValue(
              !!(rideRequest && rideRequest.driverNotifiedOfCancellationAt),
            );

            this.tripUrl = this.buildTripUrl();

            if (this.cancelled) {
              this.driverNotified.disable();
              this.clientNotified.disable();
            } else {
              this.driverNotified.enable();
              this.clientNotified.enable();
            }

            this.cdr.markForCheck();

            if (!rideRequest) return;

            this.titleService.setTitle(
              `Ride Request | ${
                rideRequest.client.nameWithRetiredStatus
              } ${formatDate(rideRequest.datetime, 'short', 'en-US')} | Hub`,
            );
          }),
          // Get possible duplicate ride request
          switchMap((request) => {
            return !request
              ? of(null)
              : this.fs.getRideRequests(
                  {
                    clientId: request.clientId,
                    startAfterOrOnDate: startOfDay(request.datetime),
                    endBeforeDate: endOfDay(request.datetime),
                    isFlexible: false,
                    requestIsCancelled: false,
                  },
                  { driver: true },
                );
          }),
        )
        .subscribe((rideRequests) => {
          this.cdr.markForCheck();

          if (!rideRequests || !this.rideRequest) return;

          this.possibleDuplicate =
            rideRequests.find((r) => r.uid !== this.rideRequest!.uid) ?? null;
        }),
    );

    this.subscriptions.push(
      this.fs
        .getCurrentDispatcher()
        .subscribe((dispatcher) => (this.dispatcher = dispatcher)),
      this.routerStore
        .get$((store) => store.state.params.rideRequestId)
        .pipe(
          switchMap((rideRequestId) =>
            !rideRequestId
              ? of([])
              : this.loadingService.add(
                  this.fs.getNoteThreads({ rideRequestId }),
                  { key: 'ride-request-note-threads' },
                ),
          ),
        )
        .subscribe((threads) => {
          this.noteThreads = threads;
          this.flaggedRideRequestNoteThreads = threads.filter((t) => t.flag);
          this.cdr.markForCheck();
        }),
      rideRequest$
        .pipe(
          switchMap((rideRequest) =>
            !rideRequest || !rideRequest.driverId
              ? of([])
              : this.fs.getNoteThreads({
                  driverId: rideRequest.driverId,
                  flag: true,
                }),
          ),
        )
        .subscribe((threads) => {
          this.flaggedDriverNoteThreads = threads.filter((t) => t.flag);
          this.cdr.markForCheck();
        }),
      rideRequest$
        .pipe(
          switchMap((rideRequest) =>
            !rideRequest
              ? of([])
              : this.fs.getNoteThreads({
                  clientId: rideRequest.clientId,
                  flag: true,
                }),
          ),
        )
        .subscribe((threads) => {
          this.flaggedClientNoteThreads = threads.filter((t) => t.flag);
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  buildTripUrl() {
    if (!this.rideRequest) {
      return '';
    }

    return this.mapsService.directionsMapUrl([
      this.rideRequest.pickupAddress,
      ...this.rideRequest.destinationAddresses,
    ]);
  }

  addressNumber(index: number) {
    switch (index) {
      case 0:
        return 'First';
      case 1:
        return 'Second';
      case 2:
        return 'Third';
      case 3:
        return 'Fourth';
      case 4:
        return 'Fifth';
      case 5:
        return 'Sixth';
      default:
        throw new Error('Too many destination addresses');
    }
  }

  findDriver() {
    this.dialog.open(FindDriverForRideRequestComponent, {
      width: '100vw',
      height: '100vh',
      closeOnNavigation: true,
      panelClass: 'find-driver-for-ride-request',
    });
  }

  async editRideRequest() {
    const requestType = await this.editWhichRideRequest('editRideRequest');

    if (!requestType) return;

    this.dialog.open(EditRideRequestComponent, {
      width: '600px',
      disableClose: true,
      data: {
        requestType,
        overviewEl: this.el.nativeElement,
      },
    });
  }

  suggestCarpools() {
    if (!this.rideRequest) return;

    this.dialog.open(RequestCarpoolSuggestionsComponent, {
      width: '600px',
      data: {
        requestId: this.rideRequest.uid,
      },
    });
  }

  async toggleDriverNotified() {
    if (!this.rideRequest || !this.dispatcher || this.driverNotified.disabled) {
      return;
    }

    const requestType = await this.editWhichRideRequest('toggleDriverNotified');

    if (!requestType) return;

    const update = this.rideRequest.driverNotifiedAt
      ? {
          requestType,
          driverNotifiedAt: null,
          driverNotifiedById: null,
          updatedAt: timestamp() as Timestamp,
        }
      : {
          requestType,
          driverNotifiedAt: timestamp() as Timestamp,
          driverNotifiedById: this.dispatcher.uid,
          updatedAt: timestamp() as Timestamp,
        };

    this.loadingService.add(
      this.fs.updateRideRequest(this.rideRequest.uid, update),
    );
  }

  async toggleClientNotified() {
    if (!this.rideRequest || !this.dispatcher || this.clientNotified.disabled) {
      return;
    }

    const requestType = await this.editWhichRideRequest('toggleClientNotified');

    if (!requestType) return;

    const update = this.rideRequest.clientNotifiedOfDriverAt
      ? {
          requestType,
          clientNotifiedOfDriverAt: null,
          clientNotifiedOfDriverById: null,
          updatedAt: timestamp() as Timestamp,
        }
      : {
          requestType,
          clientNotifiedOfDriverAt: timestamp() as Timestamp,
          clientNotifiedOfDriverById: this.dispatcher.uid,
          updatedAt: timestamp() as Timestamp,
        };

    this.loadingService.add(
      this.fs.updateRideRequest(this.rideRequest.uid, update),
    );
  }

  async toggleDriverNotifiedOfCancellation() {
    if (
      !this.rideRequest ||
      !this.dispatcher ||
      this.driverNotifiedOfCancellation.disabled
    ) {
      return;
    }

    const requestType = await this.editWhichRideRequest(
      'toggleDriverNotifiedOfCancellation',
    );

    if (!requestType) return;

    const update = this.rideRequest.driverNotifiedOfCancellationAt
      ? {
          requestType,
          driverNotifiedOfCancellationAt: null,
          driverNotifiedOfCancellationById: null,
          updatedAt: timestamp() as Timestamp,
        }
      : {
          requestType,
          driverNotifiedOfCancellationAt: timestamp() as Timestamp,
          driverNotifiedOfCancellationById: this.dispatcher.uid,
          updatedAt: timestamp() as Timestamp,
        };

    this.loadingService.add(
      this.fs.updateRideRequest(this.rideRequest.uid, update),
    );
  }

  async cancelRideRequest() {
    if (
      !this.rideRequest ||
      !this.dispatcher ||
      this.rideRequest.requestCancelledAt
    ) {
      return;
    }

    const requestType = await this.editWhichRideRequest('cancelRideRequest');

    if (!requestType) return;

    this.dialog
      .open(CancelRideRequestComponent, {
        width: '600px',
        data: {
          requestType,
          rideRequestId: this.rideRequest.uid,
          dispatcherId: this.dispatcher.uid,
        },
      })
      .afterClosed()
      .subscribe(() => {
        setTimeout(() => {
          if (this.rideRequest?.requestCancelledAt) {
            this.el.nativeElement.scrollTo({
              behavior: 'smooth',
              top: 0,
            });
          }
        }, 500);
      });
  }

  deleteRideRequest() {
    if (
      !this.rideRequest ||
      !this.dispatcher ||
      !this.rideRequest.requestCancelledAt
    ) {
      return;
    }

    this.dialog.open(DeleteRideRequestComponent, {
      width: '600px',
      closeOnNavigation: true,
      data: {
        rideRequestId: this.rideRequest.uid,
        dispatcherId: this.dispatcher.uid,
      },
    });
  }

  async uncancelRideRequest() {
    if (
      !this.rideRequest ||
      !this.dispatcher ||
      !this.rideRequest.requestCancelledAt
    ) {
      return;
    }

    await this.loadingService.add(
      Promise.all([
        this.fs.updateRideRequest(this.rideRequest.uid, {
          requestType: this.rideRequest.recurringRideRequestId
            ? 'RECURRING'
            : 'ONCE',
          requestCancelled: false,
          requestCancelledAt: null,
          requestCancelledBy: null,
          idOfDispatcherWhoCancelledRequest: null,
          cancellationReason: null,
          driverNotifiedOfCancellationAt: null,
          driverNotifiedOfCancellationById: null,
          driverNotifiedAt: null,
          driverNotifiedById: null,
          clientNotifiedOfDriverAt: null,
          clientNotifiedOfDriverById: null,
        }),
        this.fs.addThread({
          rideRequestId: this.rideRequest.uid,
          flag: false,
          text: `REQUEST UN-CANCELLED`,
        }),
      ]),
    );

    if (this.rideRequest.recurringRideRequestId) {
      await this.fs.generateRecurringRideRequests({ force: true });
    }
  }

  doesDriverHaveVacation(driver?: Driver) {
    if (!driver) return false;

    return (
      !driver.retiredAt &&
      driver.vacationEndsAt.valueOf() >= new Date().valueOf()
    );
  }

  mapAddressToLabel(address: Address, index: number) {
    return address.label || `destination ${index + 1}`;
  }

  renderNote(text: string) {
    return text.replaceAll('\n', '<br>');
  }

  async replyToFlag(thread: NoteThread) {
    const note = thread.notes[thread.notes.length - 1];

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            reply: note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.reply) return;

    return this.replyToNote(result.reply);
  }

  async addNote() {
    if (!this.rideRequest?.uid) return;

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            askIfAssociatedWithVolunteer: true,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.new) return;

    return this.loadingService.add(
      this.fs.addThread({
        rideRequestId: this.rideRequest.uid,
        driverId: result.new.volunteer?.uid,
        flag: result.new.flaggedAsImportant,
        text: result.new.text,
      }),
    );
  }

  replyToNote(args: IReplyToNoteOutput) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.addNote(args));
  }

  editNote(args: IEditNoteOutput) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.updateNote(args));
  }

  deleteNote(args: IDeleteNoteOutput) {
    if (!this.rideRequest?.uid) return;

    if (args.deleteThread) {
      return this.loadingService.add(this.fs.deleteThread(args));
    }

    return this.loadingService.add(this.fs.deleteNote(args));
  }

  flagNote(args: Omit<IFlagNoteOutput, 'noteId'>) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }

  suppressFlagTodo(args: {
    noteThreadId: string;
    flag: boolean;
    suppressTodo: boolean;
  }) {
    if (!this.rideRequest?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }

  printRecurrenceSchedule() {
    const r = this.rideRequest!.recurringRideRequest;

    if (!r) return;

    const start = setTime(r.startDate, r.time);
    let text = '';

    if (this.rideRequest!.flexible) {
      text = `${
        r.scheduleName
      } starting the week of ${formatFlexibleRequestDatetime(start)}`;

      if (r.endDate) {
        text += ` and ending the week of ${formatFlexibleRequestDatetime(
          r.endDate,
        )}.`;
      }
    } else {
      text = `${r.scheduleName} starting on ${formatDate(
        start,
        'EEEE, MMMM d, y @ h:mmaaa',
        'en-US',
      )}`;

      if (r.endDate) {
        text += ` and ending on ${formatDate(
          r.endDate,
          'EEEE, MMMM d, y',
          'en-US',
        )}.`;
      }
    }

    if (!r.endDate) {
      text += `.`;
    }

    return text;
  }

  private async editWhichRideRequest(
    changeType:
      | 'editRideRequest'
      | 'toggleDriverNotified'
      | 'toggleClientNotified'
      | 'toggleDriverNotifiedOfCancellation'
      | 'cancelRideRequest',
  ): Promise<'ONCE' | 'RECURRING' | undefined> {
    if (!this.rideRequest?.recurringRideRequest) return 'ONCE';

    // If the current ride request instance has been edited to be different
    // then the parent recurring request, then these operations should only
    // be applied to this specific instance
    const sensitiveOperations = [
      'toggleDriverNotified',
      'toggleClientNotified',
      'toggleDriverNotifiedOfCancellation',
    ];

    if (
      sensitiveOperations.includes(changeType) &&
      isRideRequestInstanceDifferentFromRecurringParent(
        this.rideRequest,
        this.rideRequest.recurringRideRequest,
      )
    ) {
      return 'ONCE';
    }

    return this.dialog
      .open(WhichRideRequestComponent, {
        width: '600px',
        data: {
          inPast: this.rideRequest.flexible
            ? isBefore(this.rideRequest.datetime, startOfWeek(new Date()))
            : isBefore(this.rideRequest.datetime, startOfDay(new Date())),
        },
      })
      .afterClosed()
      .toPromise();
  }
}
