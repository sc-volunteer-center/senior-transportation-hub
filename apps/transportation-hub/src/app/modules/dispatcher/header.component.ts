import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  AfterViewInit,
  ViewEncapsulation,
  Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddRideRequestComponent } from './add-ride-request/add-ride-request.component';
import { AddClientComponent } from './add-client/add-client.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { Dispatcher } from '@local/models';
import { FirestoreService } from '../../firestore.service';
import { AuthService } from '../../auth.service';
import { RouterStoreService } from '../../state/router-store.service';

@Component({
  selector: 'app-non-secure-mode-warning',
  template: `
    <h1 mat-dialog-title>Non-secure mode</h1>

    <mat-dialog-content>
      <p>
        When you logged into the Hub, you were asked if you were on a secure
        device and you said "no." As a result, the Hub is operating in a
        non-secure mode which costs more money. If you are actually on a secure
        device (e.g. a device that you or the Volunteer Center owns) then you
        should sign out and then sign back in.
      </p>
    </mat-dialog-content>

    <mat-dialog-actions>
      <button mat-button type="button" matDialogClose>Close</button>

      <div class="flex-1"></div>

      <button mat-button type="button" (click)="logout()">Sign out</button>
    </mat-dialog-actions>
  `,
})
export class NonSecureModeWarningComponent {
  constructor(
    private ref: MatDialogRef<NonSecureModeWarningComponent>,
    private authService: AuthService,
  ) {}

  logout() {
    this.ref.close();
    return this.authService.logout();
  }
}

@Component({
  selector: 'app-header',
  template: `
    <mat-toolbar *ngIf="!fs.persistanceEnabled" color="accent">
      <mat-toolbar-row class="flex-center">
        <p>Warning: non-secure mode enabled</p>

        <button
          mat-button
          type="button"
          (click)="openNonSecureDescription()"
          style="margin-left: 1rem"
        >
          learn more
        </button>
      </mat-toolbar-row>
    </mat-toolbar>

    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <div id="app-menu-outlet">
          <div #menuOutlet></div>
        </div>

        <div class="flex-1"></div>

        <div *ngIf="dispatcher" style="display: flex; flex-shrink: 0;">
          <button mat-button (click)="addRideRequest()">Add request</button>
          <button mat-button (click)="addClient()">Add client</button>
          <button mat-button (click)="addDriver()">Add Volunteer</button>
        </div>
      </mat-toolbar-row>
    </mat-toolbar>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'app-header' },
  styles: [
    `
      .TESTING-APP .mat-toolbar.mat-primary {
        background: #c2185b;
      }
    `,
  ],
})
export class HeaderComponent implements AfterViewInit {
  @Input() dispatcher?: Dispatcher;

  @ViewChild('menuOutlet', { read: ViewContainerRef, static: true })
  menuOutlet: ViewContainerRef;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private factory: ComponentFactoryResolver,
    public fs: FirestoreService,
    private routerStore: RouterStoreService,
  ) {}

  ngAfterViewInit() {
    this.routerStore
      .get$((store) => store.state.breadcrumbs)
      .subscribe((breadcrumbs) => {
        this.menuOutlet.clear();

        if (!breadcrumbs || breadcrumbs.length === 0) {
          return;
        }

        const factory = this.factory.resolveComponentFactory(
          breadcrumbs[breadcrumbs.length - 1],
        );

        this.menuOutlet.createComponent(factory);
        this.cdr.markForCheck();
      });
  }

  addRideRequest() {
    this.dialog
      .open(AddRideRequestComponent, {
        width: '600px',
        panelClass: 'add-ride-request',
        disableClose: true,
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.router.navigate([`/dispatcher/ride-request`, id, 'overview']);
        }
      });
  }

  addClient() {
    this.dialog
      .open(AddClientComponent, {
        width: '600px',
        disableClose: true,
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.router.navigate([`/dispatcher/client`, id, 'profile']);
        }
      });
  }

  addDriver() {
    this.dialog
      .open(AddDriverComponent, {
        width: '600px',
        disableClose: true,
      })
      .afterClosed()
      .subscribe((id) => {
        if (id) {
          this.router.navigate([`/dispatcher/driver`, id, 'profile']);
        }
      });
  }

  openNonSecureDescription() {
    this.dialog.open(NonSecureModeWarningComponent, {
      width: '600px',
    });
  }
}
