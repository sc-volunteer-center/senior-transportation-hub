import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Driver } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';

@Component({
  selector: 'app-driver-breadcrumb',
  template: ` Volunteer - {{ driver?.nameWithRetiredStatus }} `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DriverBreadcrumbComponent implements OnInit {
  driver: Driver | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDriver(id))))
        .subscribe((driver) => {
          this.driver = driver;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
