import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Driver, Tag } from '@local/models';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import {
  toPromise,
  stringComparer,
} from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-edit-driver',
  templateUrl: './edit-driver.component.html',
  styleUrls: ['./edit-driver.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditDriverComponent implements OnInit {
  driver: Driver | null = null;
  driverForm?: FormGroup;

  blacklistableClientOptions: Array<{ id: string; label: string }> = [];
  blacklistedClientsControl = new FormControl([]);

  driverTags: Tag[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<EditDriverComponent>,
    private loadingService: IsLoadingService,
    private dialog: MatDialog,
  ) {}

  async ngOnInit() {
    this.driverTags = await toPromise(this.fs.getTags({ type: 'driver' }));

    this.subscriptions.push(
      this.fs.getTags({ type: 'driver' }).subscribe((tags) => {
        this.driverTags = tags;
        this.cdr.markForCheck();
      }),
    );

    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((id) =>
            !id ? of(null) : this.fs.getDriver(id, { full: true }),
          ),
        )
        .subscribe((driver) => {
          this.driver = driver;
          this.driverForm = undefined;
          this.cdr.markForCheck();

          if (!driver) {
            this.blacklistedClientsControl.patchValue([]);
            return;
          } else {
            this.blacklistedClientsControl.patchValue(
              driver.blacklistedClients.map((client) => ({
                id: client.uid,
                label: client.nameWithRetiredStatus,
              })),
            );
          }

          this.driverForm = this.fb.group({
            nickname: [driver.nickname],
            firstName: [driver.firstName, Validators.required],
            middleName: [driver.middleName],
            lastName: [driver.lastName, Validators.required],
            phoneNumber: [driver.phoneNumber, Validators.required],
            emailAddress: [driver.emailAddress, Validators.email],
            ethnicity: [driver.ethnicity, Validators.required],
            birthdate: driver.birthdate,
            genderIdentity: driver.genderIdentity,
            address: this.fb.group({
              street: [driver.address.street, Validators.required],
              apartmentNumber: [driver.address.apartmentNumber],
              city: [driver.address.city, Validators.required],
              zip: [driver.address.zip, Validators.required],
              comments: [driver.address.comments],
            }),
            comments: driver.comments,
            driversLicenseExpirationDate: [
              driver.driversLicenseExpirationDate,
              Validators.required,
            ],
            carInsuranceExpirationDate: [
              driver.carInsuranceExpirationDate,
              Validators.required,
            ],
            availability: this.fb.group(driver.availability),
            carInfo: driver.carInfo,
            tagIds: [driver.tagIds || []],
          });
        }),
    );

    this.subscriptions.push(
      this.fs.getClients().subscribe((clients) => {
        this.blacklistableClientOptions = clients
          .sort((a, b) => {
            const name = stringComparer(a.name, b.name);
            return name !== 0 ? name : stringComparer(a.uid, b.uid);
          })
          .map((client) => ({
            id: client.uid,
            label: client.nameWithRetiredStatus,
          }));

        this.cdr.markForCheck();
      }),
    );

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async submit() {
    if (!this.driverForm || this.driverForm.invalid) {
      return;
    }

    const form = {
      ...this.driverForm.value,
      blacklistedClientIds: this.blacklistedClientsControl.value.map(
        (option: { id: string; label: string }) => option.id,
      ),
    };

    await this.loadingService.add(
      this.fs.updateDriver(this.driver!.uid, form),
      { key: 'edit-driver' },
    );

    this.ref.close();
  }
}
