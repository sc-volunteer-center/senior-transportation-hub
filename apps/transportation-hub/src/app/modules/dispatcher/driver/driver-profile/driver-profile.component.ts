import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import {
  Driver,
  Dispatcher,
  Tag,
  NoteThread,
  RecurringRideRequest,
  RideRequest,
} from '@local/models';
import { Subscription, of, combineLatest } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MapsService } from '../../../../maps.service';
import { EditDriverComponent } from '../edit-driver/edit-driver.component';
import { MatDialog } from '@angular/material/dialog';
import { ngForTrackByID, Timestamp, toPromise } from '../../../../utilities';
import { IsLoadingService } from '@service-work/is-loading';
import { SafeResourceUrl, Title } from '@angular/platform-browser';
import {
  startOfToday,
  addMonths,
  startOfDay,
  addWeeks,
  endOfDay,
} from 'date-fns';
import { EditDriverVacationComponent } from '../edit-driver-vacation/edit-driver-vacation.component';
import { DeleteDriverComponent } from '../delete-driver/delete-driver.component';
import { RetireDriverComponent } from '../retire-driver/retire-driver.component';
import {
  EditNoteComponent,
  IEditNoteComponentData,
  IEditNoteResponse,
  IReplyToNoteOutput,
  IEditNoteOutput,
  IDeleteNoteOutput,
  IFlagNoteOutput,
} from '../../../shared/notes/edit-note/edit-note.component';
import { WarningService } from '../../../shared/warning/warning.service';

@Component({
  selector: 'app-driver-profile',
  templateUrl: './driver-profile.component.html',
  styleUrls: ['./driver-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DriverProfileComponent implements OnInit {
  driver: Driver | null = null;
  mapUrl: SafeResourceUrl = '';

  driverTags: Tag[] = [];

  noteThreads: NoteThread[] = [];
  flaggedNoteThreads: NoteThread[] = [];

  upcomingRideRequests: RideRequest[] | null = null;
  activeRecurringRideRequests: RecurringRideRequest[] | null = null;
  previousRideRequest: RideRequest | null = null;
  nextRideRequest: RideRequest | null = null;

  // used to construct the "find request for volunteer" URL
  currentTime = new Date().valueOf();

  private dispatcher: Dispatcher | null = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private mapsService: MapsService,
    private dialog: MatDialog,
    private loadingService: IsLoadingService,
    private titleService: Title,
    private warningService: WarningService,
  ) {}

  trackByID = ngForTrackByID;

  async ngOnInit() {
    this.driverTags = await toPromise(this.fs.getTags({ type: 'driver' }));

    // get driver, set page title, set driver map location
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((id) =>
            !id ? of(null) : this.fs.getDriver(id, { full: true }),
          ),
        )
        .subscribe((driver) => {
          this.driver = driver;
          this.titleService.setTitle(
            `Volunteer | ${this.driver?.nameWithRetiredStatus} | Hub`,
          );
          this.mapUrl = this.driverAddressUrl();
          this.cdr.markForCheck();
        }),
    );

    // get notes and flagged notes
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((driverId) =>
            !driverId
              ? of([])
              : this.loadingService.add(this.fs.getNoteThreads({ driverId }), {
                  key: 'driver-note-threads',
                }),
          ),
        )
        .subscribe((threads) => {
          this.noteThreads = threads;
          this.flaggedNoteThreads = threads.filter((t) => t.flag);
          this.cdr.markForCheck();
        }),
    );

    // get current dispatcher
    this.subscriptions.push(
      this.fs
        .getCurrentDispatcher()
        .subscribe((disp) => (this.dispatcher = disp)),
    );

    // get driver's active recurring ride requests
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((driverId) =>
            !driverId
              ? of([])
              : this.fs.getRecurringRideRequests({
                  driverId,
                  // startsBeforeOrOn: new Date(),
                  endAfterDate: startOfDay(new Date()),
                }),
          ),
        )
        .subscribe((rrr) => {
          this.activeRecurringRideRequests = rrr;
          this.cdr.markForCheck();
        }),
    );

    // get driver's upcoming ride requests for the next 3 weeks
    this.subscriptions.push(
      combineLatest([
        this.fs.getRideRequests({
          startAfterOrOnDate: startOfDay(new Date()),
          endBeforeDate: addWeeks(endOfDay(new Date()), 3),
          requestIsCancelled: false,
          cache: { expirationTime: 1000 * 60 * 10 },
        }),
        this.routerStore.get$((store) => store.state.params.driverId),
      ]).subscribe(([rrr, driverId]) => {
        this.upcomingRideRequests = rrr.filter((r) => r.driverId === driverId);
        this.cdr.markForCheck();
      }),
    );

    // get driver's next and previous non-cancelled requests
    this.subscriptions.push(
      this.routerStore
        .get$((s) => s.state.params.driverId)
        .pipe(
          switchMap((driverId) =>
            !driverId
              ? of([null, null])
              : this.loadingService.add(
                  combineLatest([
                    this.fs.getRideRequests({
                      driverId,
                      endBeforeDate: startOfDay(new Date()),
                      order: 'desc',
                      limit: 2,
                      requestIsCancelled: false,
                    }),
                    this.fs.getRideRequests({
                      driverId,
                      startAfterOrOnDate: startOfDay(new Date()),
                      limit: 1,
                      requestIsCancelled: false,
                    }),
                  ]),
                  {
                    key: 'next/previous requests',
                  },
                ),
          ),
        )
        .subscribe(([previous, next]) => {
          this.previousRideRequest = previous?.[0] || null;
          this.nextRideRequest = next?.[0] || null;

          if (
            this.previousRideRequest &&
            this.previousRideRequest.uid === this.nextRideRequest?.uid
          ) {
            this.previousRideRequest = previous![1] || null;
          }

          this.cdr.markForCheck();
        }),
    );

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editDriver() {
    this.dialog.open(EditDriverComponent, {
      width: '600px',
      disableClose: true,
    });
  }

  async toggleRetireDriver() {
    if (!this.driver || !this.dispatcher) {
      return;
    }

    if (this.driver.retiredAt) {
      await this.loadingService.add(
        Promise.all([
          this.fs.updateDriver(this.driver.uid, {
            retiredAt: null,
            retiredById: null,
            retiredReason: null,
          }),
          this.fs.addThread({
            driverId: this.driver.uid,
            flag: false,
            text: `DRIVER UN-RETIRED`,
          }),
        ]),
      );

      await this.warningService
        .popup({
          title: 'Checklist',
          text: `After un-retiring a volunteer, <strong>don't forget to create a Google Contacts entry for them (if they don't already have one)</strong> so that their name shows up next to their phone number when they call us. Additionally, also remember to apply the "STP" and/or "GSP" labels to their profile in the Hub, as appropriate.

        To do this, open up “Google Contacts” (<em>Important! Make sure you are using Google Contacts with the transportation google account and not another google account</em>). Add a contact entry for this client if one doesn’t already exist for them. Be sure the contact entry includes the volunteer's name, email address, and phone number. Last, apply the “GSP Volunteer" label to the volunteer’s contact entry if they are participating in the GSP program and add the “STP Volunteer” label to their contact entry if they are participating in the STP program.`,
          affirmative: 'Gotcha',
        })
        .afterClosed()
        .toPromise();
    } else {
      this.retireDriver();
    }
  }

  retireDriver() {
    if (!this.driver || !this.dispatcher) {
      return;
    }

    this.dialog
      .open(RetireDriverComponent, {
        width: '600px',
        data: {
          driverId: this.driver.uid,
          dispatcherId: this.dispatcher.uid,
        },
      })
      .afterClosed()
      .subscribe();
  }

  driverAddressUrl() {
    if (!this.driver) {
      return '';
    }

    return this.mapsService.addressMapUrl(this.driver.address);
  }

  async endVacation() {
    if (!this.driver || !this.dispatcher) {
      return;
    }

    await this.loadingService.add(
      Promise.all([
        this.fs.updateDriver(this.driver!.uid, {
          vacationStartsAt: startOfToday() as any as Timestamp,
          vacationEndsAt: startOfToday() as any as Timestamp,
          vacationingById: null,
        }),
        this.fs.addThread({
          driverId: this.driver!.uid,
          flag: false,
          text: `VACATION ENDED EARLY`,
        }),
      ]),
    );

    this.cdr.markForCheck();
  }

  editVacation() {
    this.dialog.open(EditDriverVacationComponent, {
      width: '600px',
    });
  }

  deleteDriver() {
    if (!this.driver || !this.dispatcher || !this.driver.retiredAt) {
      return;
    }

    this.dialog.open(DeleteDriverComponent, {
      width: '600px',
      closeOnNavigation: true,
      data: {
        driverId: this.driver.uid,
      },
    });
  }

  get isDriverVacationing() {
    return !this.driver!.retiredAt && this.driver!.isOnVacation();
  }

  get doesDriverHaveUpcomingVacation() {
    return (
      !this.driver!.retiredAt &&
      this.driver!.vacationStartsAt.valueOf() >= new Date().valueOf()
    );
  }

  get isLicenseOrInsuranceExpiring() {
    const now = addMonths(new Date(), 1).valueOf();

    return (
      this.driver!.carInsuranceExpirationDate.valueOf() <= now ||
      this.driver!.driversLicenseExpirationDate.valueOf() <= now
    );
  }

  get displayBlacklistedClients() {
    return this.driver!.blacklistedClients.length > 0;
  }

  renderNote(text: string) {
    return text.replaceAll('\n', '<br>');
  }

  async replyToFlag(thread: NoteThread) {
    const note = thread.notes[thread.notes.length - 1];

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            reply: note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.reply) return;

    return this.replyToNote(result.reply);
  }

  async addNote() {
    if (!this.driver?.uid) return;

    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
        },
      )
      .afterClosed()
      .toPromise();

    if (!result?.new) return;

    return this.loadingService.add(
      this.fs.addThread({
        driverId: this.driver.uid,
        flag: result.new.flaggedAsImportant,
        text: result.new.text,
      }),
    );
  }

  replyToNote(args: IReplyToNoteOutput) {
    if (!this.driver?.uid) return;

    return this.loadingService.add(this.fs.addNote(args));
  }

  editNote(args: IEditNoteOutput) {
    if (!this.driver?.uid) return;

    return this.loadingService.add(this.fs.updateNote(args));
  }

  deleteNote(args: IDeleteNoteOutput) {
    if (!this.driver?.uid) return;

    if (args.deleteThread) {
      return this.loadingService.add(this.fs.deleteThread(args));
    }

    return this.loadingService.add(this.fs.deleteNote(args));
  }

  flagNote(args: Omit<IFlagNoteOutput, 'noteId'>) {
    if (!this.driver?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }

  suppressFlagTodo(args: {
    noteThreadId: string;
    flag: boolean;
    suppressTodo: boolean;
  }) {
    if (!this.driver?.uid) return;

    return this.loadingService.add(this.fs.updateThread(args));
  }
}
