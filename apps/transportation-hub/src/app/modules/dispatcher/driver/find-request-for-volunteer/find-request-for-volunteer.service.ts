import { Injectable } from '@angular/core';
import { of, combineLatest, Observable } from 'rxjs';
import { switchMap, map, tap } from 'rxjs/operators';
import {
  addMonths,
  subMonths,
  addWeeks,
  addDays,
  subWeeks,
  subDays,
} from 'date-fns';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { IsLoadingService } from '@service-work/is-loading';
import { Driver, RideRequest } from '@local/models';
import { Sort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { RideRequestsViewService } from '../../ride-requests/ride-requests-view.service';
import { FirestoreService } from 'apps/transportation-hub/src/app/firestore.service';
import { EmailService } from '../../../shared/email.service';
import { MapsService } from 'apps/transportation-hub/src/app/maps.service';
import { numberComparer } from 'apps/transportation-hub/src/app/utilities';

@Injectable()
export class FindRequestForVolunteerService extends RideRequestsViewService {
  driver: Driver | null = null;

  protected readonly loadingKey = 'find-request-for-volunteer';

  constructor(
    fs: FirestoreService,
    routerStore: RouterStoreService,
    emailService: EmailService,
    isLoadingService: IsLoadingService,
    router: Router,
    protected mapsService: MapsService,
  ) {
    super(fs, routerStore, emailService, isLoadingService, router);
  }

  getRideRequests(
    view: 'month' | 'week' | 'day',
  ): Observable<Array<RideRequest & { distance?: number }>> {
    return combineLatest([
      this.fs.getTags({ type: 'driver' }),
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((driverId) =>
            !driverId ? of(null) : this.fs.getDriver(driverId),
          ),
          tap((driver) => {
            this.driver = driver;
          }),
        ),
      super.getRideRequests(view) as Observable<
        Array<RideRequest & { distance?: number }>
      >,
    ]).pipe(
      map(([tags, driver, rr]) => {
        const stpTag = tags.find((t) => t.label === 'STP');
        const gspTag = tags.find((t) => t.label === 'GSP');

        if (!driver || !stpTag || !gspTag) return [];

        const isSTP = driver.tagIds.some((id) => stpTag.uid === id);
        const isGSP = driver.tagIds.some((id) => gspTag.uid === id);

        return rr
          .filter((r) => {
            if (r.requestCancelledAt) return false;
            if (r.driverId === driver.uid) return false;

            return r.category === 'GSP' ? isGSP : isSTP;
          })
          .map((r) => {
            const d = this.mapsService.birdsDistance([
              driver!.address,
              r.pickupAddress,
            ]);

            if (!d) return r;

            r.distance = Number.parseFloat(d.toFixed(2));

            return r;
          });
      }),
    );
  }

  sortRideRequests(
    rideRequests: Array<RideRequest & { distance?: number }>,
    event: Sort,
  ) {
    switch (event.active) {
      case 'distance': {
        return rideRequests.slice().sort(sortByDistance);
      }
      default: {
        return super.sortRideRequests(rideRequests, event);
      }
    }
  }

  nextDate() {
    const view = this.getSelectedView();

    const fn =
      view === 'month' ? addMonths : view === 'week' ? addWeeks : addDays;

    this.router.navigate(
      [
        '/dispatcher/driver',
        this.driver!.uid,
        'suggest-requests',
        fn(this.selectedDate$.value, 1).valueOf(),
        view,
      ],
      {
        replaceUrl: true,
      },
    );
  }

  prevDate() {
    const view = this.getSelectedView();

    const fn =
      view === 'month' ? subMonths : view === 'week' ? subWeeks : subDays;

    this.router.navigate(
      [
        '/dispatcher/driver',
        this.driver!.uid,
        'suggest-requests',
        fn(this.selectedDate$.value, 1).valueOf(),
        view,
      ],
      {
        replaceUrl: true,
      },
    );
  }
}

function sortByDistance(
  a: RideRequest & { distance?: number },
  b: RideRequest & { distance?: number },
) {
  if (a.distance === b.distance) return 0;
  if (a.distance === undefined) return 1;
  if (b.distance === undefined) return -1;

  return numberComparer(a.distance, b.distance);
}
