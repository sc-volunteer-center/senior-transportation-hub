import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  Directive,
} from '@angular/core';
import { RideRequest } from '@local/models';
import { combineLatest, of, Subscription } from 'rxjs';
import { format } from 'date-fns';
import { IsLoadingService } from '@service-work/is-loading';
import { Router } from '@angular/router';
import { EmailService } from 'apps/transportation-hub/src/app/modules/shared/email.service';
import { Title } from '@angular/platform-browser';
import {
  endOfWeek,
  navigateToPathFn,
  ngForTrackByID,
  startOfWeek,
} from 'apps/transportation-hub/src/app/utilities';
import { FindRequestForVolunteerService } from './find-request-for-volunteer.service';
import { MatSort } from '@angular/material/sort';
import { formatDate } from '@angular/common';
import { map, startWith, switchMap, tap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import {
  MatDatepicker,
  MatDatepickerInputEvent,
} from '@angular/material/datepicker';
import { FirestoreService } from 'apps/transportation-hub/src/app/firestore.service';
import { FormControl } from '@angular/forms';

@Directive({
  selector: 'app-find-request-for-volunteer-base',
})
export abstract class FindRequestForVolunteerComponentBase implements OnInit {
  rideRequests: RideRequest[] = [];
  selectedDate: Date = new Date();

  filterOptions: Array<'unassignedOnly' | 'map'> = [];
  filterOptionsControl = new FormControl([]);

  protected abstract readonly dateFormatter: (date: Date) => string;
  protected abstract readonly view: 'month' | 'week' | 'day';

  @ViewChild(MatSort) protected matSort: MatSort;
  protected subscriptions: Subscription[] = [];

  constructor(
    protected emailService: EmailService,
    protected cdr: ChangeDetectorRef,
    public findRequestForVolunteerService: FindRequestForVolunteerService,
    protected titleService: Title,
    protected router: Router,
    protected isLoadingService: IsLoadingService,
    protected routerStore: RouterStoreService,
    protected fs: FirestoreService,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    // need to mark 'ride-requests' as loading from OnInit to
    // AfterViewInit because otherwise
    // ('ride-requests' | swIsLoading | async) === false briefly
    // in the template which causes the mat-table to initialize
    // and then a tick later it is destroyed which causes
    // swScrollPosition to erase the "good" scroll position
    // and set a scroll position of 0.
    this.isLoadingService.add({ key: 'find-request-for-volunteer' });

    this.filterOptionsControl.patchValue([
      ...this.findRequestForVolunteerService.viewOptions$.value,
    ]);

    this.subscriptions.push(
      this.filterOptionsControl.valueChanges
        .pipe(startWith(this.filterOptionsControl.value))
        .subscribe((options: typeof this['filterOptions']) => {
          this.findRequestForVolunteerService.viewOptions$.next([...options]);
        }),
    );
  }

  ngAfterViewInit() {
    this.isLoadingService.remove({ key: 'find-request-for-volunteer' });

    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(
          switchMap((id) =>
            !id
              ? of([null, null])
              : combineLatest([
                  this.fs.getDriver(id),
                  this.findRequestForVolunteerService.selectedDate$,
                ]),
          ),
        )
        .subscribe(([driver, date]) => {
          if (!driver || !date) return;

          this.titleService.setTitle(
            `Find Request, ${this.dateFormatter(date)} | Volunteer ${
              driver?.nameWithRetiredStatus
            } | Hub`,
          );
        }),
      this.findRequestForVolunteerService
        .getRideRequests(this.view)
        .subscribe((rideRequests) => {
          this.rideRequests = rideRequests;
          this.findRequestForVolunteerService.rideRequestCount$.next(
            rideRequests.length,
          );
          this.sortRideRequests();
        }),
    );

    this.subscriptions.push(
      this.emailService.selectedRideRequests.subscribe(() => {
        this.cdr.detectChanges();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  sortRideRequests() {
    const event = {
      // Not sure why this is needed but it solves a bug where
      // `this.matSort` is `undefined`.
      active: (this.matSort && this.matSort.active) || 'distance',
      direction: (this.matSort && this.matSort.direction) || 'desc',
    };

    this.rideRequests = this.findRequestForVolunteerService.sortRideRequests(
      this.rideRequests,
      event,
    );

    this.cdr.detectChanges();
  }

  displaySelectedDate() {
    return this.findRequestForVolunteerService.selectedDate$.pipe(
      tap((date) => {
        this.selectedDate = date;
        this.cdr.markForCheck();
      }),
      switchMap((date) =>
        this.routerStore
          .get$((store) => store.url)
          .pipe(
            map(() => {
              if (this.view === 'month') {
                return `Month of ${format(date, 'MMMM yyyy')}`;
              } else if (this.view === 'week') {
                const start = format(startOfWeek(date), 'MMM d');
                const end = format(endOfWeek(date), 'MMM d, yyyy');

                return `Week of ${start} - ${end}`;
              } else {
                return format(date, 'EEEE, MMM d, yyyy');
              }
            }),
          ),
      ),
    );
  }

  selectDate(datepicker: MatDatepicker<Date>) {
    datepicker.opened ? datepicker.close() : datepicker.open();
  }

  dateSelected(event: MatDatepickerInputEvent<Date>) {
    if (!event.value || !this.findRequestForVolunteerService.driver) return;

    this.isLoadingService.remove({ key: 'find-request-for-volunteer' });

    this.router.navigate([
      '/dispatcher/driver',
      this.findRequestForVolunteerService.driver.uid,
      'suggest-requests',
      event.value.valueOf(),
      this.view,
    ]);
  }

  toggleUnassignedOnly() {
    const unassignedOnly =
      this.filterOptionsControl.value.includes('unassignedOnly');

    this.filterOptionsControl.patchValue(
      unassignedOnly ? [] : ['unassignedOnly'],
    );
  }
}

@Component({
  selector: 'app-find-request-for-volunteer-day-view',
  templateUrl: './find-request-for-volunteer-view.component.html',
  styleUrls: ['./find-request-for-volunteer-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'ride-requests-view',
  },
  providers: [FindRequestForVolunteerService],
})
export class FindRequestForVolunteerDayViewComponent extends FindRequestForVolunteerComponentBase {
  protected readonly view = 'day';
  protected readonly dateFormatter = (date: Date) =>
    formatDate(date, 'shortDate', 'en-US');
}

@Component({
  selector: 'app-find-request-for-volunteer-week-view',
  templateUrl: './find-request-for-volunteer-view.component.html',
  styleUrls: ['./find-request-for-volunteer-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'ride-requests-view',
  },
  providers: [FindRequestForVolunteerService],
})
export class FindRequestForVolunteerWeekViewComponent extends FindRequestForVolunteerComponentBase {
  protected readonly view = 'week';
  protected readonly dateFormatter = (date: Date) => {
    const start = format(startOfWeek(date), 'MMM d');
    const end = format(endOfWeek(date), 'MMM d, yyyy');
    return `${start} - ${end}`;
  };
}

@Component({
  selector: 'app-find-request-for-volunteer-month-view',
  templateUrl: './find-request-for-volunteer-view.component.html',
  styleUrls: ['./find-request-for-volunteer-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'ride-requests-view',
  },
  providers: [FindRequestForVolunteerService],
})
export class FindRequestForVolunteerMonthViewComponent extends FindRequestForVolunteerComponentBase {
  protected readonly view = 'month';
  protected readonly dateFormatter = (date: Date) =>
    formatDate(date, 'MMMM yyyy', 'en-US');
}
