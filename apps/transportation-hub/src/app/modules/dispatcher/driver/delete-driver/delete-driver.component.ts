import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { Observable } from 'rxjs';
import { Dispatcher } from '@local/models';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { toPromise } from '../../../../utilities';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-delete-driver',
  templateUrl: './delete-driver.component.html',
  styleUrls: ['./delete-driver.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteDriverComponent implements OnInit {
  dispatcher: Observable<Dispatcher | null>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: { driverId: string },
    private fs: FirestoreService,
    private router: Router,
    private ref: MatDialogRef<DeleteDriverComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.dispatcher = this.fs.getCurrentDispatcher();
  }

  async delete() {
    const driver = await toPromise(this.fs.getDriver(this.data.driverId));

    if (!this.fs.dispatcher || !driver || !driver.retiredAt) {
      return;
    }

    await this.loadingService.add(this.fs.destroyDriver(this.data.driverId), {
      key: 'delete-driver',
    });

    this.router.navigate(['/dispatcher/drivers', new Date().valueOf(), 'week']);

    this.ref.close();
  }
}
