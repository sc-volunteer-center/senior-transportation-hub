import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
} from '@angular/core';
import { FirestoreService } from '../../../../firestore.service';
import { Driver } from '@local/models';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, NavigationEnd } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { timestamp } from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-retire-driver',
  templateUrl: './retire-driver.component.html',
  styleUrls: ['./retire-driver.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RetireDriverComponent implements OnInit {
  driver: Driver | null = null;
  retirementForm: FormGroup | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private ref: MatDialogRef<RetireDriverComponent>,
    private router: Router,
    private loadingService: IsLoadingService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    private data: { driverId: string; dispatcherId: string },
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.router.events
        .pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => this.ref.close()),
    );

    this.subscriptions.push(
      this.fs.getDriver(this.data.driverId).subscribe((driver) => {
        this.driver = driver;
        this.cdr.markForCheck();

        if (!driver) {
          this.ref.close();
          return;
        }

        this.retirementForm = this.fb.group({
          retiredReason: ['', Validators.required],
        });
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  close() {
    this.ref.close();
  }

  async submit() {
    if (!this.retirementForm || this.retirementForm.invalid || !this.driver) {
      return;
    }

    const form = this.retirementForm.value as {
      retiredReason: string;
    };

    await this.loadingService.add(
      Promise.all([
        this.fs.updateDriver(this.driver.uid, {
          ...form,
          retiredAt: timestamp(),
          retiredById: this.data.dispatcherId,
        }),
        this.fs.addThread({
          driverId: this.driver.uid,
          flag: false,
          text: `DRIVER RETIRED:\n` + form.retiredReason,
        }),
      ]),
      { key: 'retire-client' },
    );

    this.ref.close();
  }
}
