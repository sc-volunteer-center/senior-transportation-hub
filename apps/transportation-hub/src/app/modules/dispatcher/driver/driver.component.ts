import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { isNavigationOver } from '../../../utilities';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DriverComponent implements OnInit {
  activeRoute = 'profile';
  now = new Date();

  private subscriptions: Subscription[] = [];

  constructor(private router: Router, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.subscriptions.push(
      this.router.events.pipe(filter(isNavigationOver)).subscribe(() => {
        this.activeRoute = this.router.url.split('/').pop()!;

        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submit() {}
}
