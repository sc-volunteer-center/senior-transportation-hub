import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Driver } from '@local/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { Timestamp } from '../../../../utilities';
import { MatDialogRef } from '@angular/material/dialog';
import { startOfToday } from 'date-fns';
import { IsLoadingService } from '@service-work/is-loading';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-edit-driver-vacation',
  templateUrl: './edit-driver-vacation.component.html',
  styleUrls: ['./edit-driver-vacation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditDriverVacationComponent implements OnInit {
  driver: Driver | null = null;
  driverForm?: FormGroup;

  hasUpcomingVacation = false;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<EditDriverVacationComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDriver(id))))
        .subscribe((driver) => {
          this.driver = driver;
          this.driverForm = undefined;
          this.cdr.markForCheck();

          if (!driver) {
            return;
          }

          this.hasUpcomingVacation =
            driver.vacationStartsAt.valueOf() >= new Date().valueOf();

          const vacationStartsAt =
            driver.isOnVacation() || this.hasUpcomingVacation
              ? driver.vacationStartsAt
              : '';
          const vacationEndsAt =
            driver.isOnVacation() || this.hasUpcomingVacation
              ? driver.vacationEndsAt
              : '';

          this.driverForm = this.fb.group({
            vacationStartsAt: [vacationStartsAt, Validators.required],
            vacationEndsAt: [vacationEndsAt, Validators.required],
          });
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async cancelVacation() {
    if (!this.driver) {
      return;
    }

    await this.loadingService.add(
      Promise.all([
        this.fs.updateDriver(this.driver!.uid, {
          vacationStartsAt: startOfToday() as any as Timestamp,
          vacationEndsAt: startOfToday() as any as Timestamp,
          vacationingById: null,
        }),
        this.fs.addThread({
          driverId: this.driver!.uid,
          flag: false,
          text: `VACATION ENDED EARLY`,
        }),
      ]),
      { key: 'edit-driver-vacation' },
    );

    this.ref.close();
  }

  async submit() {
    if (!this.driverForm || this.driverForm.invalid || !this.fs.dispatcher) {
      return;
    }

    const { vacationStartsAt, vacationEndsAt } = this.driverForm!.value;

    await this.loadingService.add(
      Promise.all([
        this.fs.updateDriver(this.driver!.uid, {
          vacationStartsAt,
          vacationEndsAt,
          vacationingById: this.fs.dispatcher.uid,
        }),
        this.fs.addThread({
          driverId: this.driver!.uid,
          flag: false,
          text:
            `ON VACATION:\n` +
            `${formatDate(
              vacationStartsAt,
              'shortDate',
              'en-US',
            )} - ${formatDate(vacationEndsAt, 'shortDate', 'en-US')}`,
        }),
      ]),
      { key: 'edit-driver-vacation' },
    );

    this.ref.close();
  }
}
