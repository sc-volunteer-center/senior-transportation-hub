import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { RideRequest, Destination } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { switchMap, tap, map, filter } from 'rxjs/operators';
import { startOfMonth, endOfMonth, subMonths, addMonths } from 'date-fns';
import {
  MatDatepickerInputEvent,
  MatDatepicker,
} from '@angular/material/datepicker';
import { IsLoadingService } from '@service-work/is-loading';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailService } from 'apps/transportation-hub/src/app/modules/shared/email.service';
import { Title } from '@angular/platform-browser';
import {
  navigateToPathFn,
  isTruthy,
  ngForTrackByID,
} from 'apps/transportation-hub/src/app/utilities';

@Component({
  selector: 'app-driver-ride-requests',
  templateUrl: './driver-ride-requests.component.html',
  styleUrls: ['./driver-ride-requests.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DriverRideRequestsComponent implements OnInit {
  rideRequests: RideRequest[] = [];
  rideRequestsDate?: Date;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private isLoadingService: IsLoadingService,
    private router: Router,
    private route: ActivatedRoute,
    public emailService: EmailService,
    private titleService: Title,
  ) {}

  navigateToPath = navigateToPathFn(this.router);
  trackBy = ngForTrackByID;

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.driverId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDriver(id))))
        .subscribe((driver) => {
          this.titleService.setTitle(
            `Volunteer Requests | ${driver?.nameWithRetiredStatus} | Hub`,
          );
        }),
      this.routerStore
        .get$((store) => store.state.params.rideRequestsDate)
        .pipe(
          filter(isTruthy),
          map((timestamp: string) => new Date(parseInt(timestamp, 10))),
          tap((date) => {
            this.rideRequestsDate = new Date(date);
          }),
          switchMap((date) => {
            const obs = this.routerStore
              .get$((store) => store.state.params.driverId)
              .pipe(
                switchMap((driverId) =>
                  !driverId
                    ? of([])
                    : this.fs.getRideRequests({
                        driverId,
                        startAfterOrOnDate: startOfMonth(date),
                        endBeforeDate: endOfMonth(date),
                      }),
                ),
              );

            return this.isLoadingService.add(obs, {
              key: 'driver-ride-requests',
            });
          }),
        )
        .subscribe((rideRequests) => {
          this.rideRequests = rideRequests;
          this.cdr.markForCheck();
        }),
      this.emailService.selectedRideRequests.subscribe(() => {
        this.cdr.detectChanges();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  nextDate() {
    if (!this.rideRequestsDate) return;

    const date = addMonths(startOfMonth(this.rideRequestsDate), 1);

    this.router.navigate(['../', date.valueOf()], {
      relativeTo: this.route,
      replaceUrl: true,
    });
  }

  prevDate() {
    if (!this.rideRequestsDate) return;

    const date = subMonths(startOfMonth(this.rideRequestsDate), 1);

    this.router.navigate(['../', date.valueOf()], {
      relativeTo: this.route,
      replaceUrl: true,
    });
  }

  openDateSelector(datepicker: MatDatepicker<Date>) {
    datepicker.opened ? datepicker.close() : datepicker.open();
  }

  dateSelected(event: MatDatepickerInputEvent<Date>) {
    if (!event.value) {
      return;
    }

    this.router.navigate(['../', event.value.valueOf()], {
      relativeTo: this.route,
    });
  }

  destinationSummary: (destinations: Destination[]) => string = (() => {
    const prevDestinations: Destination[] = [];
    let summary = '';

    return (destinations: Destination[]) => {
      if (
        prevDestinations.length === destinations.length &&
        prevDestinations[0] === destinations[0]
      ) {
        return summary;
      }

      summary = destinations[0].toString();

      if (destinations.length === 2) {
        summary = `${summary} and 1 other location`;
      } else if (destinations.length > 2) {
        summary = `${summary} and ${destinations.length - 1} other locations`;
      }

      return summary;
    };
  })();

  clientName(rideRequest: RideRequest) {
    if (rideRequest.requestCancelledAt) return 'CANCELLED';

    return rideRequest.client.nameWithRetiredStatus;
  }
}
