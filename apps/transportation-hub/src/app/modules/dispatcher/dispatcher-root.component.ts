import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterViewInit,
  ViewEncapsulation,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Dispatcher, IOrganization } from '@local/models';
import { Subscription } from 'rxjs';
import { FirestoreService } from '../../firestore.service';
import { Router } from '@angular/router';
import { wait } from '@local/utilities';

@Component({
  selector: 'app-dispatcher-root',
  template: `
    <app-header class="mat-elevation-z8" [dispatcher]="dispatcher">
    </app-header>

    <mat-sidenav-container class="flex-1">
      <mat-sidenav
        mode="side"
        opened
        disableClose
        position="start"
        class="mat-elevation-z8"
        style="width: 200px;"
      >
        <app-sidebar
          [organization]="organization"
          [dispatcher]="dispatcher"
        ></app-sidebar>
      </mat-sidenav>

      <mat-sidenav-content>
        <router-outlet></router-outlet>
      </mat-sidenav-content>
    </mat-sidenav-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'flex-column',
  },
})
export class DispatcherRootComponent implements OnInit, OnDestroy {
  // user: IUser | null = null;
  organization: IOrganization | null = null;
  dispatcher: Dispatcher | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private router: Router,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.fs.getOrganization().subscribe(async (org) => {
        this.organization = org;

        if (!org) {
          await this.fs.endDispatcherShift();
          // need to wait for the cookieService to pick up the changes
          await wait(1010);

          this.router.navigate(['dispatcher/signin']);
        }

        this.cdr.markForCheck();
      }),
      this.fs.getCurrentDispatcher().subscribe((disp) => {
        if (!disp) {
          this.router.navigate(['dispatcher/signin']);
        }

        this.dispatcher = disp;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
