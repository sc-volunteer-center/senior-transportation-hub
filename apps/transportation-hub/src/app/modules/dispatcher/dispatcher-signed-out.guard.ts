import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap, switchMap } from 'rxjs/operators';
import { FirestoreService } from '../../firestore.service';

@Injectable({
  providedIn: 'root',
})
export class DispatcherSignedOutGuard implements CanActivate {
  constructor(private fs: FirestoreService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.fs.getOrganization().pipe(
      switchMap((org) =>
        !org
          ? of(true)
          : this.fs.getCurrentDispatcher().pipe(map((disp) => !disp)),
      ),
      tap((passing) => {
        if (!passing) {
          this.router.navigate(['/dispatcher/overview']);
        }
      }),
    );
  }
}
