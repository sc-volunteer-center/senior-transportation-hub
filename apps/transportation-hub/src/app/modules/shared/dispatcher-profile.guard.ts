import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreService } from '../../firestore.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DispatcherProfileGuard implements CanActivate {
  constructor(private fs: FirestoreService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!next.params['dispatcherId']) {
      return false;
    }

    return this.fs.getDispatcher(next.params['dispatcherId']).pipe(
      map(driver => !!driver),
      tap(passing => {
        if (!passing) {
          this.router.navigate(['../../dispatchers']);
        }
      }),
    );
  }
}
