import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-warning',
  template: `
    <h3 mat-dialog-title>{{ data.title }}</h3>

    <mat-dialog-content>
      <div
        style="max-width: 600px"
        [innerHTML]="data.text.replaceAll(newLineChar, '<br />')"
      ></div>
    </mat-dialog-content>

    <mat-dialog-actions>
      <button mat-button type="button" matDialogClose>Cancel</button>

      <div class="flex-1"></div>

      <button mat-raised-button color="accent" (click)="accept()">
        {{ data.affirmative || 'Gotcha' }}
      </button>
    </mat-dialog-actions>
  `,
  styleUrls: ['./warning.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WarningComponent implements OnInit {
  newLineChar = '\n';

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { title: string; text: string; affirmative?: string },
    private ref: MatDialogRef<WarningComponent>,
  ) {}

  ngOnInit() {}

  accept() {
    this.ref.close(true);
  }
}
