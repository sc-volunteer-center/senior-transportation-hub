import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WarningComponent } from './warning.component';

@Injectable({ providedIn: 'root' })
export class WarningService {
  constructor(private dialog: MatDialog) {}

  popup(args: { title: string; text: string; affirmative?: string }) {
    return this.dialog.open(WarningComponent, {
      data: args,
    });
  }

  error(code: number | string) {
    return this.popup({
      title: `Error`,
      text: `
        Oops, looks like you've encountered a bug! 
        You should reach out to John to let him know that he should fix this.
        He'll want to know what you were doing when the bug happened so that 
        he can try and figure out what went wrong.

        He'll also want to know the error code.

        Error code: "${code}"
      `,
    });
  }
}
