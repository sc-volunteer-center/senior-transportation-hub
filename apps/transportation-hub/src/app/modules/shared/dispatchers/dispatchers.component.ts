import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { Subscription, BehaviorSubject, of, combineLatest } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { Dispatcher } from '@local/models';
import {
  distinctUntilChanged,
  switchMap,
  debounceTime,
  map,
  startWith,
} from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { stringComparer } from '../../../utilities';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dispatchers',
  templateUrl: './dispatchers.component.html',
  styleUrls: ['./dispatchers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DispatchersComponent implements AfterViewInit, OnDestroy {
  allDispatchers: Dispatcher[] = [];
  dispatchers: Dispatcher[] = [];
  retiredDispatchers: Dispatcher[] = [];
  showRetiredDispatchers = new BehaviorSubject(false);

  searchControl = new FormControl('');

  filteredText = '';
  filteredDispatchers: Dispatcher[] = [];

  @ViewChild(MatSort, { static: true }) private matSort: MatSort;
  private subscriptions: Subscription[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private firestore: FirestoreService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
  ) {}

  ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get('search') || '';
    this.titleService.setTitle(`Dispatchers | Hub`);

    this.searchControl.patchValue(query);

    this.subscriptions.push(
      this.searchControl.valueChanges
        .pipe(debounceTime(500))
        .subscribe((value) => {
          this.router.navigate([], {
            queryParams: {
              search: value.toLowerCase(),
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
      this.route.queryParamMap.subscribe((params) => {
        const search = params.get('search') || '';
        this.searchControl.patchValue(search, { emitEvent: false });
        this.filterDispatchers(search);
        this.showRetiredDispatchers.next(params.get('retired') === 'true');
      }),
    );
  }

  ngAfterViewInit() {
    this.subscriptions.push(
      this.firestore
        .getDispatchers({ retired: false })
        .subscribe((dispatchers) => {
          this.dispatchers = dispatchers;
          this.processDispatchers();
        }),
    );

    this.subscriptions.push(
      combineLatest([
        this.searchControl.valueChanges.pipe(
          startWith(this.searchControl.value),
        ),
        this.showRetiredDispatchers,
      ])
        .pipe(
          map(([search, showRetired]) => showRetired || search),
          distinctUntilChanged(),
          switchMap((value) =>
            value ? this.firestore.getDispatchers({ retired: true }) : of([]),
          ),
        )
        .subscribe((dispatchers) => {
          this.retiredDispatchers = dispatchers;
          this.processDispatchers();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  displayActiveStatus(dispatcher: Dispatcher) {
    return dispatcher.active ? 'Yes' : 'Retired';
  }

  filterDispatchers(text?: string) {
    this.filteredText = (text && text.toLowerCase()) || '';

    const searchText = this.filteredText;

    this.filteredDispatchers = this.allDispatchers.filter((dispatcher) =>
      dispatcher.name.toLowerCase().includes(searchText),
    );

    this.cdr.detectChanges();
  }

  toggleRetiredDispatchers(event: MatCheckboxChange) {
    this.router.navigate([], {
      queryParams: { retired: event.checked },
      queryParamsHandling: 'merge',
      replaceUrl: true,
    });
  }

  sortDispatchers() {
    const column = this.matSort.active || 'name';
    const direction = this.matSort.direction || 'desc';
    const clients = this.allDispatchers.slice();
    const fn =
      column === 'name'
        ? (a: Dispatcher, b: Dispatcher) => stringComparer(a.name, b.name)
        : (a: Dispatcher, b: Dispatcher) => {
            if (a.active === b.active) return stringComparer(a.name, b.name);
            else if (a.active) return 1;
            else return -1;
          };

    clients.sort(fn);

    if (direction === 'desc') {
      clients.reverse();
    }

    this.allDispatchers = clients;

    this.filterDispatchers(this.filteredText);
  }

  private processDispatchers() {
    this.allDispatchers = [...this.dispatchers, ...this.retiredDispatchers];
    this.filteredDispatchers = this.allDispatchers;
    this.sortDispatchers();
  }
}
