import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-dispatchers-breadcrumb',
  template: `
    Dispatchers
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DispatchersBreadcrumbComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
