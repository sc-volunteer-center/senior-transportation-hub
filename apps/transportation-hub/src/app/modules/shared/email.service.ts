import {
  Injectable,
  ChangeDetectorRef,
  Inject,
  forwardRef,
} from '@angular/core';
import { BehaviorSubject, Subscription, combineLatest } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { generateRequestDetailText } from '@local/utilities';

import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { toPromise } from '../../utilities';
import { FirestoreService } from '../../firestore.service';
import { RideRequest } from '@local/models';
import { isBefore, isAfter } from 'date-fns';
import { MapsService } from '../../maps.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Clipboard } from '@angular/cdk/clipboard';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { formatFlexibleRequestDatetime } from './flexible-request-datetime.pipe';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-which-ride-request',
  template: `
    <div mat-dialog-title>
      <h3 style="margin-top: 0;">
        Do you want to recruit an "one-time" volunteer or an "ongoing" volunteer
        for this request?
      </h3>
    </div>

    <mat-dialog-content style="height: 75px;">
      <mat-radio-group
        [formControl]="form"
        (change)="submit()"
        class="flex-column"
        color="primary"
      >
        <mat-radio-button value="ONCE" style="margin-bottom: 1rem">
          Recruit a one-time volunteer
        </mat-radio-button>

        <mat-radio-button value="RECURRING">
          Recruit an ongoing volunteer
        </mat-radio-button>
      </mat-radio-group>
    </mat-dialog-content>
  `,
  styles: [''],
})
export class RecruitForWhichRideRequestComponent {
  form = new FormControl('', Validators.required);

  constructor(private ref: MatDialogRef<RecruitForWhichRideRequestComponent>) {}

  submit() {
    if (!this.form.valid) return;
    this.ref.close(this.form.value);
  }
}

@Component({
  selector: 'app-selected-ride-requests',
  template: `
    <span style="margin-right: 1rem;">
      {{ count }} ride {{ count === 1 ? 'request' : 'requests' }} selected
    </span>

    <button
      mat-icon-button
      matTooltip="generate email and copy it to the clipboard"
      (click)="composeEmail()"
    >
      <mat-icon>email</mat-icon>
    </button>

    <button
      mat-icon-button
      matTooltip="clear selected ride requests"
      (click)="clear()"
    >
      <mat-icon>close</mat-icon>
    </button>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedRideRequestsComponent implements OnInit, OnDestroy {
  count = 0;

  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(forwardRef(() => EmailService)) private emailService: EmailService,
    private cdr: ChangeDetectorRef,
    private isLoadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.emailService.selectedRideRequests.subscribe((set) => {
        this.count = set.size;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  composeEmail() {
    this.isLoadingService.add(this.emailService.composeEmail());
  }

  clear() {
    this.emailService.clearRideRequests();
  }
}

@Injectable({
  providedIn: 'root',
})
export class EmailService {
  // key is the request ID and the boolean is recurring yes/no
  selectedRideRequests = new BehaviorSubject<Map<string, boolean>>(new Map());

  constructor(
    private matSnackbar: MatSnackBar,
    private fs: FirestoreService,
    private mapsService: MapsService,
    private fireAuth: AngularFireAuth,
    private clipboardService: Clipboard,
    private dialog: MatDialog,
  ) {
    this.selectedRideRequests.subscribe((set) => {
      if (set.size === 0) {
        this.matSnackbar.dismiss();
      } else if (!this.matSnackbar._openedSnackBarRef) {
        this.matSnackbar.openFromComponent(SelectedRideRequestsComponent, {
          horizontalPosition: 'center',
        });
      }
    });
  }

  isRideRequestSelected(id: string) {
    return this.selectedRideRequests.value.has(id);
  }

  requestSelected(r: RideRequest) {
    return this.isRideRequestSelected(r.uid)
      ? this.deselectRideRequest(r)
      : this.selectRideRequest(r);
  }

  async selectRideRequest(r: RideRequest) {
    const result = await this.recruitForWhichRideRequest(r);

    if (!result) return;

    const selected = this.selectedRideRequests.value;
    selected.set(r.uid, result === 'RECURRING');
    this.selectedRideRequests.next(selected);
  }

  async deselectRideRequest(r: RideRequest) {
    const selected = this.selectedRideRequests.value;
    selected.delete(r.uid);
    this.selectedRideRequests.next(selected);
  }

  clearRideRequests() {
    const selected = this.selectedRideRequests.value;
    selected.clear();
    this.selectedRideRequests.next(selected);
  }

  async composeEmail() {
    const org = this.fs.organization;

    if (!org) return;

    const user = await toPromise(this.fireAuth.user);

    if (!user) return;

    const data = user.providerData.find(
      (data) => !!(data && data.providerId === 'google.com'),
    );

    if (!data) return;

    const selectedRideRequests = this.selectedRideRequests.value;

    if (selectedRideRequests.size === 0) return;

    const ids: string[] = Array.from(selectedRideRequests.keys());

    const rideRequests = (
      await toPromise(
        combineLatest(ids.map((id) => this.fs.getRideRequest(id))),
      )
    ).filter((request) => !!request) as RideRequest[];

    rideRequests.sort((a, b) => {
      if (isBefore(a.datetime, b.datetime)) {
        return -1;
      } else if (isAfter(a.datetime, b.datetime)) {
        return 1;
      } else {
        return 0;
      }
    });

    const email = await generateRequestDetailText(
      rideRequests,
      this.buildTitle.bind(this),
    );

    this.clearRideRequests();

    const pendingCopy = this.clipboardService.beginCopy(email);

    pendingCopy.copy();
    pendingCopy.destroy();

    this.matSnackbar.open(
      'Email text copied! Now paste it into an email.',
      undefined,
      {
        duration: 2000,
      },
    );
  }

  private async buildTitle(r: RideRequest, order: number) {
    const recurring = this.selectedRideRequests.value.get(r.uid);

    if (recurring === undefined) {
      throw new Error('Could not find selected ride request??? ' + r.uid);
    }

    let datetimeString: string;

    if (r.flexible) {
      datetimeString =
        'the week of ' + formatFlexibleRequestDatetime(r.datetime);
    } else {
      datetimeString = 'on ' + format(r.datetime, 'dddd, MMMM D, YYYY @ h:mmA');
    }

    datetimeString += `\n- - -\n\n`;

    if (!recurring) {
      return `- - - - - -\n${order}.  ${r.client.name} ONE TIME ${datetimeString}`;
    }

    const rr = await toPromise(
      this.fs.getRecurringRideRequest(r.recurringRideRequestId!),
    );

    if (!rr) {
      throw new Error(
        'Could not find selected recurring ride request??? ' +
          r.recurringRideRequestId,
      );
    }

    return (
      `- - - - - -\n${order}.  ${r.client.name} ` +
      `${rr.scheduleName.toUpperCase()} starting ${datetimeString}`
    );
  }

  private async recruitForWhichRideRequest(
    r: RideRequest,
  ): Promise<'ONCE' | 'RECURRING' | undefined> {
    if (!r.recurringRideRequestId) return 'ONCE';

    return this.dialog
      .open(RecruitForWhichRideRequestComponent, {
        width: '600px',
      })
      .afterClosed()
      .toPromise();
  }
}

function format(date: Date, fmt: string) {
  return moment.tz(date, 'America/Los_Angeles').format(fmt);
}
