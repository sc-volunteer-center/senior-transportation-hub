import {
  Component,
  OnInit,
  Input,
  HostBinding,
  Optional,
  Self,
  ElementRef,
  Renderer2,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatFormFieldControl } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { NgControl, FormControl, ControlValueAccessor } from '@angular/forms';
import { FocusMonitor } from '@angular/cdk/a11y';

import { Subject, Subscription } from 'rxjs';

export interface AutocompleteChipsOption {
  id: string;
  label: string;
  color?: string;
}

@Component({
  selector: 'form-autocomplete-chips-input',
  template: `
    <mat-form-field appearance="fill" style="width: 100%;">
      <mat-label>{{ placeholder }}</mat-label>

      <mat-chip-list #chipList>
        <mat-chip
          *ngFor="let option of value"
          [ngClass]="addMatChipClasses(option)"
          [selectable]="false"
          [removable]="true"
          (removed)="removeChip(option)"
        >
          <span>{{ option.label }}</span>
          <mat-icon matChipRemove>cancel</mat-icon>
        </mat-chip>

        <input
          matInput
          #searchInput
          [matChipInputFor]="chipList"
          [matAutocomplete]="autoComplete"
          [matChipInputAddOnBlur]="true"
          [disabled]="disabled"
          (keyup)="search(searchInput.value)"
          (blur)="clearInput(searchInput); _onTouched()"
        />
      </mat-chip-list>

      <mat-autocomplete
        #autoComplete="matAutocomplete"
        (optionSelected)="addChip($event, searchInput)"
        [ngSwitch]="whatShouldAutocompleteDisplay()"
      >
        <mat-option
          *ngSwitchCase="'LOADING'"
          [value]=""
          style="pointer-events: none;"
        >
          <span>{{ loadingMessage }}</span>
        </mat-option>

        <mat-option
          *ngSwitchCase="'NO_OPTIONS'"
          [value]=""
          style="pointer-events: none;"
        >
          <span>{{ noOptionsMessage }}</span>
        </mat-option>

        <ng-template [ngSwitchCase]="'RESULTS'">
          <mat-option *ngFor="let chip of _filteredOptions" [value]="chip">
            <span>{{ chip.label }}</span>
          </mat-option>
        </ng-template>
      </mat-autocomplete>
    </mat-form-field>
  `,
  styleUrls: ['./autocomplete-chips-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: AutocompleteChipsInputComponent,
    },
  ],
})
export class AutocompleteChipsInputComponent
  implements
    OnInit,
    MatFormFieldControl<AutocompleteChipsOption[]>,
    ControlValueAccessor {
  static nextId = 0;
  @HostBinding()
  id = `form-autocomplete-chips-input-${AutocompleteChipsInputComponent.nextId++}`;

  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @HostBinding('attr.aria-describedby') describedBy = '';
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  @ViewChild(MatInput, { static: true }) private matInput: MatInput;

  // required by MatFormFieldControl interface
  onContainerClick() {}

  public stateChanges = new Subject<void>();
  public focused = false;
  public errorState = false;
  private firstTouch = true; // when true, errorState suppressed
  public readonly controlType = 'sw-autocomplete-chips-input';
  /**
   * @hidden
   */
  private propogateChange: (_: any) => void = () => {};
  /**
   * @hidden
   */
  public _onTouched: () => void = () => {};

  writeValue(value: any): void {
    this.handleInput(value);
  }

  registerOnChange(fn: (_: any) => void): void {
    this.propogateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.renderer.setAttribute(this.el.nativeElement, 'disabled', '');
    } else {
      this.renderer.removeAttribute(this.el.nativeElement, 'disabled');
    }
    this.disabled = isDisabled;
  }

  get empty() {
    return this.value.length === 0;
  }

  @Input()
  get required() {
    return this._required;
  }
  set required(req) {
    this._required = !!req;
    this.stateChanges.next();
  }
  private _required = false;

  @Input()
  get disabled() {
    return this._disabled;
  }
  set disabled(dis) {
    this._disabled = coerceBooleanInput(dis);
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }
  private _placeholder = '';

  @Input()
  get value(): AutocompleteChipsOption[] {
    return this._value;
  }
  set value(options: AutocompleteChipsOption[]) {
    this.handleInput(options);
    this.stateChanges.next();
  }
  private _value: AutocompleteChipsOption[] = [];

  get options() {
    return this._options;
  }
  @Input() set options(value: AutocompleteChipsOption[]) {
    this._options = value;
    this.filterOptions();
    this.stateChanges.next();
  }
  private _options: AutocompleteChipsOption[] = [];

  @Input() loading = false;
  @Input() loadingMessage = 'Loading...';
  @Input() noOptionsMessage = 'No options available';

  private searchText = '';
  public _filteredOptions: AutocompleteChipsOption[] = [];

  private subscriptions: Subscription[] = [];

  constructor(
    private fm: FocusMonitor,
    private el: ElementRef,
    @Optional()
    @Self()
    public ngControl: NgControl,
    private renderer: Renderer2,
    private ref: ChangeDetectorRef,
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.subscriptions.push(
      this.fm.monitor(this.el.nativeElement, true).subscribe(origin => {
        this.focused = !!origin;
        if (!this.focused) {
          this.firstTouch = false;
        }
        this.calculateErrorState();
        this.stateChanges.next();
      }),
    );
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.el.nativeElement);
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  search(text: string | any) {
    if (typeof text !== 'string') {
      return;
    }
    this.searchText = text.toLowerCase();
    this.filterOptions();
  }

  clearInput(input: HTMLInputElement) {
    input.value = '';
    this.filterOptions();
  }

  addChip(event: MatAutocompleteSelectedEvent, input: HTMLInputElement): void {
    // Define selection constant
    const selection = event.option.value as AutocompleteChipsOption;
    // Add chip for selected option

    if (selection) {
      this.clearInput(input);
      this.handleInternalValueChange([...this.value, selection]);
    }
  }

  removeChip(chip: AutocompleteChipsOption) {
    // when the MatFormField loses focus, it automaically recalculates disabled state
    // this happens right before removeChip is run, causing a ChangedAfterChecked error
    // when handleChange() recalculates disabled state. One solution is to keep focus
    // within the MatFormField by switching it to MatInput `this.matInput.focus()`.
    // `setTimout` doesn't seem to fix it.
    this.matInput.focus();
    // Find key of object in array
    const index = this.value.indexOf(chip);
    // If key exists
    if (index >= 0) {
      // Remove key from chips array
      const options: AutocompleteChipsOption[] = this.value;
      options.splice(index, 1);
      this.handleInternalValueChange(options);
    }
  }

  whatShouldAutocompleteDisplay() {
    if (this.loading) {
      return 'LOADING';
    } else if (this._filteredOptions.length === 0) {
      return 'NO_OPTIONS';
    } else {
      return 'RESULTS';
    }
  }

  addMatChipClasses(option: AutocompleteChipsOption) {
    if (!option.color) return '';

    return `chip-bg-color-${option.color.replace(' ', '-')}`;
  }

  private filterOptions() {
    // Set filteredOptions array to options that are not already chosen
    this._filteredOptions = this.options.filter(
      option => !this.value.some(chip => chip.id === option.id),
    );

    // If a search string has been entered, use it to filter options more
    if (this.searchText) {
      this._filteredOptions = this._filteredOptions.filter(option => {
        return option.label.toLowerCase().indexOf(this.searchText) === 0;
      });
    }

    this.ref.markForCheck();
    return this._filteredOptions;
  }

  private handleInternalValueChange(options: AutocompleteChipsOption[]) {
    this._value = options;
    this.filterOptions();
    this.propogateChange(this.value);
    this.calculateErrorState();
  }

  /**
   * Function to handle external changes via input properties
   */
  private handleInput(values: AutocompleteChipsOption[]) {
    if ([null, undefined, ''].includes(values as any)) {
      values = [];
    }

    this._value = values;
  }

  private calculateErrorState() {
    if (!this.firstTouch) {
      if (this.ngControl) {
        this.errorState = !!this.ngControl.invalid;
      } else {
        this.errorState = false;
      }
    }
  }
}

function coerceBooleanInput(input: boolean | string): boolean {
  if (typeof input === 'string') {
    return true;
  }

  return input;
}
