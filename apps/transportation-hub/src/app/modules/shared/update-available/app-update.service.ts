import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { switchMap, take, skip } from 'rxjs/operators';
import { interval, BehaviorSubject } from 'rxjs';
import { addMilliseconds } from 'date-fns';
import { AuthService } from '../../../auth.service';

const ONE_MINUTE = 60 * 1000;
const THIRTY_MINUTES = ONE_MINUTE * 30;

@Injectable({
  providedIn: 'root',
})
export class AppUpdateService {
  private _updateAvailable = new BehaviorSubject(false);
  readonly updateAvailable = this._updateAvailable.asObservable();

  /** The time when the update will automatically happen */
  updateAtTime: Date | null = null;

  private activating = false;

  constructor(private updates: SwUpdate, private authService: AuthService) {
    this.init();
  }

  async updateNow() {
    if (this.activating) return;

    this.activating = true;

    try {
      await this.updates.activateUpdate();
    } finally {
      await this.authService.logout();
    }
  }

  private init() {
    if (!this.updates.isEnabled) return;

    this.updates.checkForUpdate();

    interval(ONE_MINUTE * 5).subscribe(() => this.updates.checkForUpdate());

    this.updates.available.pipe(take(1)).subscribe((event) => {
      console.log('current app version', event.current);
      console.log('available app version', event.available);
      this.updateAtTime = addMilliseconds(new Date(), THIRTY_MINUTES);
      this._updateAvailable.next(true);
    });

    this.updateAvailable
      .pipe(
        skip(1),
        take(1),
        switchMap(() => interval(THIRTY_MINUTES)),
      )
      .subscribe(() => this.updateNow());
  }
}
