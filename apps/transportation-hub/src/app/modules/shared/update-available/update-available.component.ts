import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AppUpdateService } from './app-update.service';

@Component({
  selector: 'app-update-available',
  templateUrl: './update-available.component.html',
  styleUrls: ['./update-available.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateAvailableComponent implements OnInit {
  constructor(public appUpdateService: AppUpdateService) {}

  ngOnInit(): void {}
}
