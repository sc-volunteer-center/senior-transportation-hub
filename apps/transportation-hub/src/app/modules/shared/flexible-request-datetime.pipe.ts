import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

export function formatFlexibleRequestDatetime(value: Date) {
  const start = moment.tz(value, 'America/Los_Angeles');
  const end = start.clone().endOf('week');

  return `${start.format('M/D')} - ${end.format('M/D/YY')}`;
}

@Pipe({ name: 'flexibleDatetime' })
export class FlexibleRequestDatetimePipe implements PipeTransform {
  transform(value: Date): string {
    return formatFlexibleRequestDatetime(value);
  }
}
