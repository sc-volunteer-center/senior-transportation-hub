import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Optional,
  Self,
  ElementRef,
  Renderer2,
  ViewChildren,
  QueryList,
  ViewEncapsulation,
  AfterViewInit,
  Attribute,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  ControlValueAccessor,
  NgControl,
  FormBuilder,
} from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { MatInput } from '@angular/material/input';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'app-select-other',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <mat-form-field [formGroup]="_model" appearance="fill">
      <mat-label *ngIf="label">
        {{ label }}
        <ng-template [ngIf]="includedInGeneratedEmail">
          &nbsp;<mat-icon>email</mat-icon>
        </ng-template>
      </mat-label>
      <mat-select
        #select="matSelect"
        formControlName="primaryList"
        [placeholder]="placeholder"
        panelClass="form-select-other-panel"
        (selectionChange)="_handleChange()"
        (blur)="_onTouched()"
        [required]="required"
      >
        <mat-option *ngIf="hasAnyOption" [value]="''">any</mat-option>
        <mat-option *ngFor="let option of options" [value]="option">
          {{ option }}
        </mat-option>
        <mat-option *ngIf="allowCustomOption" value="other" class="meta-input">
          other . . .
        </mat-option>
        <mat-option *ngIf="hasDeselectOption" class="meta-input">
          clear
        </mat-option>
      </mat-select>
    </mat-form-field>

    <mat-form-field
      *ngIf="_primaryList.value === 'other'"
      [formGroup]="_model"
      appearance="fill"
    >
      <mat-label *ngIf="otherLabel">
        {{ otherLabel }}
        <ng-template [ngIf]="includedInGeneratedEmail">
          &nbsp;<mat-icon>email</mat-icon>
        </ng-template>
      </mat-label>

      <input
        matInput
        #customOptionInput="matInput"
        formControlName="customOption"
        [placeholder]="otherPlaceholder"
        (keyup)="_handleChange()"
        (change)="_handleChange()"
        (blur)="_onTouched()"
        [required]="required"
      />
    </mat-form-field>
  `,
  styleUrls: ['./select-other.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { '[class.required]': 'required' },
})
export class SelectOtherComponent
  implements OnInit, AfterViewInit, OnDestroy, ControlValueAccessor {
  @Input()
  get disabled() {
    return this._disabled;
  }
  set disabled(dis) {
    this._disabled = !!dis;
    this.stateChanges.next();
  }
  private _disabled = false;

  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }
  private _placeholder: string;

  @Input()
  get otherPlaceholder() {
    return this._otherPlaceholder;
  }
  set otherPlaceholder(plh) {
    this._otherPlaceholder = plh;
    this.stateChanges.next();
  }
  private _otherPlaceholder: string;

  @Input()
  get label() {
    return this._label;
  }
  set label(plh) {
    this._label = plh;
    this.stateChanges.next();
  }
  private _label: string;

  @Input()
  get otherLabel() {
    return this._otherLabel;
  }
  set otherLabel(plh) {
    this._otherLabel = plh;
    this.stateChanges.next();
  }
  private _otherLabel: string;

  @Input()
  get value(): string {
    if (!this._primaryList.value) {
      return '';
    } else if (this._primaryList.value === 'other') {
      if (this._customOption.value) {
        return this._customOption.value;
      } else {
        return '';
      }
    } else {
      return this._primaryList.value;
    }
  }
  set value(input: string) {
    this.handleInput(input);
    this.stateChanges.next();
  }

  @Input() options: string[] = [];
  @Input() hasAnyOption = false;
  @Input() hasDeselectOption = false;
  @Input() allowCustomOption = true;

  public stateChanges = new Subject<void>();
  public focused = false;
  public errorState = false;
  public readonly controlType = 'sw-select-other';
  @Input() required = false;

  _model: FormGroup;
  _primaryList: FormControl;
  _customOption: FormControl;
  private firstTouch = true; // when true, errorState suppressed
  private subscriptions: Subscription[] = [];

  @ViewChildren('customOptionInput', { read: MatInput })
  private customOptionInput: QueryList<MatInput>;

  private propogateChange: (_: any) => void = () => {};
  /**
   * @hidden
   */
  public _onTouched: () => void = () => {};

  constructor(
    private fb: FormBuilder,
    private fm: FocusMonitor,
    private el: ElementRef,
    @Optional()
    @Self()
    public ngControl: NgControl,
    private renderer: Renderer2,
    private ref: ChangeDetectorRef,
    @Attribute('includedInGeneratedEmail')
    public readonly includedInGeneratedEmail: boolean,
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }

    this.includedInGeneratedEmail = coerceBooleanProperty(
      includedInGeneratedEmail,
    );

    this._primaryList = new FormControl();
    this._customOption = new FormControl();

    this._model = new FormGroup({
      primaryList: this._primaryList,
      customOption: this._customOption,
    });
  }

  ngOnChanges() {
    this.handleInput(this.value);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.fm.monitor(this.el.nativeElement, true).subscribe(origin => {
        this.focused = !!origin;
        if (!this.focused) {
          this.firstTouch = false;
        }
        this.calculateErrorState();
        this.stateChanges.next();
      }),
    );
  }

  ngAfterViewInit() {
    this.subscriptions.push(
      this.customOptionInput.changes.subscribe(values => {
        const input = values[0];

        if (input) {
          input.focus();
        }
      }),
    );
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  writeValue(value: any): void {
    this.handleInput(value);
  }

  registerOnChange(fn: (_: any) => void): void {
    this.propogateChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    isDisabled ? this._primaryList.disable() : this._primaryList.enable();
    isDisabled ? this._customOption.disable() : this._customOption.enable();
    this.renderer.setProperty(this.el.nativeElement, 'disabled', isDisabled);
  }

  /**
   * Function to handle internal changes from UI
   */
  _handleChange() {
    this.propogateChange(this.value);
    this.calculateErrorState();
  }

  private calculateErrorState() {
    if (!this.firstTouch) {
      if (this.ngControl) {
        this.errorState = !!this.ngControl.invalid;
      } else {
        this.errorState = false;
      }
    }
  }

  private handleInput(value: any) {
    if (
      this.hasAnyOption &&
      (value === '' || value === undefined || value === null)
    ) {
      this._primaryList.patchValue('any');
      this._customOption.patchValue(null);
    } else if (value === '' || value === undefined || value === null) {
      this._primaryList.patchValue(value);
      this._customOption.patchValue(null);
    } else if (this.options.includes(value)) {
      this._primaryList.patchValue(value);
      this._customOption.patchValue(null);
    } else {
      this._primaryList.patchValue('other');
      this._customOption.patchValue(value);
    }
  }
}
