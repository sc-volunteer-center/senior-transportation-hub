import { OnDestroy, AfterViewInit, Directive, Host } from '@angular/core';
import { Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';

@Directive({
  selector: '[matSort][appMatSortRouterSync]',
})
export class MatSortRouterSyncDirective implements AfterViewInit, OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(@Host() private matSort: MatSort, private router: Router) {}

  ngAfterViewInit() {
    // update the router state based on the matSort state
    this.subscriptions.push(
      this.matSort.sortChange
        .asObservable()
        .pipe(startWith(this.matSort))
        .subscribe(({ active: sortBy, direction }) => {
          this.router.navigate([], {
            queryParams: {
              sortBy,
              sortDir: direction,
            },
            queryParamsHandling: 'merge',
            replaceUrl: true,
          });
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
