import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Dispatcher } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { MatDialogRef } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-edit-dispatcher',
  templateUrl: './edit-dispatcher.component.html',
  styleUrls: ['./edit-dispatcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditDispatcherComponent implements OnInit {
  dispatcher: Dispatcher | null = null;
  dispatcherForm?: FormGroup;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private fb: FormBuilder,
    private ref: MatDialogRef<EditDispatcherComponent>,
    private loadingService: IsLoadingService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.dispatcherId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDispatcher(id))))
        .subscribe((dispatcher) => {
          this.dispatcher = dispatcher;
          this.dispatcherForm = undefined;
          this.cdr.markForCheck();

          if (!dispatcher) {
            return;
          }

          this.dispatcherForm = this.fb.group({
            firstName: [dispatcher.firstName, Validators.required],
            middleName: [dispatcher.middleName],
            lastName: [dispatcher.lastName, Validators.required],
            phoneNumber: dispatcher.phoneNumber,
            emailAddress: dispatcher.emailAddress,
            notes: dispatcher.notes,
          });
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  async submit() {
    if (!this.dispatcherForm || this.dispatcherForm.invalid) {
      return;
    }

    await this.loadingService.add(
      this.fs.updateDispatcher(this.dispatcher!.uid, this.dispatcherForm.value),
      { key: 'edit-dispatcher' },
    );

    this.ref.close();
  }
}
