import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Dispatcher } from '@local/models';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../../firestore.service';
import { switchMap } from 'rxjs/operators';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';
import { timestamp } from '../../../../utilities';
import { MatDialog } from '@angular/material/dialog';
import { IsLoadingService } from '@service-work/is-loading';
import { EditDispatcherComponent } from '../edit-dispatcher/edit-dispatcher.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dispatcher-profile',
  templateUrl: './dispatcher-profile.component.html',
  styleUrls: ['./dispatcher-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DispatcherProfileComponent implements OnInit {
  dispatcher: Dispatcher | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    public fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
    private loadingService: IsLoadingService,
    private dialog: MatDialog,
    private titleService: Title,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.dispatcherId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDispatcher(id))))
        .subscribe((dispatcher) => {
          this.dispatcher = dispatcher;
          this.titleService.setTitle(
            `Dispatcher | ${dispatcher?.nameWithRetiredStatus} | Hub`,
          );
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  editDispatcher() {
    this.dialog.open(EditDispatcherComponent, {
      width: '600px',
    });
  }

  async toggleRetireDispatcher() {
    if (!this.dispatcher) {
      return;
    }

    this.loadingService.add(
      this.fs.updateDispatcher(this.dispatcher.uid, {
        retiredAt: this.dispatcher.retiredAt ? null : timestamp(),
      }),
    );
  }

  isAdmin() {
    if (!this.fs.organization || !this.fs.user) return false;

    return this.fs.organization.adminEmailAddresses.includes(
      this.fs.user.emailAddress,
    );
  }
}
