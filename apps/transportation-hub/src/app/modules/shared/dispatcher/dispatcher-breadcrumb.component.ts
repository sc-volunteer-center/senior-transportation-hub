import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Dispatcher } from '@local/models';
import { switchMap } from 'rxjs/operators';
import { Subscription, of } from 'rxjs';
import { FirestoreService } from '../../../firestore.service';
import { RouterStoreService } from 'apps/transportation-hub/src/app/state/router-store.service';

@Component({
  selector: 'app-dispatcher-breadcrumb',
  template: ` Dispatcher - {{ dispatcher?.nameWithRetiredStatus }} `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DispatcherBreadcrumbComponent implements OnInit {
  dispatcher: Dispatcher | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private routerStore: RouterStoreService,
  ) {}

  ngOnInit() {
    this.subscriptions.push(
      this.routerStore
        .get$((store) => store.state.params.dispatcherId)
        .pipe(switchMap((id) => (!id ? of(null) : this.fs.getDispatcher(id))))
        .subscribe((dispatcher) => {
          this.dispatcher = dispatcher;
          this.cdr.markForCheck();
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
