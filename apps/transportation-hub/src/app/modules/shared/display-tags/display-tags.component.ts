import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  OnChanges,
  ChangeDetectorRef,
  SimpleChanges,
} from '@angular/core';
import { FirestoreService } from '../../../firestore.service';
import { Tag, TagColors } from '@local/models';
import {
  toPromise,
  isNonNullable,
  stringComparer,
  TAG_COLORS,
} from '../../../utilities';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

export interface ISimpleTag {
  label: string;
  description: string;
  /** e.g. "Light Gray" */
  color: TagColors;
}

@Component({
  selector: 'app-display-tags',
  templateUrl: './display-tags.component.html',
  styleUrls: ['./display-tags.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DisplayTagsComponent implements OnChanges {
  @Input() label = '';
  @Input() labelHref = '';

  private _tagIds: string[] = [];
  private _tags: Tag[] = [];
  @Input() get tagIds() {
    return this._tagIds;
  }
  set tagIds(value: string[]) {
    if (!Array.isArray(value)) {
      this._tagIds = [];
    } else {
      this._tagIds = value;
    }
  }

  private _customTags: ISimpleTag[] = [];
  @Input('tags') get customTags() {
    return this._customTags;
  }
  set customTags(value: ISimpleTag[]) {
    if (!Array.isArray(value)) {
      this._customTags = [];
    } else {
      this._customTags = value;
    }
  }

  private _favoritesOnly = false;
  @Input() get favoritesOnly() {
    return this._favoritesOnly;
  }
  set favoritesOnly(value: boolean) {
    this._favoritesOnly = coerceBooleanProperty(value);
  }

  tags: ISimpleTag[] = [];

  constructor(private fs: FirestoreService, private cdr: ChangeDetectorRef) {}

  async ngOnChanges(changes: SimpleChanges) {
    if (changes.tagIds) {
      this._tags = await Promise.all(
        this.tagIds.map((id) => toPromise(this.fs.getTag(id))),
      ).then((t) => t.filter(isNonNullable));

      if (this.favoritesOnly) {
        this._tags = this._tags.filter((t) => t.favorite);
      }

      this._tags.sort((a, b) => stringComparer(a.label, b.label));
    }

    this.tags = [...this._tags, ...this.customTags];
    this.cdr.markForCheck();
  }

  addMatChipClasses(tag: Tag) {
    if (!tag.color) return '';

    return `tag-bg-color-${tag.color.replace(' ', '-')}`;
  }
}
