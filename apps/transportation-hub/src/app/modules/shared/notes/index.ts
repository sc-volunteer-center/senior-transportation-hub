export {
  IEditNoteComponentData,
  INewNoteOutput,
  IEditNoteOutput,
  IFlagNoteOutput,
  IReplyToNoteOutput,
  IDeleteNoteOutput,
  IEditNoteResponse,
} from './edit-note/edit-note.component';
