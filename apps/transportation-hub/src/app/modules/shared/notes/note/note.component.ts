import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  OnChanges,
  ChangeDetectorRef,
  HostBinding,
  EventEmitter,
  Output,
} from '@angular/core';
import { FirestoreService } from 'apps/transportation-hub/src/app/firestore.service';
import { format } from 'date-fns';
import { take } from 'rxjs/operators';
import { Dispatcher, Note } from '@local/models';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  EditNoteComponent,
  IEditNoteComponentData,
  IEditNoteResponse,
  IReplyToNoteOutput,
  IEditNoteOutput,
  IDeleteNoteOutput,
  IFlagNoteOutput,
} from '../edit-note/edit-note.component';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoteComponent implements OnInit, OnChanges {
  @Input() note: Note;
  @Input() viewingAs: 'client' | 'driver' | 'rideRequest';
  @Output('replyToNote') replyToNoteOutput =
    new EventEmitter<IReplyToNoteOutput>();
  @Output('editNote') editNoteOutput = new EventEmitter<IEditNoteOutput>();
  @Output('flagNote') flagNoteOutput = new EventEmitter<IFlagNoteOutput>();
  @Output('deleteNote') deleteNoteOutput =
    new EventEmitter<IDeleteNoteOutput>();

  @HostBinding('class.my-note') myNote = false;

  creatorName: string | undefined;

  viewButtonText = '';
  relatedTo = '';
  relatedToHref = '';

  currentDispatcher: Dispatcher | null = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private fs: FirestoreService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.fs.getCurrentDispatcher().subscribe((dispatcher) => {
        this.currentDispatcher = dispatcher;
        this.cdr.markForCheck();
      }),
    );

    this.fs
      .getDispatcher(this.note.createdById)
      .pipe(take(1))
      .subscribe((dispatcher) => {
        this.creatorName = dispatcher?.name;
        this.myNote = this.note.createdById === this.fs.dispatcher!.uid;
        this.cdr.markForCheck();
      });
  }

  ngOnChanges() {
    const relatedDriver = () => {
      this.viewButtonText = `View driver profile`;
      this.relatedTo = `Related to driver "${
        this.note!.noteThread!.driver!.nameWithRetiredStatus
      }"`;
      this.relatedToHref = `/dispatcher/driver/${
        this.note.noteThread!.driverId
      }/profile`;
    };

    const relatedClient = () => {
      this.viewButtonText = `View client profile`;
      this.relatedTo = `Related to client "${
        this.note!.noteThread!.client!.nameWithRetiredStatus
      }"`;
      this.relatedToHref = `/dispatcher/client/${
        this.note.noteThread!.clientId
      }/profile`;
    };

    const relatedRideRequest = () => {
      this.viewButtonText = `View ride request`;
      const rideRequest = this.note!.noteThread!.rideRequest!;
      this.relatedTo = `Related to "${
        rideRequest.client.nameWithRetiredStatus
      }'s" ride request on ${format(rideRequest.datetime, 'M/d/yyyy')}`;
      this.relatedToHref = `/dispatcher/ride-request/${
        this.note.noteThread!.rideRequestId
      }/overview`;
    };

    const nothingRelated = () => {
      this.viewButtonText = ``;
      this.relatedTo = ``;
      this.relatedToHref = ``;
    };

    if (this.viewingAs === 'client') {
      if (this.note.noteThread!.driverId) {
        return relatedDriver();
      } else if (this.note.noteThread!.rideRequestId) {
        return relatedRideRequest();
      } else {
        return nothingRelated();
      }
    } else if (this.viewingAs === 'driver') {
      if (this.note.noteThread!.clientId) {
        return relatedClient();
      } else if (this.note.noteThread!.rideRequestId) {
        return relatedRideRequest();
      } else {
        return nothingRelated();
      }
    } else if (this.viewingAs === 'rideRequest') {
      if (this.note.noteThread!.driverId) {
        return relatedDriver();
      } else if (this.note.noteThread!.clientId) {
        return relatedClient();
      } else {
        return nothingRelated();
      }
    } else {
      throw new Error('Invalid "viewingAs" value');
    }
  }

  renderCreator() {}

  renderNote(text: string) {
    return text.replace(/[\n\r]/g, '<br />');
  }

  async editNote() {
    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            edit: this.note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result) return;

    if (result.delete) {
      this.deleteNoteOutput.emit(result.delete);
    } else {
      this.editNoteOutput.emit(result.edit);
    }
  }

  async replyToNote() {
    const result = await this.dialog
      .open<EditNoteComponent, IEditNoteComponentData, IEditNoteResponse>(
        EditNoteComponent,
        {
          width: '600px',
          data: {
            reply: this.note,
          },
        },
      )
      .afterClosed()
      .toPromise();

    if (!result) return;

    this.replyToNoteOutput.emit(result.reply);
  }

  async flagNote() {
    this.flagNoteOutput.emit({
      noteThreadId: this.note.noteThread.uid,
      noteId: this.note.uid,
      flag: !this.note.noteThread.flag,
      suppressTodo: false,
    });
  }
}
