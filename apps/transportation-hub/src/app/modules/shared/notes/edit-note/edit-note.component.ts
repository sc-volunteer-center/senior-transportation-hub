import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
  ChangeDetectorRef,
} from '@angular/core';
import { Client, Driver, Note } from '@local/models';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { stringComparer } from 'apps/transportation-hub/src/app/utilities';
import { FirestoreService } from 'apps/transportation-hub/src/app/firestore.service';

export interface IEditNoteComponentData {
  edit?: Note;
  reply?: Note;
  askIfAssociatedWithVolunteer?: boolean;
}

export interface INewNoteOutput {
  flaggedAsImportant: boolean;
  suppressTodo: false;
  text: string;
  volunteer?: Driver;
}

export interface IEditNoteOutput {
  noteThreadId: string;
  noteId: string;
  text: string;
}

export interface IFlagNoteOutput {
  noteThreadId: string;
  noteId: string;
  flag: boolean;
  suppressTodo: false;
}

export interface IReplyToNoteOutput {
  noteThreadId: string;
  text: string;
}

export interface IDeleteNoteOutput {
  noteThreadId: string;
  noteId: string;
  deleteThread: boolean;
}

export interface IEditNoteResponse {
  new?: INewNoteOutput;
  edit?: IEditNoteOutput;
  reply?: IReplyToNoteOutput;
  delete?: IDeleteNoteOutput;
}

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditNoteComponent implements OnInit {
  editForm: FormGroup;
  associatedWithVolunteerForm: FormGroup;

  filteredVolunteers: Driver[] = [];
  private volunteers: Driver[] = [];
  private subscriptions: Subscription[] = [];

  constructor(
    private ref: MatDialogRef<EditNoteComponent, IEditNoteResponse>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: undefined | IEditNoteComponentData,
    private cdr: ChangeDetectorRef,
    private fs: FirestoreService,
  ) {}

  ngOnInit() {
    this.editForm = this.fb.group({
      flaggedAsImportant: false,
      text: [
        this.data?.edit?.text || '',
        [Validators.required, Validators.minLength(1)],
      ],
    });

    this.associatedWithVolunteerForm = this.fb.group({
      associatedWithVolunteer: [null, Validators.required],
      volunteer: '',
    });

    this.subscriptions.push(
      this.fs.getDrivers({ retired: false }).subscribe((drivers) => {
        this.volunteers = drivers;

        const maybeFilterString =
          this.associatedWithVolunteerForm.get('volunteer')!.value;

        this.filterVolunteers(
          typeof maybeFilterString === 'string' ? maybeFilterString : '',
        );
      }),
    );

    this.subscriptions.push(
      this.associatedWithVolunteerForm
        .get('volunteer')!
        .valueChanges.subscribe((value) => {
          if (typeof value !== 'string') {
            return;
          }

          this.filterVolunteers(value);
        }),
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  renderNote(text: string) {
    return text.replace(/[\n\r]/g, '<br />');
  }

  get formInvalid() {
    return !!(
      !this.editForm.valid ||
      (this.data?.askIfAssociatedWithVolunteer &&
        !this.associatedWithVolunteerForm.valid) ||
      (this.associatedWithVolunteerForm.value.associatedWithVolunteer &&
        typeof this.associatedWithVolunteerForm.value.volunteer === 'string')
    );
  }

  async submit() {
    if (!this.editForm.valid) return;

    if (typeof this.associatedWithVolunteerForm.value.volunteer === 'string') {
      this.associatedWithVolunteerForm.get('volunteer')!.patchValue('');
    }

    if (this.formInvalid) return;

    const { value } = this.editForm;

    const response: IEditNoteResponse = {};

    if (this.data?.edit) {
      response.edit = {
        noteThreadId: this.data.edit.noteThread.uid,
        noteId: this.data.edit.uid,
        text: value.text,
      };
    } else if (this.data?.reply) {
      response.reply = {
        noteThreadId: this.data.reply.noteThread.uid,
        text: value.text,
      };
    } else {
      response.new = {
        flaggedAsImportant: value.flaggedAsImportant,
        suppressTodo: false,
        text: value.text,
        volunteer:
          (this.associatedWithVolunteerForm.value.associatedWithVolunteer &&
            this.associatedWithVolunteerForm.value.volunteer) ||
          undefined,
      };
    }

    this.ref.close(response);
  }

  deleteNote() {
    this.ref.close({
      delete: {
        noteThreadId: this.data!.edit!.noteThread.uid,
        noteId: this.data!.edit!.uid,
        deleteThread: this.data!.edit!.noteThread.notes.length === 1,
      },
    });
  }

  get canDelete() {
    if (!this.data?.edit) return false;
    if (this.data.edit.noteThread.notes.length === 1) return true;
    return !this.data.edit.firstNoteInThread;
  }

  private filterVolunteers(val = '') {
    const text = val.toLowerCase();

    this.filteredVolunteers = this.volunteers
      .filter((volunteer) => volunteer.matchName(text))
      .sort((a: Driver, b: Driver) => stringComparer(a.name, b.name));

    this.cdr.markForCheck();
  }

  volunteerSelectionCallback() {
    // without setTimeout, if someone starts to filter the list,
    // then uses the mouse to make a selection, a blur event will
    // fire before the selection is made and the control will be
    // inadvertently cleared
    setTimeout(() => {
      const volunteer = this.associatedWithVolunteerForm.get('volunteer')!
        .value as string | Driver;

      if (typeof volunteer === 'string') {
        this.associatedWithVolunteerForm.get('volunteer')!.patchValue('');
      }

      this.cdr.markForCheck();
    }, 500);
  }

  personDisplayFn(person?: Client | Driver): string | undefined {
    return person?.nameWithRetiredStatus;
  }
}
