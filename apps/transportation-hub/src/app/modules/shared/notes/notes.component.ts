import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { NoteThread } from '@local/models';
import { ngForTrackByID } from '../../../utilities';
import {
  IReplyToNoteOutput,
  IEditNoteOutput,
  IFlagNoteOutput,
  IDeleteNoteOutput,
} from './edit-note/edit-note.component';

@Component({
  selector: 'app-notes',
  exportAs: 'appNotes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotesComponent implements OnInit {
  @Input() noteThreads: NoteThread[] = [];
  @Input() viewingAs: 'client' | 'driver' | 'rideRequest';

  @Output('replyToNote')
  replyToNoteOutput = new EventEmitter<IReplyToNoteOutput>();
  @Output('editNote') editNoteOutput = new EventEmitter<IEditNoteOutput>();
  @Output('flagNote') flagNoteOutput = new EventEmitter<IFlagNoteOutput>();
  @Output('deleteNote')
  deleteNoteOutput = new EventEmitter<IDeleteNoteOutput>();

  @ViewChild('messageViewerDiv', { read: ElementRef, static: false })
  messageViewerDiv: ElementRef<HTMLDivElement>;

  trackByID = ngForTrackByID;

  ngOnInit(): void {}

  ngOnChanges() {
    this.scrollToNewestMessage();
  }

  ngAfterViewInit() {
    this.scrollToNewestMessage();
  }

  scrollToNewestMessage() {
    if (!this.messageViewerDiv?.nativeElement) return;

    setTimeout(() => {
      this.messageViewerDiv.nativeElement.scrollTop = this.messageViewerDiv.nativeElement.scrollHeight;
    }, 10);
  }
}
