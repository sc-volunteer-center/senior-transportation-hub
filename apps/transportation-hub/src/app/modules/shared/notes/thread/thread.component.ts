import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output,
} from '@angular/core';
import { NoteThread } from '@local/models';
import { ngForTrackByID } from 'apps/transportation-hub/src/app/utilities';
import {
  IReplyToNoteOutput,
  IEditNoteOutput,
  IFlagNoteOutput,
  IDeleteNoteOutput,
} from '../edit-note/edit-note.component';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThreadComponent implements OnInit {
  @Input() thread: NoteThread;
  @Input() viewingAs: 'client' | 'driver' | 'rideRequest';

  @Output('replyToNote')
  replyToNoteOutput = new EventEmitter<IReplyToNoteOutput>();
  @Output('editNote') editNoteOutput = new EventEmitter<IEditNoteOutput>();
  @Output('flagNote') flagNoteOutput = new EventEmitter<IFlagNoteOutput>();
  @Output('deleteNote')
  deleteNoteOutput = new EventEmitter<IDeleteNoteOutput>();

  relatedTo = '';
  relatedToHref = '';

  constructor() {}

  trackByID = ngForTrackByID;

  ngOnInit(): void {}

  ngOnChanges() {}
}
