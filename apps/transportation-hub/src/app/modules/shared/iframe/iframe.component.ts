import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ElementRef,
  Input,
  ViewChild,
  ChangeDetectorRef,
  AfterViewInit,
} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'flex-center',
  },
})
export class IframeComponent implements OnInit, AfterViewInit {
  @Input('url') set urlSetter(value: string) {
    if (value !== this.stringUrl) {
      this.stringUrl = value;
      this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(value);
    }
  }

  safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('');
  stringUrl = '';

  @ViewChild('iframe', { read: ElementRef, static: true })
  iframe: ElementRef<HTMLIFrameElement>;

  loading = true;

  constructor(
    private cdr: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    const that = this;
    this.iframe.nativeElement.addEventListener(
      'loadstart',
      this.loadStart(that),
    );
    this.iframe.nativeElement.addEventListener('load', this.loadEnd(that));
  }

  private loadStart(that: this) {
    return () => {
      that.loading = true;
      that.cdr.markForCheck();
    };
  }

  private loadEnd(that: this) {
    return () => {
      that.loading = false;
      that.cdr.markForCheck();
    };
  }
}
