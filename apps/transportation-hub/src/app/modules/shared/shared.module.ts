import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IsLoadingModule } from '@service-work/is-loading';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import { CountdownModule } from 'ngx-countdown';

import { IframeComponent } from './iframe/iframe.component';
import { DispatchersComponent } from './dispatchers/dispatchers.component';
import { DispatcherComponent } from './dispatcher/dispatcher.component';
import { DispatcherBreadcrumbComponent } from './dispatcher/dispatcher-breadcrumb.component';
import { DispatchersBreadcrumbComponent } from './dispatchers/dispatchers-breadcrumb.component';
import { EditDispatcherComponent } from './dispatcher/edit-dispatcher/edit-dispatcher.component';
import { DispatcherProfileComponent } from './dispatcher/dispatcher-profile/dispatcher-profile.component';
import { AutocompleteChipsInputComponent } from './autocomplete-chips-input/autocomplete-chips-input.component';
import { SelectOtherComponent } from './select-other/select-other.component';
import { WarningComponent } from './warning/warning.component';
import { DisplayTagsComponent } from './display-tags/display-tags.component';
import { UpdateAvailableComponent } from './update-available/update-available.component';
import { NotesComponent } from './notes/notes.component';
import { NoteComponent } from './notes/note/note.component';
import { EditNoteComponent } from './notes/edit-note/edit-note.component';
import { ThreadComponent } from './notes/thread/thread.component';
import { getPersistenceSetting } from '../../auth.service';
import {
  SelectedRideRequestsComponent,
  RecruitForWhichRideRequestComponent,
} from './email.service';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatRadioModule } from '@angular/material/radio';

import { ScrollPositionDirectiveModule } from '@service-work/scroll-position';
import { FlexibleRequestDatetimePipe } from './flexible-request-datetime.pipe';

import { MatSortRouterSyncDirective } from './mat-sort-router-sync.directive';

const persistence = getPersistenceSetting();

if (persistence === undefined) {
  throw new Error('Persistence setting is not defined');
}

@NgModule({
  imports: [
    A11yModule,
    CommonModule,
    CountdownModule,
    IsLoadingModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    RouterModule,
    ClipboardModule,
    ScrollPositionDirectiveModule,
  ],
  declarations: [
    SelectOtherComponent,
    IframeComponent,
    DispatchersComponent,
    DispatcherComponent,
    DispatcherBreadcrumbComponent,
    DispatchersBreadcrumbComponent,
    EditDispatcherComponent,
    DispatcherProfileComponent,
    AutocompleteChipsInputComponent,
    WarningComponent,
    DisplayTagsComponent,
    UpdateAvailableComponent,
    NotesComponent,
    NoteComponent,
    EditNoteComponent,
    ThreadComponent,
    SelectedRideRequestsComponent,
    RecruitForWhichRideRequestComponent,
    FlexibleRequestDatetimePipe,
    MatSortRouterSyncDirective,
  ],
  exports: [
    ScrollPositionDirectiveModule,
    SelectOtherComponent,
    IframeComponent,
    DispatchersComponent,
    DispatcherComponent,
    DispatcherBreadcrumbComponent,
    DispatchersBreadcrumbComponent,
    EditDispatcherComponent,
    DispatcherProfileComponent,
    AutocompleteChipsInputComponent,
    WarningComponent,
    DisplayTagsComponent,
    UpdateAvailableComponent,
    NotesComponent,
    EditNoteComponent,
    SelectedRideRequestsComponent,
    FlexibleRequestDatetimePipe,
    MatSortRouterSyncDirective,
  ],
})
export class SharedModule {}
