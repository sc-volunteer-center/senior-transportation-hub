import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostBinding,
} from '@angular/core';
// import { DocDeletionService } from './state/doc-deletion.service';
// import { FirestoreService } from './firestore.service';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
} from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { filter, switchMap, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { API_VERSION } from './utilities';
import { AppUpdateService } from './modules/shared/update-available/app-update.service';
import { of, interval } from 'rxjs';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  template: `
    <mat-progress-bar
      *ngIf="'default' | swIsLoading | async"
      mode="indeterminate"
      color="warn"
      style="position: absolute; top: 0; z-index: 2000; height: 10px;"
    >
    </mat-progress-bar>

    <router-outlet></router-outlet>
  `,
  host: { class: 'flex-column' },
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
      app-root {
        width: 100%;
        height: 100%;
      }
    `,
  ],
})
export class AppComponent implements OnInit {
  @HostBinding('class.TESTING-APP') isTest = !environment.production;

  constructor(
    private router: Router,
    private isLoadingService: IsLoadingService,
    public updateService: AppUpdateService, // create the update service
  ) {}

  ngOnInit() {
    console.log(`API_VERSION = ${API_VERSION}`);

    this.router.events
      .pipe(
        filter(
          (event) =>
            event instanceof NavigationStart ||
            event instanceof NavigationEnd ||
            event instanceof NavigationCancel ||
            event instanceof NavigationError,
        ),
      )
      .subscribe((event) => {
        // if it's the start of navigation, `add()` a loading indicator
        if (event instanceof NavigationStart) {
          this.isLoadingService.add();
          return;
        }

        // else navigation has ended, so `remove()` a loading indicator
        this.isLoadingService.remove();
      });
  }
}
