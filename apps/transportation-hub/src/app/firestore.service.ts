import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { Observable, of, combineLatest } from 'rxjs';
import {
  take,
  switchMap,
  map,
  distinctUntilChanged,
  tap,
} from 'rxjs/operators';
import {
  Dispatcher,
  IUser,
  IOrganization,
  IDispatcher,
  IDispatcherShift,
  IClient,
  IRideRequest,
  IDriver,
  IAnswer,
  IQuestion,
  ISelectQuestion,
  IBooleanQuestion,
  ITextQuestion,
  IQuestionSectionBreak,
  RideRequest,
  Question,
  IDestination,
  Driver,
  DispatcherShift,
  Client,
  Destination,
  SelectQuestion,
  TextQuestion,
  BooleanQuestion,
  QuestionSectionBreak,
  Tag,
  ITag,
  FirestoreInput,
  rideRequestUpdateD,
  rideRequestCreateD,
  clientCreateD,
  clientUpdateD,
  driverCreateD,
  driverUpdateD,
  destinationD,
  tagCreateD,
  tagUpdateD,
  dispatcherCreateD,
  dispatcherUpdateD,
  NoteThread,
  NOTE_THREAD_CREATE_DECODER,
  INoteThread,
  NOTE_CREATE_DECODER,
  NOTE_THREAD_UPDATE_DECODER,
  IRecurringRideRequest,
  RecurringRideRequest,
  SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL,
  SERIALIZABLE_SERVER_DATE_SENTINAL,
  IEmail,
  setToStartOf,
} from '@local/models';
import { CookieService } from '@local/ngx-cookie';
import { addHours, format, compareAsc, subDays } from 'date-fns';
import 'firebase/compat/firestore';
import {
  toPromise,
  timestamp,
  API_VERSION,
  IApiDoc,
  startOfWeek,
} from './utilities';
import { environment } from '../environments/environment';
import { MapsService } from './maps.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import {
  ClientService,
  DriverService,
  DestinationService,
  DispatcherService,
  DispatcherShiftService,
  QuestionService,
  RideRequestService,
  UserService,
  IRideRequestsGetParams,
  IGetTagsParams,
  TagService,
  IGetClientsParams,
  IGetDriversParams,
  NoteThreadService,
  IGetNoteThreadsParams,
  IRecurringRideRequestsGetParams,
  QueryCacheService,
  IQueryCacheOptions,
  extractQueryCacheOptions,
} from './state';
// import { GoogleService } from './google.service';
import isEqual from 'lodash-es/isEqual';
import { assert } from 'ts-decoders';
import { Timestamp, DocumentReference } from '@firebase/firestore-types';

import { AuthService, CURRENTLY_RUNNING_API_KEY } from './auth.service';
import { Router } from '@angular/router';
import { IsLoadingService } from '@service-work/is-loading';
import { arrayD } from 'ts-decoders/decoders';
import * as moment from 'moment-timezone';
import { wait } from '@local/utilities';
import firebase from 'firebase/compat/app';
import { WarningService } from './modules/shared/warning/warning.service';

@Injectable({
  providedIn: 'root',
})
export class FirestoreService {
  user: IUser | null = null;
  organization: IOrganization | null = null;
  dispatcher: Dispatcher | null = null;

  persistanceEnabled = false;

  constructor(
    private fs: AngularFirestore,
    private cookieService: CookieService,
    private mapsService: MapsService,
    private fns: AngularFireFunctions,
    private clientService: ClientService,
    private destinationService: DestinationService,
    private tagService: TagService,
    private dispatcherService: DispatcherService,
    private dispatcherShiftService: DispatcherShiftService,
    private driverService: DriverService,
    private questionService: QuestionService,
    private rideRequestService: RideRequestService,
    private userService: UserService,
    private noteThreadService: NoteThreadService,
    // private googleService: GoogleService,
    private authService: AuthService,
    private router: Router,
    private isLoadingService: IsLoadingService,
    private warningService: WarningService,
    private cacheService: QueryCacheService,
  ) {
    patchAuthServiceLogoutMethod(authService, isLoadingService, fs.firestore);

    this.userService.getUserState().subscribe((info) => {
      this.user = info.user || null;
      this.organization = info.organization || null;
      this.dispatcher = info.dispatcher || null;
    });

    this.fs.persistenceEnabled$.subscribe((p) => {
      this.persistanceEnabled = p;

      if (!environment.production) {
        console.log('persistenceEnabled', p);
      }
    });

    this.fs
      .doc<IApiDoc>('apiVersion/latest')
      .valueChanges()
      .subscribe((doc) => {
        const apiOfPersistedDB: number | null = JSON.parse(
          localStorage.getItem(CURRENTLY_RUNNING_API_KEY) || 'null',
        );

        if (
          Number.isInteger(apiOfPersistedDB) &&
          apiOfPersistedDB !== API_VERSION
        ) {
          this.authService.logout();
          return;
        }

        if (doc?.version === API_VERSION) return;

        this.router.navigate(['/down-for-maintenance']);
      });
  }

  async completeLogin() {
    this.isLoadingService.add();
    const firebaseUser = await this.authService.firebaseUser
      .pipe(take(1))
      .toPromise();

    if (!firebaseUser) {
      await this.router.navigate(['/']);
      this.isLoadingService.remove();
      return;
    }

    let user = await toPromise(this.getUser(firebaseUser!.email!));

    if (!user?.name || !user?.photoUrl) {
      user = await this.mergeUser(firebaseUser!.email!, {
        name: firebaseUser!.displayName,
        photoUrl: firebaseUser!.photoURL,
      });
    }

    this.isLoadingService.remove();

    return user;
  }

  // async authorizeOfflineAccess() {
  //   const org = await toPromise(this.getOrganization());

  //   if (!org) {
  //     alert('Must be logged into an organization to request offline access');
  //     return { success: false };
  //   }

  //   const offlineAccessCode = await this.googleService.grantOfflineAccess();

  //   if (!offlineAccessCode) {
  //     alert('Must grant offline access in order to continue');
  //     return { success: false };
  //   }

  //   const updateAccessKey = this.fns.httpsCallable(
  //     'updateOrganizationGoogleAccessCode',
  //   );

  //   try {
  //     await updateAccessKey({
  //       organizationId: org.uid,
  //       offlineAccessCode,
  //     }).toPromise();
  //   } catch (e) {
  //     if (
  //       e.message ===
  //       // tslint:disable-next-line: quotemark
  //       "offlineAccessCode not equal to currentUser's email address"
  //     ) {
  //       alert(
  //         'Oops, you authorized using the wrong email address. You must ' +
  //           'authorize using the dispatcher email address',
  //       );
  //       return { success: false };
  //     }

  //     console.error(e);

  //     alert('Oops, there was an error while trying to authenticate.');

  //     return { success: false, error: e.message };
  //   }

  //   return { success: true };
  // }

  getCurrentUser() {
    const query = () =>
      this.userService.getUserState().pipe(
        map((info) => info.user || null),
        distinctUntilChanged<IUser | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getCurrentUser',
      query,
      options: { expirationTime: Number.POSITIVE_INFINITY },
    });
  }

  getUser(emailAddress: string) {
    const query = () =>
      this.fs
        .doc<IUser>(`users/${emailAddress}`)
        .valueChanges()
        .pipe(
          map((val) => val || null),
          distinctUntilChanged<IUser | null>(isEqual),
        );

    return this.resolveQuery({ method: 'getUser', query });
  }

  async mergeUser(
    emailAddress: string,
    args: {
      name: string | null;
      photoUrl: string | null;
    } = { name: null, photoUrl: null },
  ) {
    await this.fs.doc<FirestoreInput<IUser>>(`users/${emailAddress}`).set(
      {
        emailAddress,
        updatedAt: timestamp(),
        ...args,
      },
      { merge: true },
    );

    return (await toPromise(this.getUser(emailAddress)))!;
  }

  async addOrganization(args: {
    name: string;
    dispatcherEmailAddress: string;
    rideRequestEmailPreface: string;
  }) {
    const addOrganization: (data: {
      name: string;
      dispatcherEmailAddress: string;
      rideRequestEmailPreface: string;
    }) => Observable<any> = this.fns.httpsCallable('addOrganization');

    try {
      return await toPromise(addOrganization(args));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  getOrganization() {
    const query = () =>
      this.userService.getUserState().pipe(
        map((info) => info.organization || null),
        distinctUntilChanged<IOrganization | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getOrganization',
      query,
      options: { expirationTime: Number.POSITIVE_INFINITY },
    });
  }

  async updateOrganization(args: {
    name: string;
    dispatcherEmailAddress: string;
    rideRequestEmailPreface: string;
  }) {
    const updateOrganization: (data: {
      name: string;
      rideRequestEmailPreface: string;
      dispatcherEmailAddress: string;
    }) => Observable<any> = this.fns.httpsCallable('updateOrganization');

    try {
      return toPromise(updateOrganization(args));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  async updateOrganizationAdminEmailAddresses(adminEmailAddresses: string[]) {
    const updateOrganizationAdminEmailAddresses: (data: {
      adminEmailAddresses: string[];
    }) => Observable<any> = this.fns.httpsCallable(
      'updateOrganizationAdminEmailAddresses',
    );

    try {
      return toPromise(
        updateOrganizationAdminEmailAddresses({ adminEmailAddresses }),
      );
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  async addOrganizationAdminEmailAddress(args: { adminEmailAddress: string }) {
    const addOrganizationAdminEmailAddress: (data: {
      adminEmailAddress: string;
    }) => Observable<any> = this.fns.httpsCallable(
      'addOrganizationAdminEmailAddress',
    );

    try {
      return toPromise(addOrganizationAdminEmailAddress(args));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  async removeOrganizationAdminEmailAddress(args: {
    adminEmailAddress: string;
  }) {
    const removeOrganizationAdminEmailAddress: (data: {
      adminEmailAddress: string;
    }) => Observable<any> = this.fns.httpsCallable(
      'removeOrganizationAdminEmailAddress',
    );

    try {
      return toPromise(removeOrganizationAdminEmailAddress(args));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  getCurrentDispatcherShift() {
    const query = () =>
      this.userService.getUserState().pipe(
        switchMap((info) =>
          !info.dispatcherShiftCookie
            ? of(null)
            : this.dispatcherShiftService.getDispatcherShift(
                info.dispatcherShiftCookie.shiftId,
              ),
        ),
        distinctUntilChanged<DispatcherShift | null>(isEqual),
      );

    return this.resolveQuery({ method: 'getCurrentDispatcherShift', query });
  }

  getCurrentDispatcher() {
    const query = () =>
      this.userService.getCurrentDispatcherInfo().pipe(
        map((info) => info?.dispatcher || null),
        distinctUntilChanged<Dispatcher | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getCurrentDispatcher',
      query,
      options: { expirationTime: Number.POSITIVE_INFINITY },
    });
  }

  getDispatcher(id: string) {
    const query = () =>
      this.dispatcherService
        .getDispatcher(id)
        .pipe(distinctUntilChanged<Dispatcher | null>(isEqual));

    return this.resolveQuery({ method: 'getDispatcher', args: [id], query });
  }

  getDispatchers(
    args: {
      retired?: boolean;
      limit?: number;
      startAfter?: AngularFirestoreDocument<IDispatcher>;
    } = {},
  ): Observable<Dispatcher[]> {
    const query = () =>
      this.dispatcherService
        .getDispatchers(args)
        .pipe(distinctUntilChanged<Dispatcher[]>(isEqual));

    return this.resolveQuery({ method: 'getDispatchers', args: [args], query });
  }

  async addDispatcher(args: {
    firstName: string;
    middleName?: string;
    lastName: string;
    phoneNumber: string | null;
    emailAddress: string | null;
    notes: string | null;
  }) {
    const org = await toPromise(this.getOrganization());

    if (!org) return;

    const validator = assert(dispatcherCreateD());

    const output = await validator({
      middleName: null,
      ...args,
      uid: this.fs.createId(),
      createdAt: timestamp(),
      updatedAt: timestamp(),
      organizationId: org.uid,
      retiredAt: null,
    });

    await this.fs
      .doc(`organizations/${org.uid}/dispatchers/${output.uid}`)
      .set(output);

    return output.uid;
  }

  async updateDispatcher(
    id: string,
    args: FirestoreInput<{
      retiredAt?: Timestamp | null;
      firstName?: string;
      middleName?: string | null;
      lastName?: string;
      phoneNumber?: string | null;
      emailAddress?: string | null;
      notes?: string | null;
    }>,
  ) {
    const org = await toPromise(this.getOrganization());

    if (!org) {
      return;
    }

    const validator = assert(dispatcherUpdateD());

    const output = await validator({
      ...args,
      updatedAt: timestamp(),
    });

    return await this.fs
      .doc<FirestoreInput<IDispatcher>>(
        `organizations/${org.uid}/dispatchers/${id}`,
      )
      .update(output);
  }

  async retireDispatcher(id: string) {
    const org = await toPromise(this.getOrganization());

    if (!org) return;

    const validator = assert(dispatcherUpdateD());

    const output = await validator({
      retiredAt: timestamp(),
      updatedAt: timestamp(),
    });

    return await this.fs
      .doc(`organizations/${org.uid}/dispatchers/${id}`)
      .update(output);
  }

  getDispatcherShifts(
    args: {
      dispatcherId?: string;
      startDate?: Date;
      endDate?: Date;
      limit?: number;
      startAfter?: AngularFirestoreDocument<IDispatcherShift>;
    } = {},
  ) {
    const query = () =>
      this.dispatcherShiftService
        .getDispatcherShifts(args)
        .pipe(distinctUntilChanged<DispatcherShift[]>(isEqual));

    return this.resolveQuery({
      method: 'getDispatcherShifts',
      args: [args],
      query,
    });
  }

  async startDispatcherShift(dispatcherId: string) {
    const uid = this.fs.createId();
    const startAt = timestamp();

    const org = await toPromise(this.getOrganization());

    if (!org) {
      return;
    }

    // Whenever a dispatcher signs in, take the opportunity to generate
    // upcoming recurring ride requests as needed.
    // Do not await completion of this task though.
    this.generateRecurringRideRequests().catch((e) =>
      console.error('Error while generating recurring ride requests', e),
    );

    this.retryRecentlyFailedEmails().catch((e) =>
      console.error('Error while resending failed emails', e),
    );

    const shifts = await this.fs
      .collection<IDispatcherShift>(
        `organizations/${org.uid}/dispatcherShifts`,
        (ref) =>
          ref.where('endAt', '==', null).orderBy('startAt', 'desc').limit(1),
      )
      .get()
      .toPromise()
      .then((s) => s.docs.flatMap((d) => d.data() as IDispatcherShift));

    let shift = shifts[0];

    // If the shift was created less than 9 hours ago, use it
    if (
      shift &&
      addHours(shift.startAt.toDate(), 9).valueOf() > new Date().valueOf()
    ) {
      this.cookieService.putObject(
        'current_dispatcher_shift',
        {
          shiftId: shift.uid,
          organizationId: org.uid,
          dispatcherId,
        },
        {
          secure: environment.production,
          expires: addHours(shift.startAt.toDate(), 9),
        },
      );

      return shift;
    }

    await this.fs
      .doc<FirestoreInput<IDispatcherShift>>(
        `organizations/${org.uid}/dispatcherShifts/${uid}`,
      )
      .set({
        uid,
        startAt: startAt,
        endAt: null,
        dispatcherId,
        updatedAt: timestamp(),
      });

    shift = await this.fs
      .doc<IDispatcherShift>(`organizations/${org.uid}/dispatcherShifts/${uid}`)
      .get()
      .toPromise()
      .then((s) => s.data() as IDispatcherShift);

    this.cookieService.putObject(
      'current_dispatcher_shift',
      {
        shiftId: shift.uid,
        organizationId: org.uid,
        dispatcherId,
      },
      {
        secure: environment.production,
        expires: addHours(shift.startAt.toDate(), 9),
      },
    );

    return shift;
  }

  async endDispatcherShift() {
    const cookie = await this.userService
      .getUserState()
      .pipe(
        map((info) => info.dispatcherShiftCookie),
        take(1),
      )
      .toPromise();

    if (!cookie) return;

    await this.fs
      .doc<FirestoreInput<IDispatcherShift>>(
        `organizations/${cookie.organizationId}/dispatcherShifts/${cookie.shiftId}`,
      )
      .update({
        endAt: timestamp(),
        updatedAt: timestamp(),
      });

    this.cookieService.remove('current_dispatcher_shift');
  }

  getClient(id: string, options: { full?: boolean } = {}) {
    const query = () =>
      (!id ? of(null) : this.clientService.getClient(id)).pipe(
        switchMap((client) =>
          !client || !client.retiredById || !options.full
            ? of(client)
            : this.getDispatcher(client.retiredById).pipe(
                map((dispatcher) => {
                  client.retiredBy = dispatcher!;
                  return client;
                }),
              ),
        ),
        switchMap((client) =>
          !client || !client.createdById || !options.full
            ? of(client)
            : this.getDispatcher(client.createdById).pipe(
                map((dispatcher) => {
                  client.createdBy = dispatcher!;
                  return client;
                }),
              ),
        ),
        switchMap((client) =>
          !client || client.answers.length === 0 || !options.full
            ? of(client)
            : combineLatest(
                client.answers.map((answer) =>
                  this.getClientQuestion(answer.questionId),
                ),
              ).pipe(
                map((questions) => {
                  client.answers.forEach((answer, index) => {
                    answer.question = questions[index]!;
                  });

                  return client;
                }),
              ),
        ),
        distinctUntilChanged<Client | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getClient',
      args: [id, options],
      query,
    });
  }

  getClients(args?: IGetClientsParams) {
    const query = () =>
      this.clientService
        .getClients(args)
        .pipe(distinctUntilChanged<Client[]>(isEqual));

    const options = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getClients',
      args: [args],
      query,
      options,
    });
  }

  async addClient(args: {
    firstName: string;
    middleName: string;
    lastName: string;
    phoneNumber: string;
    emailAddress: string | null;
    address: {
      street: string;
      apartmentNumber?: string;
      city: string;
      zip: string;
      comments: string | null;
    };
    mediCalOrSsi: boolean;
    ethnicity: string;
    birthdate: Date;
    answers: IAnswer[];
    authorizeSharingInfoWithDrivers: true;
    comments: string | null;
    restrictions: string | null;
    tagIds: string[];
    contacts: Array<{
      label: string;
      firstName: string;
      lastName: string;
      phoneNumber: string;
      relationshipToPerson: string;
    }>;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const validator = assert(
      clientCreateD({
        mapService: this.mapsService,
      }),
    );

    const output = await validator({
      ...args,
      uid: this.fs.createId(),
      createdAt: timestamp(),
      createdById: dispatcher.uid,
      updatedAt: timestamp(),
      updatedById: dispatcher.uid,
      retiredAt: null,
      retiredById: null,
      retiredReason: null,
      // flag: null,
      // flagLastUpdatedAt: null,
      // flagLastUpdatedById: null,
      flags: 0,
    });

    await this.fs
      .doc(`organizations/${org.uid}/clients/${output.uid}`)
      .set(output);

    return output.uid;
  }

  async updateClient(clientId: string, args: Partial<FirestoreInput<IClient>>) {
    const [org, dispatcher, client] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getClient(clientId)),
    ]);

    if (!org || !dispatcher || !client) {
      return;
    }

    const validator = assert(
      clientUpdateD({
        mapService: this.mapsService,
        clientAddress: client.address,
      }),
    );

    try {
      const output = await validator({
        ...args,
        updatedAt: timestamp(),
        updatedById: dispatcher.uid,
      });

      await this.fs
        .doc(`organizations/${org.uid}/clients/${clientId}`)
        .update(output);
    } catch (e: any) {
      console.log(e.errors);
      console.error(e);
    }
  }

  async destroyClient(clientId: string) {
    const deleteClient: (data: { clientId: string }) => Observable<any> =
      this.fns.httpsCallable('deleteClient');

    try {
      return await toPromise(deleteClient({ clientId }));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  getDriver(id: string, options: { full?: boolean } = {}) {
    const query = () =>
      this.driverService.getDriver(id).pipe(
        switchMap((driver) =>
          !driver || !driver.retiredById || !options.full
            ? of(driver)
            : this.getDispatcher(driver.retiredById).pipe(
                map((dispatcher) => {
                  driver.retiredBy = dispatcher!;
                  return driver;
                }),
              ),
        ),
        switchMap((driver) =>
          !driver || !driver.createdById || !options.full
            ? of(driver)
            : this.getDispatcher(driver.createdById).pipe(
                map((dispatcher) => {
                  driver.createdBy = dispatcher!;
                  return driver;
                }),
              ),
        ),
        switchMap((driver) =>
          !driver || !options.full || driver.blacklistedClientIds.length === 0
            ? of(driver)
            : combineLatest(
                driver.blacklistedClientIds.map((id) => this.getClient(id)),
              ).pipe(
                map((clients) => {
                  clients.forEach((client, index) => {
                    driver.blacklistedClients[index] = client!;
                  });

                  return driver;
                }),
              ),
        ),
        distinctUntilChanged<Driver | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getDriver',
      args: [id, options],
      query,
    });
  }

  getDrivers(args?: IGetDriversParams) {
    const query = () =>
      this.driverService
        .getDrivers(args)
        .pipe(distinctUntilChanged<Driver[]>(isEqual));

    const options = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getDrivers',
      args: [args],
      query,
      options,
    });
  }

  async addDriver(args: {
    firstName: string;
    middleName: string | null;
    lastName: string;
    driversLicenseExpirationDate: Date;
    carInsuranceExpirationDate: Date;
    phoneNumber: string;
    emailAddress: string | null;
    address: {
      street: string;
      apartmentNumber?: string;
      city: string;
      zip: string;
      comments: string | null;
    };
    comments: string | null;
    birthdate: Date | null;
    genderIdentity: string | null;
    availability: {
      sun: boolean;
      mon: boolean;
      tue: boolean;
      wed: boolean;
      thu: boolean;
      fri: boolean;
      sat: boolean;
    };
    blacklistedClientIds: string[];
    carInfo: string;
    tagIds: string[];
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const validator = assert(
      driverCreateD({
        mapService: this.mapsService,
      }),
    );

    const output = await validator({
      ...args,
      uid: this.fs.createId(),
      createdAt: timestamp(),
      createdById: dispatcher.uid,
      updatedAt: timestamp(),
      updatedById: dispatcher.uid,
      retiredAt: null,
      retiredById: null,
      retiredReason: null,
      vacationStartsAt: setToStartOf(new Date(), 'day'),
      vacationEndsAt: setToStartOf(new Date(), 'day'),
      vacationingById: null,
      organizationId: org.uid,
      // flag: null,
      // flagLastUpdatedAt: null,
      // flagLastUpdatedById: null,
      flags: 0,
    });

    await this.fs
      .doc(`organizations/${org.uid}/drivers/${output.uid}`)
      .set(output);

    return output.uid;
  }

  async updateDriver(driverId: string, args: Partial<FirestoreInput<IDriver>>) {
    const [org, dispatcher, driver] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getDriver(driverId)),
    ]);

    if (!org || !dispatcher || !driver) {
      return;
    }

    const validator = assert(
      driverUpdateD({
        mapService: this.mapsService,
        driverAddress: driver.address,
      }),
    );

    const output = await validator({
      ...args,
      updatedAt: timestamp(),
      updatedAtId: dispatcher.uid,
    });

    await this.fs
      .doc(`organizations/${org.uid}/drivers/${driverId}`)
      .update(output);
  }

  async destroyDriver(driverId: string) {
    const deleteDriver: (data: { driverId: string }) => Observable<any> =
      this.fns.httpsCallable('deleteDriver');

    try {
      return await toPromise(deleteDriver({ driverId }));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  getDestination(id: string) {
    const query = () =>
      this.destinationService
        .getDestination(id)
        .pipe(distinctUntilChanged<Destination | null>(isEqual));

    return this.resolveQuery({ method: 'getDestination', args: [id], query });
  }

  getDestinations() {
    const query = () =>
      this.destinationService
        .getDestinations()
        .pipe(distinctUntilChanged<Destination[]>(isEqual));

    return this.resolveQuery({
      method: 'getDestinations',
      query,
      options: { expirationTime: Number.POSITIVE_INFINITY },
    });
  }

  async addDestination(args: {
    label: string;
    street: string;
    apartmentNumber?: string;
    city: string;
    zip: string;
    comments: string | null;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const validator = assert(destinationD(this.mapsService));

    const output = await validator({
      ...args,
      createdAt: timestamp(),
      updatedAt: timestamp(),
      lastUsedAt: null,
      uid: this.fs.createId(),
    });

    await this.fs
      .doc<FirestoreInput<IDestination>>(
        `organizations/${org.uid}/destinations/${output.uid}`,
      )
      .set(output);

    return output.uid;
  }

  async updateDestination(
    destinationId: string,
    args: Partial<FirestoreInput<IDestination>>,
  ) {
    const [org, dispatcher, destination] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getDestination(destinationId)),
    ]);

    if (!org || !dispatcher || !destination) {
      return;
    }

    const validator = assert(
      destinationD(this.mapsService, {
        update: destination,
      }),
    );

    const output = await validator({
      ...args,
      updatedAt: timestamp(),
    });

    await this.fs
      .doc(`organizations/${org.uid}/destinations/${destinationId}`)
      .update(output);
  }

  async destroyDestination(destinationId: string) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    await this.fs
      .doc<IDestination>(
        `organizations/${org.uid}/destinations/${destinationId}`,
      )
      .delete();
  }

  getTag(id: string) {
    const query = () =>
      this.tagService
        .getTag(id)
        .pipe(distinctUntilChanged<Tag | null>(isEqual));

    return this.resolveQuery({
      method: 'getTag',
      args: [id],
      query,
      options: { expirationTime: 1000 * 60 * 15 },
    });
  }

  getTags(args: IGetTagsParams = {}) {
    const query = () =>
      this.tagService.getTags(args).pipe(distinctUntilChanged<Tag[]>(isEqual));

    const options = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getTags',
      args: [args],
      query,
      options,
    });
  }

  async addTag(args: {
    type: 'client' | 'driver';
    label: string;
    description: string;
    color: string;
    favorite: boolean;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const validator = assert(tagCreateD());

    const output = await validator({
      ...args,
      createdAt: timestamp(),
      updatedAt: timestamp(),
      uid: this.fs.createId(),
    });

    await this.fs
      .doc(`organizations/${org.uid}/tags/${output.uid}`)
      .set(output);

    return output.uid;
  }

  async updateTag(tagId: string, args: Partial<FirestoreInput<ITag>>) {
    const [org, dispatcher, tag] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getTag(tagId)),
    ]);

    if (!org || !dispatcher || !tag) {
      return;
    }

    const validator = assert(tagUpdateD());

    const output = await validator({
      ...args,
      updatedAt: timestamp(),
    });

    await this.fs.doc(`organizations/${org.uid}/tags/${tagId}`).update(output);
  }

  async destroyTag(tagId: string) {
    const deleteTag: (data: { tagId: string }) => Observable<any> =
      this.fns.httpsCallable('deleteTag');

    try {
      return await toPromise(deleteTag({ tagId }));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  getNoteThreads(args: IGetNoteThreadsParams): Observable<NoteThread[]> {
    if (!this.organization) {
      throw new Error('Must be signed in');
    }

    const query = () =>
      this.noteThreadService.getNoteThreads(args).pipe(
        switchMap((threads) =>
          threads.length === 0
            ? of(threads)
            : combineLatest(
                threads.map((thread) => {
                  const obs: Observable<unknown>[] = [];

                  if (thread.clientId) {
                    obs.push(
                      this.getClient(thread.clientId).pipe(
                        tap((client) => (thread.client = client!)),
                      ),
                    );
                  }

                  if (thread.driverId) {
                    obs.push(
                      this.getDriver(thread.driverId).pipe(
                        tap((driver) => (thread.driver = driver!)),
                      ),
                    );
                  }

                  if (thread.rideRequestId) {
                    obs.push(
                      this.getRideRequest(thread.rideRequestId).pipe(
                        tap(
                          (rideRequest) => (thread.rideRequest = rideRequest!),
                        ),
                      ),
                    );
                  }

                  if (obs.length === 0) return of(thread);

                  return combineLatest(obs).pipe(map(() => thread));
                }),
              ),
        ),
      );

    const options = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getNoteThreads',
      args: [args],
      query,
      options,
    });
  }

  async addThread(args: {
    clientId?: string;
    driverId?: string;
    rideRequestId?: string;
    flag: boolean;
    text: string;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const noteId = this.fs.createId();
    const threadId = this.fs.createId();

    const profileRefs: DocumentReference[] = [];

    if (args.clientId) {
      profileRefs.push(
        this.fs.doc<IClient>(
          `organizations/${org.uid}/clients/${args.clientId}`,
        ).ref,
      );
    }

    if (args.driverId) {
      profileRefs.push(
        this.fs.doc<IDriver>(
          `organizations/${org.uid}/drivers/${args.driverId}`,
        ).ref,
      );
    }

    if (args.rideRequestId) {
      profileRefs.push(
        this.fs.doc<IRideRequest>(
          `organizations/${org.uid}/rideRequests/${args.rideRequestId}`,
        ).ref,
      );
    }

    const threadRef = this.fs.doc(
      `organizations/${org.uid}/noteThreads/${threadId}`,
    ).ref;

    await this.fs.firestore.runTransaction(async (transaction) => {
      const snaps = await Promise.all(
        profileRefs.map((ref) => transaction.get(ref)),
      );

      if (snaps.some((snap) => !snap.exists)) {
        throw new Error(
          `Invalid associated ID for thread: ${snaps
            .map((s) => s.ref.path)
            .join(', ')}`,
        );
      }

      if (args.flag) {
        for (const snap of snaps) {
          transaction.update(snap.ref, {
            updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
            flags: firebase.firestore.FieldValue.increment(1),
          });
        }
      }

      transaction.set(
        threadRef,
        assert(NOTE_THREAD_CREATE_DECODER)({
          ...args,
          uid: threadId,
          createdAt: firebase.firestore.FieldValue.serverTimestamp(),
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          noteAddedAt: firebase.firestore.FieldValue.serverTimestamp(),
          notes: [
            {
              uid: noteId,
              createdAt: Date.now(),
              createdById: dispatcher.uid,
              updatedAt: Date.now(),
              text: args.text,
            },
          ],
        }),
      );
    });
  }

  async updateThread(args: {
    noteThreadId: string;
    flag: boolean;
    suppressTodo?: boolean;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const threadRef = this.fs.doc<INoteThread>(
      `organizations/${org.uid}/noteThreads/${args.noteThreadId}`,
    ).ref;

    await this.fs.firestore.runTransaction(async (transaction) => {
      const threadSnap = await transaction.get(threadRef);

      if (!threadSnap.exists) return;

      const threadData = threadSnap.data()!;

      if (
        threadData.flag === args.flag &&
        threadData.suppressTodo === args.suppressTodo
      ) {
        return;
      }

      const snaps: Array<
        firebase.firestore.DocumentSnapshot<firebase.firestore.DocumentData>
      > = [];

      if (threadData.clientId) {
        const clientSnap = await transaction.get(
          this.fs.doc<IClient>(
            `organizations/${org.uid}/clients/${threadData.clientId}`,
          ).ref,
        );

        if (!clientSnap.exists) {
          throw new Error(
            `Can't update noteThread because clientId is invalid`,
          );
        }

        snaps.push(clientSnap);
      }

      if (threadData.driverId) {
        const driverSnap = await transaction.get(
          this.fs.doc<IDriver>(
            `organizations/${org.uid}/drivers/${threadData.driverId}`,
          ).ref,
        );

        if (!driverSnap.exists) {
          throw new Error(
            `Can't update noteThread because driverId is invalid`,
          );
        }

        snaps.push(driverSnap);
      }

      if (threadData.rideRequestId) {
        const rideRequestSnap = await transaction.get(
          this.fs.doc<IRideRequest>(
            `organizations/${org.uid}/rideRequests/${threadData.rideRequestId}`,
          ).ref,
        );

        if (!rideRequestSnap.exists) {
          throw new Error(
            `Can't update noteThread because rideRequestId is invalid`,
          );
        }

        snaps.push(rideRequestSnap);
      }

      for (const snap of snaps) {
        transaction.update(snap.ref, {
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          flags: firebase.firestore.FieldValue.increment(args.flag ? 1 : -1),
        });
      }

      transaction.update(
        threadRef,
        assert(NOTE_THREAD_UPDATE_DECODER)({
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          flag: args.flag,
          suppressTodo: args.suppressTodo ?? threadData.suppressTodo ?? false,
        }),
      );
    });
  }

  async deleteThread(args: { noteThreadId: string }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    await this.fs
      .doc(`organizations/${org.uid}/noteThreads/${args.noteThreadId}`)
      .delete();
  }

  async addNote(args: { noteThreadId: string; text: string }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const noteId = this.fs.createId();

    const threadRef = this.fs.doc(
      `organizations/${org.uid}/noteThreads/${args.noteThreadId}`,
    ).ref;

    await this.fs.firestore.runTransaction(async (transaction) => {
      const threadSnap = await transaction.get(threadRef);

      if (!threadSnap.exists) return;

      transaction.update(
        threadRef,
        assert(NOTE_THREAD_UPDATE_DECODER)({
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          noteAddedAt: firebase.firestore.FieldValue.serverTimestamp(),
          notes: firebase.firestore.FieldValue.arrayUnion(
            assert(NOTE_CREATE_DECODER)({
              uid: noteId,
              createdAt: Date.now(),
              createdById: dispatcher.uid,
              updatedAt: Date.now(),
              text: args.text,
            }),
          ),
        }),
      );
    });
  }

  async updateNote(args: {
    noteThreadId: string;
    noteId: string;
    text: string;
  }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const threadRef = this.fs.doc(
      `organizations/${org.uid}/noteThreads/${args.noteThreadId}`,
    ).ref;

    await this.fs.firestore.runTransaction(async (transaction) => {
      const threadSnap = await transaction.get(threadRef);

      const thread = threadSnap.data() as INoteThread;

      thread.notes.forEach((note) => {
        if (note.uid !== args.noteId) return;

        note.updatedAt = Date.now();
        note.text = args.text;
      });

      transaction.update(
        threadRef,
        assert(NOTE_THREAD_UPDATE_DECODER)({
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          notes: assert(arrayD(NOTE_CREATE_DECODER))(thread.notes),
        }),
      );
    });
  }

  async deleteNote(args: { noteThreadId: string; noteId: string }) {
    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) {
      return;
    }

    const threadRef = this.fs.doc(
      `organizations/${org.uid}/noteThreads/${args.noteThreadId}`,
    ).ref;

    await this.fs.firestore.runTransaction(async (transaction) => {
      const threadSnap = await transaction.get(threadRef);

      const thread = threadSnap.data() as INoteThread | undefined;

      if (!thread) return;
      if (thread.notes[0].uid === args.noteId) return;

      thread.notes = thread.notes.filter((n) => n.uid !== args.noteId);

      transaction.update(
        threadRef,
        assert(NOTE_THREAD_UPDATE_DECODER)({
          updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
          notes: assert(arrayD(NOTE_CREATE_DECODER))(thread.notes),
        }),
      );
    });
  }

  getRideRequest(
    id: string,
    options: {
      dispatcher?: boolean;
      driver?: boolean;
      recurringRideRequest?: boolean;
    } = {},
  ) {
    const query = () =>
      this.rideRequestService.getRideRequest(id).pipe(
        switchMap((rideRequest) =>
          !rideRequest || !options.dispatcher
            ? of(rideRequest)
            : this.getDispatcher(rideRequest.requestTakenById).pipe(
                map((dispatcher) => {
                  rideRequest.requestTakenBy = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest || !options.dispatcher || !rideRequest.driverAssignedById
            ? of(rideRequest)
            : this.getDispatcher(rideRequest.driverAssignedById).pipe(
                map((dispatcher) => {
                  rideRequest.driverAssignedBy = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest ||
          !options.dispatcher ||
          !rideRequest.clientNotifiedOfDriverById
            ? of(rideRequest)
            : this.getDispatcher(rideRequest.clientNotifiedOfDriverById).pipe(
                map((dispatcher) => {
                  rideRequest.clientNotifiedOfDriverBy = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest ||
          !options.dispatcher ||
          !rideRequest.idOfDispatcherWhoCancelledRequest
            ? of(rideRequest)
            : this.getDispatcher(
                rideRequest.idOfDispatcherWhoCancelledRequest,
              ).pipe(
                map((dispatcher) => {
                  rideRequest.dispatcherWhoCancelledRequest = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest || !options.dispatcher || !rideRequest.driverNotifiedById
            ? of(rideRequest)
            : this.getDispatcher(rideRequest.driverNotifiedById).pipe(
                map((dispatcher) => {
                  rideRequest.driverNotifiedBy = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest ||
          !options.dispatcher ||
          !rideRequest.driverNotifiedOfCancellationById
            ? of(rideRequest)
            : this.getDispatcher(
                rideRequest.driverNotifiedOfCancellationById,
              ).pipe(
                map((dispatcher) => {
                  rideRequest.driverNotifiedOfCancellationBy = dispatcher!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest || !options.driver || !rideRequest.driverId
            ? of(rideRequest)
            : this.getDriver(rideRequest.driverId).pipe(
                map((driver) => {
                  rideRequest!.driver = driver!;
                  return rideRequest;
                }),
              ),
        ),
        switchMap((rideRequest) =>
          !rideRequest ||
          !options.recurringRideRequest ||
          !rideRequest.recurringRideRequestId
            ? of(rideRequest)
            : this.rideRequestService
                .getRecurringRideRequest(rideRequest.recurringRideRequestId)
                .pipe(
                  map((r) => {
                    rideRequest!.recurringRideRequest = r!;
                    return rideRequest;
                  }),
                ),
        ),
        distinctUntilChanged<RideRequest | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getRideRequest',
      args: [id, options],
      query,
    });
  }

  getRideRequests(
    args: IRideRequestsGetParams = {},
    options: { driver?: boolean; recurringRideRequest?: boolean } = {},
  ): Observable<RideRequest[]> {
    const query = () =>
      this.rideRequestService.getRideRequests(args).pipe(
        switchMap((rideRequests) =>
          rideRequests.length === 0 || !options.driver
            ? of(rideRequests)
            : combineLatest(
                rideRequests.map((request) =>
                  request.driverId
                    ? this.getDriver(request.driverId)
                    : of(null),
                ),
              ).pipe(
                map((drivers) => {
                  rideRequests.forEach((request, index) => {
                    request.driver = drivers[index];
                  });

                  return rideRequests;
                }),
              ),
        ),
        switchMap((rideRequests) =>
          rideRequests.length === 0 || !options.recurringRideRequest
            ? of(rideRequests)
            : combineLatest(
                rideRequests.map((request) =>
                  request.recurringRideRequestId
                    ? this.getRecurringRideRequest(
                        request.recurringRideRequestId,
                      )
                    : of(null),
                ),
              ).pipe(
                map((rrr) => {
                  rideRequests.forEach((request, index) => {
                    request.recurringRideRequest = rrr[index];
                  });

                  return rideRequests;
                }),
              ),
        ),
        distinctUntilChanged<RideRequest[]>(isEqual),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getRideRequests',
      args: [args, options],
      query,
      options: cacheOptions,
    });
  }

  getRecurringRideRequest(
    id: string,
    options: { driver?: boolean } = {},
  ): Observable<RecurringRideRequest | null> {
    const query = () =>
      this.rideRequestService.getRecurringRideRequest(id).pipe(
        switchMap((rr) =>
          !rr || !options.driver || !rr.driverId
            ? of(rr)
            : this.getDriver(rr.driverId).pipe(
                map((driver) => {
                  rr.driver = driver;
                  return rr;
                }),
              ),
        ),
        distinctUntilChanged<RecurringRideRequest | null>(isEqual),
      );

    return this.resolveQuery({
      method: 'getRecurringRideRequest',
      args: [id, options],
      query,
    });
  }

  getRecurringRideRequests(
    args: IRecurringRideRequestsGetParams = {},
    options: { driver?: boolean } = {},
  ): Observable<RecurringRideRequest[]> {
    const query = () =>
      this.rideRequestService.getRecurringRideRequests(args).pipe(
        switchMap((rr) =>
          rr.length === 0 || !options.driver
            ? of(rr)
            : combineLatest(
                rr.map((request) =>
                  request.driverId
                    ? this.getDriver(request.driverId)
                    : of(null),
                ),
              ).pipe(
                map((drivers) => {
                  rr.forEach((request, index) => {
                    request.driver = drivers[index];
                  });

                  return rr;
                }),
              ),
        ),
        distinctUntilChanged<RecurringRideRequest[]>(isEqual),
      );

    const cacheOptions = extractQueryCacheOptions(args);

    return this.resolveQuery({
      method: 'getRecurringRideRequests',
      args: [args, options],
      query,
      options: cacheOptions,
    });
  }

  async addRideRequest(
    args: {
      clientId: string;
      requestMadeBy: string;
      pickupAddress?: {
        label: string | null;
        street: string;
        apartmentNumber: string | null;
        city: string;
        zip: string;
        comments: string | null;
      };
      destinationAddresses: Array<{
        label: string | null;
        street: string;
        apartmentNumber: string | null;
        city: string;
        zip: string;
        comments: string | null;
        tripPurpose: string;
      }>;
      priority: string;
      peopleImpacted: number;
      peopleImpactedDescription: string | null;
      comments: string | null;
      flexible: boolean;
      flexibleSchedule?: string | null;
    } & (
      | {
          requestType: 'ONCE';
          date: Date;
          time: string;
        }
      | {
          requestType: 'RECURRING';
          startDate: Date;
          startTime: string;
          scheduleName: string;
          endDate: Date;
        }
    ),
  ) {
    if (args.destinationAddresses.length === 0) {
      return;
    }

    const [org, dispatcher, client] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getClient(args.clientId)),
    ]);

    if (!org || !dispatcher || !client) {
      return;
    }

    const validator = assert(
      rideRequestCreateD({
        recurring: args.requestType === 'RECURRING',
        mapService: this.mapsService,
        clientAddress: client.address,
      }),
    );

    const output = await validator({
      ...args,
      createdAt: timestamp(),
      updatedAt: timestamp(),
      uid: this.fs.createId(),
      clientId: client.uid,
      requestTakenById: dispatcher.uid,
      // flag: null,
      // hasFlag: false,
      flags: 0,

      driverId: null,
      driverAssignedById: null,
      driverNotifiedAt: null,
      driverNotifiedById: null,
      clientNotifiedOfDriverAt: null,
      clientNotifiedOfDriverById: null,
      estimatedRideSeconds: null,
      estimatedRideMeters: null,
      hasWillingDriver: false,

      idOfDispatcherWhoCancelledRequest: null,
      requestCancelled: false,
      requestCancelledAt: null,
      requestCancelledBy: null,
      cancellationReason: null,
      driverNotifiedOfCancellationAt: null,
      driverNotifiedOfCancellationById: null,

      recurringRideRequestId: null, // will be stripped when using the recurring decoder
      lastGeneratedEndDate: null, // will be stripped when using the non-recurring decoder
    });

    let firstRideRequestId: string;

    if ('datetime' in output) {
      await this.fs
        .doc<FirestoreInput<IRideRequest>>(
          `organizations/${org.uid}/rideRequests/${output.uid}`,
        )
        .set(output);

      firstRideRequestId = output.uid;
    } else {
      await this.fs
        .doc<FirestoreInput<IRecurringRideRequest>>(
          `organizations/${org.uid}/recurringRideRequests/${output.uid}`,
        )
        .set(output);

      const recurring = await this.fs
        .doc<FirestoreInput<IRecurringRideRequest>>(
          `organizations/${org.uid}/recurringRideRequests/${output.uid}`,
        )
        .get()
        .toPromise()
        .then(
          (s) => new RecurringRideRequest(s.data() as IRecurringRideRequest),
        );

      const instanceValidator = assert(
        rideRequestCreateD({
          recurring: false,
          trustAddresses: true,
          trustDate: true,
        }),
      );

      const generatedEndDate = moment
        .tz(recurring.startDate, 'America/Los_Angeles')
        .add(2, 'months');

      const [hours, minutes] = recurring.time
        .split(':')
        .map((s) => parseInt(s, 10));

      const instances = await Promise.all(
        recurring.schedule
          .occurrences({
            end: generatedEndDate,
          })
          .toArray()
          .map((o) => {
            const instanceUid = this.fs.createId();
            const date = o.date
              .clone()
              .set('hours', hours)
              .set('minutes', minutes)
              .toDate();

            return instanceValidator({
              ...output,
              date: output.flexible ? startOfWeek(date) : date,
              recurringRideRequestId: output.uid,
              uid: instanceUid,
            });
          }),
      );

      firstRideRequestId = instances[0].uid;

      this.fs.firestore.runTransaction(async (transaction) => {
        instances.forEach((instanceOutput) => {
          transaction.set(
            this.fs.doc<FirestoreInput<IRideRequest>>(
              `organizations/${org.uid}/rideRequests/${instanceOutput.uid}`,
            ).ref,
            instanceOutput,
          );
        });

        transaction.update(
          this.fs.doc<FirestoreInput<IRecurringRideRequest>>(
            `organizations/${org.uid}/recurringRideRequests/${output.uid}`,
          ).ref,
          {
            lastGeneratedEndDate: generatedEndDate.toDate(),
          },
        );
      });
    }

    return firstRideRequestId;
  }

  /**
   * If the requestType === 'RECURRING', then we update the current ride
   * request, all future ride requests, and we also update the assocaited
   * recurring ride request.
   *
   * If the requestType === 'ONCE', then we only update the current ride
   * request.
   */
  async updateRideRequest(
    rideRequestId: string,
    args:
      | (Partial<
          FirestoreInput<IRideRequest & { date?: Date; time?: string }>
        > & { requestType: 'ONCE' })
      | (Partial<
          FirestoreInput<
            IRecurringRideRequest & {
              startDate?: Date;
              startTime?: string;
              scheduleName?: string;
              endDate?: Date;
            }
          >
        > & { requestType: 'RECURRING' }),
  ) {
    const [org, dispatcher, rideRequest] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getRideRequest(rideRequestId)),
    ]);

    if (!org || !dispatcher || !rideRequest) {
      return;
    }

    const validator = assert(
      rideRequestUpdateD({
        recurring: args.requestType === 'RECURRING',
        mapService: this.mapsService,
        rideRequest,
      }),
    );

    let validatedArgs: Awaited<ReturnType<typeof validator>>;

    try {
      validatedArgs = await validator({
        ...args,
        updatedAt: timestamp(),
      });
    } catch (e) {
      await this.warningService
        .error('updateRideRequest validator')
        .afterClosed()
        .toPromise();

      throw e;
    }

    if (args.requestType === 'ONCE') {
      await this.fs
        .doc<FirestoreInput<IRideRequest>>(
          `organizations/${org.uid}/rideRequests/${rideRequestId}`,
        )
        .update(validatedArgs);
    } else {
      const updateRecurringRideRequest: (data: {
        rideRequestId: string;
        update: Partial<FirestoreInput<IRecurringRideRequest>> &
          Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>;
      }) => Observable<any> = this.fns.httpsCallable(
        'updateRecurringRideRequest',
      );

      // Need to serialize object values when passing to the server
      for (const [prop, value] of Object.entries(validatedArgs)) {
        if (
          value instanceof firebase.firestore.FieldValue &&
          firebase.firestore.FieldValue.serverTimestamp().isEqual(value)
        ) {
          (validatedArgs as any)[prop] = SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL;
        } else if (value instanceof Date) {
          (validatedArgs as any)[
            prop
          ] = `${SERIALIZABLE_SERVER_DATE_SENTINAL}:${value.valueOf()}`;
        }
      }

      try {
        await toPromise(
          updateRecurringRideRequest({
            rideRequestId,
            update: validatedArgs,
          }),
        );
      } catch (e) {
        await this.warningService
          .error('updateRecurringRideRequest')
          .afterClosed()
          .toPromise();

        throw e;
      }
    }
  }

  async duplicateRideRequest(id: string, dates: Date[]) {
    if (dates.length === 0) {
      throw new Error('Must be provided at least one date');
    }

    const [org, dispatcher] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
    ]);

    if (!org || !dispatcher) return;

    const rideRequest = await toPromise(
      this.fs
        .doc<IRideRequest>(`organizations/${org.uid}/rideRequests/${id}`)
        .valueChanges(),
    );

    if (!rideRequest) return;

    const validator = assert(
      rideRequestCreateD({
        recurring: false,
        trustAddresses: true,
      }),
    );

    const outputs: FirestoreInput<IRideRequest>[] = await Promise.all(
      dates.map((date) =>
        validator({
          createdAt: timestamp(),
          updatedAt: timestamp(),
          uid: this.fs.createId(),
          clientId: rideRequest.clientId,
          date,
          time: format(rideRequest.datetime.toDate(), 'HH:mm'),
          pickupAddress: rideRequest.pickupAddress,
          destinationAddresses: rideRequest.destinationAddresses,
          priority: rideRequest.priority,
          requestMadeBy: rideRequest.requestMadeBy,
          requestTakenById: dispatcher.uid,
          comments: rideRequest.comments,
          flexible: rideRequest.flexible,
          flexibleSchedule: rideRequest.flexibleSchedule,
          // flag: null,
          flags: 0,
          peopleImpacted: rideRequest.peopleImpacted ?? 1,
          peopleImpactedDescription:
            rideRequest.peopleImpactedDescription ?? null,

          driverId: rideRequest.driverId,
          driverAssignedById: rideRequest.driverAssignedById && dispatcher.uid,
          driverNotifiedAt: rideRequest.driverNotifiedAt && timestamp(),
          driverNotifiedById: rideRequest.driverNotifiedById && dispatcher.uid,
          clientNotifiedOfDriverAt:
            rideRequest.clientNotifiedOfDriverAt && timestamp(),
          clientNotifiedOfDriverById:
            rideRequest.clientNotifiedOfDriverById && dispatcher.uid,
          estimatedRideSeconds: rideRequest.estimatedRideSeconds,
          estimatedRideMeters: rideRequest.estimatedRideMeters,
          hasWillingDriver: false,

          idOfDispatcherWhoCancelledRequest: null,
          requestCancelled: false,
          requestCancelledAt: null,
          requestCancelledBy: null,
          cancellationReason: null,
          driverNotifiedOfCancellationAt: null,
          driverNotifiedOfCancellationById: null,

          recurringRideRequestId: rideRequest.recurringRideRequestId || null,
        }),
      ),
    );

    await this.fs.firestore.runTransaction(async (transaction) => {
      outputs.forEach((output) => {
        transaction.set(
          this.fs.doc<FirestoreInput<IRideRequest>>(
            `organizations/${org.uid}/rideRequests/${output.uid}`,
          ).ref,
          output,
        );
      });
    });

    outputs.sort((a, b) => compareAsc(a.datetime as Date, b.datetime as Date));

    return outputs.map((o) => o.uid);
  }

  async generateRecurringRideRequests(options: { force?: boolean } = {}) {
    const [user, org] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
    ]);

    if (!user || !org) return;

    const fn = this.fns.httpsCallable('generateRecurringRideRequests');

    await fn(options).toPromise();
  }

  async destroyRideRequest(rideRequestId: string) {
    const deleteRideRequest: (data: {
      rideRequestId: string;
    }) => Observable<any> = this.fns.httpsCallable('deleteRideRequest');

    try {
      return await toPromise(deleteRideRequest({ rideRequestId }));
    } catch (e) {
      console.error(e);
      console.error('Error with cloud function');
    }
  }

  async assignDriver(
    rideRequestId: string,
    driverId: string,
    requestType: 'ONCE' | 'RECURRING',
  ) {
    const [org, dispatcher, rideRequest, driver] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getRideRequest(rideRequestId)),
      toPromise(this.getDriver(driverId)),
    ]);

    if (!org || !dispatcher || !rideRequest || !driver) {
      return;
    }

    const { seconds, meters } = await this.mapsService.directionsDistance([
      driver.address,
      rideRequest.pickupAddress,
      ...rideRequest.destinationAddresses,
      rideRequest.pickupAddress,
      driver.address,
    ]);

    await this.updateRideRequest(rideRequestId, {
      requestType,
      driverId,
      driverAssignedById: dispatcher.uid,
      driverNotifiedAt: null,
      driverNotifiedById: null,
      clientNotifiedOfDriverAt: null,
      clientNotifiedOfDriverById: null,
      estimatedRideSeconds: seconds,
      estimatedRideMeters: meters,
    });
  }

  async unassignDriver(
    rideRequestId: string,
    requestType: 'ONCE' | 'RECURRING',
  ) {
    const [org, dispatcher, rideRequest] = await Promise.all([
      toPromise(this.getOrganization()),
      toPromise(this.getCurrentDispatcher()),
      toPromise(this.getRideRequest(rideRequestId)),
    ]);

    if (!org || !dispatcher || !rideRequest) {
      return;
    }

    await this.updateRideRequest(rideRequestId, {
      requestType,
      driverId: null,
      driverAssignedById: null,
      driverNotifiedAt: null,
      driverNotifiedById: null,
      estimatedRideSeconds: null,
      estimatedRideMeters: null,
    });
  }

  getClientQuestion(id: string) {
    const query = () =>
      this.questionService
        .getQuestion(id)
        .pipe(
          distinctUntilChanged<
            | SelectQuestion
            | TextQuestion
            | BooleanQuestion
            | QuestionSectionBreak
            | null
          >(isEqual),
        );

    return this.resolveQuery({
      method: 'getClientQuestion',
      args: [id],
      query,
    });
  }

  getClientQuestions(
    args: {
      retired?: boolean;
      limit?: number;
      startAfter?: AngularFirestoreDocument<IDispatcher>;
    } = {},
  ) {
    const query = () =>
      this.questionService
        .getQuestions(args)
        .pipe(
          distinctUntilChanged<
            Array<
              | SelectQuestion
              | TextQuestion
              | BooleanQuestion
              | QuestionSectionBreak
            >
          >(isEqual),
        );

    return this.resolveQuery({
      method: 'getClientQuestions',
      args: [args],
      query,
    });
  }

  async addClientSelectQuestion(args: {
    text: string;
    required: boolean;
    options: string[];
    allowOther: boolean;
  }) {
    const [user, org, questions] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestions()),
    ]);

    if (!user || !org || !org.adminEmailAddresses.includes(user.emailAddress)) {
      return;
    }

    const uid = this.fs.createId();

    await this.fs
      .doc<FirestoreInput<ISelectQuestion>>(
        `organizations/${org.uid}/clientQuestions/${uid}`,
      )
      .set({
        ...args,
        updatedAt: timestamp(),
        retiredAt: null,
        uid,
        type: 'select',
        order: getOrderForLastQuestion(questions!),
      });

    return uid;
  }

  async updateClientSelectQuestion(
    questionId: string,
    args: Partial<ISelectQuestion>,
  ) {
    const [user, org, question] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestion(questionId)),
    ]);

    if (
      !user ||
      !org ||
      !org.adminEmailAddresses.includes(user.emailAddress) ||
      !question
    ) {
      return;
    }

    await this.fs
      .doc<FirestoreInput<ISelectQuestion>>(
        `organizations/${org.uid}/clientQuestions/${questionId}`,
      )
      .update({
        ...args,
        updatedAt: timestamp(),
      });
  }

  async addClientTextQuestion(args: {
    text: string;
    required: boolean;
    valueType: 'text' | 'integer' | 'date';
  }) {
    const [user, org, questions] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestions()),
    ]);

    if (!user || !org || !org.adminEmailAddresses.includes(user.emailAddress)) {
      return;
    }

    const uid = this.fs.createId();

    await this.fs
      .doc<FirestoreInput<ITextQuestion>>(
        `organizations/${org.uid}/clientQuestions/${uid}`,
      )
      .set({
        ...args,
        updatedAt: timestamp(),
        retiredAt: null,
        uid,
        type: 'text',
        order: getOrderForLastQuestion(questions!),
      });

    return uid;
  }

  async updateClientTextQuestion(
    questionId: string,
    args: Partial<ITextQuestion>,
  ) {
    const [user, org, question] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestion(questionId)),
    ]);

    if (
      !user ||
      !org ||
      !org.adminEmailAddresses.includes(user.emailAddress) ||
      !question
    ) {
      return;
    }

    await this.fs
      .doc<FirestoreInput<ITextQuestion>>(
        `organizations/${org.uid}/clientQuestions/${questionId}`,
      )
      .update({
        ...args,
        updatedAt: timestamp(),
      });
  }

  async addClientBooleanQuestion(args: { text: string; required: boolean }) {
    const [user, org, questions] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestions()),
    ]);

    if (!user || !org || !org.adminEmailAddresses.includes(user.emailAddress)) {
      return;
    }

    const uid = this.fs.createId();

    await this.fs
      .doc<FirestoreInput<IBooleanQuestion>>(
        `organizations/${org.uid}/clientQuestions/${uid}`,
      )
      .set({
        ...args,
        updatedAt: timestamp(),
        retiredAt: null,
        uid,
        type: 'boolean',
        order: getOrderForLastQuestion(questions!),
      });

    return uid;
  }

  async updateClientBooleanQuestion(
    questionId: string,
    args: Partial<IBooleanQuestion>,
  ) {
    const [user, org, question] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestion(questionId)),
    ]);

    if (
      !user ||
      !org ||
      !org.adminEmailAddresses.includes(user.emailAddress) ||
      !question
    ) {
      return;
    }

    await this.fs
      .doc<FirestoreInput<IBooleanQuestion>>(
        `organizations/${org.uid}/clientQuestions/${questionId}`,
      )
      .update({
        ...args,
        updatedAt: timestamp(),
      });
  }

  async addClientQuestionSectionBreak(args: {
    text: string;
    body: string | null;
  }) {
    const [user, org, questions] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestions()),
    ]);

    if (!user || !org || !org.adminEmailAddresses.includes(user.emailAddress)) {
      return;
    }

    const uid = this.fs.createId();

    await this.fs
      .doc<FirestoreInput<IQuestionSectionBreak>>(
        `organizations/${org.uid}/clientQuestions/${uid}`,
      )
      .set({
        ...args,
        updatedAt: timestamp(),
        retiredAt: null,
        uid,
        type: 'section-break',
        order: getOrderForLastQuestion(questions!),
        required: false,
      });

    return uid;
  }

  async updateClientQuestionSectionBreak(
    questionId: string,
    args: Partial<IQuestionSectionBreak>,
  ) {
    const [user, org, question] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestion(questionId)),
    ]);

    if (
      !user ||
      !org ||
      !org.adminEmailAddresses.includes(user.emailAddress) ||
      !question
    ) {
      return;
    }

    await this.fs
      .doc<FirestoreInput<IQuestionSectionBreak>>(
        `organizations/${org.uid}/clientQuestions/${questionId}`,
      )
      .update({
        ...args,
        updatedAt: timestamp(),
      });
  }

  async updateClientQuestionOrder(
    args: { questionId: string; order: number }[],
  ) {
    const [user, org] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
    ]);

    if (!user || !org || !org.adminEmailAddresses.includes(user.emailAddress)) {
      return;
    }

    await this.fs.firestore.runTransaction(async (transaction) => {
      const questions = await Promise.all(
        args.map((arg) =>
          transaction.get(
            this.fs.doc<IQuestion>(
              `organizations/${org.uid}/clientQuestions/${arg.questionId}`,
            ).ref,
          ),
        ),
      );

      questions.forEach((doc, index) => {
        if (!doc.exists) return;

        const arg = args[index];

        transaction.update(doc.ref, {
          order: arg.order,
          updatedAt: timestamp(),
        });
      });
    });
  }

  async destroyClientQuestion(questionId: string) {
    const [user, org, question] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
      toPromise(this.getClientQuestion(questionId)),
    ]);

    if (
      !user ||
      !org ||
      !org.adminEmailAddresses.includes(user.emailAddress) ||
      !question
    ) {
      return;
    }

    await this.fs
      .doc(`organizations/${org.uid}/clientQuestions/${questionId}`)
      .delete();
  }

  async retryRecentlyFailedEmails() {
    if (!environment.production) return;

    const [user, org] = await Promise.all([
      toPromise(this.getCurrentUser()),
      toPromise(this.getOrganization()),
    ]);

    if (!user || !org) {
      return;
    }

    const emailSnaps = await this.fs
      .collection(`organizations/${org.uid}/emails`, (ref) => {
        return ref
          .where('delivery.state', '==', 'ERROR')
          .where('delivery.startTime', '>', subDays(new Date(), 4));
      })
      .get()
      .toPromise();

    const emails = emailSnaps.docs.filter((snap) => {
      const doc = snap.data() as IEmail;

      return (
        Number.isInteger(doc.delivery?.attempts) &&
        doc.delivery!.attempts! <= 3 &&
        doc.delivery!.error?.includes('Try again later')
      );
    });

    for (const email of emails) {
      // If two dispatchers sign in at the same time, make sure they
      // don't both try to send emails to the same people
      await this.fs.firestore.runTransaction(async (transaction) => {
        const snap = await transaction.get(email.ref);

        const em = snap.data() as IEmail | undefined;

        if (em?.delivery?.state !== 'ERROR') return;

        transaction.update(email.ref, {
          delivery: {
            state: 'RETRY',
          },
        });
      });

      await wait(1000);
    }
  }

  private resolveQuery<T>(args: {
    method: string;
    args?: unknown[];
    query: () => Observable<T>;
    options?: IQueryCacheOptions;
  }): Observable<T> {
    return this.cacheService.resolve(
      'FirestoreService',
      args.method,
      args.args || [],
      args.query,
      args.options,
    );
  }
}

function getOrderForLastQuestion(questions: Question[]) {
  return questions.length > 0 ? questions[questions.length - 1].order + 1 : 0;
}

function firebaseUserIsGoogleUser(
  firebaseUser: firebase.User | null,
  googleID: string,
) {
  return (
    firebaseUser &&
    firebaseUser.providerData.some(
      (provider) =>
        !!provider &&
        provider.providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
        provider.uid === googleID,
    )
  );
}

function patchAuthServiceLogoutMethod(
  authService: AuthService,
  isLoadingService: IsLoadingService,
  firestore: firebase.firestore.Firestore,
) {
  const oldLogout = authService.logout.bind(authService);

  authService.logout = async function () {
    isLoadingService.add();

    const persistence = authService.persistenceSetting$.value;

    if (persistence) {
      await firestore.terminate();
      await firestore.clearPersistence();
    }

    await oldLogout();

    document.location.reload();
  };
}
