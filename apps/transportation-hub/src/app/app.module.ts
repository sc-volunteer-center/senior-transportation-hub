import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_NATIVE_DATE_FORMATS,
  NativeDateAdapter,
} from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
  MatDialogModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { CookieModule } from '@local/ngx-cookie';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { A11yModule } from '@angular/cdk/a11y';
import {
  AngularFireFunctionsModule,
  REGION,
} from '@angular/fire/compat/functions';

import { IsLoadingModule } from '@service-work/is-loading';
import { HomeComponent } from './home.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { PersistancePopupComponent } from './persistance-popup.component';
import {
  ENABLE_PERSISTENCE,
  PERSISTENCE_SETTINGS,
} from '@angular/fire/compat/firestore';
import { getPersistenceSetting } from './auth.service';
import { Injectable } from '@angular/core';

if (!environment.production) {
  console.log('environment', environment);
}

@Injectable()
export class MyDateAdapter extends NativeDateAdapter {
  getFirstDayOfWeek(): number {
    return 1; // configure Monday as the first day of the week
  }
}

@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    A11yModule,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    BrowserModule,
    IsLoadingModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerWithDelay:5000',
    }),
  ],
  declarations: [AppComponent, HomeComponent, PersistancePopupComponent],
  providers: [
    {
      provide: ENABLE_PERSISTENCE,
      useFactory: () => {
        const p = getPersistenceSetting();

        if (!environment.production) {
          console.warn('getPersistenceSetting', p);
        }

        return p;
      },
    },
    {
      provide: PERSISTENCE_SETTINGS,
      useValue: {
        synchronizeTabs: true,
      },
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        closeOnNavigation: true,
        disableClose: false,
        hasBackdrop: true,
      },
    },
    { provide: REGION, useValue: 'us-central1' },
    { provide: DateAdapter, useClass: MyDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MAT_NATIVE_DATE_FORMATS },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
