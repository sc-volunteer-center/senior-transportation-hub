import { Observable, from } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  areAddressLocationsEqual,
  RideRequest,
  RecurringRideRequest,
  TagColors,
  setToStartOf,
  setToEndOf,
} from '@local/models';
import firebase from 'firebase/compat/app';
import { SafeResourceUrl } from '@angular/platform-browser';
import {
  NavigationEnd,
  NavigationError,
  NavigationCancel,
  Event,
  Router,
} from '@angular/router';
import type { FieldValue, Timestamp } from '@firebase/firestore-types';
import { formatDate } from '@angular/common';
import { isEqual, pick } from 'lodash-es';
import { Injectable, TrackByFunction } from '@angular/core';
import { startOfWeek as _startOfWeek, endOfWeek as _endOfWeek } from 'date-fns';
import {
  MatDateRangeSelectionStrategy,
  DateRange,
} from '@angular/material/datepicker';

export { Timestamp, areAddressLocationsEqual };

export function isNonNullable<T>(val: T): val is NonNullable<T> {
  return val !== undefined && val !== null;
}

export function toObservable<T>(promise: Promise<T>) {
  return from(promise).pipe(take(1));
}

export function toPromise<T>(obs: Observable<T>) {
  return obs.pipe(take(1)).toPromise();
}

export function timestamp() {
  return firebase.firestore.FieldValue.serverTimestamp() as FieldValue;
}

export function arrayUnion(value: any) {
  return firebase.firestore.FieldValue.arrayUnion(value) as FieldValue;
}

export function arrayRemove(value: any) {
  return firebase.firestore.FieldValue.arrayRemove(value) as FieldValue;
}

export function isTruthy<T>(value: T): value is NonNullable<T> {
  return !!value;
}

export function areSafeResourceUrlsEqual(
  a?: SafeResourceUrl,
  b?: SafeResourceUrl,
) {
  if (
    a &&
    b &&
    (a as any)['changingThisBreaksApplicationSecurity'] ===
      (b as any)['changingThisBreaksApplicationSecurity']
  ) {
    return true;
  } else if (!a && !b) {
    return true;
  }

  return false;
}

export function isNavigationOver(event: Event) {
  return (
    event instanceof NavigationEnd ||
    event instanceof NavigationError ||
    event instanceof NavigationCancel
  );
}

export function comparer<A>(
  fn: (a: A, b: A) => [string | number, string | number],
  options: { direction?: 'ASC' | 'DESC' } = {},
) {
  return (a: A, b: A) => {
    const [x, y] = fn(a, b);

    if (options.direction === 'DESC') {
      if (x === y) return 0;
      else if (x > y) return -1;
      else return 1;
    } else {
      if (x === y) return 0;
      else if (x > y) return 1;
      else return -1;
    }
  };
}

export function stringComparer(aa: string, bb: string) {
  const a = aa.toLowerCase();
  const b = bb.toLowerCase();

  if (a === b) return 0;
  else if (a === '') return -1;
  else if (b === '') return 1;
  else if (a > b) return 1;
  else return -1;
}

export function numberComparer(a: number, b: number) {
  if (a === b) return 0;
  else if (a > b) return 1;
  else return -1;
}

export const TAG_COLORS = new Map<TagColors, string>([
  // ['Red', '#F44336'],
  ['Pink', '#E91E63'],
  ['Purple', '#9C27B0'],
  // ['Deep Purple', '#673AB7'],
  ['Indigo', '#3F51B5'],
  ['Blue', '#1E88E5'],
  // ['Light Blue', '#29B6F6'],
  ['Cyan', '#00BCD4'],
  ['Teal', '#009688'],
  ['Green', '#66BB6A'],
  ['Lime', '#CDDC39'],
  ['Orange', '#FF9800'],
  ['Deep Orange', '#F4511E'],
  ['Brown', '#795548'],
  ['Gray', '#757575'],
  ['Light Gray', '#E0E0E0'],
] as const);

export const API_VERSION = 7;

export interface IApiDoc {
  updatedAt: Timestamp;
  version: number;
  versionUpdatedAt: Timestamp;
  message: string;
}

/**
 * Creates a click handle function for navigating to a route. Supports opening a link
 * in a new tab if command/control is held down while clicking the link.
 */
export function navigateToPathFn(router: Router) {
  return async function navigateToPath(event: MouseEvent, path: string[]) {
    if (event.ctrlKey || event.metaKey) {
      const currentURL = new URL(document.location.href);
      const newURL = currentURL.origin + path.join('/');
      window.open(newURL, '_blank');
      return true;
    }

    return router.navigate(path);
  };
}

export function isRideRequestInstanceDifferentFromRecurringParent(
  rideRequest: RideRequest,
  recurringRideRequest: RecurringRideRequest,
) {
  const keys = [
    // 'cancellationReason',
    'clientId',
    // 'clientNotifiedOfDriverAt',
    // 'clientNotifiedOfDriverById',
    'comments',
    // 'createdAt',
    // 'destinationAddresses',
    // 'driverAssignedById',
    'driverId',
    // 'driverNotifiedAt',
    // 'driverNotifiedById',
    // 'driverNotifiedOfCancellationAt',
    // 'driverNotifiedOfCancellationById',
    // 'estimatedRideMeters',
    // 'estimatedRideSeconds',
    // 'flags',
    // 'hasWillingDriver',
    // 'idOfDispatcherWhoCancelledRequest',
    'peopleImpacted',
    'peopleImpactedDescription',
    'pickupAddress',
    'priority',
    // 'requestCancelledAt',
    // 'requestCancelledBy',
    'requestMadeBy',
    // 'requestTakenById',
    // 'uid',
    // 'updatedAt',
  ] as const; // as Array<keyof IRideRequestBase>;

  const r = pick(rideRequest, ...keys);
  const rr = pick(recurringRideRequest, ...keys);

  const diffAddresses = rideRequest.destinationAddresses.some(
    (d, i) =>
      recurringRideRequest.destinationAddresses[i].toString() !== d.toString(),
  );

  const diffCancellation =
    !!rideRequest.requestCancelledAt !==
    !!recurringRideRequest.requestCancelledAt;

  return (
    !isEqual(r, rr) ||
    diffAddresses ||
    diffCancellation ||
    formatDate(rideRequest.datetime, 'HH:mm', 'en-US') !==
      recurringRideRequest.time
  );
}

export const ngForTrackByID: TrackByFunction<{ uid: string }> = (_, item) =>
  item.uid;

export const startOfWeek = (val: Date) => setToStartOf(val, 'week');

export const endOfWeek = (val: Date) => setToEndOf(val, 'week');

@Injectable()
export class WeekRangeSelectionStrategy
  implements MatDateRangeSelectionStrategy<Date>
{
  selectionFinished(date: Date | null): DateRange<Date> {
    return this._createFiveDayRange(date);
  }

  createPreview(activeDate: Date | null): DateRange<Date> {
    return this._createFiveDayRange(activeDate);
  }

  private _createFiveDayRange(date: Date | null): DateRange<Date> {
    if (date) {
      const start = startOfWeek(date);
      const end = endOfWeek(date);
      return new DateRange<Date>(start, end);
    }

    return new DateRange<Date>(null, null);
  }
}
