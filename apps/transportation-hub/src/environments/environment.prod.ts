export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyB82hOzLB1hEYpz1xedcdEmr3TLV7glRMw',
    authDomain: 'helping-hands-d70d6.firebaseapp.com',
    databaseURL: 'https://helping-hands-d70d6.firebaseio.com',
    projectId: 'helping-hands-d70d6',
    storageBucket: 'helping-hands-d70d6.appspot.com',
    messagingSenderId: '335153671785',
  },
  // was used by `google.service.ts`
  // google: {
  //   apiKey: 'AIzaSyB82hOzLB1hEYpz1xedcdEmr3TLV7glRMw',
  //   clientId:
  //     '335153671785-2mcqdm74vdpmedob2dp11f77cmgq9adn.apps.googleusercontent.com',
  //   discoveryDocs: [
  //     'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest',
  //   ],
  //   scopes: ['https://mail.google.com/'],
  // },
};
