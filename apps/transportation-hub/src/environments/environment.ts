// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDOfCD5ee-ZvlUwM4wd8Mg32NeEibrYzjk',
    authDomain: 'trans-hub-dev.firebaseapp.com',
    databaseURL: 'https://trans-hub-dev.firebaseio.com',
    projectId: 'trans-hub-dev',
    storageBucket: 'trans-hub-dev.appspot.com',
    messagingSenderId: '997465074840',
  },
  // was used by `google.service.ts`
  // google: {
  //   apiKey: 'AIzaSyDOfCD5ee-ZvlUwM4wd8Mg32NeEibrYzjk',
  //   clientId:
  //     '997465074840-oope2heghsrtd0tkjuoln65m7eft4org.apps.googleusercontent.com',
  //   discoveryDocs: [
  //     'https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest',
  //   ],
  //   scopes: ['https://mail.google.com/'],
  // },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
