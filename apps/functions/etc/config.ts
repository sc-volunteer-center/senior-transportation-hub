import * as functions from 'firebase-functions';

export const config: {
  google: {
    client_id: string;
    client_secret: string;
  };
  uri: {
    root: string;
  };
} = functions.config() as any;
