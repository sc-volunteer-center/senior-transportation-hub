import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { IUser, IOrganization } from '@local/models';
import { Decoder, isDecoderSuccess } from 'ts-decoders';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp(functions.config().firebase);

export const db = admin.firestore();

db.settings({ timestampsInSnapshots: true });

export async function getUser(
  email: string,
  transaction?: FirebaseFirestore.Transaction,
) {
  const user = await (transaction
    ? transaction.get(db.doc(`users/${email}`))
    : db.doc(`users/${email}`).get());

  return user && (user.data() as IUser | null);
}

export async function mergeUser(
  emailAddress: string,
  args: {
    name: string | null;
    photoUrl: string | null;
  } = { name: null, photoUrl: null },
) {
  await db.doc(`users/${emailAddress}`).set(
    {
      emailAddress,
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
      ...args,
    },
    { merge: true },
  );

  return await getUser(emailAddress);
}

export async function getOrganization(
  uid?: string,
  transaction?: FirebaseFirestore.Transaction,
) {
  if (!uid) return null;

  const org = await (transaction
    ? transaction.get(db.doc(`organizations/${uid}`))
    : db.doc(`organizations/${uid}`).get());

  return org && (org.data() as IOrganization);
}

/** Verify user is authenticated with firebase or throw error */
export function assertAuthenticated(
  context: functions.https.CallableContext,
): asserts context is functions.https.CallableContext & {
  auth: {
    uid: string;
    token: admin.auth.DecodedIdToken & { email: string };
  };
} {
  if (context.auth?.token.email) return;

  throw new functions.https.HttpsError('unauthenticated', 'must be signed in');
}

export function validateParams<R, I>(decoder: Decoder<R, I>, params: I): R {
  const result = decoder.decode(params);

  if (isDecoderSuccess(result)) return result.value;

  throw new functions.https.HttpsError(
    'invalid-argument',
    result.map((r) => r.message).join('\n'),
  );
}

export function createId() {
  return db.collection('/fake').doc().id;
}

class Logger {
  log(label: string, ...data: unknown[]) {
    functions.logger.log(...this.parseData(label, data));
  }

  warn(label: string, ...data: unknown[]) {
    functions.logger.warn(...this.parseData(label, data));
  }

  error(label: string, ...data: unknown[]) {
    functions.logger.error(...this.parseData(label, data));
  }

  // functions.logger only wants a single JSON object so here
  // we ensure that, at most, two arguments are provided to
  // functions.logger.
  private parseData(label: string, data: unknown[]) {
    if (data.length === 0) return [label];
    if (data.length === 1) return [label, data[0]];
    return [label, data];
  }
}

export const logger = new Logger();
