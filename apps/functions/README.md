In order to run functions locally:

1. You will need to add the service account key to `./tasks/trans-hub-dev-emulators-credentials.private.json` as explained here: https://firebase.google.com/docs/functions/local-emulator.
2. You need to add the functions config to this monorepo's root so it can be picked up by the local emulator. Use `firebase functions:config:get > .runtimeconfig.json`.
