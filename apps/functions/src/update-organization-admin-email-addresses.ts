import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  getOrganization,
  db,
  mergeUser,
  assertAuthenticated,
} from '../etc/shared';

export const updateOrganizationAdminEmailAddresses = functions.https.onCall(
  async (data: { adminEmailAddresses?: string[] }, context) => {
    assertAuthenticated(context);

    if (!data.adminEmailAddresses) return;

    const currentUser = await getUser(context.auth.token.email);

    if (
      !currentUser ||
      !data.adminEmailAddresses.includes(currentUser.emailAddress)
    ) {
      return;
    }

    const organization = await getOrganization(currentUser.organizationId);

    if (!organization) return;

    const orgDoc = db.doc(`organizations/${organization.uid}`);
    const oldUserDocs = (organization.adminEmailAddresses as string[]).map(
      (email) => db.doc(`users/${email}`),
    );
    const newUserDocs = data.adminEmailAddresses.map((email) =>
      db.doc(`users/${email}`),
    );
    const timestamp = admin.firestore.FieldValue.serverTimestamp();

    await Promise.all(
      data.adminEmailAddresses.map((email) => mergeUser(email)),
    );

    return db.runTransaction(async (transaction) => {
      const org = (await transaction.get(orgDoc)).data();

      if (!org) return;

      const [oldUserSnaps, newUserSnaps] = await Promise.all([
        Promise.all(oldUserDocs.map((doc) => transaction.get(doc))),
        Promise.all(newUserDocs.map((doc) => transaction.get(doc))),
      ]);

      const newUsers = newUserSnaps.map((snap) => snap.data()!);

      if (
        newUsers.some(
          (user) =>
            !user ||
            (user.organizationId &&
              (user.organizationId !== org.uid ||
                user.emailAddress === org.dispatcherEmailAddress)),
        )
      ) {
        return;
      }

      oldUserSnaps.forEach((snap) => {
        transaction.update(snap.ref, {
          organizationId: null,
          updatedAt: timestamp,
        });
      });

      newUserSnaps.forEach((snap, index) => {
        transaction.set(
          snap.ref,
          {
            emailAddress: newUsers[index].emailAddress,
            organizationId: org.uid,
            updatedAt: timestamp,
          },
          { merge: true },
        );
      });

      return transaction.update(orgDoc, {
        adminEmailAddresses: newUsers.map((user) => user.emailAddress),
        updatedAt: timestamp,
      });
    });
  },
);
