import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
} from '../etc/shared';

import { objectD, stringD } from 'ts-decoders/decoders';

const paramsD = objectD({ tagId: stringD() });

export const deleteTag = functions.https.onCall(
  async (data: { tagId: string }, context) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    await db.runTransaction(async transaction => {
      const user = await getUser(context.auth.token.email, transaction);

      if (!user) return;

      const org = await getOrganization(user.organizationId, transaction);

      if (!org) return;

      const tagSnap = await transaction.get(
        db.doc(`organizations/${org.uid}/tags/${params.tagId}`),
      );

      const tagDoc = tagSnap.data();

      if (!tagDoc) {
        throw new functions.https.HttpsError(
          `invalid-argument`,
          `Invalid "tagId".`,
        );
      }

      if (tagDoc.type === 'client') {
        const clientSnaps = await transaction.get(
          db
            .collection(`organizations/${org.uid}/clients`)
            .where('tagIds', 'array-contains', tagDoc.uid),
        );

        clientSnaps.forEach(doc => {
          transaction.update(doc.ref, {
            tagIds: admin.firestore.FieldValue.arrayRemove(tagDoc.uid),
          });
        });
      } else if (tagDoc.type === 'driver') {
        const driverSnaps = await transaction.get(
          db
            .collection(`organizations/${org.uid}/drivers`)
            .where('tagIds', 'array-contains', tagDoc.uid),
        );

        driverSnaps.forEach(doc => {
          transaction.update(doc.ref, {
            tagIds: admin.firestore.FieldValue.arrayRemove(tagDoc.uid),
          });
        });
      } else {
        throw new functions.https.HttpsError(
          `internal`,
          `Unexpected tag type "${tagDoc.type}"`,
        );
      }

      transaction.delete(tagSnap.ref);
    });
  },
);
