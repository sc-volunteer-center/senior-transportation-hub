import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
  logger,
} from '../etc/shared';
import { OAuth2Client, LoginTicket } from 'google-auth-library';
import {
  anyOfD,
  exactlyD,
  objectD,
  optionalD,
  stringD,
  matchD,
} from 'ts-decoders/decoders';
import { areDecoderErrors, DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  IClient,
  IDispatcher,
  IDispatcherShift,
  IDriver,
  IRideRequest,
  ITag,
  ISelectQuestion,
  ITextQuestion,
  IBooleanQuestion,
  IQuestionSectionBreak,
  IQuestion,
  IAnswer,
} from '@local/models';

export const csvExportApi = functions.https.onRequest(async (req, res) => {
  const email = await authenticate(req, res);

  if (!email) return;

  const user = await getUser(email);

  if (!user?.organizationId) {
    logger.warn('Invalid user', email, user);
    res.status(403).send({ message: 'Invalid user' });
    return;
  }

  const organization = await getOrganization(user.organizationId);

  if (!organization) {
    res
      .status(403)
      .send({ message: 'User not associated with an organization' });
    return;
  }

  if (!organization.adminEmailAddresses.includes(user.emailAddress)) {
    res.status(403).send({ message: 'User not an org admin' });
    return;
  }

  const result = paramsD.decode(req.body);

  if (areDecoderErrors(result)) {
    logger.warn('Invalid request params', req.body, result);
    res.status(400).send({
      errors: result,
      message: 'Invalid request params',
    });

    return;
  }

  const params = { ...result.value, organizationId: organization.uid };

  let json: ReadonlyArray<readonly string[]> = [];

  switch (params.target) {
    case 'requests': {
      const rideRequests = await getRideRequests(params);

      json = [
        RIDE_REQUEST_HEADER,
        ...rideRequests.map((r) =>
          normalizeCsvData(RIDE_REQUEST_HEADER, serializeRideRequest(r)),
        ),
      ];
      break;
    }
    case 'clients': {
      const [clients, tags] = await Promise.all([
        getClients(params),
        getTags(params),
      ]);

      json = [
        CLIENT_HEADER,
        ...clients.map((r) =>
          normalizeCsvData(CLIENT_HEADER, serializeClient(r, tags)),
        ),
      ];
      break;
    }
    case 'volunteers': {
      const [drivers, tags] = await Promise.all([
        getDrivers(params),
        getTags(params),
      ]);

      json = [
        DRIVER_HEADER,
        ...drivers.map((r) =>
          normalizeCsvData(DRIVER_HEADER, serializeDriver(r, tags)),
        ),
      ];
      break;
    }
    case 'dispatchers': {
      const dispatchers = await getDispatchers(params);

      json = [
        DISPATCHER_HEADER,
        ...dispatchers.map(
          (r) =>
            normalizeCsvData(
              DISPATCHER_HEADER,
              serializeDispatcher(r),
            ) as string[],
        ),
      ];
      break;
    }
    case 'dispatcher shifts': {
      const [dispatchers, dispatcherShifts] = await Promise.all([
        getDispatchers(params),
        getDispatcherShifts(params),
      ]);

      json = [
        DISPATCHER_SHIFT_HEADER,
        ...dispatcherShifts.map(
          (r) =>
            normalizeCsvData(
              DISPATCHER_SHIFT_HEADER,
              serializeDispatcherShift(r, dispatchers),
            ) as string[],
        ),
      ];
      break;
    }
    case 'tags': {
      const tags = await getTags(params);

      json = [
        TAG_HEADER,
        ...tags.map(
          (r) => normalizeCsvData(TAG_HEADER, serializeTag(r)) as string[],
        ),
      ];
      break;
    }
    case 'client questions': {
      const questions = await getClientQuestions(params);

      json = [
        CLIENT_QUESTION_HEADER,
        ...questions.map(
          (r) =>
            normalizeCsvData(
              CLIENT_QUESTION_HEADER,
              serializeClientQuestion(r),
            ) as string[],
        ),
      ];
      break;
    }
    case 'client answers': {
      const answers = await getClientAnswers(params);

      json = [
        CLIENT_ANSWER_HEADER,
        ...answers.map(
          (r) =>
            normalizeCsvData(
              CLIENT_ANSWER_HEADER,
              serializeClientAnswer(r),
            ) as string[],
        ),
      ];
      break;
    }
  }

  res.status(200).send({
    success: true,
    result: json,
  });
});

async function authenticate(
  req: functions.https.Request,
  res: functions.Response<{ [key: string]: unknown }>,
) {
  if (req.method !== 'POST') {
    res.status(404).send({ message: 'Must be a POST request' });
    return false;
  }

  const authHeader = req.header('Authorization');

  if (!authHeader?.startsWith('Bearer ')) {
    logger.warn('Mangled token', authHeader);
    res.status(403).send({ message: 'Mangled token' });
    return false;
  }

  let info: LoginTicket;

  try {
    info = await new OAuth2Client().verifyIdToken({
      idToken: authHeader.slice(7),
    });
  } catch (e) {
    logger.error('error verifying ID token', e);
    res.status(403).send({ message: 'Invalid token' });
    return;
  }

  const token = info.getPayload();

  if (!token?.email_verified) {
    logger.warn('Invalid token details', token);
    res.status(403).send({ message: 'Invalid user' });
    return false;
  }

  return token.email;
}

const dateD = matchD(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z/, {
  errorMsg: 'invalid date string',
}).chain((s) => {
  const date = new Date(Date.parse(s));

  if (isNaN(date as any)) {
    return new DecoderError(s, 'invalid-date', 'invalid date string');
  }

  return new DecoderSuccess(date);
});

const paramsD = objectD(
  {
    target: anyOfD(
      [
        exactlyD('requests'),
        exactlyD('clients'),
        exactlyD('volunteers'),
        exactlyD('dispatchers'),
        exactlyD('dispatcher shifts'),
        exactlyD('tags'),
        exactlyD('client questions'),
        exactlyD('client answers'),
      ],
      {
        errorMsg: (input) =>
          new DecoderError(
            input,
            'invalid-value',
            'must be one of "requests", "clients", "volunteers", "dispatchers", ' +
              '"dispatcher shifts", "tags", "client questions", "client answers"',
          ),
      },
    ),
    startDate: optionalD(dateD).map((a) => a || null),
    endDate: optionalD(dateD).map((a) => a || null),
  },
  {
    allErrors: true,
  },
);

function isNonNullable<T>(val: T): val is NonNullable<T> {
  return !!val;
}

function stringComparer(
  a: string,
  b: string,
  dir = 'asc' as 'asc' | 'desc',
): 1 | 0 | -1 {
  if (a < b) return dir === 'asc' ? -1 : 1;
  if (a > b) return dir === 'asc' ? 1 : -1;
  return 0;
}

function dateComparer(
  aa: Date,
  bb: Date,
  dir = 'asc' as 'asc' | 'desc',
): 1 | 0 | -1 {
  const a = aa.valueOf();
  const b = bb.valueOf();

  if (a < b) return dir === 'asc' ? -1 : 1;
  if (a > b) return dir === 'asc' ? 1 : -1;
  return 0;
}

// converts timestamp values from firestore into date values
const timestampConverter: admin.firestore.FirestoreDataConverter<any> = (() => {
  function isPlainObject(obj: unknown): obj is object {
    if (typeof obj !== 'object' || obj === null) return false;

    let proto = obj;
    while (Object.getPrototypeOf(proto) !== null) {
      proto = Object.getPrototypeOf(proto);
    }

    return Object.getPrototypeOf(obj) === proto;
  }

  function convertTimestamp(input: unknown): unknown {
    if (!input) {
      return input;
    } else if (input instanceof admin.firestore.Timestamp) {
      return input.toDate();
    } else if (Array.isArray(input)) {
      return input.map((item) => convertTimestamp(item));
    } else if (isPlainObject(input)) {
      return Object.fromEntries(
        Object.entries(input).map(([key, value]) => [
          key,
          convertTimestamp(value),
        ]),
      );
    }

    return input;
  }

  return {
    toFirestore(obj: Record<string, any>): admin.firestore.DocumentData {
      return obj;
    },
    fromFirestore(
      snapshot: admin.firestore.QueryDocumentSnapshot,
    ): admin.firestore.DocumentData {
      return convertTimestamp(snapshot.data()!) as admin.firestore.DocumentData;
    },
  };
})();

function normalizeCsvData(
  headers: readonly string[],
  obj: { [key: string]: any },
) {
  return headers.map((h) => obj[h]);
}

const RIDE_REQUEST_HEADER = [
  'id',
  'clientId',
  'createdAt',
  'createdByDispatcherId',
  'updatedAt',
  'datetime',
  'priority',
  'requestMadeBy',
  'comments',
  'numberOfPeopleImpacted',
  'descriptionOfPeopleImpacted',
  'driverId',
  'driverAssignedByDispatcherId',
  'driverNotifiedAt',
  'driverNotifiedByDispatcherId',
  'clientNotifiedOfDriverAt',
  'clientNotifiedOfDriverByDispatcherId',
  'estimatedRideSeconds',
  'estimatedRideMeters',
  'idOfDispatcherWhoCancelledRequest',
  'requestCancelledAt',
  'requestCancelledBy',
  'cancellationReason',
  'driverNotifiedOfCancellationAt',
  'driverNotifiedOfCancellationByDispatcherId',
  'pickup address label',
  'pickup address street',
  'pickup address apartment / space number',
  'pickup address city',
  'pickup address zip',
  'pickup address comments',
  'pickup address latitude',
  'pickup address longitude',
  'totalDestinations',
  'first destination address trip purpose',
  'first destination address label',
  'first destination address street',
  'first destination address apartment / space number',
  'first destination address city',
  'first destination address zip',
  'first destination address comments',
  'first destination address latitude',
  'first destination address longitude',
  'second destination address trip purpose',
  'second destination address label',
  'second destination address street',
  'second destination address apartment / space number',
  'second destination address city',
  'second destination address zip',
  'second destination address comments',
  'second destination address latitude',
  'second destination address longitude',
  'third destination address trip purpose',
  'third destination address label',
  'third destination address street',
  'third destination address apartment / space number',
  'third destination address city',
  'third destination address zip',
  'third destination address comments',
  'third destination address latitude',
  'third destination address longitude',
  'fourth destination address trip purpose',
  'fourth destination address label',
  'fourth destination address street',
  'fourth destination address apartment / space number',
  'fourth destination address city',
  'fourth destination address zip',
  'fourth destination address comments',
  'fourth destination address latitude',
  'fourth destination address longitude',
  'fifth destination address trip purpose',
  'fifth destination address label',
  'fifth destination address street',
  'fifth destination address apartment / space number',
  'fifth destination address city',
  'fifth destination address zip',
  'fifth destination address comments',
  'fifth destination address latitude',
  'fifth destination address longitude',
  'isFlexible',
  'flexible schedule description',
  'recurringRideRequestId',
] as const;

function serializeRideRequest(rideRequest: IRideRequest) {
  const firstDest = rideRequest.destinationAddresses[0];
  const secondDest = rideRequest.destinationAddresses[1];
  const thirdDest = rideRequest.destinationAddresses[2];
  const fourthDest = rideRequest.destinationAddresses[3];
  const fifthDest = rideRequest.destinationAddresses[4];

  return {
    id: rideRequest.uid,
    clientId: rideRequest.clientId,
    recurringRideRequestId: rideRequest.recurringRideRequestId,
    createdAt: rideRequest.createdAt,
    createdByDispatcherId: rideRequest.requestTakenById,
    updatedAt: rideRequest.updatedAt,
    datetime: rideRequest.datetime,
    isFlexible: !!rideRequest.flexible,
    ['flexible schedule description']: rideRequest.flexibleSchedule,
    priority: rideRequest.priority,
    requestMadeBy: rideRequest.requestMadeBy,
    comments: rideRequest.comments,
    numberOfPeopleImpacted: rideRequest.peopleImpacted ?? 1,
    descriptionOfPeopleImpacted: rideRequest.peopleImpactedDescription,

    driverId: rideRequest.driverId,
    driverAssignedByDispatcherId: rideRequest.driverAssignedById,
    driverNotifiedAt: rideRequest.driverNotifiedAt,
    driverNotifiedByDispatcherId: rideRequest.driverNotifiedById,
    clientNotifiedOfDriverAt: rideRequest.clientNotifiedOfDriverAt,
    clientNotifiedOfDriverByDispatcherId:
      rideRequest.clientNotifiedOfDriverById,
    estimatedRideSeconds: rideRequest.estimatedRideSeconds,
    estimatedRideMeters: rideRequest.estimatedRideMeters,

    idOfDispatcherWhoCancelledRequest:
      rideRequest.idOfDispatcherWhoCancelledRequest,
    requestCancelledAt: rideRequest.requestCancelledAt,
    requestCancelledBy: rideRequest.requestCancelledBy,
    cancellationReason: rideRequest.cancellationReason,
    driverNotifiedOfCancellationAt: rideRequest.driverNotifiedOfCancellationAt,
    driverNotifiedOfCancellationByDispatcherId:
      rideRequest.driverNotifiedOfCancellationById,

    ['pickup address label']: rideRequest.pickupAddress.label,
    ['pickup address street']: rideRequest.pickupAddress.street,
    ['pickup address apartment / space number']:
      rideRequest.pickupAddress.apartmentNumber,
    ['pickup address city']: rideRequest.pickupAddress.city,
    ['pickup address zip']: rideRequest.pickupAddress.zip,
    ['pickup address comments']: rideRequest.pickupAddress.comments,
    ['pickup address latitude']: rideRequest.pickupAddress.latitude,
    ['pickup address longitude']: rideRequest.pickupAddress.longitude,
    totalDestinations: rideRequest.destinationAddresses.length,
    ['first destination address trip purpose']:
      firstDest && firstDest.tripPurpose,
    ['first destination address label']: firstDest && firstDest.label,
    ['first destination address street']: firstDest && firstDest.street,
    ['first destination address apartment / space number']:
      firstDest && firstDest.apartmentNumber,
    ['first destination address city']: firstDest && firstDest.city,
    ['first destination address zip']: firstDest && firstDest.zip,
    ['first destination address comments']: firstDest && firstDest.comments,
    ['first destination address latitude']: firstDest && firstDest.latitude,
    ['first destination address longitude']: firstDest && firstDest.longitude,
    ['second destination address trip purpose']:
      secondDest && secondDest.tripPurpose,
    ['second destination address label']: secondDest && secondDest.label,
    ['second destination address street']: secondDest && secondDest.street,
    ['second destination address apartment / space number']:
      secondDest && secondDest.apartmentNumber,
    ['second destination address city']: secondDest && secondDest.city,
    ['second destination address zip']: secondDest && secondDest.zip,
    ['second destination address comments']: secondDest && secondDest.comments,
    ['second destination address latitude']: secondDest && secondDest.latitude,
    ['second destination address longitude']:
      secondDest && secondDest.longitude,
    ['third destination address trip purpose']:
      thirdDest && thirdDest.tripPurpose,
    ['third destination address label']: thirdDest && thirdDest.label,
    ['third destination address street']: thirdDest && thirdDest.street,
    ['third destination address apartment / space number']:
      thirdDest && thirdDest.apartmentNumber,
    ['third destination address city']: thirdDest && thirdDest.city,
    ['third destination address zip']: thirdDest && thirdDest.zip,
    ['third destination address comments']: thirdDest && thirdDest.comments,
    ['third destination address latitude']: thirdDest && thirdDest.latitude,
    ['third destination address longitude']: thirdDest && thirdDest.longitude,
    ['fourth destination address trip purpose']:
      fourthDest && fourthDest.tripPurpose,
    ['fourth destination address label']: fourthDest && fourthDest.label,
    ['fourth destination address street']: fourthDest && fourthDest.street,
    ['fourth destination address apartment / space number']:
      fourthDest && fourthDest.apartmentNumber,
    ['fourth destination address city']: fourthDest && fourthDest.city,
    ['fourth destination address zip']: fourthDest && fourthDest.zip,
    ['fourth destination address comments']: fourthDest && fourthDest.comments,
    ['fourth destination address latitude']: fourthDest && fourthDest.latitude,
    ['fourth destination address longitude']:
      fourthDest && fourthDest.longitude,
    ['fifth destination address trip purpose']:
      fifthDest && fifthDest.tripPurpose,
    ['fifth destination address label']: fifthDest && fifthDest.label,
    ['fifth destination address street']: fifthDest && fifthDest.street,
    ['fifth destination address apartment / space number']:
      fifthDest && fifthDest.apartmentNumber,
    ['fifth destination address city']: fifthDest && fifthDest.city,
    ['fifth destination address zip']: fifthDest && fifthDest.zip,
    ['fifth destination address comments']: fifthDest && fifthDest.comments,
    ['fifth destination address latitude']: fifthDest && fifthDest.latitude,
    ['fifth destination address longitude']: fifthDest && fifthDest.longitude,
  };
}

async function getRideRequests(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IRideRequest[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> = db
    .collection(`organizations/${args.organizationId}/rideRequests`)
    .orderBy('datetime', 'asc');

  if (args.startDate) {
    query = query.where('datetime', '>=', args.startDate);
  }

  if (args.endDate) {
    query = query.where('datetime', '<', args.endDate);
  }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IRideRequest)
    .sort((a, b) => {
      let r1 = dateComparer(
        a.datetime as unknown as Date,
        b.datetime as unknown as Date,
      );

      if (r1 === 0) {
        r1 = stringComparer(a.clientId, b.clientId);
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const CLIENT_HEADER = [
  'id',
  'createdAt',
  'createdByDispatcherId',
  'updatedAt',
  'retiredAt',
  'retiredByDispatcherId',
  'firstName',
  'middleName',
  'lastName',
  'phoneNumber',
  'birthdate',
  'racialIdentity',
  'authorizeSharingInfoWithDrivers',
  'mediCalOrSsi',
  'address label',
  'address street',
  'address apartment / space number',
  'address city',
  'address zip',
  'address comments',
  'address latitude',
  'address longitude',
  'comments',
  'tagIds',
  'tags',
  'emergency contact first name',
  'emergency contact last name',
  'emergency contact relationship to client',
  'emergency contact phone number',
  'social worker first name',
  'social worker last name',
  'social worker relationship to client',
  'social worker phone number',
  'other contact 1 label',
  'other contact 1 first name',
  'other contact 1 last name',
  'other contact 1 relationship to client',
  'other contact 1 phone number',
  'other contact 2 label',
  'other contact 2 first name',
  'other contact 2 last name',
  'other contact 2 relationship to client',
  'other contact 2 phone number',
  'nickname',
  'emailAddress',
] as const;

function serializeClient(client: IClient, tags: ITag[]) {
  const clientContacts = [...client.contacts].sort((a, b) => {
    const res1 = stringComparer(
      a.label.toLowerCase().trim(),
      b.label.toLowerCase().trim(),
    );

    if (res1 === 0) {
      return stringComparer(
        a.firstName.toLowerCase().trim(),
        b.firstName.toLowerCase().trim(),
      );
    }

    return res1;
  });

  const emergencyContactIndex = clientContacts.findIndex(
    (c) => c.label.toLowerCase().trim() === 'emergency contact',
  );

  const emergencyContact =
    emergencyContactIndex < 0
      ? null
      : clientContacts.splice(emergencyContactIndex, 1).shift();

  const socialWorkerContactIndex = clientContacts.findIndex(
    (c) => c.label.toLowerCase().trim() === 'social worker',
  );

  const socialWorkerContact =
    socialWorkerContactIndex < 0
      ? null
      : clientContacts.splice(socialWorkerContactIndex, 1).shift();

  return {
    id: client.uid,
    createdAt: client.createdAt,
    createdByDispatcherId: client.createdById,
    updatedAt: client.updatedAt,
    retiredAt: client.retiredAt,
    retiredByDispatcherId: client.retiredById,
    nickname: client.nickname,
    firstName: client.firstName,
    middleName: client.middleName,
    lastName: client.lastName,
    phoneNumber: client.phoneNumber,
    emailAddress: client.emailAddress,
    birthdate: client.birthdate,
    racialIdentity: client.ethnicity,
    authorizeSharingInfoWithDrivers: client.authorizeSharingInfoWithDrivers,
    mediCalOrSsi: client.mediCalOrSsi,
    ['address label']: client.address.label,
    ['address street']: client.address.street,
    ['address apartment / space number']: client.address.apartmentNumber,
    ['address city']: client.address.city,
    ['address zip']: client.address.zip,
    ['address comments']: client.address.comments,
    ['address latitude']: client.address.latitude,
    ['address longitude']: client.address.longitude,
    comments: client.comments,
    tagIds: client.tagIds?.join(', '),
    tags: client.tagIds
      ?.map((id) => tags.find((t) => t.uid === id))
      .filter(isNonNullable)
      .map((tag) => tag.label)
      .join(', '),
    ['emergency contact first name']: emergencyContact?.firstName,
    ['emergency contact last name']: emergencyContact?.lastName,
    ['emergency contact relationship to client']:
      emergencyContact?.relationshipToPerson,
    ['emergency contact phone number']: emergencyContact?.phoneNumber,
    ['social worker first name']: socialWorkerContact?.firstName,
    ['social worker last name']: socialWorkerContact?.lastName,
    ['social worker relationship to client']:
      socialWorkerContact?.relationshipToPerson,
    ['social worker phone number']: socialWorkerContact?.phoneNumber,
    ['other contact 1 label']: clientContacts[0]?.label,
    ['other contact 1 first name']: clientContacts[0]?.firstName,
    ['other contact 1 last name']: clientContacts[0]?.lastName,
    ['other contact 1 relationship to client']:
      clientContacts[0]?.relationshipToPerson,
    ['other contact 1 phone number']: clientContacts[0]?.phoneNumber,
    ['other contact 2 label']: clientContacts[1]?.label,
    ['other contact 2 first name']: clientContacts[1]?.firstName,
    ['other contact 2 last name']: clientContacts[1]?.lastName,
    ['other contact 2 relationship to client']:
      clientContacts[1]?.relationshipToPerson,
    ['other contact 2 phone number']: clientContacts[1]?.phoneNumber,
  };
}

async function getClients(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IClient[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> =
    db.collection(`organizations/${args.organizationId}/clients`);

  // if (args.startDate) {
  //   query = query.where('datetime', '>=', args.startDate);
  // }

  // if (args.endDate) {
  //   query = query.where('datetime', '>=', args.endDate);
  // }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IClient)
    .sort((a, b) => {
      let r1 = stringComparer(
        a.nickname || a.firstName,
        b.nickname || b.firstName,
      );

      if (r1 === 0) {
        r1 = stringComparer(a.lastName, b.lastName);
      }

      if (r1 === 0) {
        r1 = stringComparer(a.middleName || '', b.middleName || '');
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const DRIVER_HEADER = [
  'id',
  'createdAt',
  'createdByDispatcherId',
  'updatedAt',
  'retiredAt',
  'retiredByDispatcherId',
  'firstName',
  'middleName',
  'lastName',
  'driversLicenseExpirationDate',
  'carInsuranceExpirationDate',
  'birthdate',
  'genderIdentity',
  'racialIdentity',
  'phoneNumber',
  'emailAddress',
  'address label',
  'address street',
  'address apartment / space number',
  'address city',
  'address zip',
  'address comments',
  'address latitude',
  'address longitude',
  'comments',
  'tagIds',
  'tags',
  'nickname',
] as const;

function serializeDriver(driver: IDriver, tags: ITag[]) {
  return {
    id: driver.uid,
    createdAt: driver.createdAt,
    createdByDispatcherId: driver.createdById,
    updatedAt: driver.updatedAt,
    retiredAt: driver.retiredAt,
    retiredByDispatcherId: driver.retiredById,
    nickname: driver.nickname,
    firstName: driver.firstName,
    middleName: driver.middleName,
    lastName: driver.lastName,
    driversLicenseExpirationDate: driver.driversLicenseExpirationDate,
    carInsuranceExpirationDate: driver.carInsuranceExpirationDate,
    birthdate: driver.birthdate,
    genderIdentity: driver.genderIdentity,
    racialIdentity: driver.ethnicity,
    phoneNumber: driver.phoneNumber,
    emailAddress: driver.emailAddress,
    ['address label']: driver.address.label,
    ['address street']: driver.address.street,
    ['address apartment / space number']: driver.address.apartmentNumber,
    ['address city']: driver.address.city,
    ['address zip']: driver.address.zip,
    ['address comments']: driver.address.comments,
    ['address latitude']: driver.address.latitude,
    ['address longitude']: driver.address.longitude,
    comments: driver.comments,
    tagIds: driver.tagIds?.join(', '),
    tags: driver.tagIds
      ?.map((id) => tags.find((t) => t.uid === id))
      .filter(isNonNullable)
      .map((tag) => tag.label)
      .join(', '),
  };
}

async function getDrivers(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IDriver[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> =
    db.collection(`organizations/${args.organizationId}/drivers`);

  // if (args.startDate) {
  //   query = query.where('datetime', '>=', args.startDate);
  // }

  // if (args.endDate) {
  //   query = query.where('datetime', '>=', args.endDate);
  // }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IDriver)
    .sort((a, b) => {
      let r1 = stringComparer(
        a.nickname || a.firstName,
        b.nickname || b.firstName,
      );

      if (r1 === 0) {
        r1 = stringComparer(a.lastName, b.lastName);
      }

      if (r1 === 0) {
        r1 = stringComparer(a.middleName || '', b.middleName || '');
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const DISPATCHER_HEADER = [
  'id',
  'createdAt',
  'updatedAt',
  'retiredAt',
  'organizationId',
  'firstName',
  'middleName',
  'lastName',
  'phoneNumber',
  'emailAddress',
  'notes',
] as const;

function serializeDispatcher(dispatcher: IDispatcher) {
  return {
    id: dispatcher.uid,
    createdAt: dispatcher.createdAt,
    updatedAt: dispatcher.updatedAt,
    retiredAt: dispatcher.retiredAt,
    organizationId: dispatcher.organizationId,
    firstName: dispatcher.firstName,
    middleName: dispatcher.middleName,
    lastName: dispatcher.lastName,
    phoneNumber: dispatcher.phoneNumber,
    emailAddress: dispatcher.emailAddress,
    notes: dispatcher.notes,
  };
}

async function getDispatchers(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IDispatcher[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> =
    db.collection(`organizations/${args.organizationId}/dispatchers`);

  // if (args.startDate) {
  //   query = query.where('datetime', '>=', args.startDate);
  // }

  // if (args.endDate) {
  //   query = query.where('datetime', '>=', args.endDate);
  // }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IDispatcher)
    .sort((a, b) => {
      let r1 = stringComparer(a.firstName, b.firstName);

      if (r1 === 0) {
        r1 = stringComparer(a.lastName, b.lastName);
      }

      if (r1 === 0) {
        r1 = stringComparer(a.middleName || '', b.middleName || '');
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const DISPATCHER_SHIFT_HEADER = [
  'id',
  'dispatcherId',
  'dispatcherName',
  'startAt',
  'endAt',
] as const;

function serializeDispatcherShift(
  shift: IDispatcherShift,
  dispatchers: IDispatcher[],
) {
  const dispatcher = dispatchers.find((d) => d.uid === shift.dispatcherId);
  const dispatcherName =
    (dispatcher && [dispatcher.firstName, dispatcher.lastName].join(' ')) ||
    null;

  return {
    id: shift.uid,
    dispatcherId: shift.dispatcherId,
    dispatcherName,
    startAt: shift.startAt,
    endAt: shift.endAt,
  };
}

async function getDispatcherShifts(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IDispatcherShift[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> = db
    .collection(`organizations/${args.organizationId}/dispatcherShifts`)
    .orderBy('startAt', 'asc');

  if (args.startDate) {
    query = query.where('startAt', '>=', args.startDate);
  }

  if (args.endDate) {
    query = query.where('startAt', '<=', args.endDate);
  }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IDispatcherShift)
    .sort((a, b) => {
      let r1 = dateComparer(
        a.startAt as unknown as Date,
        b.startAt as unknown as Date,
      );

      if (r1 === 0) {
        r1 = dateComparer(
          a.endAt as unknown as Date,
          b.endAt as unknown as Date,
        );
      }

      if (r1 === 0) {
        r1 = stringComparer(a.dispatcherId, b.dispatcherId);
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const TAG_HEADER = [
  'id',
  'createdAt',
  'updatedAt',
  'type',
  'label',
  'description',
  'color',
  'favorite',
] as const;

function serializeTag(tag: ITag) {
  return {
    id: tag.uid,
    createdAt: tag.createdAt,
    updatedAt: tag.updatedAt,
    type: tag.type,
    label: tag.label,
    description: tag.description,
    color: tag.color,
    favorite: tag.favorite,
  };
}

async function getTags(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<ITag[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> =
    db.collection(`organizations/${args.organizationId}/tags`);

  // if (args.startDate) {
  //   query = query.where('datetime', '>=', args.startDate);
  // }

  // if (args.endDate) {
  //   query = query.where('datetime', '>=', args.endDate);
  // }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as ITag)
    .sort((a, b) => {
      let r1 = stringComparer(a.type, b.type);

      if (r1 === 0) {
        r1 = stringComparer(a.label, b.label);
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

const CLIENT_QUESTION_HEADER = [
  'id',
  'type',
  'updatedAt',
  'retiredAt',
  'required',
  'order',
  'text',
  'selectQuestionOptions',
  'selectQuestionAllowOther',
  'textQuestionValueType',
  'sectionBreakBody',
] as const;

function serializeClientQuestion(question: IQuestion) {
  return {
    id: question.uid,
    type: question.type,
    updatedAt: question.updatedAt,
    retiredAt: question.retiredAt,
    required: question.required,
    order: question.order,
    text: question.text,

    selectQuestionOptions: (question as ISelectQuestion).options ?? null,
    selectQuestionAllowOther: (question as ISelectQuestion).allowOther ?? null,
    textQuestionValueType: (question as ITextQuestion).valueType ?? null,
    sectionBreakBody: (question as IQuestionSectionBreak).body ?? null,
  };
}

async function getClientQuestions(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IQuestion[]> {
  let query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> =
    db.collection(`organizations/${args.organizationId}/clientQuestions`);

  // if (args.startDate) {
  //   query = query.where('datetime', '>=', args.startDate);
  // }

  // if (args.endDate) {
  //   query = query.where('datetime', '>=', args.endDate);
  // }

  const results = await query.withConverter(timestampConverter).get();

  return results.docs
    .map((d) => d.data() as IQuestion)
    .sort((a, b) => {
      let r1 = stringComparer(a.type, b.type);

      if (r1 === 0) {
        r1 = stringComparer(a.text, b.text);
      }

      if (r1 === 0) {
        return stringComparer(a.uid, b.uid);
      }

      return r1;
    });
}

interface IClientAnswer extends IAnswer {
  clientId: string;
}

const CLIENT_ANSWER_HEADER = [
  'id',
  'questionId',
  'clientId',
  'updatedAt',
  'answer',
] as const;

function serializeClientAnswer(answer: IClientAnswer) {
  return {
    id: `${answer.questionId}:${answer.clientId}`,
    questionId: answer.questionId,
    clientId: answer.clientId,
    updatedAt: answer.updatedAt,
    answer: answer.answer,
  };
}

async function getClientAnswers(args: {
  organizationId: string;
  startDate: Date | null;
  endDate: Date | null;
}): Promise<IClientAnswer[]> {
  const clients = await getClients(args);

  return clients
    .flatMap((c) =>
      c.answers.map((a) => ({
        ...a,
        clientId: c.uid,
      })),
    )
    .sort((a, b) => {
      let r1 = stringComparer(a.questionId, b.questionId);

      if (r1 === 0) {
        r1 = stringComparer(a.clientId, b.clientId);
      }

      if (r1 === 0) {
        return dateComparer(
          a.updatedAt as unknown as Date,
          b.updatedAt as unknown as Date,
        );
      }

      return r1;
    });
}
