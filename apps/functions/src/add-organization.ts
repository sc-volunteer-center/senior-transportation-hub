import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as uuid from 'uuid/v4';
import { getUser, mergeUser, db, assertAuthenticated } from '../etc/shared';

export const addOrganization = functions.https.onCall(
  async (
    data: {
      name?: string;
      dispatcherEmailAddress?: string;
      rideRequestEmailPreface?: string;
    },
    context,
  ) => {
    assertAuthenticated(context);

    if (
      !data.name ||
      !data.dispatcherEmailAddress ||
      !context.auth?.token.email
    ) {
      return;
    }

    const orgId = uuid();

    const user = await getUser(context.auth.token.email);

    if (!user || user.emailAddress === data.dispatcherEmailAddress) return;

    const dispatcherUser = await mergeUser(data.dispatcherEmailAddress);

    if (dispatcherUser && dispatcherUser.organizationId) return;

    const orgDoc = db.doc(`organizations/${orgId}`);
    const userDoc = db.doc(`users/${user.emailAddress}`);
    const dispatcherUserDoc = db.doc(`users/${data.dispatcherEmailAddress}`);

    const timestamp = admin.firestore.FieldValue.serverTimestamp();

    return db.runTransaction(async (transaction) => {
      const [user, dispatcher] = (
        await Promise.all([
          transaction.get(userDoc),
          transaction.get(dispatcherUserDoc),
        ])
      ).map((res) => res.data());

      if (!user || !dispatcher || dispatcher.organizationId) return;

      return transaction
        .set(orgDoc, {
          createdAt: timestamp,
          updatedAt: timestamp,
          uid: orgId,
          name: data.name,
          adminEmailAddresses: [user.emailAddress],
          dispatcherEmailAddress: dispatcher.emailAddress,
          rideRequestEmailPreface: data.rideRequestEmailPreface,
        })
        .update(userDoc, {
          organizationId: orgId,
          updatedAt: timestamp,
        })
        .update(dispatcherUserDoc, {
          organizationId: orgId,
          updatedAt: timestamp,
        });
    });
  },
);
