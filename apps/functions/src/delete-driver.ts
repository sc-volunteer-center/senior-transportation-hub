import * as functions from 'firebase-functions';

import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
} from '../etc/shared';

import { objectD, stringD } from 'ts-decoders/decoders';

const paramsD = objectD({ driverId: stringD() });

/**
 * Delete the driver document and associated rideRequests,
 * subcollections of the rideRequests, and rideRequestContacts associated
 * with the driver.
 */

export const deleteDriver = functions.https.onCall(
  async (data: { driverId: string }, context) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    await db.runTransaction(async transaction => {
      const user = await getUser(context.auth.token.email, transaction);

      if (!user) {
        throw new Error(
          'Could not find user document for given auth.token.email',
        );
      }

      const org = await getOrganization(user.organizationId, transaction);

      if (!org) {
        throw new functions.https.HttpsError(
          'permission-denied',
          'User is not associated with an organization',
        );
      }

      // get driver
      const driverSnap = await transaction.get(
        db.doc(`organizations/${org.uid}/drivers/${params.driverId}`),
      );

      if (!driverSnap.exists) {
        throw new functions.https.HttpsError(
          `invalid-argument`,
          `Invalid "driverId".`,
        );
      }

      // get ride requests associated with driver
      const rideRequestSnaps = await transaction.get(
        db
          .collection(`organizations/${org.uid}/rideRequests`)
          .where('driverId', '==', params.driverId),
      );

      const noteThreadSnaps = await transaction.get(
        db
          .collection(`organizations/${org.uid}/noteThreads`)
          .where('driverId', '==', params.driverId),
      );

      transaction.delete(driverSnap.ref);

      rideRequestSnaps.forEach(s => {
        transaction.delete(s.ref);
      });

      noteThreadSnaps.forEach(snap => {
        transaction.delete(db.doc(snap.ref.path));
      });
    });
  },
);
