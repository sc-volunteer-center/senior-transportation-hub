import * as functions from 'firebase-functions';
import { getUser, getOrganization, db, logger } from '../etc/shared';
import { config } from '../etc/config';
import { google, gmail_v1 } from 'googleapis';
import {
  IOrganizationGoogleAccessToken,
  IOrganization,
  IUser,
} from '@local/models';
import { GaxiosResponse } from 'gaxios';
import { GetTokenResponse } from 'google-auth-library/build/src/auth/oauth2client';

const oauth2Client = new google.auth.OAuth2(
  config.google.client_id,
  config.google.client_secret,
  'postmessage',
);

export const updateOrganizationGoogleAccessCode = functions.https.onCall(
  async (data: { offlineAccessCode?: string }, context) => {
    if (!context.auth || !data.offlineAccessCode) {
      throw new functions.https.HttpsError(
        'invalid-argument',
        `missing required param "offlineAccessCode"`,
      );
    }

    // While we aren't using any kind of transaction here,
    // it shouldn't be an issue given that our app usage is
    // so low.

    const currentUser = await getUser(context.auth.token.email);

    if (!currentUser || !currentUser.organizationId) {
      throw new functions.https.HttpsError(
        'failed-precondition',
        `failed to find profile of current user`,
      );
    }

    const org = await getOrganization(currentUser.organizationId);

    if (!org) {
      throw new functions.https.HttpsError(
        'failed-precondition',
        `failed to find profile of current user's organization`,
      );
    }

    let res: GetTokenResponse['res'], tokens: GetTokenResponse['tokens'];

    try {
      const response = await oauth2Client.getToken(data.offlineAccessCode);

      res = response.res as GetTokenResponse['res'];
      tokens = response.tokens;
    } catch (e) {
      logger.log('error getting token', e);

      throw new functions.https.HttpsError('internal', `internal error`);
    }

    if (!res || res.status !== 200 || !tokens.refresh_token) {
      logger.log('token', res, tokens);

      throw new functions.https.HttpsError(
        'invalid-argument',
        `invalid offlineAccessCode`,
      );
    }

    oauth2Client.setCredentials(tokens);

    const { refresh_token } = tokens;

    let profile: GaxiosResponse<gmail_v1.Schema$Profile>;

    try {
      profile = await google
        .gmail({ version: 'v1', auth: oauth2Client })
        .users.getProfile({ userId: 'me' });
    } catch (e) {
      logger.log('error getting gmail profile', e);

      throw new functions.https.HttpsError(
        'unavailable',
        `error fetching google profile`,
      );
    }

    if (!profile.data.emailAddress) {
      logger.log('invalid gmail profile', profile);

      throw new functions.https.HttpsError(
        'invalid-argument',
        `invalid offlineAccessCode`,
      );
    } else if (profile.data.emailAddress !== currentUser.emailAddress) {
      logger.log("offlineAccessCode not equal to currentUser's email address");

      throw new functions.https.HttpsError(
        'invalid-argument',
        `offlineAccessCode not equal to currentUser's email address`,
      );
    }

    const accessCodeDoc: IOrganizationGoogleAccessToken = {
      emailAddress: profile.data.emailAddress,
      refreshToken: refresh_token,
      refreshTokenFetchedAt: new Date() as any,
      error: null,
      errorTime: null,
    };

    const orgUpdateDoc: Partial<IOrganization> = {
      accessToken: true,
      accessTokenError: null,
      accessTokenErrorTime: null,
    };

    await db.runTransaction(async (transaction) => {
      transaction.set(
        db.doc(`organizationGoogleAccessTokens/${org.uid}`),
        accessCodeDoc,
      );

      transaction.update(db.doc(`organizations/${org.uid}`), orgUpdateDoc);
    });

    return { success: true };
  },
);
