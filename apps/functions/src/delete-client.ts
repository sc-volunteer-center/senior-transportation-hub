import * as functions from 'firebase-functions';

import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
} from '../etc/shared';

import { objectD, stringD } from 'ts-decoders/decoders';
import admin from 'firebase-admin';

const paramsD = objectD({ clientId: stringD() });

/**
 * Delete the client document and associated rideRequests,
 * subcollections of the rideRequests, and remove the client's ID from
 * and driver documents who have the clientId blacklisted.
 */

export const deleteClient = functions.https.onCall(
  async (data: { clientId: string }, context) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    await db.runTransaction(async transaction => {
      const user = await getUser(context.auth.token.email, transaction);

      if (!user) {
        throw new Error(
          'Could not find user document for given auth.token.email',
        );
      }

      const org = await getOrganization(user.organizationId, transaction);

      if (!org) {
        throw new functions.https.HttpsError(
          'permission-denied',
          'User is not associated with an organization',
        );
      }

      // get client
      const clientSnap = await transaction.get(
        db.doc(`organizations/${org.uid}/clients/${params.clientId}`),
      );

      if (!clientSnap.exists) {
        throw new functions.https.HttpsError(
          `invalid-argument`,
          `Invalid "clientId".`,
        );
      }

      // get drivers who blacklisted client
      const driverSnaps = await transaction.get(
        db
          .collection(`organizations/${org.uid}/drivers`)
          .where('blacklistedClientIds', 'array-contains', params.clientId),
      );

      // get ride requests associated with client
      const rideRequestSnaps = await transaction.get(
        db
          .collection(`organizations/${org.uid}/rideRequests`)
          .where('clientId', '==', params.clientId),
      );

      const noteThreadSnaps = await transaction.get(
        db
          .collection(`organizations/${org.uid}/noteThreads`)
          .where('clientId', '==', params.clientId),
      );

      driverSnaps.docs.forEach(r => {
        transaction.update(r.ref, {
          blacklistedClientIds: admin.firestore.FieldValue.arrayRemove(
            params.clientId,
          ),
        });
      });

      transaction.delete(clientSnap.ref);

      rideRequestSnaps.forEach(s => {
        transaction.delete(s.ref);
      });

      noteThreadSnaps.forEach(snap => {
        transaction.delete(db.doc(snap.ref.path));
      });
    });
  },
);
