import * as functions from 'firebase-functions';
import { db, createId } from '../etc/shared';
import {
  IClient,
  IRideRequest,
  IDriver,
  Address,
  Driver,
  Client,
  IEmail,
  RideRequest,
} from '@local/models';
import { config } from '../etc/config';
import {
  indexToString,
  directionsMapLink,
  wait,
  generateRequestDetailText,
} from '@local/utilities';
import { differenceBy } from 'lodash';
import * as moment from 'moment-timezone';
import isEmail from 'validator/lib/isEmail';

moment.updateLocale('en', {
  week: {
    dow: 1, // configure monday to be the first day of the week
  },
});

/**
 * Generate an email to drivers that are assigned to a ride request
 * in the following week.
 *
 * Additionally, Generate an email summarizing all of next week's ride
 * requests to the dispatcher's email address.
 *
 * These emails are persisted to the relevant organizations `/emails` collection
 * where they are picked up by a firebase extension and sent.
 */

// for testing
// export const generateWeeklyEmails = functions.https.onCall(async () => {
export const generateWeeklyEmails = functions.pubsub
  .schedule('every friday 04:00')
  .timeZone('America/Los_Angeles')
  .onRun(async () => {
    const orgSnaps = await db.collection(`organizations`).get();
    const emailStore = new Map<string, IEmail[]>();

    await Promise.all(
      orgSnaps.docs.map(async (orgSnap) => {
        const orgId = orgSnap.id;
        const emails: IEmail[] = [];
        emailStore.set(orgId, emails);

        // get all ride requests from now until the following week's Sunday
        // at midnight. If someone is driving on a Friday or Saturday,
        // I figure it's better to give them two reminders,
        // the first a week+ before.
        const rideRequestsSnap = await db
          .collection(`organizations/${orgId}/rideRequests`)
          .where('datetime', '>', getMoment().toDate())
          .where('datetime', '<', getMoment().add(9, 'days').toDate())
          .get();

        // filter out ride requests which were cancelled
        const allRideRequests = rideRequestsSnap.docs
          .map((rideRequestSnap) => rideRequestSnap.data() as IRideRequest)
          .filter((rideRequest) => rideRequest.requestCancelledAt === null);

        // filter out ride requests without a driver
        const rideRequests = allRideRequests.filter(
          (rideRequest) => rideRequest.driverId !== null,
        );

        let clients = new Map<string, IClient>();

        if (allRideRequests.length > 0) {
          // get unique clients
          const clientSnaps = await db.getAll(
            ...Array.from(
              new Set(allRideRequests.map((rr) => rr.clientId)),
            ).map((id) => db.doc(`organizations/${orgId}/clients/${id}`)),
          );

          clients = new Map<string, IClient>(
            clientSnaps.map(
              (snap) => [snap.id, snap.data() as IClient] as [string, IClient],
            ),
          );

          if (Array.from(clients.values()).some((client) => client === null)) {
            const message =
              "An unexpected error occurred where the system couldn't find " +
              'the client information associated with a ride request. This ' +
              'error prevented any of the weekly emails from being sent. ' +
              'You should contact the Transportation Hub App developer ' +
              '(John Carroll as of this writing) to look into this. ';

            throw new Error(message);
          }
        }

        const groupedRideRequests = new Map<string, IRideRequest[]>();

        // group ride requests by driver
        rideRequests.forEach((rideRequest) => {
          if (!groupedRideRequests.has(rideRequest.driverId!)) {
            groupedRideRequests.set(rideRequest.driverId!, []);
          }

          groupedRideRequests.get(rideRequest.driverId!)!.push(rideRequest);
        });

        let drivers = new Map<string, IDriver>();

        if (rideRequests.length > 0) {
          // get unique drivers
          const driverSnaps = await db.getAll(
            ...Array.from(groupedRideRequests.keys()).map((id) =>
              db.doc(`organizations/${orgId}/drivers/${id}`),
            ),
          );

          drivers = new Map<string, IDriver>(
            driverSnaps.map(
              (snap) => [snap.id, snap.data() as IDriver] as [string, IDriver],
            ),
          );

          if (Array.from(drivers.values()).some((driver) => driver === null)) {
            const message =
              "An unexpected error occurred where the system couldn't find " +
              'the driver information associated with a ride request. This ' +
              'error prevented any of the weekly emails from being sent. ' +
              'You should contact the Transportation Hub App developer ' +
              '(John Carroll as of this writing) to look into this. ';

            throw new Error(message);
          }
        }

        for (const [driverId, rideRequests] of groupedRideRequests) {
          const driver = new Driver(drivers.get(driverId)!);

          if (!(driver.emailAddress && isEmail(driver.emailAddress))) {
            return;
          }

          emails.push(
            await generateDriverEmail({ driver, clients, rideRequests }),
          );
        }

        emails.push(
          generateSummaryEmail({
            drivers,
            clients,
            allRideRequests,
            rideRequestsGroupedByDriver: groupedRideRequests,
          }),
        );

        return emails;
      }),
    );

    for (const [orgId, emails] of emailStore) {
      for (const email of emails) {
        try {
          await db.collection(`organizations/${orgId}/emails`).add(email);
        } finally {
          await wait(200);
        }
      }
    }
  });

/**
 * # Example email
 *
 * ## Subject
 * Your upcoming volunteer rides - 7/1/2019
 *
 * ## Body
 * Hi John,
 *
 * This is just a friendly reminder that you've agreed to be the
 * driver for 4 senior ride requests next week (thanks!!!). You can
 * see the details for each ride below. Please call the
 * client the day before the ride to confirm the details
 * with them.
 *
 *
 * If something comes up and you can no longer be the driver for a ride request,
 * please let us know ASAP by replying to this email.
 *
 * Thanks! - The Transportation Team
 *
 *
 * ⬇️ Ride Requests for Next Week ⬇️
 *
 *
 * - - - - - -
 * 1.  Gerry Cutshaw R** on Thursday, June 27th, 2019 @ 11:00AM
 * - - -
 *
 */

// - - - - - -
// 1.  Gerry Cutshaw R** on Thursday, June 27th, 2019 @ 11:00AM
// - - -

// Trip purpose: Shopping

// Client:
// - name: Gerry Cutshaw R**.
// - phone number: 831-475-7689.
// - notes: she uses a walker, needs help with groceries, has driver bring her a grocery cart to the car for her, also needs assistance in store.

// Pickup:
// - Client's standard address, 4800 Opal Cliff Drive , 302, Capitola 95010 CA.
// - appointment time (please confirm pick up time with client): 11:00AM
// - notes: Corner of Portola and Opal Cliff Dr, 3 story condo, Gate Code: 1071 .

// Destination:
// - Lucky's - Capitola, 1475 41st ave, Capitoal 95010 CA.

// Google maps link:
// https://www.google.com/maps/dir/4800+Opal+Cliff+Drive+,+Capitola+95010+CA/1475+41st+ave,+Capitoal+95010+CA

// - - -

async function generateDriverEmail(args: {
  driver: Driver;
  clients: Map<string, IClient>;
  rideRequests: IRideRequest[];
}): Promise<IEmail> {
  const { driver } = args;

  const rideRequests = args.rideRequests
    .map((r) => new RideRequest(r, new Client(args.clients.get(r.clientId)!)))
    .sort((a, b) => {
      const aa = a.datetime.valueOf();
      const bb = b.datetime.valueOf();

      if (aa > bb) {
        return 1;
      } else if (aa < bb) {
        return -1;
      } else {
        return 0;
      }
    });

  let email =
    `Hi ${driver.nicknameOrFirstName},<br/><br/>` +
    `This is just a friendly reminder that you've agreed to ` +
    `volunteer for ${rideRequests.length} senior ` +
    `request${rideRequests.length === 1 ? '' : 's'} next ` +
    `week. You can see the details for ` +
    `${rideRequests.length === 1 ? 'this' : 'each'} request below. ` +
    `Please call the client the day before to ` +
    `confirm the details with them. And as a reminder, ` +
    `you can find a lot of helpful information on our ` +
    `<a href='https://sites.google.com/scvolunteercenter.org/stp-volunteer-info/home'>volunteer information webpage</a>, ` +
    `including <a href='https://sites.google.com/scvolunteercenter.org/stp-volunteer-info/home#h.rwfjd784l1e3'>frequently asked questions</a>. ` +
    `There is also information on that page for how to request ` +
    `a gas reimbursement from the Volunteer Center.<br/><br/>` +
    `If something comes up and you can no longer volunteer ` +
    `for a particular request, please let us know ASAP by replying to ` +
    `this email.<br/><br/>` +
    `Thanks!!! ❤️ - The Transportation Team<br/><br/><br/>` +
    `⬇️ Senior Requests for Next Week ⬇️<br/><br/><br/>`;

  async function buildTitle(r: RideRequest, order: number) {
    const momentDatetime = moment.tz(r.datetime, 'America/Los_Angeles');

    let datetimeString: string;

    if (r.flexible) {
      datetimeString = `the week of ${momentDatetime
        .startOf('week')
        .format('M/D')} - ${momentDatetime.endOf('week').format('M/D/YY')}`;
    } else {
      datetimeString =
        'on ' + momentDatetime.format('dddd, MMMM Do, YYYY @ h:mmA');
    }

    datetimeString += `\n- - -\n\n`;

    return `- - - - - -\n${order}.  ${r.client.name} ${datetimeString}`;
  }

  email += await generateRequestDetailText(rideRequests, buildTitle);

  email = email.replace(/\n/g, '<br />');

  return {
    to: [`${driver.name} <${driver.emailAddress}>`],
    message: {
      subject:
        `Your upcoming senior volunteer requests 🚗 - ` +
        getMoment().format('M/D/YYYY'),
      html: email,
    },
  };
}

/**
 * # Example email
 *
 * ## Subject line
 * Summary of next week's ride requests - 7/1/2019
 *
 * ## Body
 * Hi dispatchers,
 *
 * Here's a summary of all the ride request assignments for
 * next week (as of 7/1/2019 @ 12am). Unless otherwise specified,
 * each of the driver's listed below has received a reminder
 * email.
 *
 * Summary:
 *
 * - 2 ride requests for the week of 7/1/2019 - 7/10/2019.
 * - 1 ride request doesn't have a driver.
 * - 1 volunteer has agreed to be a driver.
 *
 * Ride requests without drivers:
 *
 *   1. 6/1/2019 @ 3:00pm for Alison Paul
 *
 * Drivers that have not received a reminder email because they
 * don't have an email address:
 *
 *   1. John Carroll
 *
 * Drivers:
 *
 *   1. John Carroll (3 rides)
 *      - 6/1/2019 @ 3:00pm for Alison Paul
 *      - 6/1/2019 @ 3:00pm for Alison Paul
 *      - 6/1/2019 @ 3:00pm for Alison Paul
 *   2. Bob Carroll (2 rides)
 *
 *
 * Thanks for volunteering your time as a dispatcher!!!
 * - John
 *
 */

function generateSummaryEmail(args: {
  drivers: Map<string, IDriver>;
  clients: Map<string, IClient>;
  allRideRequests: IRideRequest[];
  rideRequestsGroupedByDriver: Map<string, IRideRequest[]>;
}): IEmail {
  const {
    drivers,
    clients,
    allRideRequests,
    rideRequestsGroupedByDriver,
  } = args;

  const rideRequestsWithDriver: IRideRequest[] = [];

  for (const rideRequests of rideRequestsGroupedByDriver.values()) {
    rideRequestsWithDriver.push(...rideRequests);
  }

  const rideRequestsWODriver = differenceBy(
    allRideRequests,
    rideRequestsWithDriver,
    'uid',
  );

  let email =
    `Hi dispatchers,<br/>` +
    '<br/>' +
    `Here's a summary of all the request assignments for next ` +
    `week (as of ${getMoment().format('M/D/YYYY')} @ 4am). Unless ` +
    `otherwise specified, all of the drivers listed below ` +
    `have been sent a reminder email for their requests.<br/>` +
    '<br/>' +
    `<b>Summary:</b><br/>` +
    '<ul>' +
    `<li>${allRideRequests.length} trip request` +
    `${allRideRequests.length === 1 ? '' : 's'} for the week of ` +
    `${getMoment().format('M/D/YYYY')} - ` +
    `${getMoment().add(9, 'days').format('M/D/YYYY')}.</li>`;

  if (allRideRequests.length > 0) {
    email +=
      `<li>${rideRequestsWODriver.length} trip request` +
      `${rideRequestsWODriver.length === 1 ? '' : 's'} ` +
      `${rideRequestsWODriver.length === 1 ? "doesn't" : "don't"} ` +
      `have a driver.</li>`;

    if (drivers.size === 0) {
      email += `<li>No volunteers have agreed to drive 😟.</li>`;
    } else {
      email +=
        `<li>${drivers.size} volunteer${drivers.size === 1 ? '' : 's'} ` +
        `${drivers.size === 1 ? 'has' : 'have'} agreed to be ` +
        `${drivers.size === 1 ? 'a driver' : 'drivers'}.</li>`;
    }

    email += '</ul>';

    if (rideRequestsWODriver.length !== 0) {
      email += '<br/><b>Trip requests without drivers:</b><br/>';

      email += '<ol>';

      // Add list of ride requests without a driver (if any)
      rideRequestsWODriver.forEach((rideRequest) => {
        email +=
          `<li>` +
          `<a href="${config.uri.root}/dispatcher/ride-request/${rideRequest.uid}/overview">` +
          moment(rideRequest.datetime.toDate())
            .tz('America/Los_Angeles')
            .format('M/D/YYYY @ h:mmA') +
          ` for ${
            new Client(clients.get(rideRequest.clientId)!).name
          }</a></li>`;
      });

      email += '</ol>';
    }

    const driverWOEmail = Array.from(drivers.values())
      .filter(
        (driver) => !(driver.emailAddress && isEmail(driver.emailAddress)),
      )
      .map((driver) => new Driver(driver));

    if (driverWOEmail.length !== 0) {
      email +=
        '<br/><b>Drivers that have not received a reminder email ' +
        "because they don't have an email address:</b><br/>";

      email += '<ol>';

      // Add list of ride requests without a driver (if any)
      driverWOEmail.forEach((driver, index) => {
        email +=
          `<li>` +
          `<a href="${config.uri.root}/dispatcher/driver/${driver.uid}/profile">` +
          `${driver.name}</a></li>`;
      });

      email += '</ol>';
    }

    if (drivers.size !== 0) {
      email += '<br/><b>Drivers:</b><br/>';
      email += '<ol>';

      Array.from(rideRequestsGroupedByDriver)
        .map(([driverId, rideRequests]) => {
          return [new Driver(drivers.get(driverId)!), rideRequests] as [
            Driver,
            IRideRequest[],
          ];
        })
        .sort((a, b) => {
          if (a[0].name > b[0].name) return 1;
          if (a[0].name < b[0].name) return -1;
          return 0;
        })
        .forEach(([driver, rideRequests], index) => {
          email +=
            `<li>${driver.name} ` +
            `(${rideRequests.length} ride` +
            `${rideRequests.length === 1 ? '' : 's'})</li>`;

          email += '<ul>';

          rideRequests
            .sort((a, b) => {
              const aa = a.datetime.toDate().valueOf();
              const bb = b.datetime.toDate().valueOf();

              if (aa > bb) return 1;
              if (aa < bb) return -1;
              return 0;
            })
            .forEach((rideRequest) => {
              email +=
                `<li>` +
                `<a href="${config.uri.root}/dispatcher/ride-request/${rideRequest.uid}/overview">` +
                moment(rideRequest.datetime.toDate())
                  .tz('America/Los_Angeles')
                  .format('M/D/YYYY @ h:mmA') +
                ` for ${
                  new Client(clients.get(rideRequest.clientId)!).name
                }</a></li>`;
            });

          email += '</ul>';
        });

      email += '</ol>';
    }
  } else {
    email += '</ul>';
  }

  email += '<br/><br/>';
  email +=
    `Thanks for volunteering your time as a dispatcher!!! ❤️<br/>` +
    `- John<br/><br/><br/>`;

  return {
    to: ['transportation@scvolunteercenter.org'],
    message: {
      subject:
        `Summary of next week's ride requests 🚕 - ` +
        getMoment().format('M/D/YYYY'),
      html: email,
    },
  };
}

function getMoment() {
  return moment.tz('America/Los_Angeles');
}
