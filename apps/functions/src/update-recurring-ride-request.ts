import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
} from '../etc/shared';
import { objectD, stringD, predicateD } from 'ts-decoders/decoders';
import {
  FirestoreInput,
  IRecurringRideRequest,
  IRideRequest,
  SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL,
  SERIALIZABLE_SERVER_DATE_SENTINAL,
} from '@local/models';
import * as moment from 'moment-timezone';

moment.updateLocale('en', {
  week: {
    dow: 1, // configure monday to be the first day of the week
  },
});

type DataParam = {
  rideRequestId?: string;
  update?: Partial<FirestoreInput<IRecurringRideRequest>> &
    Pick<FirestoreInput<IRecurringRideRequest>, 'updatedAt'>;
};

const paramsD = objectD({
  rideRequestId: stringD(),
  update: predicateD<Exclude<DataParam['update'], undefined>>(
    (input) => typeof input === 'object' && input !== null,
  ),
});

export const updateRecurringRideRequest = functions.https.onCall(
  async (data: DataParam, context) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    // Need to convert any server timestamp placeholders into
    // proper server timestamps
    for (const [prop, value] of Object.entries(params.update)) {
      if (value !== SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL) {
        continue;
      }

      (params.update as any)[
        prop
      ] = admin.firestore.FieldValue.serverTimestamp();
    }

    // Need to convert any server placeholders into
    // proper values
    for (const [prop, value] of Object.entries(params.update)) {
      if (value === SERIALIZABLE_SERVER_TIMESTAMP_SENTINAL) {
        (params.update as any)[
          prop
        ] = admin.firestore.FieldValue.serverTimestamp();
      } else if (
        typeof value === 'string' &&
        value.startsWith(SERIALIZABLE_SERVER_DATE_SENTINAL)
      ) {
        (params.update as any)[prop] = new Date(
          parseInt(value.split(':')[1], 10),
        );
      }
    }

    // remove recurring props from the update params
    const futureRideRequestUpdate = { ...params.update };
    delete futureRideRequestUpdate.startDate;
    delete futureRideRequestUpdate.time;
    delete futureRideRequestUpdate.scheduleName;
    delete futureRideRequestUpdate.endDate;

    await db.runTransaction(async (transaction) => {
      const user = await getUser(context.auth.token.email, transaction);

      if (!user) {
        throw new Error(
          'Could not find user document for given auth.token.email',
        );
      }

      const org = await getOrganization(user.organizationId, transaction);

      if (!org) {
        throw new functions.https.HttpsError(
          'permission-denied',
          'User is not associated with an organization',
        );
      }

      const rideRequestSnap = await transaction.get(
        db.doc(`organizations/${org.uid}/rideRequests/${params.rideRequestId}`),
      );

      const rideRequestData = rideRequestSnap.data() as
        | IRideRequest
        | undefined;

      if (!rideRequestData || !rideRequestData.recurringRideRequestId) return;

      let futureRideRequestSnaps = (
        await transaction.get(
          db
            .collection(`organizations/${org.uid}/rideRequests`)
            .where(
              'recurringRideRequestId',
              '==',
              rideRequestData.recurringRideRequestId,
            )
            .where('datetime', '>=', rideRequestData.datetime)
            .orderBy('datetime', 'asc'),
        )
      ).docs;

      const recurringRideRequestSnap = await transaction.get(
        db.doc(
          `organizations/${org.uid}/recurringRideRequests/${rideRequestData.recurringRideRequestId}`,
        ),
      );

      const recurringRideRequestData = recurringRideRequestSnap.data() as
        | IRecurringRideRequest
        | undefined;

      if (!recurringRideRequestData) return;

      if (params.update.requestCancelledAt) {
        // delete any future requests after the first
        const deletedRideRequestSnaps = futureRideRequestSnaps.slice(1);

        // if we have further updates, we only want to make them to the non-deleted
        // ride request
        futureRideRequestSnaps = [futureRideRequestSnaps[0]];

        const noteThreadSnaps = await Promise.all(
          deletedRideRequestSnaps.map((s) =>
            transaction.get(
              db
                .collection(`organizations/${org.uid}/noteThreads`)
                .where('rideRequestId', '==', s.id),
            ),
          ),
        ).then((r) => r.flatMap((q) => q.docs));

        noteThreadSnaps.forEach((snap) => {
          transaction.delete(snap.ref);
        });

        deletedRideRequestSnaps.forEach((snap) => {
          transaction.delete(snap.ref);
        });

        transaction.update(recurringRideRequestSnap.ref, {
          endDate: rideRequestData.flexible
            ? moment
                .tz(rideRequestData.datetime.toDate(), 'America/Los_Angeles')
                .endOf('week')
                .toDate()
            : rideRequestData.datetime,
        });
      }

      if (params.update.time) {
        const [h, m] = params.update.time
          .split(':')
          .map((i) => parseInt(i, 10));

        // update the time associated with all the future requests
        futureRideRequestSnaps.forEach((snap) => {
          const rideRequestDoc = snap.data() as IRideRequest;

          const datetime = moment
            .tz(rideRequestDoc.datetime.toDate(), 'America/Los_Angeles')
            .set('hour', h)
            .set('minute', m)
            .toDate();

          transaction.update(snap.ref, {
            ...futureRideRequestUpdate,
            datetime,
          });
        });
      } else {
        futureRideRequestSnaps.forEach((snap) => {
          transaction.update(snap.ref, futureRideRequestUpdate);
        });
      }

      if (
        recurringRideRequestData.requestCancelledAt &&
        params.update.requestCancelledAt === null
      ) {
        transaction.update(recurringRideRequestSnap.ref, {
          ...params.update,
          endDate: null,
          lastGeneratedEndDate: recurringRideRequestData.endDate!,
        });
      } else {
        transaction.update(recurringRideRequestSnap.ref, params.update);
      }
    });
  },
);
