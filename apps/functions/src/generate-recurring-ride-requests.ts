import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  createId,
  logger,
  validateParams,
} from '../etc/shared';
import {
  IRecurringRideRequest,
  RecurringRideRequest,
  rideRequestCreateD,
  // tslint:disable-next-line: no-implicit-dependencies
} from '@local/models';
// tslint:disable-next-line: no-implicit-dependencies
import type { Timestamp } from '@firebase/firestore-types';
import { isAfter, subDays, isBefore } from 'date-fns';
import { assert } from 'ts-decoders';
import * as moment from 'moment-timezone';
import { booleanD, objectD, undefinableD } from 'ts-decoders/decoders';

moment.updateLocale('en', {
  week: {
    dow: 1, // configure monday to be the first day of the week
  },
});

const paramsD = objectD(
  {
    force: undefinableD(booleanD()),
  },
  {
    removeUndefinedProperties: true,
  },
);

const validator = assert(
  rideRequestCreateD({
    recurring: false,
    trustAddresses: true,
    trustDate: true,
  }),
);

export const generateRecurringRideRequests = functions.https.onCall(
  async (data: { force?: boolean }, context) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    await db.runTransaction(async (transaction) => {
      const user = await getUser(context.auth.token.email!, transaction);

      if (!user) {
        throw new Error(
          'Could not find user document for given auth.token.email',
        );
      }

      const org = await getOrganization(user.organizationId, transaction);

      if (!org) {
        throw new functions.https.HttpsError(
          'permission-denied',
          'User is not associated with an organization',
        );
      }

      const config = await transaction
        .get(
          db.doc(
            `organizations/${org.uid}/singletons/recurrenceGenerationConfig`,
          ),
        )
        .then((s) => s.data() as { lastGeneratedAt: Timestamp } | undefined);

      if (
        !params.force &&
        config &&
        isAfter(config.lastGeneratedAt.toDate(), subDays(new Date(), 3))
      ) {
        // if someone has generated recurring ride requests within the last 3 days
        return;
      }

      const results = await Promise.all([
        // get non-cancelled recurring requests without an end date
        transaction.get(
          db
            .collection(`organizations/${org.uid}/recurringRideRequests`)
            .where('requestCancelledAt', '==', null)
            .where('endDate', '==', null),
        ),
        // get non-cancelled recurring requests with an end date in the future
        transaction.get(
          db
            .collection(`organizations/${org.uid}/recurringRideRequests`)
            .where('requestCancelledAt', '==', null)
            .where('endDate', '>=', new Date()),
        ),
      ]);

      const recurring = results
        .flatMap((r) => r.docs)
        .map(
          (r) =>
            [
              r.data(),
              new RecurringRideRequest(r.data() as IRecurringRideRequest),
            ] as const,
        );

      logger.log(`found ${recurring.length} recurring requests`);

      await Promise.all(
        recurring.map(async ([originalParams, r]) => {
          const [hours, minutes] = r.time
            .split(':')
            .map((t) => parseInt(t, 10));

          // If we've never generated requests before (which would only happen
          // because of a bug of some kind) then have no generatedStartDate.
          // Otherwise, start the day after the last generation ended
          const generatedStartDate =
            (r.lastGeneratedEndDate &&
              moment
                .tz(r.lastGeneratedEndDate, 'America/Los_Angeles')
                .add(1, 'day')
                // `lastGeneratedEndDate` might not have the correct hour/minute
                .set('hours', hours)
                .set('minutes', minutes)) ??
            undefined;

          // If we've generated requests before, end two months from today.
          // Else, generate the first two months of requests.
          const generatedEndDate = r.lastGeneratedEndDate
            ? moment.tz('America/Los_Angeles').add(2, 'months')
            : moment.tz(r.startDate, 'America/Los_Angeles').add(2, 'months');

          generatedEndDate.set('hours', hours).set('minutes', minutes);

          logger.log(
            `generation start date: ${generatedStartDate?.toISOString()}`,
          );

          logger.log(`generation end date: ${generatedEndDate?.toISOString()}`);

          const occurrences = r.schedule
            .occurrences({
              start: generatedStartDate,
              end: generatedEndDate,
            })
            .toArray();

          logger.log(`creating ${occurrences.length} requests`);

          if (occurrences.length === 0) return;
          else if (occurrences.length > 30) {
            logger.error(`too many requests`, {
              org: org.uid,
              recurringRequestId: r.uid,
              lastGeneratedEndDate: r.lastGeneratedEndDate,
            });

            throw new functions.https.HttpsError(
              'internal',
              'too many requests are being generated',
            );
          }

          transaction.update(
            db.doc(`organizations/${org.uid}/recurringRideRequests/${r.uid}`),
            {
              lastGeneratedEndDate: generatedEndDate.toDate(),
            },
          );

          return Promise.all(
            occurrences.map(async (o) => {
              const uid = createId();

              const date = o.date
                .clone()
                .set('hours', hours)
                .set('minutes', minutes);

              try {
                const result = await validator({
                  ...originalParams,
                  uid,
                  recurringRideRequestId: r.uid,
                  date: originalParams.flexible
                    ? date.startOf('week').toDate()
                    : date.toDate(),
                });

                return transaction.set(
                  db.doc(`organizations/${org.uid}/rideRequests/${uid}`),
                  result,
                );
              } catch (e) {
                logger.warn('error generating recurrence', e);

                throw new functions.https.HttpsError(
                  'internal',
                  'one of the generated requests failed validation',
                  e,
                );
              }
            }),
          );
        }),
      );

      transaction.set(
        db.doc(
          `organizations/${org.uid}/singletons/recurrenceGenerationConfig`,
        ),
        {
          lastGeneratedAt: admin.firestore.FieldValue.serverTimestamp(),
        },
      );
    });
  },
);
