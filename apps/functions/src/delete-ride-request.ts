import * as functions from 'firebase-functions';
import {
  getUser,
  getOrganization,
  db,
  assertAuthenticated,
  validateParams,
} from '../etc/shared';
import { objectD, stringD } from 'ts-decoders/decoders';
import { IRideRequest } from '@local/models';

const paramsD = objectD({ rideRequestId: stringD() });

export const deleteRideRequest = functions.https.onCall(
  async (
    data: { rideRequestId?: string },
    context: functions.https.CallableContext,
  ) => {
    assertAuthenticated(context);

    const params = validateParams(paramsD, data);

    return db.runTransaction(
      async (transaction: FirebaseFirestore.Transaction) => {
        const user = await getUser(context.auth.token.email, transaction);

        if (!user) {
          throw new Error(
            'Could not find user document for given auth.token.email',
          );
        }

        const org = await getOrganization(user.organizationId, transaction);

        if (!org) {
          throw new functions.https.HttpsError(
            'permission-denied',
            'User is not associated with an organization',
          );
        }

        const rideRequestSnap = await transaction.get(
          db.doc(
            `organizations/${org.uid}/rideRequests/${params.rideRequestId}`,
          ),
        );

        const rideRequestData = rideRequestSnap.data() as
          | IRideRequest
          | undefined;

        if (!rideRequestData) return;

        const noteThreadSnaps = await transaction.get(
          db
            .collection(`organizations/${org.uid}/noteThreads`)
            .where('rideRequestId', '==', params.rideRequestId),
        );

        if (rideRequestData.recurringRideRequestId) {
          const recurringRideRequestSnaps = await transaction.get(
            db
              .collection(`organizations/${org.uid}/rideRequests`)
              .where(
                'recurringRideRequestId',
                '==',
                rideRequestData.recurringRideRequestId,
              )
              .limit(2),
          );

          if (recurringRideRequestSnaps.docs.length === 1) {
            transaction.delete(
              db.doc(
                `organizations/${org.uid}/recurringRideRequests/${rideRequestData.recurringRideRequestId}`,
              ),
            );
          }
        }

        transaction.delete(rideRequestSnap.ref);

        noteThreadSnaps.forEach((snap) => {
          transaction.delete(snap.ref);
        });
      },
    );
  },
);
