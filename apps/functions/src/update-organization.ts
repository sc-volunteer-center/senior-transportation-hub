import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import {
  getUser,
  mergeUser,
  getOrganization,
  db,
  assertAuthenticated,
} from '../etc/shared';

export const updateOrganization = functions.https.onCall(
  async (
    data: {
      name?: string;
      dispatcherEmailAddress?: string;
      rideRequestEmailPreface?: string;
    },
    context,
  ) => {
    assertAuthenticated(context);

    if (!(data.name && data.dispatcherEmailAddress)) return;

    const currentUser = await getUser(context.auth.token.email);

    if (!currentUser) return;

    const organization = await getOrganization(currentUser.organizationId);

    if (!organization) return;

    const orgDoc = db.doc(`organizations/${organization.uid}`);
    const oldUserDoc = db.doc(`users/${organization.dispatcherEmailAddress}`);
    const newUserDoc = db.doc(`users/${data.dispatcherEmailAddress}`);
    const timestamp = admin.firestore.FieldValue.serverTimestamp();

    if (data.dispatcherEmailAddress !== organization.dispatcherEmailAddress) {
      const newUser = await mergeUser(data.dispatcherEmailAddress);

      if (newUser && newUser.organizationId) return;
    }

    return db.runTransaction(async (transaction) => {
      const org = (await transaction.get(orgDoc)).data();

      if (!org) return;

      if (data.dispatcherEmailAddress !== org.dispatcherEmailAddress) {
        const [oldUser, newUser] = await Promise.all([
          transaction.get(oldUserDoc),
          transaction.get(newUserDoc),
        ]);

        if (newUser.exists && newUser.data()!.organizationId) {
          return;
        }

        transaction
          .update(oldUser.ref, {
            organizationId: null,
            updatedAt: timestamp,
          })
          .set(
            newUser.ref,
            {
              emailAddress: data.dispatcherEmailAddress,
              organizationId: org.uid,
              updatedAt: timestamp,
            },
            { merge: true },
          );
      }

      return transaction.update(orgDoc, {
        name: data.name,
        rideRequestEmailPreface: data.rideRequestEmailPreface || null,
        dispatcherEmailAddress: data.dispatcherEmailAddress,
        updatedAt: timestamp,
      });
    });
  },
);
