#!/usr/bin/env bash

yarn build && \
  GOOGLE_APPLICATION_CREDENTIALS="/Users/John/apps/senior-transportation-hub/apps/functions/tasks/trans-hub-dev-emulators-credentials.private.json" \
  firebase serve --only functions;
