/**
 * This script first transpiles the typescript to javascript,
 * and then goes through and replaces any `@local/*` imports
 * with relative path imports. The `@local/*` imports are
 * imports using the tsconfig `paths` option. Typescript
 * doesn't automatically update these paths for you when
 * transpiling, so we need to do it manually.
 */

import { exec as defaultExec } from 'child_process';
import { promisify } from 'util';
import * as fs from 'fs';
import * as defaultGlob from 'glob';

const exec = promisify(defaultExec);
const glob = promisify(defaultGlob);
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);
const pathRoot = __dirname + '/../';

async function main() {
  try {
    // it will error if there is no dist/functions folder
    await exec('rm -r dist/functions', { cwd: pathRoot });
  } catch (e) {}

  try {
    await exec('tsc', { cwd: pathRoot });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }

  const paths = await glob(pathRoot + 'dist/functions/**/*.js');

  await Promise.all(
    paths.map(async path => {
      const file = await readFile(path, 'utf8');

      let newPath = '';
      let index = path.split('dist/functions/')[1].split('/').length - 1;

      while (index > 0) {
        newPath += '../';
        index--;
      }

      newPath += 'libs/';

      const replace = file.replace(/\@local\//g, newPath);

      await writeFile(path, replace, { encoding: 'utf8' });
    }),
  );
}

main();
