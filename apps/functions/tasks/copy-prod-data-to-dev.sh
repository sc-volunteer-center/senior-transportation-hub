#!/bin/bash

# Taken from https://stackoverflow.com/a/246128/5490505
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Taken from https://stackoverflow.com/q/54057656/5490505
SRC_PROJECT="helping-hands-d70d6"
SRC_ACCOUNT="importexport@$SRC_PROJECT.iam.gserviceaccount.com"
SRC_CREDENTIALS="$CURRENT_DIR/$SRC_PROJECT-credentials.private.json"

DEST_PROJECT="trans-hub-dev"
DEST_ACCOUNT="importexport@$DEST_PROJECT.iam.gserviceaccount.com"
DEST_CREDENTIALS="$CURRENT_DIR/$DEST_PROJECT-credentials.private.json"

function timestamp {
  date '+%Y%m%d%H%M%S'
}

GCLOUD_STORAGE="gs://trans-hub-dev-admin/backup-20190219174819"

# echo "-------------------------------------------"
# echo " Activate Service Account $SRC_PROJECT"
# echo "-------------------------------------------"
# gcloud --quiet config set project ${SRC_PROJECT}
# gcloud beta auth activate-service-account ${SRC_ACCOUNT} --key-file=${SRC_CREDENTIALS} 

# echo "-------------------------------------------"
# echo " Exporting $SRC_PROJECT to $GCLOUD_STORAGE"
# echo "-------------------------------------------"
# gcloud beta firestore export $GCLOUD_STORAGE

echo "-------------------------------------------"
echo " Activate Service Account $DEST_PROJECT"
echo "-------------------------------------------"
gcloud --quiet config set project ${DEST_PROJECT}
gcloud beta auth activate-service-account ${DEST_ACCOUNT} --key-file=${DEST_CREDENTIALS} 

echo "-------------------------------------------"
echo " Importing $DEST_PROJECT"
echo "-------------------------------------------"
gcloud beta firestore import $GCLOUD_STORAGE
